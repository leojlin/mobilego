#include <PalmOS.h>
#include <StdIOPalm.h>
#include "CStorage.h"
#include "go.h"
#include "nngs.h"
#include "rsc.h"
#include "functions.h"
#define appFileCreator			'MCGS'	// register your own at http://www.palmos.com/dev/creatorid/
#define NDELIMITER				';'		//Delimiter for node
#define VBEGINDELIMITER			'('		//Delimiter for begining of variation
#define VENDDELIMITER			')'		//Delimiter for ending of variation
//static 	Char *valEnd;

CStorage::CStorage()
{
	storageNum = 0;
}
CStorage::~CStorage()
{}
void CStorage::Save(const TSgfInfo& sgfInfo, CMoveRecord& record)
{
	Err err;
	Char *headString = NULL;
	Char *moveString = NULL;
	Char *sgfString = NULL;
	void *stringPtrP;
	UInt32 headStringLen = 0;
	UInt32 moveStringLen = 0;
	UInt32 sgfStringLen = 0;

	GetHeadString(headString,sgfInfo);	
	GetMoveString(moveString,record);
	if (headString)
		headStringLen = StrLen(headString);
	if (moveString)
		moveStringLen = StrLen(moveString);
	sgfStringLen = headStringLen+moveStringLen+27;	//Need more space for Actual root info. Implement later
	sgfString = (Char*)MemPtrNew(sgfStringLen);	
	StrCopy(sgfString,"(;FF[4]GM[1]AP[MobileGo]");
	if (headString)
		StrCat(sgfString,headString);
	//StrCat(sgfString,";");	//End of root
	if (moveString)
		StrCat(sgfString,moveString);
	StrCat(sgfString,")");
	//Save sgf string to feature memory
	if (FtrGet(appFileCreator,storageNum,(UInt32 *)&stringPtrP) != 0)
	{
		err = FtrPtrNew(appFileCreator,storageNum,sgfStringLen,(void**)&stringPtrP);
	}
	else
	{
		err = FtrPtrFree(appFileCreator,storageNum);
		err = FtrPtrNew(appFileCreator,storageNum,sgfStringLen,(void**)&stringPtrP);
	}
	if (err==0)
	{
		DmStrCopy(stringPtrP,0,sgfString);
		MemPtrUnlock(stringPtrP);
	}
	//Release heap memory
	MemPtrFree(sgfString);
	if (headString)
		MemPtrFree(headString);
	if (moveString)
		MemPtrFree(moveString);
}
/*
Err CStorage::Load(TSgfInfo& sgfInfo, CMoveRecord& record)
{
	Char *sgfString;
	Char *nodeBegin,*nodeEnd;
	if (FtrGet(appFileCreator,storageNum,(UInt32 *)&sgfString) != 0)
	{
		return 1;
	}
	//Parse head node
	nodeBegin = StrChr(sgfString,NDELIMITER); //Point to begining of head node. Starting from first ';'
	if (!nodeBegin)
	{
		return 1;
	}
	nodeBegin++;
	if (GetNode(nodeBegin,nodeEnd)!=0)
	{
		return 1;
	}
	else
		GetHeadStruct(nodeBegin,nodeEnd,sgfInfo);
	//Parse move nodes
	while (*nodeEnd)
	{
		nodeBegin = nodeEnd;
		if (GetNode(nodeBegin,nodeEnd)!=0)
		{
			return 1;
		}
		else
			GetMoveStruct(nodeBegin,nodeEnd,record);
	}
	return 0;
}
*/
Err CStorage::Load(TSgfInfo& sgfInfo, CMoveRecord& record, Char* sgfFileName)
{
	Char *sgfString;
	Char *nodeBegin,*nodeEnd;
	Err     err;
	if (sgfFileName)	//Load from VFS
	{
		err = SgfReadFile(sgfFileName,&sgfString);
		if (err) 
			return 1;		
	}
	else 	//Load from Feature Memory
	if (FtrGet(appFileCreator,storageNum,(UInt32 *)&sgfString) != 0)
	{
		return 1;
	}
	//Parse head node
	err = 0;
	nodeBegin = StrChr(sgfString,NDELIMITER); //Point to begining of head node. Starting from first ';'
	if (!nodeBegin)
	{
		//return 1;
		err = 1;
		goto return_here;
	}
	nodeBegin++;
	if (GetNode(nodeBegin,nodeEnd)!=0)
	{
		//return 1;
		err = 1;
		goto return_here;
	}
	else
		GetHeadStruct(nodeBegin,nodeEnd,sgfInfo);
	//Parse move nodes
	while (*nodeEnd)
	{
		nodeBegin = nodeEnd;
		if (GetNode(nodeBegin,nodeEnd)!=0)
		{
			//Ignor illegal string
			break;
		}
		else
			GetMoveStruct(nodeBegin,nodeEnd,record);
	}
return_here:
	if (sgfFileName)
		MemPtrFree(sgfString);	
	return err;
}
void CStorage::AllowSave()
{
}
void CStorage::AllowLoad()
{
}
void CStorage::GetHeadString(Char* &headString, const TSgfInfo &sgfInfo)
{
	Char *newString = NULL;
	Char sizeString[10];
	Char komiString[10];
	Char handicapString[10];
	Char WEString[15];
	Char BEString[15];
	Int16 len = 0;
	Int16 dInteger,dFraction;
	if (headString)	//Initial headString should be always NULL
		return;
	//Calculate memory space that should be allocated
	if (sgfInfo.GN)
		len += StrLen(sgfInfo.GN)+4;
	if (sgfInfo.EV)
		len += StrLen(sgfInfo.EV)+4;
	if (sgfInfo.RO)
		len += StrLen(sgfInfo.RO)+4;
	if (sgfInfo.PB)
		len += StrLen(sgfInfo.PB)+4;
	if (sgfInfo.BR)
		len += StrLen(sgfInfo.BR)+4;
	if (sgfInfo.PW)
		len += StrLen(sgfInfo.PW)+4;
	if (sgfInfo.WR)
		len += StrLen(sgfInfo.WR)+4;
	if (sgfInfo.RE)
		len += StrLen(sgfInfo.RE)+4;
	if (sgfInfo.PC)
		len += StrLen(sgfInfo.PC)+4;
	if (sgfInfo.DT)
		len += StrLen(sgfInfo.DT)+4;
	if (sgfInfo.TM)
		len += StrLen(sgfInfo.TM)+4;
	if (sgfInfo.ID)
		len += StrLen(sgfInfo.ID)+4;
	//SZ
	MemSet(sizeString,10,NULL);
	sprintf(sizeString,"SZ[%d]",sgfInfo.SZ);
	len += StrLen(sizeString); 
	//KM
	MemSet(komiString,10,NULL);
	dInteger = sgfInfo.KM / 10;
	dFraction = sgfInfo.KM % 10;
	if (sgfInfo.KM < 0)
		dFraction = (-1*sgfInfo.KM) % 10;
	sprintf(komiString,"KM[%d.%d]",dInteger,dFraction);
	len += StrLen(komiString);
	//HA
	MemSet(handicapString,10,NULL);
	sprintf(handicapString,"HA[%d]",sgfInfo.HA);
	len += StrLen(handicapString);
	//WE
	MemSet(WEString,15,NULL);
	sprintf(WEString,"WE[%d]",sgfInfo.WE);
	len += StrLen(WEString);
	//BE
	MemSet(BEString,15,NULL);
	sprintf(BEString,"BE[%d]",sgfInfo.BE);
	len += StrLen(BEString);
	
	//Allocate memory for headstring
	newString = (Char*)MemPtrNew(len+1);
	if(!newString)
	{
		//Memory error
		return;
	}
	StrCopy(newString,sizeString);
	StrCat(newString,komiString);
	StrCat(newString,handicapString);
	StrCat(newString,WEString);
	StrCat(newString,BEString);
	if (sgfInfo.GN)
	{
		StrCat(newString,"GN[");
		StrCat(newString,sgfInfo.GN);
		StrCat(newString,"]");
	}	if (sgfInfo.EV)
	{
		StrCat(newString,"EV[");
		StrCat(newString,sgfInfo.EV);
		StrCat(newString,"]");
	}
	if (sgfInfo.RO)
	{
		StrCat(newString,"RO[");
		StrCat(newString,sgfInfo.RO);
		StrCat(newString,"]");
	}
	if (sgfInfo.PB)
	{
		StrCat(newString,"PB[");
		StrCat(newString,sgfInfo.PB);
		StrCat(newString,"]");
	}
	if (sgfInfo.BR)
	{
		StrCat(newString,"BR[");
		StrCat(newString,sgfInfo.BR);
		StrCat(newString,"]");
	}
	if (sgfInfo.PW)
	{
		StrCat(newString,"PW[");
		StrCat(newString,sgfInfo.PW);
		StrCat(newString,"]");
	}
	if (sgfInfo.WR)
	{
		StrCat(newString,"WR[");
		StrCat(newString,sgfInfo.WR);
		StrCat(newString,"]");
	}
	if (sgfInfo.RE)
	{
		StrCat(newString,"RE[");
		StrCat(newString,sgfInfo.RE);
		StrCat(newString,"]");
	}
	if (sgfInfo.PC)
	{
		StrCat(newString,"PC[");
		StrCat(newString,sgfInfo.PC);
		StrCat(newString,"]");
	}
	if (sgfInfo.DT)
	{
		StrCat(newString,"DT[");
		StrCat(newString,sgfInfo.DT);
		StrCat(newString,"]");
	}
	if (sgfInfo.TM)
	{
		StrCat(newString,"TM[");
		StrCat(newString,sgfInfo.TM);
		StrCat(newString,"]");
	}
	if (sgfInfo.ID)
	{
		StrCat(newString,"ID[");
		StrCat(newString,sgfInfo.ID);
		StrCat(newString,"]");
	}
	headString = newString;
}
void CStorage::GetHeadStruct(Char* headBegin, Char* headEnd, TSgfInfo &sgfInfo)
{
	UInt16 tagOffset = 0;
	Char tag[TAGLEN];
	strcpy(tag,"??");
	while (headBegin+tagOffset<headEnd)
	{
		Char *valBegin=NULL,*valEnd=NULL;
      	tagOffset = GetTag(headBegin,tagOffset,tag,valBegin,valEnd);
      	if (!tag[0])
      	{
         	break;
      	}
     	else if (!strcmp(tag,"GN"))
      	{
        	if(!(sgfInfo.GN = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.GN,valBegin,valEnd-valBegin);
      		sgfInfo.GN[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"DT"))
      	{
        	if(!(sgfInfo.DT = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.DT,valBegin,valEnd-valBegin);
      		sgfInfo.DT[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"EV"))
      	{
        	if(!(sgfInfo.EV = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.EV,valBegin,valEnd-valBegin);
      		sgfInfo.EV[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"RO"))
      	{
        	if(!(sgfInfo.RO = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.RO,valBegin,valEnd-valBegin);
      		sgfInfo.RO[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"PB"))
      	{
        	if(!(sgfInfo.PB = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.PB,valBegin,valEnd-valBegin);
      		sgfInfo.PB[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"PW"))
      	{
        	if(!(sgfInfo.PW = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.PW,valBegin,valEnd-valBegin);
      		sgfInfo.PW[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"BR"))
      	{
        	if(!(sgfInfo.BR = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.BR,valBegin,valEnd-valBegin);
      		sgfInfo.BR[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"WR"))
      	{
        	if(!(sgfInfo.WR = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.WR,valBegin,valEnd-valBegin);
      		sgfInfo.WR[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"RE"))
      	{
        	if(!(sgfInfo.RE = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.RE,valBegin,valEnd-valBegin);
      		sgfInfo.RE[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"PC"))
      	{
        	if(!(sgfInfo.PC = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.PC,valBegin,valEnd-valBegin);
      		sgfInfo.PC[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"TM"))
      	{
        	if(!(sgfInfo.TM = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.TM,valBegin,valEnd-valBegin);
      		sgfInfo.TM[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"ID"))	//Game ID, MobileGo Specific
      	{
        	if(!(sgfInfo.ID = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(sgfInfo.ID,valBegin,valEnd-valBegin);
      		sgfInfo.ID[valEnd-valBegin] = NULL;
      	}
     	else if (!strcmp(tag,"KM"))
      	{
      		Char *valueString = NULL;
        	if(!(valueString = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(valueString,valBegin,valEnd-valBegin);
      		valueString[valEnd-valBegin] = NULL;
      		sgfInfo.KM = FString2Integer(valueString);
      		MemPtrFree(valueString);
      	}
     	else if (!strcmp(tag,"HA"))
      	{
      		Char *valueString = NULL;
        	if(!(valueString = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(valueString,valBegin,valEnd-valBegin);
      		valueString[valEnd-valBegin] = NULL;
      		sgfInfo.HA = String2Integer(valueString);
      		MemPtrFree(valueString);
      	}
     	else if (!strcmp(tag,"SZ"))
      	{
      		Char *valueString = NULL;
        	if(!(valueString = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(valueString,valBegin,valEnd-valBegin);
      		valueString[valEnd-valBegin] = NULL;
      		sgfInfo.SZ = String2Integer(valueString);
      		MemPtrFree(valueString);
      	}
     	else if (!strcmp(tag,"WE"))	//White Elapsed, MobileGo Specific
      	{
      		Char *valueString = NULL;
        	if(!(valueString = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(valueString,valBegin,valEnd-valBegin);
      		valueString[valEnd-valBegin] = NULL;
      		sgfInfo.WE = String2Integer(valueString);
      		MemPtrFree(valueString);
      	}
     	else if (!strcmp(tag,"BE"))	//Black Elapsed, MobileGo Specific
      	{
      		Char *valueString = NULL;
        	if(!(valueString = (Char *)MemPtrNew(valEnd-valBegin+1)))
			{
        		FrmAlert(MemoryFailureAlert);
        		return;
			}
      		StrNCopy(valueString,valBegin,valEnd-valBegin);
      		valueString[valEnd-valBegin] = NULL;
      		sgfInfo.BE = String2Integer(valueString);
      		MemPtrFree(valueString);
      	}
   	}
   //tagOffset = oldOffset;
	return;
}
void CStorage::GetMoveString(Char* &moveString, CMoveRecord &record)
{
	Char *newString = NULL;
	Char singleMoveString[7];
	Char cx,cy,cc;
	TMove move;
	Int16 len = 0;
	Int16 count,index;
	count = record.GetMoveCount();
	if (count <=0)
		return;
	else
		len = 6 * count;
	newString = (Char*) MemPtrNew(len+2);
	if (!newString)
		return;
	StrCopy(newString,"\n");
	for (index = 0; index<count; index++)
	{
		record.GetMove(index,move);
		if (move.position.x == -1 || move.position.y == -1)	//handicap or pass
		{
			if (move.color == WHITE)
				cc = 'W';
			else
				cc = 'B';
			MemSet(singleMoveString,7,NULL);
			sprintf(singleMoveString,";%c[]",cc);	
		}
		else
		{
			cx = 'a' + move.position.x;
			if (cx >= 'i')
				cx++;
			cy = 'a' + move.position.y;
			if (cy >= 'i')
				cy++;
			if (move.color == WHITE)
				cc = 'W';
			else
				cc = 'B';
			MemSet(singleMoveString,7,NULL);
			sprintf(singleMoveString,";%c[%c%c]",cc,cx,cy);
		}
		StrCat(newString,singleMoveString);
	}	
	moveString = newString;
	return;
}
void CStorage::GetMoveStruct(Char* moveBegin, Char* moveEnd, CMoveRecord &record)
{
	Int16 x = -1,y = -1;
	Int16 moveId;
	TMove newMove;
	UInt16 tagOffset = 0;
	Char tag[TAGLEN];
	strcpy(tag,"??");
	while (moveBegin+tagOffset<moveEnd)
	{
		Char *valBegin=NULL,*valEnd=NULL;
      	tagOffset = GetTag(moveBegin,tagOffset,tag,valBegin,valEnd);
      	if (!tag[0])
      	{
         	break;
      	}
     	else if (!strcmp(tag,"B"))
      	{
			moveId = record.GetMoveCount();
         	if (valBegin[0]==']') // [Sylvain] pass SGF >= 4 style
            	x = y = -1;
         	else {
            	x = valBegin[0]-'a';
            	x = x>8 ? x-1 : x;
            	y = valBegin[1]-'a';
            	y = y>8 ? y-1 : y;
         	}
         	if (x<19 && y<19)
         	{
				newMove.id = moveId;
				newMove.color = BLACK;
				newMove.position.x = x;
				newMove.position.y = y; 
				record.AddMove(newMove);
         	}
     	}
     	else if (!strcmp(tag,"W"))
      	{
			moveId = record.GetMoveCount();
         	if (valBegin[0]==']') // [Sylvain] pass SGF >= 4 style
            	x = y = -1;
         	else {
            	x = valBegin[0]-'a';
            	x = x>8 ? x-1 : x;
            	y = valBegin[1]-'a';
            	y = y>8 ? y-1 : y;
         	}
         	if (x<19 && y<19)
         	{
				newMove.id = moveId;
				newMove.color = WHITE;
				newMove.position.x = x;
				newMove.position.y = y; 
				record.AddMove(newMove);
         	}
     	}
	}
	return;
}
//Parse a tag, return non-blank position after it
UInt16 CStorage::GetTag(Char *sgf, UInt16 pos, Char *tag, Char*& val, Char*& valend)
{
	UInt16 tagpos=0;
   	Char *x=sgf+pos;
   	val = 0;
   	while (TxtCharIsSpace(*x))
   		++x;
   	for (;*x;++x)
   	{
    	if ('A'<=*x && *x<='Z')
      	{ 
      		if (tagpos<TAGLEN-1)
         		tag[tagpos++]=*x;
      	}
      	else if ('a'<=*x && *x<='z')
      	{
        	// skipped tag character
      	}
      	else if (*x=='[')
      	{ 
      		val = x+1;
         	while (*x)
         	{
            	if (*x=='\\')
               		++x;
            	else if (*x==']')
            	{ 
            		valend = x;
               		++x;
               		break;
            	}
            	++x;
         	}
         	break;
      	}
      	else
        	break;
   	}
   	if (!tagpos && !val && (tag[tagpos++]=*x))
      	++x;  /* consume 1 unrecognised character, except 0 */
   	if (tagpos)   // preserve previous tag if none found
    	tag[tagpos]=0;
   	while (tag[0] && TxtCharIsSpace(*x))  // skip trailing white space
    	++x;
   	return x-sgf;
}
Int16 CStorage::GetNode(Char* begin, Char* &end)
{
	Char	*val;
	if (!*begin)
		return 1;
	val = StrChr(begin,NDELIMITER);	
	if (!val)
	{
		val = StrChr(begin,VENDDELIMITER);
		if (!val) 
			return 1;	//Can't find neigther node delimiter nor variation delimiter. It's an illegal SGF string
	}
	end = val +1;
	return 0;
}
		
		
