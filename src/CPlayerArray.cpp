#include <PalmOS.h>
#include "CPlayerArray.h"

CPlayerArray::CPlayerArray()
{
	pArray = NULL;
	size = 0;
}

//template <class CPlayerItem>
Boolean CPlayerArray::Add(CPlayerItemPtr node)
{
	Int32	newsize;
	CPlayerItemPtr		*pTemp;
	
	//Allocate new array
	newsize = size+1;
	pTemp = new CPlayerItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	//Copy array bit by bit
	if (pArray)
	{
		MemMove(pTemp,pArray,size * sizeof(CPlayerItemPtr));
	}
	pTemp[newsize -1]=node;
	//delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}
/*
void CPlayerArray::Swap(Int32 index1, Int32 index2)
{
	CPlayerItemPtr		temp;
	if (index1==index2 || index1<0 || index1>=size || index2<0 || index2>=size)
		return;
	temp = pArray[index1];
	pArray[index1] = pArray[index2];
	pArray[index2] = temp;
}
*/
//template <class CPlayerItemPtr>
void CPlayerArray::Clear()
{
	CPlayerItem	temp;
	Int32 i, k;
	if (pArray)
	{
		k = size;
		for (i=0;i<k;i++)
		{
			delete pArray[i]; //we need to free the item as well
		}
		delete pArray;
		pArray = NULL;
		size = 0;
	}
}

//template <class CPlayerItemPtr>
CPlayerArray::~CPlayerArray()
{
	Clear();
}

//template <class CPlayerItemPtr>
/*
Int32 CPlayerArray::Find(CPlayerItemPtr node)
{
	for (Int32 i=0;i<size;i++)
	{
		if(pArray[i]==node)
		{
			return i;
		}
	}
	//Not found
	return -1;
}
*/
Int32 CPlayerArray::FindByName(const Char *name)
{
	for (Int32 i=0;i<size;i++)
	{
		if(!StrCompare(pArray[i]->playerName,name))
		{
			return i;
		}
	}
	//Not found
	return -1;
}

//template <class CPlayerItemPtr>
Boolean CPlayerArray::Insert(CPlayerItemPtr node,Int32 index)
{
	Int32	newsize;
	CPlayerItemPtr		*pTemp;
	
	if (index<0)
	{
		index = 0;;
	}
	//Make room for insertion
	newsize = size+1;
	pTemp = new CPlayerItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		//Move elements up to the insertion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CPlayerItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index+1,&pArray[index],(newsize-index-1)*sizeof(CPlayerItemPtr));
	}
	//Insert the new element
	pTemp[index] = node;
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

//template <class CPlayerItemPtr>
Boolean CPlayerArray::Remove(Int32 index)
{
	Int32	newsize;
	CPlayerItemPtr		*pTemp;

	if (index<0 ||index>=size)
	{
		return false;
	}
	//make the array smaller
	newsize = size-1;
	pTemp = new CPlayerItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		pArray[index]->Release();	//Release dynamic memory allocated for this node
		delete pArray[index];
		//Move elements up to the deletion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CPlayerItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index,&pArray[index+1],(newsize-index)*sizeof(CPlayerItemPtr));
	}
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

//template <class CPlayerItemPtr>
Int32 CPlayerArray::Size()
{
	return size;
}

//template <class CPlayerItemPtr>
CPlayerItemPtr CPlayerArray::operator[](Int32 index)
{
	return pArray[index];
}
Int16 CPlayerArray::Compare(const CPlayerItem *itemP1, const CPlayerItem *itemP2, PLAYERFIELD fld, Boolean ascending)
{
	//CPlayerItem *p1,*p2;
	//p1 = itemP1;
	//p2 = itemP2;
	Int16	ret = 0;
	switch (fld){
		case GM:
			if (itemP1->gameID == itemP2->gameID)
				ret = 0;
			else 
			{
				if (itemP1->gameID < itemP2->gameID)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case NM:
			if (StrCompare(itemP1->playerName,itemP2->playerName) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->playerName,itemP2->playerName) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case RK:
/*			if (RankCompare(itemP1->rank,itemP2->rank) == 0)
				ret = 0;
			else 
			{
				if (RankCompare(itemP1->rank,itemP2->rank) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
*/
			if (itemP1->numericalRank==itemP2->numericalRank)
				ret = 0;
			else 
			{
				if (itemP1->numericalRank<itemP2->numericalRank)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case CN:
			if (StrCompare(itemP1->country,itemP2->country) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->country,itemP2->country) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case WN:
			if (itemP1->won == itemP2->won)
				ret = 0;
			else 
			{
				if (itemP1->won < itemP2->won)
					ret = ascending ? 1 : -1 ;
				else
					ret = (ascending?-1:1);
			}
			break;
		case LS:
			if (itemP1->lost == itemP2->lost)
				ret = 0;
			else 
			{
				if (itemP1->lost < itemP2->lost)
					ret = ascending ? 1 : -1 ;
				else
					ret = (ascending?-1:1);
			}
			break;
		case ST:
			Char	s1[3],s2[3];
			s1[0] = itemP1->status[0];
			s1[1] = itemP1->status[1];
			s2[0] = itemP2->status[0];
			s2[1] = itemP2->status[1];
			s1[2] = s2[2] = NULL;	//Null terminal
			if (StrNCompare(s1,s2,2) == 0)
				ret = 0;
			else 
			{
				if (StrNCompare(s1,s2,2) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		default:
			break;
	}
	return ret;		
}
/*
void CPlayerArray::QuickSort(Int32 low, Int32 high,PLAYERFIELD fld, Boolean  ascending)
{
	//local variables holding the mid index of the range and
	//its values A[mid] and the scanning indices
	CPlayerItemPtr	pivot,temp,node1,node2;
	Int32 scanUp, scanDown, mid;
	
	//if the range is not at least two elements, return
	if (high - low <=0)
		return;
	else
		//if sublist has two elements, compare them and exchange their values if necessary
		if (high - low == 1)
		{
			if (Compare(pArray[high], pArray[low],fld,ascending)<0)
			{
				//swap(pArray[low],pArray[high]);
				node1 = pArray[high];
				node2 = pArray[low];
				temp = pArray[high];
				pArray[high] = pArray[low];
				pArray[low] = temp;
			}
			return;
		}
	//get the mid index and assign its value to pivot
	mid = (low + high) / 2;
	pivot = pArray[mid];
	//exchange the pivot and the low end of the range
	//and initialize the indices scanUp and scanDown.
	//swap(pArray[mid],pArray[low]);
	temp = pArray[mid];
	pArray[mid] = pArray[low];
	pArray[low] = temp;
	scanUp = low + 1;
	scanDown = high;
	
	//manage the indices to locate elements that are in 
	//the wrong sublist; stop when scanDown < scanUp
	do
	{
		//move up lower sublist; stop when scanUp enters 
		//upper sublist or indentifies an element > pivot
		while (scanUp <= scanDown)
		{
			node1 = pArray[scanUp];
			if  (Compare(pArray[scanUp],pivot,fld,ascending)<=0)
				break;
			scanUp++;
		}
		//scan down upper sublist; stop when scanDown locates//
		//an element <=pivot; we guarantee a stop at pArray[low]
		while (scanUp <= scanDown)
		{
			node2 = pArray[scanDown];
			if (Compare(pivot, pArray[scanDown], fld, ascending) <0)
				break;
			scanDown--;
		}
		//if indices are still in their sublists, the they
		//indetify two elements in wrong sublists.
		//exchange them
		if (scanUp < scanDown)
		{
			//swap(pArray[scanUp],pArray[scanDown]);
			temp = pArray[scanUp];
			pArray[scanUp] = pArray[scanDown];
			pArray[scanDown] = temp;
		}
	} while (scanUp<scanDown);
	//copy pivot to index (scanDown) that partitions sublists
	pArray[low] = pArray[scanDown];
	pArray[scanDown]=pivot;
	
	//if the lower sublist (low to scanDown-1) has 2 or more
	//elements, make the recursive call
	if (low < scanDown-1)
		QuickSort(low,scanDown-1,fld,ascending);
	//if the higher sublist (scanDown+1 to high) has 2 or more
	//elements, make the recursive call
	if (scanDown+1 < high)
		QuickSort(scanDown+1,high,fld,ascending);
}
*/
void CPlayerArray::HeapSort(Int32 count,PLAYERFIELD fld, Boolean  ascending)
{
	Int32	parent,child,n,i;
	CPlayerItemPtr	temp;
	n = count;
	i = n/2;
	for(;;)
	{
		if (i>0)
		{
			i--;
			temp = pArray[i];
		}
		else
		{
			n--;
			if (n<=0)
				return;
			temp = pArray[n];
			pArray[n] = pArray[0];
		}
		
		parent = i;
		child = i*2 + 1;
		
		while(child<n)
		{
			if (child+1<n && Compare(pArray[child+1],pArray[child],fld,ascending)<0)
			{
				child++;
			}
			if (Compare(pArray[child],temp,fld,ascending)<0)
			{
				pArray[parent] = pArray[child];
				parent = child;
				child = parent*2 + 1;
			}
			else
				break;
		}
		pArray[parent] = temp;
	}
}	