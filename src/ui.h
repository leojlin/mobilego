#ifndef _UI_H_
#define _UI_H_
#include "sections.h"

typedef struct  TKey
{
	UInt32	data;	//the time the application was first started
	UInt8	value;	//the number of days remaining in the trial
	Int32	code;	//the registration code
};
//#define colorOldAPI		1
//#define colorNewAPI		2
#define keyFileCreator			'TUNH'	// register your own at http://www.palmos.com/dev/creatorid/
#define keyPrefID				0x00
#define keyPrefMiniVersionNum	0x01	//Prior to Mobile Go V2.2
#define keyPrefVersionNum		0x02	//Since Mobile Go V2.2

#define ObjectHasFocus(frmP,objId) \
		(FrmGetFocus (frmP) == FrmGetObjectIndex (frmP, objId))
/*
extern UInt8			colorMode;
extern UInt32			colorDepth;
*/
//extern RGBColorType		blue;
extern RGBColorType		white;
extern RGBColorType		black;
extern RGBColorType		grey;
//extern RGBColorType		lightGrey;
//extern RGBColorType		lightBlue;
extern RGBColorType		red;
//extern RGBColorType		green;
//extern RGBColorType		purple;
//extern RGBColorType		olive;
//extern RGBColorType		maroon;
//extern RGBColorType		paleBlue;
//extern RGBColorType		bgColor;

//void DisableControl( FormPtr frm, UInt16 objectID ) SEC_CMN;
//void EnableControl( FormPtr frm, UInt16 objectID ) SEC_CMN;
//void DisableField( FormPtr frm, UInt16 objectID ) SEC_CMN;
//void EnableField( FormPtr frm, UInt16 objectID ) SEC_CMN;
void ShowObject( FormPtr frm, UInt16 objectID ) SEC_CMN;
void HideObject( FormPtr frm, UInt16 objectID ) SEC_CMN;
//void SetFocus( FormPtr frm, UInt16 objectID ) SEC_CMN;
//void DrawBitmapSimple( Int16 resID, Int16 x, Int16 y ) SEC_CMN;
//void DrawBitmap( Int16 resID, Int16 x, Int16 y ) SEC_CMN;
//void CtlFreeMemory( UInt16 id ) SEC_CMN;
//void CtlInit( UInt16 id ) SEC_CMN;
//void* GetFocusObjectPtr() SEC_CMN;
//ControlPtr SetControlLabel( UInt16 ctlID, Char* str, Int16 allowWidth ) SEC_CMN;
//FieldPtr SetFieldTextFromHandle( UInt16 fldID, MemHandle txtH, Boolean draw ) SEC_CMN;
void SetFieldText(FieldType	*pField, Char* s) SEC_CMN;
void DrawFieldText(FieldType	*pField, Char* s) SEC_CMN;
//UInt16 calcTableRows( TablePtr table, Boolean isChooser ) SEC_CMN;
//void SetupColorSupport() SEC_CMN;
//void ColorSet( RGBColorType *fore, RGBColorType *back, RGBColorType *text, RGBColorType *foreBW, RGBColorType *backBW ) SEC_CMN;
//void ColorUnset() SEC_CMN;
void* GetObjectPtr( UInt16 id ) SEC_CMN;
Int32 GenerateRCode() SEC_CMN;
Boolean CheckRegistered(UInt8& remainingDay) SEC_CMN;
void SaveRegisterCode(UInt32) SEC_CMN;
#endif
