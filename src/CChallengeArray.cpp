#include <PalmOS.h>
#include "CChallengeArray.h"
CChallengeArray::CChallengeArray()
{
	pArray = NULL;
	size = 0;
}

Boolean CChallengeArray::Add(CChallengeItemPtr node)
{
	Int32	newsize;
	CChallengeItemPtr		*pTemp;
	
	//Allocate new array
	newsize = size+1;
	pTemp = new CChallengeItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	//Copy array bit by bit
	if (pArray)
	{
		MemMove(pTemp,pArray,size * sizeof(CChallengeItemPtr));
	}
	pTemp[newsize -1]=node;
	//delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

void CChallengeArray::Clear()
{
	Int32 i, k;
	if (pArray)
	{
		k = size;
		for (i=0;i<k;i++)
		{
			//*(&temp) = *pArray[i];
			//pArray[i]->Release();	//Release dynamic memory allocated for each node
			delete pArray[i];
		}
		delete pArray;
		pArray = NULL;
		size = 0;
	}
}

CChallengeArray::~CChallengeArray()
{
	Clear();
}

Int32 CChallengeArray::FindByName(const Char *name)
{
	for (Int32 i=0;i<size;i++)
	{
		if(!StrCompare(pArray[i]->playerName,name))
		{
			return i;
		}
	}
	//Not found
	return -1;
}

Boolean CChallengeArray::Insert(CChallengeItemPtr node,Int32 index)
{
	Int32	newsize;
	CChallengeItemPtr		*pTemp;
	
	if (index<0)
	{
		index = 0;;
	}
	//Make room for insertion
	newsize = size+1;
	pTemp = new CChallengeItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		//Move elements up to the insertion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CChallengeItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index+1,&pArray[index],(newsize-index-1)*sizeof(CChallengeItemPtr));
	}
	//Insert the new element
	pTemp[index] = node;
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

Boolean CChallengeArray::Remove(Int32 index)
{
	Int32	newsize;
	CChallengeItemPtr		*pTemp;

	if (index<0 ||index>=size)
	{
		return false;
	}
	//make the array smaller
	newsize = size-1;
	pTemp = new CChallengeItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		pArray[index]->Release();	//Release dynamic memory allocated for this node
		delete pArray[index];
		//Move elements up to the deletion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CChallengeItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index,&pArray[index+1],(newsize-index)*sizeof(CChallengeItemPtr));
	}
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

Int32 CChallengeArray::Size()
{
	return size;
}

CChallengeItemPtr CChallengeArray::operator[](Int32 index)
{
	return pArray[index];
}
