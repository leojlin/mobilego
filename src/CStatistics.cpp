#include "CStatistics.h"
#include <StdIOPalm.h>
#include "main.h"
extern IndexedColorType	gStripColor;
CStatistics::CStatistics()
{
	UInt32	currentTimeStamp;
	//Read from pref record
	LoadPrefs();
	currentTimeStamp = TimGetSeconds();
	if (!SameAsToday(currentTimeStamp))	//The saved Today is not current day
		ClearToday();	
	if (total.openingTimeStamp==0)	//First time use or screwed up
		total.openingTimeStamp = currentTimeStamp;
}
CStatistics::~CStatistics()
{
//Save to pref record
	SavePrefs();
}
Boolean CStatistics::SameAsToday(UInt32 currentTimeStamp)
{
	DateTimeType	dt_today,dt_current;
	TimSecondsToDateTime(today.openingTimeStamp,&dt_today);
	TimSecondsToDateTime(currentTimeStamp,&dt_current);
	if (dt_today.year == dt_current.year && \
	dt_today.month == dt_current.month && \
	dt_today.day == dt_current.day)
		return true;
	else
		return false;
}
void CStatistics::TouchToday(UInt32 currentTimeStamp)
{
	today.openingTimeStamp = currentTimeStamp;
	today.endingTimeStamp = currentTimeStamp;
}
void CStatistics::ClearToday()
{
	today.duration = 0;
	today.readBytes = 0;
	today.sentBytes =0;
}
void CStatistics::ResetLastSession(UInt32 currentTimeStamp)
{
	//Today is not current day
	if (!SameAsToday(currentTimeStamp))
		//Clear today's statistics fields and reset time stamp for Today
	{
		ClearToday();
		TouchToday(currentTimeStamp);		
	}
	else
		//Only update today's time stamp, keep duration and other fields unchanged
		TouchToday(currentTimeStamp);
		
	//Last session is not current day, increase day count by one
	if (!SameAsToday(lastSession.openingTimeStamp))
		total.dayCount++;
	
	//Reset last session (it becomes current session).	
	lastSession.openingTimeStamp = currentTimeStamp;
	lastSession.endingTimeStamp = currentTimeStamp;
	lastSession.duration = 0;
	lastSession.readBytes = 0;
	lastSession.sentBytes =0;		
}
void CStatistics::ResetTotal(UInt32 currentTimeStamp)
{
	total.openingTimeStamp = currentTimeStamp;
	total.endingTimeStamp = currentTimeStamp;
	total.duration = 0;
	total.readBytes = 0;
	total.sentBytes =0;
	total.dayCount = 0;
}
void CStatistics::ResetAll()
{
	UInt32	currentTimeStamp;
	currentTimeStamp = TimGetSeconds();
	ResetLastSession(currentTimeStamp);
	ClearToday();
	TouchToday(currentTimeStamp);
	ResetTotal(currentTimeStamp); 
}
void CStatistics::UpdateDuration(UInt32 currentTimeStamp)
{
	//Update last session
	lastSession.duration = currentTimeStamp - lastSession.openingTimeStamp;
	//Update today
	today.duration += currentTimeStamp - today.endingTimeStamp;
	//Update total
	total.duration += currentTimeStamp - today.endingTimeStamp;
	//Update ending time stamp of today for calculating next duration
	today.endingTimeStamp = currentTimeStamp;	
}
void CStatistics::UpdateBytes(Int8 direction, UInt32 bytes)
{
	switch (direction)
	{
		case DIRECTION_READ:	//Read
			//Update last session
			lastSession.readBytes += bytes;
			//Update today
			today.readBytes += bytes;
			//Update total
			total.readBytes += bytes;
			break;
		case DIRECTION_SENT:	//Sent				
			//Update last session
			lastSession.sentBytes += bytes;
			//Update today
			today.sentBytes += bytes;
			//Update total
			total.sentBytes += bytes;
			break;
		default:
			break;
	}
}
void CStatistics::UpdateForm()
{
	//Update Statistics Form Statistics Data
	Char	displayString[20];
	DateTimeType	dt;
	DateType		dd;
	Int16	dCount=0;
	
	RectangleType	r;
	r.topLeft.x = 54;
	r.topLeft.y = 46;
	r.extent.x = 104;
	r.extent.y = 12;
	
	WinDrawLine( 2, 44, 157, 44 );
	
   	WinPushDrawState();
	WinSetBackColor(gStripColor);
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	//Display recent received
	if (lastSession.readBytes<1024)
		sprintf(displayString,"%luB",lastSession.readBytes);
	else if (lastSession.readBytes<1048576)
		sprintf(displayString,"%luK",lastSession.readBytes>>10);
	else //(lastSession.readBytes>=1048576)
		sprintf(displayString,"%luM",lastSession.readBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display recent duration
	TimSecondsToDateTime(lastSession.duration,&dt);
	sprintf(displayString,"%d:%d:%d",dt.hour,dt.minute,dt.second);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	
	WinPopDrawState();
	r.topLeft.y += 12;
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	//Display recent sent
	if (lastSession.sentBytes<1024)
		sprintf(displayString,"%luB",lastSession.sentBytes);
	else if (lastSession.sentBytes<1048576)
		sprintf(displayString,"%luK",lastSession.sentBytes>>10);
	else //(lastSession.sentBytes>=1048576)
		sprintf(displayString,"%luM",lastSession.sentBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display recent R+S (received + sent)
	if ((lastSession.readBytes+lastSession.sentBytes)<1024)
		sprintf(displayString,"%luB",lastSession.readBytes+lastSession.sentBytes);
	else if ((lastSession.readBytes+lastSession.sentBytes)<1048576)
		sprintf(displayString,"%luK",(lastSession.readBytes+lastSession.sentBytes)>>10);
	else //((lastSession.readBytes+lastSession.readBytes)>=1048576)
		sprintf(displayString,"%luM",(lastSession.readBytes+lastSession.sentBytes)>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	
	
	r.topLeft.y += 12;
	//Display today received
   	WinPushDrawState();
	WinSetBackColor(gStripColor);
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	if (today.readBytes<1024)
		sprintf(displayString,"%luB",today.readBytes);
	else if (today.readBytes<1048576)
		sprintf(displayString,"%luK",today.readBytes>>10);
	else //(today.readBytes>=1048576)
		sprintf(displayString,"%luM",today.readBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display today duration
	TimSecondsToDateTime(today.duration,&dt);
	sprintf(displayString,"%d:%d:%d",dt.hour,dt.minute,dt.second);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	
	WinPopDrawState();

	//Display today sent
	r.topLeft.y += 12;
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	if (today.sentBytes<1024)
		sprintf(displayString,"%luB",today.sentBytes);
	else if (today.sentBytes<1048576)
		sprintf(displayString,"%luK",today.sentBytes>>10);
	else //(today.sentBytes>=1048576)
		sprintf(displayString,"%luM",today.sentBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display today R+S (received + sent)
	if ((today.readBytes+today.sentBytes)<1024)
		sprintf(displayString,"%luB",today.readBytes+today.sentBytes);
	else if ((today.readBytes+today.sentBytes)<1048576)
		sprintf(displayString,"%luK",(today.readBytes+today.sentBytes)>>10);
	else //((today.readBytes+today.readBytes)>=1048576)
		sprintf(displayString,"%luM",(today.readBytes+today.sentBytes)>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	
			
	r.topLeft.y += 12;
	//Display total received
   	WinPushDrawState();
	WinSetBackColor(gStripColor);
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	if (total.readBytes<1024)
		sprintf(displayString,"%luB",total.readBytes);
	else if (total.readBytes<1048576)
		sprintf(displayString,"%luK",total.readBytes>>10);
	else //(total.readBytes>=1048576)
		sprintf(displayString,"%luM",total.readBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display total duration
	TimSecondsToDateTime(total.duration,&dt);
	sprintf(displayString,"%d:%d:%d",dt.hour,dt.minute,dt.second);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);				
	WinPopDrawState();

	r.topLeft.y += 12;	
	//Display Since when
	DateSecondsToDate(total.openingTimeStamp,&dd);
	if (dd.month>=10 && dd.day >=10)
		sprintf(displayString,"%d-%d-%d",dd.year+1904,dd.month,dd.day);
	else if (dd.month>=10 && dd.day <10)
		sprintf(displayString,"%d-%d-0%d",dd.year+1904,dd.month,dd.day);
	else if (dd.month<10 && dd.day >=10)
		sprintf(displayString,"%d-0%d-%d",dd.year+1904,dd.month,dd.day);
	else if (dd.month<10 && dd.day <10)
		sprintf(displayString,"%d-0%d-0%d",dd.year+1904,dd.month,dd.day);
	WinDrawChars(displayString,StrLen(displayString),3,r.topLeft.y);
	//Display total sent
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	if (total.sentBytes<1024)
		sprintf(displayString,"%luB",total.sentBytes);
	else if (total.sentBytes<1048576)
		sprintf(displayString,"%luK",total.sentBytes>>10);
	else //(total.sentBytes>=1048576)
		sprintf(displayString,"%luM",total.sentBytes>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display total R+S (received + sent)
	if ((total.readBytes+total.sentBytes)<1024)
		sprintf(displayString,"%luB",total.readBytes+total.sentBytes);
	else if ((total.readBytes+total.sentBytes)<1048576)
		sprintf(displayString,"%luK",(total.readBytes+total.sentBytes)>>10);
	else //((total.readBytes+total.readBytes)>=1048576)
		sprintf(displayString,"%luM",(total.readBytes+total.sentBytes)>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	

	r.topLeft.y += 12;
   	WinPushDrawState();
	// Draw the background rectangle
	WinSetBackColor(gStripColor);
	WinEraseRectangle( &r, 0 );	

	dCount = (total.dayCount>0?total.dayCount:1);
	//Display average received
	if ((total.readBytes/dCount)<1024)
		sprintf(displayString,"%luB",total.readBytes/dCount);
	else if ((total.readBytes/dCount)<1048576)
		sprintf(displayString,"%luK",(total.readBytes/dCount)>>10);
	else //((total.readBytes/dCount)>=1048576)
		sprintf(displayString,"%luM",(total.readBytes/dCount)>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display average duration
	TimSecondsToDateTime(total.duration/dCount,&dt);
	sprintf(displayString,"%d:%d:%d",dt.hour,dt.minute,dt.second);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);		 		
	WinPopDrawState();

	//Display average sent
	r.topLeft.y += 12;
	//Display day counts
	if (dCount>1)
		sprintf(displayString,"in %d days",dCount);
	else
		sprintf(displayString,"in %d day",dCount);
	WinDrawChars(displayString,StrLen(displayString),3,r.topLeft.y);
	// Draw the background rectangle
	WinEraseRectangle( &r, 0 );	
	if ((total.sentBytes/dCount)<1024)
		sprintf(displayString,"%luB",(total.sentBytes/dCount));
	else if ((total.sentBytes/dCount)<1048576)
		sprintf(displayString,"%luK",(total.sentBytes/dCount)>>10);
	else //((total.sentBytes/dCount)>=1048576)
		sprintf(displayString,"%luM",(total.sentBytes/dCount)>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x,r.topLeft.y);	
	//Display average R+S (received + sent)
	if ((total.readBytes+total.sentBytes)/dCount<1024)
		sprintf(displayString,"%luB",(total.readBytes+total.sentBytes)/dCount);
	else if ((total.readBytes+total.sentBytes)/dCount<1048576)
		sprintf(displayString,"%luK",(total.readBytes+total.sentBytes)/dCount>>10);
	else //((total.readBytes+total.readBytes)/dCount>=1048576)
		sprintf(displayString,"%luM",(total.readBytes+total.sentBytes)/dCount>>20);
	WinDrawChars(displayString,StrLen(displayString),r.topLeft.x+54,r.topLeft.y);	
}

void CStatistics::LoadPrefs()
{
	TStatistics	statPrefs;
	UInt16	prefsSize = sizeof(TStatistics);
    Int16 prefsVersion;
	//Get preferences

   	//Load recent
	prefsVersion = PrefGetAppPreferences (appFileCreator, appPrefID+11, &statPrefs, &prefsSize, true);
    if (prefsVersion>appPrefVersionNum)
         prefsVersion = noPreferenceFound;
    if (prefsVersion!=noPreferenceFound)
    {
    	lastSession.openingTimeStamp = statPrefs.openingTimeStamp;
    	lastSession.endingTimeStamp = statPrefs.endingTimeStamp;
    	lastSession.duration = statPrefs.duration;
    	lastSession.readBytes = statPrefs.readBytes;
    	lastSession.sentBytes = statPrefs.sentBytes;
    }
   	//Load today
	prefsVersion = PrefGetAppPreferences (appFileCreator, appPrefID+12, &statPrefs, &prefsSize, true);
    if (prefsVersion>appPrefVersionNum)
         prefsVersion = noPreferenceFound;
    if (prefsVersion!=noPreferenceFound)
    {
    	today.openingTimeStamp = statPrefs.openingTimeStamp;
    	today.endingTimeStamp = statPrefs.endingTimeStamp;
    	today.duration = statPrefs.duration;
    	today.readBytes = statPrefs.readBytes;
    	today.sentBytes = statPrefs.sentBytes;
    }
    //Load total
	prefsVersion = PrefGetAppPreferences (appFileCreator, appPrefID+13, &statPrefs, &prefsSize, true);
    if (prefsVersion>appPrefVersionNum)
         prefsVersion = noPreferenceFound;
    if (prefsVersion!=noPreferenceFound)
    {
    	total.openingTimeStamp = statPrefs.openingTimeStamp;
    	total.endingTimeStamp = statPrefs.endingTimeStamp;
    	total.duration = statPrefs.duration;
    	total.readBytes = statPrefs.readBytes;
    	total.sentBytes = statPrefs.sentBytes;    	
    	total.dayCount = statPrefs.dayCount;    	
    }
}
void CStatistics::SavePrefs()
{
	TStatistics	statPrefs;
	//save preferences

   	//Save recent
	statPrefs.openingTimeStamp = lastSession.openingTimeStamp;
	statPrefs.endingTimeStamp = lastSession.endingTimeStamp;
	statPrefs.duration = lastSession.duration;
	statPrefs.readBytes = lastSession.readBytes;
	statPrefs.sentBytes = lastSession.sentBytes;
    PrefSetAppPreferences (appFileCreator, appPrefID+11, appPrefVersionNum, &statPrefs, sizeof(TStatistics), true);
    
   	//Save today
	statPrefs.openingTimeStamp = today.openingTimeStamp;
	statPrefs.endingTimeStamp = today.endingTimeStamp;
	statPrefs.duration = today.duration;
	statPrefs.readBytes = today.readBytes;
	statPrefs.sentBytes = today.sentBytes;
    PrefSetAppPreferences (appFileCreator, appPrefID+12, appPrefVersionNum, &statPrefs, sizeof(TStatistics), true);

    //Save total
	statPrefs.openingTimeStamp = total.openingTimeStamp;
	statPrefs.endingTimeStamp = total.endingTimeStamp;
	statPrefs.duration = total.duration;
	statPrefs.readBytes = total.readBytes;
	statPrefs.sentBytes = total.sentBytes;
	statPrefs.dayCount = total.dayCount;
    PrefSetAppPreferences (appFileCreator, appPrefID+13, appPrefVersionNum, &statPrefs, sizeof(TStatistics), true);
}