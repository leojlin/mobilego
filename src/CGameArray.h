#ifndef CGAMEARRAY_H
#define CGAMEARRAY_H
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"

class CGameArray{
public:

	CGameArray() SEC_ARR;
	virtual ~CGameArray() SEC_ARR;
	//void Swap(Int32, Int32) SEC_ARR;
	void HeapSort(Int32 count,GAMEFIELD fld, Boolean  ascending) SEC_ARR;	
	Boolean Add(CGameItemPtr) SEC_ARR;
	void Clear() SEC_ARR;
	Boolean Insert(CGameItemPtr,Int32) SEC_ARR;
	Int32 FindById(UInt16) SEC_ARR;
	Boolean Remove(Int32) SEC_ARR;
	Int32 Size() SEC_ARR;
	CGameItemPtr operator[](Int32) SEC_ARR;

protected:
	Int16 Compare(CGameItem *itemP1, CGameItem *itemP2, GAMEFIELD fld, Boolean ascending) SEC_ARR;
	CGameItemPtr	*pArray;
	Int32	size;	
};

#endif // CGAMEARRAY_H
