//
//	Palm App Name:   		"MobileGo"
//
//	Palm App Version:		"1.0"
#ifndef	RSC_H
#define	RSC_H
#define GameOverAlert							1000
#define MaxAccountAlert							1100
#define MinAccountAlert							1200
#define DeclineMatchAlert						1300
#define WithdrawMatchAlert						1400
#define ServerResponseAlert						1500
#define GameResignAlert							1600
#define CheckScoreAlert							1700
#define LoginFailureAlert						1800
#define NetworkFailureAlert						1900
#define GamePlayingAlert						2000
#define GameUnobserveAlert						2100
#define RegisterPleaseAlert						2200
#define TrialExpiredAlert						2300
#define ConnectionLostAlert						2400
#define ConfirmUnobserveAlert					2500
#define GameAdjournAlert						2600
#define MemoryFailureAlert						2700
#define RomIncompatibleAlert					2800
#define GameTitleAlert							2900
#define GameCloseAlert							3000
#define InvalidDefsAlert						3100
#define BytesTimeAlert							3200
#define	FeatureUnavailableAlert					3300

//String resources
#define StringGamePreparing						1000
#define StringGameInprogress					1001
#define StringGameScoring						1002
#define StringGameOver							1003
#define StringGameInterrupted					1004
#define StringGameLoaded						1005

#define StringPlayingGame						1100
#define StringObservingGame						1101
#define StringVersus							1200
#define StringUnassigned						1300
#define StringAssigned							1301

#define	StringStandardEdition					1400
#define	StringBasicEdition						1401
#define	StringProfessionalEdition				1402

#define StringChallenge							1500
#define StringAccept							1501

//	Resource: tFRM 1000
#define MainForm								1000
#define	MainFormTerminalField					1001
#define	MainFormLoginButton						1002
#define	MainFormGamesButton					  	1003
#define	MainFormPlayersButton					1004
#define MainFormTalkCheckBox					1005
#define	MainFormTerminalScrollBar				1006
#define	MainFormChallengeButton					1007
#define	MainFormBoardButton						1008
#define	MainFormInputField						1009
//Game options setting
#define	MainFormClosePushButton					1010
#define	MainFormOpenPushButton					1011
#define	MainFormLookingPushButton				1012
#define MainFormChatBuddyTrigger				1013
#define MainFormChatBuddyList					1014
#define MainFormSeekPushButton					1015


//	Resource: tFRM 1100
#define AboutForm                               1100	//(Left Origin = 2, Top Origin = 2, Width = 156, Height = 156, Usable = 1, Modal = 1, Save Behind = 1, Help ID = 0, Menu Bar ID = 0, Default Button ID = 0)
#define AboutEditionLabel                     	1101
#define AboutBuildLabel                         1102	//(Left Origin = 54, Top Origin = 25, Usable = 1, Font = Bold 12)
#define AboutTextLabel	                        1103	//(Left Origin = 23, Top Origin = 54, Usable = 1, Font = Standard)
#define AboutVersionLabel                       1104	//(Left Origin = 50, Top Origin = 104, Usable = 1, Font = Bold)
#define AboutFormOKButton                       1105	//(Left Origin = 58, Top Origin = 139, Width = 40, Height = 12, Usable = 1, Anchor Left = 1, Frame = 1, Non-bold Frame = 1, Font = Standard)
#define AboutFormRegisterButton                 1106	//(Left Origin = 58, Top Origin = 139, Width = 40, Height = 12, Usable = 1, Anchor Left = 1, Frame = 1, Non-bold Frame = 1, Font = Standard)
													
//	Resource: tFRM 1200
#define BoardForm								1200
#define BoardFormWhiteTimer						1201
#define BoardFormBlackTimer						1202
#define BoardFormUndoButton						1203	//'U'
#define BoardFormDoneButton						1204	//'D'
#define BoardFormMoveLabel						1205
#define BoardFormMarkButton						1206	//'@'
#define BoardFormInfoButton						1210	//'I'
#define BoardFormConnected						1211	//'='
#define BoardFormResignButton					1212	//'R'
#define BoardFormAdjournButton					1214	//'A'
#define BoardFormPassButton						1215	//'P'
#define	BoardFormCancelButton					1216	//'-'
#define BoardFormChallengeButton				1217	//'?'
#define BoardFormChatButton						1218	//'C'
#define BoardFormRefreshButton					1219	
#define BoardFormUnobserveButton				1220	//'x'
#define BoardFormSaveButton						1221
#define BoardFormLoadButton						1222
#define BoardFormBoardSelectButton				1223	//'0'

#define BoardFormFForwardButton					1240
#define BoardFormFBackwardButton				1241
#define BoardFormForwardButton					1243
#define BoardFormBackwardButton					1242
#define BoardFormBeginButton					1244
#define BoardFormEndButton						1245

//	Resource: tFRM 1300
#define GameListForm							1300
#define	GameListFormTable						1301
#define	GameListFormScrollBar					1302
#define	GameListFormRefreshButton				1303
#define	GameListFormCancelButton				1304
#define GameListFormChallengeButton				1306
#define GameListFormLeftButton					1307
#define GameListFormRightButton					1308
//	Resource: tFRM 1400
#define PlayerListForm							1400
#define	PlayerListFormTable						1401
#define	PlayerListFormScrollBar					1402
#define	PlayerListFormRefreshButton				1403
#define	PlayerListFormCancelButton				1404
#define PlayerListFormChallengeButton			1406

//	Resource: tFRM 1900
#define ChallengeListForm							1900
#define	ChallengeListFormTable						1901
#define	ChallengeListFormScrollBar					1902
#define	ChallengeListFormSelectButton				1903
#define	ChallengeListFormCancelButton				1904
//	Resource: tFRM 1500
#define GameInfoForm							1500
#define GameInfoFormGN							1501
#define GameInfoFormDT							1502
#define GameInfoFormPC							1503
#define GameInfoFormKM							1504
#define GameInfoFormHA							1505
#define GameInfoFormRE							1506
#define GameInfoFormTM							1507
#define GameInfoFormPW							1508
#define GameInfoFormWR							1509
#define GameInfoFormPB							1510
#define GameInfoFormBR							1511
#define GameInfoFormAction						1512
#define GameInfoFormStatus						1513
//#define GameInfoFormWE							1514	//white Time Elapse
//#define GameInfoFormBE							1515	//black Time Elapse

#define GameInfoFormOKButton					1520


#define AccountListForm							1600	
#define	AccountListFormTable					1601
#define	AccountListFormScrollBar				1602
#define AccountListFormOKButton					1606
#define AccountListFormNewButton				1607
#define AccountListFormConnectButton			1608

#define AccountEditForm							1700
#define AccountEditFormDescription				1701
#define AccountEditFormUserName					1702
#define AccountEditFormPassword					1703
#define AccountEditFormServerName				1704
#define AccountEditFormServerPort				1705
#define AccountEditFormUsed						1706
#define AccountEditFormConnectButton			1707
#define AccountEditFormSaveButton				1708
#define AccountEditFormDeleteButton				1709
#define AccountEditFormCancelButton				1710
#define AccountEditFormServerTypeTrigger		1711
#define AccountEditFormServerTypeList			1712
#define AccountEditFormNmatch					1713
#define AccountEditFormProxy					1714
#define	AccountEditFormProxyButton				1715

#define AccountDeleteConfirmForm				1800
#define AccountDeleteConfirmFormOKButton		1801
#define AccountDeleteConfirmFormCancelButton	1802

#define PlayerFilterForm						2000
#define PlayerFilterFormUpperRankTrigger		2001
#define PlayerFilterFormLowerRankTrigger		2002
#define PlayerFilterFormUpperRankList			2003
#define PlayerFilterFormLowerRankList			2004
#define PlayerFilterFormOpenPushButton			2005
#define PlayerFilterFormAllPushButton			2006
#define PlayerFilterFormApplyCheckBox			2007
#define PlayerFilterFormOKButton				2010
#define PlayerFilterFormCancelButton			2011

#define ColorPrefForm							2100
#define ColorPrefOKButton						2101
#define ColorPrefCancelButton					2102
#define ColorPrefDefaultButton					2103

#define MatchForm								2200
#define MatchFormAcceptButton					2201
#define MatchFormDeclineButton					2202
#define MatchFormCancelButton					2203
#define MatchFormWhitePushButton				2204
#define MatchFormBlackPushButton				2205
#define MatchFormMainTimeField					2206
#define MatchFormBYTimeField					2207
#define MatchFormStonesField					2208
#define MatchFormHandicapField					2209
#define MatchFormMatchTypeField					2210
#define MatchFormAutoMatchCheckBox				2211
	
#define StoredForm								2300	
#define StoredFormSelectGameTrigger				2301
#define StoredFormSelectGameList				2302
#define StoredFormLoadButton					2303
#define StoredFormResignButton					2304
#define StoredFormDeleteButton					2305
#define StoredFormCancelButton					2306

#define SoundPrefForm							2400
#define SoundPrefPlaySoundCheckBoxButton		2401
#define SoundPrefAlertValueField				2402
#define SoundPrefScrollUpButton					2403
#define SoundPrefScrollDownButton				2404
#define SoundPrefOKButton						2405
#define SoundPrefCancelButton					2406

#define PlayerViewForm							2500
#define PlayerViewFormOKButton					2501
#define PlayerViewFormMatchButton				2502
#define PlayerViewFormTellButton				2503
#define PlayerViewFormObserveButton				2504

#define RegisterForm							2600
#define RegisterFormCode1Field					2601				
#define RegisterFormCode2Field					2602				
#define RegisterFormOKButton					2603
#define RegisterFormCancelButton				2604
#define RegisterFormEditionLabel				2605

#define SeekForm								2700
#define SeekFormConfig0							2701
#define SeekFormConfig1							2702
#define SeekFormConfig2							2703
#define SeekFormConfig3							2704
#define SeekFormSizeList						2705
#define SeekFormHandicapList					2706
#define SeekFormHandicapTrigger					2707
#define SeekFormOKButton						2708
#define SeekFormStopButton						2709
#define SeekFormCancelButton					2710

#define ProxyForm								2800
#define ProxyFormName							2801
#define ProxyFormPort							2802
#define ProxyFormTypeList						2803
#define ProxyFormOKButton						2804
#define ProxyFormCancelButton					2805

#define NetworkPrefForm							2900
#define NetworkPrefOKButton						2901
#define NetworkPrefCancelButton					2902
#define NetworkPrefDefaultButton				2903
#define NetworkPrefNetQualityTrigger			2904
#define NetworkPrefNetQualityList				2905
#define NetworkPrefNetLagField					2906
#define NetworkPrefScrollUpButton				2907
#define NetworkPrefScrollDownButton				2908

#define PasswordForm							3000
#define PasswordFormPasswordField				3001
#define PasswordFormOKButton					3002
#define PasswordFormCancelButton				3003

#define DefaultForm								3100
#define DefaultFormOkButton						3101
#define DefaultFormCancelButton					3102
#define DefaultFormSizeField1					3103
#define DefaultFormSizeField2					3104
#define DefaultFormMainTimeField				3105
#define DefaultFormBYTimeField					3106
#define DefaultFormStonesField					3107

#define StatisticsForm							3200
#define StatisticsFormOkButton					3201
#define StatisticsFormResetButton				3202

#define TitleForm								3300
#define TitleFormTitleField						3301
#define TitleFormOKButton						3302
#define TitleFormCancelButton					3303

//	Resource: Talt 1001
#define RomIncompatibleOK                         0


//	Resource: MBAR 1000
#define MainFormMenuBar                           1000


//	Resource: MENU 1000

#define MainMenuBar                        		1000
#define MainOptionsAboutStarterApp              1000
#define MainOptionsAccounts						1001
#define MainCommandLogout						1002
#define MainCommandLogin						1003
#define MainCommandStats						1004
#define MainOptionsColors						1005
#define MainOptionsCriteria						1006
#define MainOptionsDisconnect					1007
#define MainCommandStored						1008
#define MainOptionsSound						1009
#define MainOptionsNetwork						1010
#define MainCommandDefault						1011
#define MainStatisticsOnline					1012
#define MainCommandTeach						1013

#define BoardMenuBar                        	1200
#define BoardLibararyLoad                      	1200
#define BoardLibararySave                      	1201
#define BoardLibararySaveAll                   	1202

#define PlayerListMenuBar						1400
#define PlayerListCommandRefresh				1400
#define PlayerListOptionsFilter					1401

#define StatMenuBar								3200
#define StatCommandReset						3201

#define WhiteStoneBitmap						1000
#define BlackStoneBitmap						1100

#define ClockTickingWav							1000
#define PlaceStoneWav							1100
#define NewMessageWav							1200
//	Resource: taif 1000
#define Largeicons12and8bitsAppIconFamily         1000

//	Resource: taif 1001
#define Smallicons12and8bitsAppIconFamily         1001

#define	terminalField (FieldType*)GetObjectPtr(MainFormTerminalField)
#endif
