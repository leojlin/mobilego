#ifndef FORMS_H
#define FORMS_H
#include <PalmOS.h>	
#include "sections.h"

#define frmReloadUpdateCode        	0x0002
#define frmFilterUpdateCode        	0x0008
#define frmReSortUpdateCode			0x0016
#define frmChangeBoardUpdateCode   	0x0032

Boolean GameListFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean PlayerListFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean ChallengeListFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean BoardFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean AccountListFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean AccountEditFormHandleEvent( EventPtr event ) SEC_FRM1;
Boolean GameInfoFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean ColorPrefFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean SoundPrefFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean NetworkPrefFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean AboutFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean RegisterFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean SeekFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean ProxyFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean PlayerFilterFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean MatchFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean StoredFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean PlayerViewFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean PasswordFormHandleEvent(EventPtr event) SEC_FRM1;
Boolean TitleFormHandleEvent(EventPtr event) SEC_FRM1;
void DrawLine() SEC_FRM1;
void DrawGameListHeader() SEC_FRM1;
void DrawStoredGameListItem(Int16, RectangleType *, Char **) SEC_FRM1;
Boolean GameListFormHandlePenDown(EventPtr event) SEC_FRM1;
Boolean PlayerListFormHandlePenDown(EventPtr event) SEC_FRM1;
Boolean GameListFormHandleFormOpen() SEC_FRM1;
Boolean PlayerListFormHandleFormOpen() SEC_FRM1;
Boolean ChallengeListFormHandleFormOpen() SEC_FRM1;
Boolean BoardFormHandleFormOpen() SEC_FRM1;
Boolean AccountListFormHandleFormOpen() SEC_FRM1;
Boolean GameListFormHandleFormUpdate( EventPtr event ) SEC_FRM1;
Boolean PlayerListFormHandleFormUpdate( EventPtr event ) SEC_FRM1;
Boolean ChallengeListFormHandleFormUpdate( EventPtr event ) SEC_FRM1;
Boolean BoardFormHandleFormUpdate(EventPtr event) SEC_FRM1;
void DrawAccountItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds) SEC_FRM1;
void AccountListFormInitTable(TablePtr table) SEC_FRM1;
void AccountListFormUpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw) SEC_FRM1;
Boolean AccountListFormHandleFormUpdate( EventPtr event ) SEC_FRM1;
Boolean AccountFieldChanged() SEC_FRM1;
void SaveAccount() SEC_FRM1;
void MatchFormHandleAccept() SEC_FRM1;
void MatchFormHandleDecline() SEC_FRM1;
Boolean MatchFormDisplay() SEC_FRM1;
Boolean MatchFormHandleFormUpdate(EventPtr event) SEC_FRM1;
Boolean DefaultFormHandleEvent(EventPtr event) SEC_FRM1;
//Boolean MatchFormHandleFormOpen() SEC_FRM1;
Boolean ObserveGame(UInt16 gameid) SEC_FRM1;
Boolean StatisticsFormHandleEvent(EventPtr event) SEC_FRM1;
//Boolean TeachFormHandleEvent(EventPtr event) SEC_FRM1;
#endif // FORMS_H
