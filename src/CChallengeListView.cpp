//#include <TblGlue.h>
#include <FrmGlue.h>
#include <PalmOne_68K.h>
#include "main.h"
#include "forms.h"
#include "CChallengeArray.h"
#include "nngs.h"
#include "CChallengeListView.h"
#include "network.h"
#include "rsc.h"
#include "ui.h"

extern CChallengeItemPtr	gCurrentMatch;
extern Char gCurrentOpponent[];
extern Boolean gNavSupport;
extern Int16	gCurrentMain;
extern Int16	gCurrentByoyomi;
//template <class CGameItem>
void CChallengeListView::SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	customDrawFunction = f;
}
//template <class CGameItem>
CChallengeListView::CChallengeListView()
{
	hasNewItem = false;
	skippedItems=0; 
	updatingList = false;
	list = new CChallengeArray;
	//Check memory allocation here
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
}
//template <class CChallengeItem>
CChallengeListView::CChallengeListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	hasNewItem = false;
	skippedItems=0; 
	updatingList = false;
	list = new CChallengeArray;
	//Check memory allocation here
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
	customDrawFunction = f;
}
//template <class CChallengeItem>
CChallengeListView::~CChallengeListView()
{
	Clear(); 
}
//template <class CChallengeItem>
void CChallengeListView::Clear()
{
	if (list)
	{
		delete list; 
		list = NULL;
	}
}

//template <class CChallengeItem>
Boolean CChallengeListView::HasNewItem()
{
	if (!list->Size())
		return false;	
	else
		return hasNewItem;
}
Boolean CChallengeListView::HasItem()
{
	if (list->Size())
		return true;
	else
		return false;
}
Boolean CChallengeListView::TimeoutItem(UInt32 currentTime, UInt32 timeout)
{
	Int32 i;
	CChallengeItem *itemP;
	Boolean removed = false;
	for (i=list->Size();i>0;i--)
	{
		itemP = (*list)[i-1];
		if (itemP->timeStamp + timeout < currentTime)
		{
			list->Remove(i-1);
			removed = true;
		}
	}
	return removed;
}
void CChallengeListView::RemoveItem(Char *playerName)
{
	Int32 index;
	index = list->FindByName(playerName);
	if ( index != -1)
		list->Remove(index); 
}
void CChallengeListView::SetItemRank(Char *playerName, Char *rank)
{
	Int32 index;
	index = list->FindByName(playerName);
	if ( index != -1)
	{
		CChallengeItem *item;
		item = (*list)[index]; 
		item->playerRank = (Char*)MemPtrNew(StrLen(rank)+1);
		if (item->playerRank)
			StrCopy(item->playerRank,rank);
	}
}
CChallengeItem* CChallengeListView::FindItemByName(Char *playerName)
{
	Int32 index;
	index = list->FindByName(playerName);
	if ( index != -1)
	{
		CChallengeItem *item;
		item = (*list)[index]; 
		return item;
	}
	else
		return NULL;
}
void CChallengeListView::UpdateList(CChallengeItem *item)
{
	Int32 index;
	
	if (updatingList)
	{
		index = list->FindByName(item->playerName);	//Has this player challenged me already?
		if ( index != -1)							//Yes, delete previous challenge
		{
			if (!list->Remove(index)) 				
				return;								//Fail to delete
		}
		if (list->Insert(item,0))	//Insert the new challenge to the top of challenge list
		{
			hasNewItem = true;		//Set new challenge notification 
		}
		else
		{
			return;					//Fail to insert
		}
	}
	return ;
}

//template <class CChallengeItem>
void CChallengeListView::EndOfList()
{
	if (updatingList)
	{
		updatingList = false;
		FrmUpdateForm(ChallengeListForm,frmReloadUpdateCode);
	}
}
//template <class CChallengeItem>
void CChallengeListView::BeginOfList()
{
	skippedItems=0; 
	updatingList = true;
   	highLighted = false;
   	highLightedRow = 0;
}
void CChallengeListView::Flush()
{
	if (list) 
		delete list;
	list = new CChallengeArray;
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
	skippedItems=0; 
   	highLighted = false;
   	highLightedRow = 0;
}

//template <class CChallengeItem>
void CChallengeListView::UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw)
{
	Int16	row;
	Int16	totalItems;
	Int16	tableRows = TABLEROWS;
	CChallengeItem *itemP;
	totalItems = list->Size();
	if( skippedItems < 0 )     // if we page down, this could end up negative
    	skippedItems = 0;

   	if( skippedItems + tableRows > totalItems && totalItems >= tableRows )
      	skippedItems = totalItems - tableRows;

   	if( skippedItems > 0 && totalItems <= tableRows )
      	skippedItems = 0;
   	InitTable(table);
	for( row=0; row<tableRows; row++ )
    {
    	if( row >= totalItems )
        	TblSetRowUsable( table, row, false );			// so it won't get drawn
		else
        {
        	itemP = (*list)[row + skippedItems ];
			TblSetRowUsable( table, row, true );			// so it will get drawn
			TblMarkRowInvalid( table, row );					// so it will get updated
			TblSetItemPtr( table, row, 0, (void *)itemP);
			itemP = (CChallengeItem*)TblGetItemPtr( table, row, 0 );
		}
	}
	if( redraw )
    	TblDrawTable( table );
	UpdateScrollers( table, scrollBar );  
	if (FrmGetActiveFormID() == ChallengeListForm)
		hasNewItem = false;	//Clear new challenge notification 
}

//template <class CChallengeItem>
void CChallengeListView::Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage)
{
	Int16	tableRows = TABLEROWS;

	if( fullPage )
	{
    	dir *= tableRows;
		skippedItems += dir;
	}
	else
	{
		highLightedRow +=dir;
		if (skippedItems+highLightedRow >= list->Size()) //the last item is highlighted
			highLightedRow = highLightedRow -1;
		if (highLightedRow >= tableRows)
		{
			highLightedRow = highLightedRow -1;
			skippedItems += dir;
		}
		else if (highLightedRow < 0)
		{
			highLightedRow = 0;
			skippedItems += dir;
		}
	}
	UpdateTable( table, scrollBar, true );
}

//template <class CChallengeItem>
void CChallengeListView::InitTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
	{		
		TblSetItemStyle( table, row, 0, customTableItem );
		TblSetItemFont( table, row, 0, stdFont );
		TblSetRowUsable( table, row, false );
	}
	TblSetColumnUsable( table, 0, true );
   	TblSetCustomDrawProcedure( table, 0, customDrawFunction );
}

//template <class CChallengeItem>
void CChallengeListView::DisableTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
		TblSetRowUsable( table, row, false );
}

//template <class CChallengeItem>
void CChallengeListView::UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar)
{
	Int16	totalItems;
   	Int16 	tableRows = TABLEROWS;
	
	totalItems = list->Size();
   	if( totalItems <= tableRows )
		SclSetScrollBar( scrollBar, 0, 0, 0, tableRows-1 );
	else	
		SclSetScrollBar( scrollBar, skippedItems, 0, totalItems - tableRows, tableRows-1 );
}

//template <class CChallengeItem>
Boolean CChallengeListView::HandleEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	FormPtr	pForm = FrmGetActiveForm();
	RectangleType	recTable;

	switch( event->eType )
	{
		case sclRepeatEvent:
			handled = HandleRepeatEvent( event, table, scrollBar );
			break;
		case tblEnterEvent:
			handled = HandleTableEnterEvent( event, table, scrollBar );
			break;
		case keyDownEvent:
			if (!gNavSupport || (gNavSupport && ObjectHasFocus(pForm, ChallengeListFormTable)))
		  	{
		  		//in Object focus mode, only handle key down envent when table gets focus
				handled = HandleKeyDownEvent( event, table, scrollBar );
		  	}
			break;
		case frmObjectFocusTakeEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case ChallengeListFormTable:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,ChallengeListFormTable)); 
					FrmGetObjectBounds(pForm,FrmGetObjectIndex(pForm,ChallengeListFormTable),&recTable);
					FrmGlueNavDrawFocusRing(pForm,ChallengeListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
				default:
					break;
			}
			break;
		case frmObjectFocusLostEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case ChallengeListFormTable:
					highLighted = false;
					highLightedRow = 0;
					UpdateTable(table, scrollBar, true );
					handled = true;
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return handled;
}

//template <class CChallengeItem>
Boolean CChallengeListView::HandleKeyDownEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	if (NavSelectPressed(event))
	{
		if (!highLighted)
		{
			highLighted = true;
			highLightedRow = 0; //top item on table
			UpdateTable( table, scrollBar, true );
			handled = true;
		}
		else	//highlighted, same as tap on the highlighted item
		{
			CChallengeItem *item = (CChallengeItem*)TblGetItemPtr( table, highLightedRow, 0 );
			handled = HandleSelectItem(item);
		}
		//return handled;
	}
	if (NavDirectionPressed(event, Up))
	{
		Scroll( table, scrollBar, -1, highLighted?false:true );
		handled = true;
	}
	if (NavDirectionPressed(event, Down))
	{
		Scroll( table, scrollBar, 1, highLighted?false:true );
		handled = true;
	}
/*	
	switch( event->data.keyDown.chr )
    {
    	case vchrRockerCenter:
    		if (!highLighted)
    		{
    			highLighted = true;
    			highLightedRow = 0; //top item on table
    			UpdateTable( table, scrollBar, true );
    			handled = true;
    		}
    		else	//highlighted, same as tap on the highlighted item
    		{
				CChallengeItem *item = (CChallengeItem*)TblGetItemPtr( table, highLightedRow, 0 );
				handled = HandleSelectItem(item);
			}
			break;
    	case vchrRockerUp:
		case vchrPageUp:
			Scroll( table, scrollBar, -1, highLighted?false:true );
			handled = true;
         	break;
        case vchrRockerDown: 
		case vchrPageDown:
			Scroll( table, scrollBar, 1, highLighted?false:true );
			handled = true;
			break;
		default:	
			break;
	}
*/
    return handled;
}


/**
 * Handles repeat events (i.e. the scrollers).
 */
//template <class CChallengeItem>
Boolean CChallengeListView::HandleRepeatEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Scroll( table, scrollBar, event->data.sclRepeat.newValue - event->data.sclRepeat.value, false );
	return false;
}

/**
 * Handles what happens when the user taps on an item in the tree/table.
 */
//template <class CChallengeItem>
Boolean CChallengeListView::HandleTableEnterEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	UInt16   row = event->data.tblSelect.row;
	CChallengeItem *item = (CChallengeItem*)TblGetItemPtr( table, row, 0 );
/*
	if( !item )
	{
		return false;
	}
	else
	{
		SetCurrentMatch(item);
		FrmPopupForm(MatchForm);
		return true;
	}
*/
	return HandleSelectItem(item);
}
/**
 * Handles what happens when the highlighted item is selected.
 */
Boolean CChallengeListView::HandleSelectItem(CChallengeItem *item)
{
	if( !item )
	{
		return false;
	}
	else
	{
		SetCurrentMatch(item);
		FrmPopupForm(MatchForm);
		return true;
	}
}
CChallengeItem::CChallengeItem()
{
	matchtype = MATCH;
	offered = true;
	playerName = NULL;
	playerRank = NULL;
	size = 19;
	handicap = 0;
	komi = 55;
	main = gCurrentMain;
	byoyomi = gCurrentByoyomi;
	stones = 25;
	count = 0;	//Koryo count 
	koryo = 0;	//Koryo time (in second)
	prebyoyomi = 0; //Prebyoyomi time (in second)
}
CChallengeItem::~CChallengeItem()
{
	Release();
}

Boolean CChallengeItem::operator==(const CChallengeItem& pi)
{	
	return (!StrCompare(playerName, pi.playerName));
}
void CChallengeItem::Release()
{
	if (playerName)
	{
		MemPtrFree(playerName);
		playerName = NULL;
	}
	if (playerRank)
	{
		MemPtrFree(playerRank);
		playerRank = NULL;
	}
	return;
}
