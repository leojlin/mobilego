#ifndef	__GO_H__
#define	__GO_H__
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"
#ifdef _PE_
const Int16	MAX_BOARDS = 4;
#else
const Int16	MAX_BOARDS = 1;
#endif
const Int16	MAX_MOVECOUNT =	400;
//const UInt8 EMPTY =	0x00;
//const UInt8 WHITE =	0x01;
//const UInt8 BLACK =	0x02;
const UInt8 MARK =	0x03;
const UInt8 PIECEMASK =	0x03;
const UInt8 FLAG = 0x80;
const UInt8 BLACKTERRITORY = 0x40;
const UInt8 WHITETERRITORY = 0x20;
const UInt8 TRIANGLE = 0x04;
const UInt8 SQUARE = 0x08;
const UInt8 CIRCLE = 0x10;
const UInt8 MARKMASK = 0x7C;
const UInt8 UNKNOWN = 0x1C;
typedef struct TPosition
{
	Int16	x;
	Int16	y;
};

typedef	struct TMove
{
	Int16 id;
	Int16 color;
	TPosition	position;
};

class CMoveRecord
{
public:
	CMoveRecord() SEC_GO;
	virtual	~CMoveRecord() SEC_GO;
	void Init() SEC_GO;
	void AddMove(TMove) SEC_GO;
	void DeleteMove() SEC_GO;
	Boolean GetMove(Int16,TMove&) SEC_GO;
	Int16	GetMoveCount() SEC_GO;
protected:
	Int16	moveCount;
	TMove move[MAX_MOVECOUNT];
};


class CBoard
{
public:
	CBoard() SEC_GO;
	virtual ~CBoard() SEC_GO;
	void Init() SEC_GO;
	void Clear() SEC_GO;
	void Save() SEC_GO;
//	void Load() SEC_GO;
	void Load(Char*) SEC_GO;
	void Refresh() SEC_GO;
	void NewMove(TMoveInfo) SEC_GO;
	void ChangeKomi(Int16) SEC_GO;
	void UndoMove(TMoveInfo) SEC_GO;
	void EndOfMove() SEC_GO;
	void EndOfGame(UInt16, Char*) SEC_GO;
	void EndOfMatch(Char*, Char*) SEC_GO;
	void CheckScore() SEC_GO;
	//void FlagGroup(Int16, Int16) SEC_GO;
	void MarkDead(Int16, Int16) SEC_GO;
	void UnmarkDead() SEC_GO;
	void InitGameInfo(CGameItem *) SEC_GO;
	void UpdateGameInfo(UInt16,UInt8*, UInt8*, Int16 *,Int16 *) SEC_GO;
	void UpdateSgfInfo(TSgfInfo) SEC_GO;
	void GetSgfInfo(TSgfInfo&) SEC_GO;
	void GetGameInfo(TGameInfo&) SEC_GO;
	void GetTimerInfo(TTimerInfo&) SEC_GO;
	void UpdateTimer(Int16) SEC_GO;
	void UpdateTitle(Char*) SEC_GO;
	void UpdateByoyomiStones(UInt16,Int16) SEC_GO;
	void NewTimerInfo(TTimerInfo) SEC_GO;
	void ShowBoard() SEC_GO;
	void UpdateBoard() SEC_GO;
	void Resign() SEC_GO;
	void Pass() SEC_GO;
	void Adjourn() SEC_GO;
	void Undo() SEC_GO;
	void Done() SEC_GO;
	void Mark() SEC_GO;
	void Unobserve() SEC_GO;
	void PenMove(EventPtr) SEC_GO;
	void PenUp(EventPtr) SEC_GO;
	Boolean KeyDown(EventPtr) SEC_GO;
	void SwitchLabel() SEC_GO;
	void MoveBackward() SEC_GO;
	void MoveForward() SEC_GO;
	void MoveFastBackward() SEC_GO;
	void MoveFastForward() SEC_GO;
	void MoveBegin() SEC_GO;
	void MoveEnd() SEC_GO;
	
protected:
	Boolean UpdatePosition(TMove) SEC_GO;
	void SetLoadedPosition() SEC_GO;
	Boolean CheckCapture(Int16, Int16) SEC_GO;
	void CleanBoard() SEC_GO;
	void CleanScreen() SEC_GO;
	void UpdateButtons() SEC_GO;
	void DrawStatus() SEC_GO;
	void DrawTimer() SEC_GO;
	void DrawMoveLabel() SEC_GO;
	void DrawMarker(Int16, Int16, UInt8) SEC_GO;
	void DrawMarkers() SEC_GO;
	void DrawNumber(Int16, Int16, UInt8, Int16) SEC_GO;
	void DrawOnBoard(Int16, Int16, UInt8, Int16 Size, Int16, Int16) SEC_GO;
	void DrawPiece(Int16, Int16, UInt8) SEC_GO;
	void InvertZone(Int16, Int16) SEC_GO;
	void JumpToMoveN(Int16) SEC_GO;
	Boolean undoingMode;
	Boolean addingMode; 
	Boolean reviewingMode;
	Boolean scoringMode;
	UInt8 p[19][19];	//position
	UInt8 screen[19][19]; //screen
	Int16 prisoners[4];
	Int16 score[4];
	Int16 play;	//next player color
	Int16 size;
	Int16 stones;	//Byoyomi Stones
	Int16 displayedMoveCount;
	Int16 previouseDisplayedMoveCount;
	Int16 lastx,lasty;
	//CGame game;
	CMoveRecord record;
	Int16	systemMoveCount;
	Int16 recentMoveCount;
	Int16 baseMoveCount;
	Int16 offset, width;
	Int16 vsize ,viewx,viewy;
	Int16 virtualX; 
	Int16 virtualY;  
	Int16 prevPenX;
	Int16 prevPenY;
	//setting
	UInt16 currentGameId;
	TGameInfo gameInfo;
	TSgfInfo sgfInfo;
	TTimerInfo timerInfo;
	Boolean showMovePosition;
};

class CBoardPool{
public:

	CBoardPool()  SEC_GO;
	virtual ~CBoardPool()  SEC_GO;
	CBoard*	GetCurrentBoard();	
	void	SetCurrentBoard(const CBoard*)  SEC_GO;	
	void	SetCurrentBoard(const Int16)  SEC_GO;	
	Int16	GetBoardIndex(const CBoard*)  SEC_GO;
	//Int16	GetCurrentBoardIndex()  SEC_GO;
	CBoard*	GetNewBoard()  SEC_GO;
	CBoard*	GetInactiveBoard()  SEC_GO;
	CBoard*	GetBoardByID (const UInt16)  SEC_GO;
	CBoard*	GetBoardByIndex (const Int16)  SEC_GO;
	void	CloseBoard (const UInt16)  SEC_GO;
	void	CloseBoard (const CBoard*)  SEC_GO;
	void	DiscardBoard (const CBoard*)  SEC_GO;
	void	UpdateTimer(Int16)  SEC_GO; 
	void	InterruptGame()  SEC_GO;
	//CBoard*	OpenBoard (const UInt16 gameID)  SEC_GO;
	//Boolean	AllOpened()  SEC_GO;
	UInt16	recentUpdatedGameID;
	UInt16	matchingGameID;
	Boolean waitNewGameID;
	Boolean unobservedGameID;
private:
	CBoard*	boardArray[4];
	Int16	currentBoardIndex;
};
CBoard*	OpenNewBoard()  SEC_GO;
#endif	