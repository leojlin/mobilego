#include <PalmOS.h>
#include <FrmGlue.h>
#include <StdIOPalm.h>
#include <PalmOne_68K.h>
#include "nngs.h"
#include "go.h"
#include "rsc.h"
#include "main.h"
#include "ui.h"
#include "network.h"
#include "CStorage.h"


//#define ZoomSize	13
extern BitmapPtr	whiteStoneP,blackStoneP;
extern MemPtr	tickingAudioP,moveAudioP;
extern Char	gMyAccountName[];
extern Char gChatBuddyName[];
extern IndexedColorType	gBoardColor,gTextColor;
extern IndexedColorType	greyColor,blackColor,whiteColor;
extern Boolean 	gPlaySound;
extern Int16	gByoAlert;			//in seconds
extern Int16			gAccountIdInUsed;
extern TGoAccount		gAccountList[]; 
extern Boolean	gWaitDraw;
extern Boolean	gSupportColor;
extern Boolean gHiRes;
extern Boolean gSupportSound; 
extern CBoardPool	gBoardPool;
//extern Boolean gNavSupport;
extern CustomPatternType  CustomPtn;
void ConvertTimerString(Int16, Char*);
void NotifyGameOver(UInt16, Char*);
#define RESETCURSOR	\
	virtualX = size; \
	virtualY = size; \
	prevPenX = size; \
	prevPenY = size;
#define SHOWCURSOR	\
	virtualX = lastx; \
	virtualY = lasty; \
	if (virtualX < 0 && virtualY < 0)	\
	{	\
		virtualX = 15;	\
		virtualY = 3;	\
	}	\
	prevPenX = virtualX; \
	prevPenY = virtualY; \
	InvertZone(virtualX,virtualY); 
#define HIDECURSOR	\
	if (virtualX<size && virtualY<size)	\
		InvertZone(virtualX,virtualY);	\
	RESETCURSOR
#define MYTURN	(gameInfo.color & play) != 0			

/************************************************
 * Function:	CMoveRecord::Init()
 * Purpose:		Initialize CMoveRecord
 */
void CMoveRecord::Init() 
{
	Int16 i;
	moveCount = 0;
	for (i=0;i<MAX_MOVECOUNT;i++)
	{
		move[i].color = EMPTY;
		move[i].position.x = -1;
		move[i].position.y = -1;
	}
}
CMoveRecord::CMoveRecord()
{
	Init();
}
CMoveRecord::~CMoveRecord()
{
}
/************************************************
 * Function:	CMoveRecord::AddMove()
 * Purpose:		Add a new move to move array
 */
void CMoveRecord::AddMove(TMove newMove)
{
	if (moveCount==newMove.id && moveCount<MAX_MOVECOUNT)
	{
		move[moveCount++]=newMove;		
	}
	return;
}
/************************************************
 * Function:	CMoveRecord::DeleteMove()
 * Purpose:		Delete the lastest move from move array
 */
void CMoveRecord::DeleteMove()
{
	if (moveCount>0)
		moveCount--;		
	return;
}

/************************************************
 * Function:	CMoveRecord::GetMove()
 * Purpose:		Get a move from move array
 */
Boolean CMoveRecord::GetMove(Int16 index, TMove &theMove)
{
	if (index>=0 && index <moveCount)
	{
		theMove = move[index];		
		return true;
	}
	else 
		return false;
}

/************************************************
 * Function:	CMoveRecord::GetMoveCount()
 * Purpose:		Return moveCount of CMoveRecord
 */
Int16 CMoveRecord::GetMoveCount()
{
	return moveCount;
}
CBoard::CBoard()
{
	Init();
}

/************************************************
 * Function:	CBoard::Init()
 * Purpose:		Initialize CBoard, set default values
 */
void CBoard::Init()
{
	size = 19;	//Default size = 19
	//moveCount = 0;
	prisoners[WHITE] = prisoners[BLACK] = 0;
	score[WHITE] = score[BLACK] = 0;
	play = BLACK;	//whose turn for next move
	displayedMoveCount = -1;
	previouseDisplayedMoveCount = displayedMoveCount;
	scoringMode = false;
	lastx = lasty = -1;
	vsize = size;
	viewx = viewy = 0;
	/*
	virtualX = size;  
	virtualY = size;  
	prevPenX = size;
	prevPenY = size;
	*/
	RESETCURSOR
	systemMoveCount = 20;
	recentMoveCount = 0;
	baseMoveCount = 0;
	//setting
	stones = 0;
	undoingMode = false;
	addingMode = false;
	reviewingMode = false;
	showMovePosition = false; 
	//Init record
	record.Init();
	//Init timerInfo 
	timerInfo.gameID = 0;
	timerInfo.whitePlayer = NULL;
	timerInfo.blackPlayer = NULL;
	timerInfo.whiteCapture = 0;
	timerInfo.whiteTimeLeft = -1;
	timerInfo.whiteStoneLeft = -1;
	timerInfo.blackCapture = 0;
	timerInfo.blackTimeLeft = -1;
	timerInfo.blackStoneLeft = -1;
	//timerInfo.whiteTimeElapse = 0;
	//timerInfo.blackTimeElapse = 0;
	//Init gameInfo
	gameInfo.action = IDLE;
	gameInfo.id = 0;
	gameInfo.status = PREPARING;
	gameInfo.byoyomi = 0;
	gameInfo.color = EMPTY;
	//Init sgfInfo
	/*
	sgfInfo.GN = NULL;
	sgfInfo.EV = NULL;
	sgfInfo.RO = NULL;
	sgfInfo.PB = NULL;
	sgfInfo.BR = NULL;
	sgfInfo.PW = NULL;
	sgfInfo.WR = NULL;
	sgfInfo.RE = NULL;
	sgfInfo.PC = NULL;
	sgfInfo.DT = NULL;
	sgfInfo.TM = NULL;
	sgfInfo.ID = NULL;
	sgfInfo.KM = 65;
	sgfInfo.HA = 0;
	sgfInfo.SZ = 19;
	sgfInfo.WE = 0;
	sgfInfo.BE = 0;
	*/
	sgfInfo.Init();
	CleanBoard();
}
/************************************************
 * Function:	CBoard::Refresh()
 * Purpose:		refresh CBoard, reload moves and redraw board
 */
void CBoard::Refresh()
{
	Char cmd[20];
	size = 19;	//Default size = 19
	//moveCount = 0;
	prisoners[WHITE] = prisoners[BLACK] = 0;
	score[WHITE] = score[BLACK] = 0;
	play = BLACK;	//whose turn for next move
	displayedMoveCount = -1;
	scoringMode = false;
	lastx = lasty = -1;
	vsize = size;
	viewx = viewy = 0;
	/*
	virtualX = size;  
	virtualY = size;  
	prevPenX = size;
	prevPenY = size;
	*/
	RESETCURSOR
	systemMoveCount = 20;
	recentMoveCount = 0;
	baseMoveCount = 0;
	//setting
	reviewingMode = false;
	showMovePosition = false;
	//Init record
	record.Init();
	//Init timerInfo 
	timerInfo.gameID = 0;
	timerInfo.whitePlayer = NULL;
	timerInfo.blackPlayer = NULL;
	timerInfo.whiteCapture = 0;
	timerInfo.whiteTimeLeft = -1;
	timerInfo.whiteStoneLeft = -1;
	timerInfo.blackCapture = 0;
	timerInfo.blackTimeLeft = -1;
	timerInfo.blackStoneLeft = -1;
	/*Keep gameInfo and sgfInfo
	//Init gameInfo
	gameInfo.action = IDLE;
	gameInfo.id = 0;
	gameInfo.status = PREPARING;
	gameInfo.byoyomi = 0;
	gameInfo.color = EMPTY;
	//Init sgfInfo
	sgfInfo.GN = NULL;
	sgfInfo.EV = NULL;
	sgfInfo.RO = NULL;
	sgfInfo.PB = NULL;
	sgfInfo.BR = NULL;
	sgfInfo.PW = NULL;
	sgfInfo.WR = NULL;
	sgfInfo.RE = NULL;
	sgfInfo.PC = NULL;
	sgfInfo.DT = NULL;
	sgfInfo.TM = NULL;
	sgfInfo.KM = 65;
	sgfInfo.HA = 0;
	sgfInfo.SZ = 19;
	*/
	CleanBoard();
	sprintf(cmd,"move %d",gameInfo.id);
	WriteServer(cmd);	//get move info
}
void CBoard::Clear()
{
	//We have to unobserve the game if we are currently observing a game
/*	
	Char cmd[20];
	if (gameInfo.action == OBSERVE && gameInfo.status == INPROGRESS)
	{
		sprintf(cmd,"unobserve %d",gameInfo.id);
		WriteServer(cmd);	//Send UNOBSERVE command
	}
*/			
	timerInfo.Release();
	sgfInfo.Release();
}
CBoard::~CBoard()
{
	Clear();
}
void CBoard::Save()
{
	CStorage storage;
	if (gameInfo.action != IDLE && gameInfo.status != PREPARING && gameInfo.status != LOADED)
	//if (gameInfo.status != PREPARING)
		storage.Save(sgfInfo,record);	
		//CStorage::Save(sgfInfo,record);	
}
/*void CBoard::Load()
{
	UInt8 status = LOADED;
	CStorage storage;
	if (!storage.Load(sgfInfo,record))
	//if (!CStorage::Load(sgfInfo,record))
	{
		UpdateGameInfo(0,NULL,&status,NULL,NULL);
		SetLoadedPosition();
	}
}*/
void CBoard::Load(Char* sgfFileName)
{
	UInt8 status = LOADED;
	CStorage storage;
	if (!storage.Load(sgfInfo,record,sgfFileName))
	//if (!CStorage::Load(sgfInfo,record))
	{
		UpdateGameInfo(0,NULL,&status,NULL,NULL);
		SetLoadedPosition();
	}
}
/************************************************
 * Function:	CBoard::NewMove()
 * Purpose:		Handle new move
 */
void CBoard::NewMove(TMoveInfo newMoveInfo)
{
	TMove		newMove;
	TSgfInfo		sgf;
	//if (currentGameId != gameInfo.id)	//The new move is not what we are observing
	//	return;
	//if (newMoveInfo.moveID == 0 && newMoveInfo.handicap > 0)
	//	sgfInfo.HA = newMoveInfo.handicap;

	//addingMode = true;
	if (record.GetMoveCount() == (Int16)newMoveInfo.moveID)
	{
		newMove.id = newMoveInfo.moveID;
		newMove.color = newMoveInfo.playerColor;
		newMove.position.x = newMoveInfo.x;
		newMove.position.y = size - newMoveInfo.y - 1; //Mirro y scale
		//Update SgfInfo if handicap <> 0
		if (newMoveInfo.moveID == 0 && newMoveInfo.handicap>0)
		{  
			sgf.Init();
			sgf.HA = newMoveInfo.handicap;
			UpdateSgfInfo(sgf);
			sgf.Release();
		}
		record.AddMove(newMove);
		//Update borad position
		if (!reviewingMode)
		{
			UpdatePosition(newMove);
			//lastx = newMove.position.x;
			//lasty = newMove.position.y;
			//play = newMoveInfo.playerColor == WHITE?BLACK:WHITE;
			previouseDisplayedMoveCount = displayedMoveCount;   
			displayedMoveCount++ ;
		}
		play = newMoveInfo.playerColor == WHITE?BLACK:WHITE;
//		addingMode = true;
		//if (BoardForm == FrmGetActiveFormID())
		//{
		//	if (displayedMoveCount == -1 || displayedMoveCount < record.GetMoveCount()-2)
		//		ShowBoard();
		//	else
		//		UpdateBoard();
			//FrmUpdateForm(BoardForm,frmRedrawUpdateCode);
		//}
	}
}
/************************************************
 * Function:	CBoard::UndoMove()
 * Purpose:		Handle undo move
 */
void CBoard::UndoMove(TMoveInfo lastMoveInfo)
{
	//if (currentGameId != gameInfo.id)	//The new move is not what we are observing
	//	return;
	record.DeleteMove();
		//Update borad position
//	if (!reviewingMode)
//	{
//		UpdatePosition(newMove);
		//lastx = newMove.position.x;
		//lasty = newMove.position.y;
		//play = newMoveInfo.playerColor == WHITE?BLACK:WHITE;
//		previouseDisplayedMoveCount = displayedMoveCount;   
//		displayedMoveCount++ ;
//	}
	play = play == WHITE?BLACK:WHITE;
	undoingMode = true;
		//if (BoardForm == FrmGetActiveFormID())
		//{
		//	if (displayedMoveCount == -1 || displayedMoveCount < record.GetMoveCount()-2)
		//		ShowBoard();
		//	else
		//		UpdateBoard();
			//FrmUpdateForm(BoardForm,frmRedrawUpdateCode);
		//}
}
/************************************************
 * Function:	CBoard::EndOfMove()
 * Purpose:		Show board or update board on the end of a move
 */
void CBoard::EndOfMove()
{
	if (!reviewingMode && !addingMode && !scoringMode && !undoingMode)
		return;
	if (undoingMode) 
	{
		if (addingMode)	//See the last move
		{
			CleanBoard();
			SetLoadedPosition();
			undoingMode = false;
			addingMode = false;
		}
		else
			return;	
	}
	if (!reviewingMode)
		addingMode = false;
	if (BoardForm == FrmGetActiveFormID() && !gWaitDraw && 	this == gBoardPool.GetCurrentBoard())
	{
		//Only redraw screen when it's in board screen and the move is for current board
		if (!reviewingMode && !scoringMode)
		{
			if (displayedMoveCount == -1 || displayedMoveCount < record.GetMoveCount()-2)
				ShowBoard();
			else
				UpdateBoard();
			//displayedMoveCount = record.GetMoveCount()-1;
		}
		else	//Review mode or scoring
			UpdateBoard();
		 
	}
	if (gSupportSound && gPlaySound && moveAudioP && !reviewingMode && !scoringMode)
		SndPlayResource(moveAudioP,sndGameVolume,sndFlagAsync);
	
}
/************************************************
 * Function:	CBoard::ChangeKomi()
 * Purpose:		Change Komi
 */
void CBoard::ChangeKomi(Int16 komi)
{
	if (currentGameId != gameInfo.id)	//The new komi is not about what we are observing
		return;
	TSgfInfo sgfInfo;
	sgfInfo.Init();
	sgfInfo.KM = komi;
	UpdateSgfInfo(sgfInfo);
	sgfInfo.Release();
		
}
/************************************************
 * Function:	CBoard::EndOfGame()
 * Purpose:		Update game info and SGF info on the end of a game.
 * 				Also notify that game is over.
 */
void CBoard::EndOfGame(UInt16 gameId, Char* result)
{
	TSgfInfo sgf;
	UInt8 status = GAMEOVER;

	if (gameId !=0 && gameId != gameInfo.id)	//The new move is not what we are observing
		return;
	if (gameInfo.status == GAMEOVER) //Game is over, duplicated notification.
		return;
	UpdateGameInfo(0,NULL,&status,NULL,NULL);
	sgf.Init();
	sgf.id = gameInfo.id;
	if(result)
	{
		if((sgf.RE = (Char*)MemPtrNew(StrLen(result)+1)))
			StrCopy(sgf.RE,result);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	UpdateSgfInfo(sgf);
	NotifyGameOver(gameInfo.id,result);
	sgf.Release();
	UpdateButtons();
	scoringMode = false;	//Restore the dead group 
}
/************************************************
 * Function:	CBoard::EndOfMatch()
 * Purpose:		Update game info and SGF info on the end of current match (playing) game.
 * 				Also notify that game is over.
 */
void CBoard::EndOfMatch(Char* player, Char* result)
{
	Char	sgfResult[20];
	TSgfInfo sgf;
	UInt8 status = GAMEOVER;
	StrCopy(sgfResult,"");	//clear sgfResult
	if (gameInfo.action != PLAY && gameInfo.action != TEACH )	//It is not the game that we are playing
		return;
	if (gameInfo.status == GAMEOVER) //Game is over, duplicated notification.
		return;
	UpdateGameInfo(0,NULL,&status,NULL,NULL);
	sgf.Init();
	sgf.id = gameInfo.id;
	if(result)
	{
		if (!StrCompare(player,gMyAccountName))	//I lost
			sprintf(sgfResult,"%c+%s",gameInfo.color==BLACK?'W':'B',result);
		else	//I won
			sprintf(sgfResult,"%c+%s",gameInfo.color==BLACK?'B':'W',result);
		if((sgf.RE = (Char*)MemPtrNew(StrLen(sgfResult)+1)))
		{
			StrCopy(sgf.RE,sgfResult);
		}
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	UpdateSgfInfo(sgf);
	NotifyGameOver(gameInfo.id,sgfResult);
	sgf.Release();
	UpdateButtons();
	scoringMode = false;	//Restore the dead group 
}
/************************************************
 * Function:	CBoard::CheckScore()
 * Purpose:		Change game status to SCORING, and mark territory.
 * 				Also show instruction of removing dead stones.
 */
void CBoard::CheckScore()
{
	gameInfo.status = SCORING;
	FrmAlert(CheckScoreAlert);
	/*
{
   Int16 i; Int16 j;
   Int16 more;
   Int16 bs,ws;
   for (i=0;i<size;++i)
      for (j=0;j<size;++j)
      {
         //Int16 x = game.p[i][j] &~(BLACKTERRITORY|WHITETERRITORY);
         Int16 x = p[i][j] &~(MARKMASK); // keep color and flags
         switch (x)
         {
            case BLACK:
               x |= BLACKTERRITORY;
               break;
            case WHITE:
               x |= WHITETERRITORY;
               break;
         }
         p[i][j] = x;	
      }
   do
   {
      more = 0;
      for (i=0;i<size;++i)
         for (j=0;j<size;++j)
         {
            if (p[i][j]&BLACKTERRITORY)
            {
#define CHECKBT(i,j)                   \
               if ((p[i][j]&BLACKTERRITORY)==0 && p[i][j]!=(WHITE|WHITETERRITORY))   \
                  p[i][j] |= BLACKTERRITORY, more = 1;

               if (i>0) CHECKBT(i-1,j);
               if (j>0) CHECKBT(i,j-1);
               if (i+1<size) CHECKBT(i+1,j);
               if (j+1<size) CHECKBT(i,j+1);
            }
            if (p[i][j]&WHITETERRITORY)
            {
#define CHECKWT(i,j)                   \
               if ((p[i][j]&WHITETERRITORY)==0 && p[i][j]!=(BLACK|BLACKTERRITORY))   \
                  p[i][j] |= WHITETERRITORY, more = 1;

               if (i>0) CHECKWT(i-1,j);
               if (j>0) CHECKWT(i,j-1);
               if (i+1<size) CHECKWT(i+1,j);
               if (j+1<size) CHECKWT(i,j+1);
            }
         }
   } while (more);
   bs = ws = 0;
   for (i=0;i<size;++i)
      for (j=0;j<size;++j)
         switch (p[i][j])
         {
            case BLACKTERRITORY:
               ++bs;
               break;
            case WHITETERRITORY:
               ++ws;
               break;
            case BLACKTERRITORY|WHITE|FLAG:
               bs+=2;
               break;
            case WHITETERRITORY|BLACK|FLAG:
               ws+=2;
               break;
         }
   score[BLACK] = bs - prisoners[BLACK];
   score[WHITE] = ws - prisoners[WHITE];
   scoringMode = true;
}
*/
	if (BoardForm == FrmGetActiveFormID())
	{
		UpdateBoard();
		UpdateButtons();
	}
}
/************************************************
 * Function:	CBoard::TimeRunOut(Char *)
 * Purpose:		Update game info and SGF info on the end of a game.
 * 				Also notify that game is over.
 */
/*void CBoard::TimeRunOut(Char* player)
{
	TSgfInfo sgf;
	UInt8 status = GAMEOVER;
	Char result[10]="Time Out";
	
	//indentify the color for this player
	
	if (!StrCompare(player,sgfInfo.PW))
		StrCopy(result,"B+Time");
	else
		StrCopy(result,"W+Time");
	UpdateGameInfo(0,NULL,&status,NULL,NULL);
	sgf.Init();
	sgf.id = gameInfo.id;
	if(result)
	{
		if((sgf.RE = (Char*)MemPtrNew(StrLen(result)+1)))
			StrCopy(sgf.RE,result);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	UpdateSgfInfo(sgf);
	sgf.Release();
	NotifyGameOver(gameInfo.id,result);
}
*/
/************************************************
 * Function:	CBoard::InitGameInfo()
 * Purpose:		Initialize game info structure
 */
void CBoard::InitGameInfo(CGameItem *gameP)
{
	TSgfInfo	sgf;
	UInt8 status = INPROGRESS;
	Int16 byoyomi;
	Int16	myColor = EMPTY;
	DateType	today;
	Char	cmd[20],tmpString[40];
	UInt8	action = PLAY;
	
	if (gameInfo.status == PREPARING && gameInfo.id == gameP->gameID)
	{
		if (gameInfo.action == IDLE || gameInfo.action == PLAY || gameInfo.action == TEACH)
			gBoardPool.matchingGameID = gameInfo.id;
		if (gameInfo.action == IDLE && gameP->flag)	//A restarted/reloaded game
		{
			//Check if it's a teaching game
			if (*(gameP->flag) == 'T')
				action = TEACH;
		}
		//Clear();
		//Init();
		sgf.Init();
		//Set SGF infomation for the game
		sgf.id = gameP->gameID; 
		byoyomi = gameP->byoyomi;
		if(gameP->whitePlayer)
		{
			if ((sgf.PW = (Char*)MemPtrNew(StrLen(gameP->whitePlayer)+1)))
				StrCopy(sgf.PW,gameP->whitePlayer);
			else
			{
				// Memory Allocation Failed.
				return;
			}
		//Indentify my color
			if (!StrCompare(gMyAccountName,gameP->whitePlayer))
			{
				myColor += WHITE;
				if(gameP->blackPlayer)
					UpdateChatBuddyList(gameP->blackPlayer);	//Set opponent as my current chat buddy
			}
		}
		if(gameP->whiteRank)
		{
			if ((sgf.WR = (Char*)MemPtrNew(StrLen(gameP->whiteRank)+1)))
				StrCopy(sgf.WR,gameP->whiteRank);
			else
			{
				// Memory Allocation Failed.
				return;
			}
		}
		if(gameP->blackPlayer)
		{
			if ((sgf.PB = (Char*)MemPtrNew(StrLen(gameP->blackPlayer)+1)))
				StrCopy(sgf.PB,gameP->blackPlayer);
			else
			{
				// Memory Allocation Failed.
				return;
			}
		//Indentify my color
			if (!StrCompare(gMyAccountName,gameP->blackPlayer))
			{
				myColor += BLACK; 
				if(gameP->whitePlayer)
					UpdateChatBuddyList(gameP->whitePlayer);	//Set opponent as my current chat buddy
			}
		}
		if(gameP->blackRank)
		{
			if ((sgf.BR = (Char*)MemPtrNew(StrLen(gameP->blackRank)+1)))
				StrCopy(sgf.BR,gameP->blackRank);
			else
			{
				// Memory Allocation Failed.
				return;
			}
		}
		sgf.HA = gameP->handicap;
		sgf.SZ = gameP->size;
		sgf.KM = gameP->komi;
		//Set TM
		if(byoyomi>0)
		{
			sprintf(tmpString,"%d",byoyomi*60);
			if ((sgf.TM = (Char*)MemPtrNew(StrLen(tmpString)+1)))
				StrCopy(sgf.TM,tmpString);
			else
			{
				// Memory Allocation Failed.
				return;
			}
		}
		//Set DT
		DateSecondsToDate(TimGetSeconds(),&today);
		if (today.month>=10 && today.day >=10)
			sprintf(tmpString,"%d-%d-%d",today.year+1904,today.month,today.day);
		else if (today.month>=10 && today.day <10)
			sprintf(tmpString,"%d-%d-0%d",today.year+1904,today.month,today.day);
		else if (today.month<10 && today.day >=10)
			sprintf(tmpString,"%d-0%d-%d",today.year+1904,today.month,today.day);
		else if (today.month<10 && today.day <10)
			sprintf(tmpString,"%d-0%d-0%d",today.year+1904,today.month,today.day);
		//DateToAscii(today.month,today.day,today.year+1904,dfYMDLongWithDot,tmpString);
		if ((sgf.DT = (Char*)MemPtrNew(StrLen(tmpString)+1)))
			StrCopy(sgf.DT,tmpString);
		else
		{
			// Memory Allocation Failed.
			return;
		}
		//Set PC
		sprintf(tmpString,"%s:%s",gAccountList[gAccountIdInUsed].serverName,gAccountList[gAccountIdInUsed].serverPort);
		if ((sgf.PC = (Char*)MemPtrNew(StrLen(tmpString)+1)))
			StrCopy(sgf.PC,tmpString);
		else
		{
			// Memory Allocation Failed.
			return;
		}
		//Set ID
		sprintf(tmpString,"%d",sgf.id);
		if ((sgf.ID = (Char*)MemPtrNew(StrLen(tmpString)+1)))
			StrCopy(sgf.ID,tmpString);
		else
		{
			// Memory Allocation Failed.
			return;
		}
		
		UpdateSgfInfo(sgf);
		if (gameInfo.action == IDLE)
			UpdateGameInfo(0,&action,&status,&byoyomi,&myColor);
		else
			UpdateGameInfo(0,NULL,&status,&byoyomi,&myColor);
		sgf.Release();
		sprintf(cmd,"move %d",gameInfo.id);
		WriteServer(cmd);	//get move info
	}
}
/************************************************
 * Function:	CBoard::UpdateGameInfo()
 * Purpose:		Update game info structure
 */
void CBoard::UpdateGameInfo(UInt16 gameId, UInt8 *gameActionP, UInt8 *gameStatusP, Int16 *gameByoyomiP, Int16 *gameColorP)
{
	if (gameId != 0)
		gameInfo.id = gameId;
	if (gameActionP)
		gameInfo.action = *gameActionP;
	if (gameByoyomiP)
		gameInfo.byoyomi = *gameByoyomiP;
	if (gameColorP)
		gameInfo.color = *gameColorP;
	if (gameStatusP)
	{
		gameInfo.status = *gameStatusP;
		UpdateButtons();	//Update board buttons based on new game status
	}	
}
/************************************************
 * Function:	CBoard::UpdateByoyomiStones()
 * Purpose:		Update Byoyomi Stones infomation
 */
void CBoard::UpdateByoyomiStones(UInt16 gameId, Int16 byoyomiStones)
{
	if (gameInfo.id == gameId && stones != byoyomiStones \
	&& byoyomiStones < 25 && byoyomiStones > 0)
	{
		stones = byoyomiStones;
	}
		
}
/************************************************
 * Function:	CBoard::UpdateSgfInfo()
 * Purpose:		Update SGF info structure
 */
void CBoard::UpdateSgfInfo(TSgfInfo info)
{
	if (info.PW)
	{
		sgfInfo.PW = (Char*)MemPtrNew(StrLen(info.PW)+1);
		if (sgfInfo.PW)
			StrCopy(sgfInfo.PW,info.PW);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.WR)
	{
		sgfInfo.WR = (Char*)MemPtrNew(StrLen(info.WR)+1);
		if (sgfInfo.WR)
			StrCopy(sgfInfo.WR,info.WR);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.PB)
	{
		sgfInfo.PB = (Char*)MemPtrNew(StrLen(info.PB)+1);
		if (sgfInfo.PB)
			StrCopy(sgfInfo.PB,info.PB);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.BR)
	{
		sgfInfo.BR = (Char*)MemPtrNew(StrLen(info.BR)+1);
		if (sgfInfo.BR)
			StrCopy(sgfInfo.BR,info.BR);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.RE)
	{
		sgfInfo.RE = (Char*)MemPtrNew(StrLen(info.RE)+1);
		if (sgfInfo.RE)
			StrCopy(sgfInfo.RE,info.RE);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.DT)
	{
		sgfInfo.DT = (Char*)MemPtrNew(StrLen(info.DT)+1);
		if (sgfInfo.DT)
			StrCopy(sgfInfo.DT,info.DT);
		else
		{
			// Memory Allocation Failed.
			return;
		} 
	}
	if (info.TM)
	{
		sgfInfo.TM = (Char*)MemPtrNew(StrLen(info.TM)+1);
		if (sgfInfo.TM)
			StrCopy(sgfInfo.TM,info.TM);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.PC)
	{
		sgfInfo.PC = (Char*)MemPtrNew(StrLen(info.PC)+1);
		if (sgfInfo.PC)
			StrCopy(sgfInfo.PC,info.PC);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.ID)
	{
		sgfInfo.ID = (Char*)MemPtrNew(StrLen(info.ID)+1);
		if (sgfInfo.ID)
			StrCopy(sgfInfo.ID,info.ID);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.SZ!=19)	//Not default size 19 by 19
		sgfInfo.SZ = info.SZ;	
	if (info.KM!=65)	//Not default komi
		sgfInfo.KM = info.KM;
	if (info.HA!=0)
		sgfInfo.HA = info.HA;	
}
void CBoard::GetSgfInfo(TSgfInfo &info)
{
	info = sgfInfo;
}
void CBoard::GetGameInfo(TGameInfo &game)
{
	game = gameInfo;
}
void CBoard::GetTimerInfo(TTimerInfo &timer)
{
	timer = timerInfo;
}
/************************************************
 * Function:	CBoard::NewTimeerInfo()
 * Purpose:		Update timer info structure
 */
void CBoard::NewTimerInfo(TTimerInfo info)
{
	if (gameInfo.status == PREPARING)
	{
		Char	cmd[20],idString[10];
		StrIToA(idString,info.gameID);
		StrCopy(cmd,"game ");
		StrCat(cmd,idString);
		WriteServer(cmd);	//Get current game's infomation
		gameInfo.id = info.gameID;
		//Clear unobservedGameID because we have gotten the new game's ID.
		//So we don't have to care about the unobserved one
		gBoardPool.unobservedGameID = 0;
		return;
	}
	//currentGameId = info.gameID;
	//if (currentGameId != gameInfo.id)	//New timer info is not about the one we are observing
	if (info.gameID != gameInfo.id)	//New timer info is not about the one we are observing
		return;
	timerInfo.gameID = info.gameID;
	addingMode = true;	//Allow to update board for observing a teaching game
	if (info.whitePlayer && !timerInfo.whitePlayer)
	{
		timerInfo.whitePlayer = (Char*)MemPtrNew(StrLen(info.whitePlayer)+1);
		if (timerInfo.whitePlayer)
		 	StrCopy(timerInfo.whitePlayer,info.whitePlayer);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	if (info.blackPlayer && !timerInfo.blackPlayer)
	{
		timerInfo.blackPlayer = (Char*)MemPtrNew(StrLen(info.blackPlayer)+1);
		if (timerInfo.blackPlayer)
		 	StrCopy(timerInfo.blackPlayer,info.blackPlayer);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
	//calculate time elapse
	//timerInfo.whiteTimeElapse += timerInfo.whiteTimeLeft - info.whiteTimeLeft;
	//timerInfo.blackTimeElapse += timerInfo.blackTimeLeft - info.blackTimeLeft;

	timerInfo.whiteCapture = info.whiteCapture;
	timerInfo.whiteTimeLeft = info.whiteTimeLeft;
	timerInfo.whiteStoneLeft = info.whiteStoneLeft;
	timerInfo.blackCapture = info.blackCapture;
	timerInfo.blackTimeLeft = info.blackTimeLeft;
	timerInfo.blackStoneLeft = info.blackStoneLeft;
}
/************************************************
 * Function:	CBoard::CleanScreen()
 * Purpose:		Clean the screen area of board
 */
void CBoard::CleanScreen()
{
   	static RectangleType r={{0,0},{151,151}};
   	WinEraseRectangle(&r,0);
}

/************************************************
 * Function:	CBoard::DrawMaker()
 * Purpose:		Draw a marker on a specified position
 */
void CBoard::DrawMarker(Int16 x,Int16 y, UInt8 color)
{
   	RectangleType r;
   	Int16 marker_offset = offset - 1;
   	r.topLeft.x = x*width + marker_offset;
   	r.topLeft.y = y*width + marker_offset;
   	r.extent.x  = r.extent.y = 3;
   	if (color==BLACK)
      	WinDrawRectangle(&r,0);
   	else
      	WinEraseRectangle(&r,0);
}
/************************************************
 * Function:	CBoard::DrawMakers()
 * Purpose:		Draw markers on board for better presentation
 */
void CBoard::DrawMarkers(void)
{
   	Int16 start = size<13?2:3;
   	Int16 end = size-1-start;
   	Int16 step = end-start<1?1:end-start; 
   	Int16 i, j;

   	if (size == 17 || size == 19)
      	step >>= 1; 

   	//draw markers for better presentation
   	for (i=start;i<=end;i+=step)
   	{
      	for (j=start;j<=end;j+=step)
      	{
         	if (i>=viewx && i<(viewx+vsize) &&
            	j>=viewy && j<(viewy+vsize) &&
               	p[i][j] == EMPTY)
            	DrawMarker(i-viewx,j-viewy,BLACK);
      	}
   	}

   	if (size&1 && size>9)
   	{
      	i = j = size >> 1;
      	if (i>=viewx && i<(viewx+vsize) &&
            j>=viewy && j<(viewy+vsize) &&
            p[i][j] == EMPTY)
         	DrawMarker(i-viewx,j-viewy,BLACK);
   	}
}
/************************************************
 * Function:	CBoard::DrawOnBoard()
 * Purpose:		Draw actual stone on the board
 */
void CBoard::DrawOnBoard(Int16 x, Int16 y,
      UInt8 color,
      Int16 Size, Int16 Diam, Int16 fill)
{
   Int16 x1,x2,y1,y2;
   RectangleType r;
   Int16 off_minus_size = offset - Size ;

   r.topLeft.x = x*width + off_minus_size;
   r.topLeft.y = y*width + off_minus_size;
   r.extent.x  = r.extent.y  = 2 * Size + 1;
	
   if (gSupportColor) WinSetBackColor(gBoardColor);
   WinEraseRectangle(&r,0);
   WinSetPattern(&CustomPtn);

	//[Jin] 
	WinPushDrawState();
	if (gHiRes) // Scale coordinates
	{
	   	WinSetCoordinateSystem(kCoordinatesDouble);   	
		WinScaleRectangle(&r);
		Diam <<=1;
	}
   switch (color)
   {
      case BLACK:
      	{
         		WinDrawBitmap(blackStoneP,r.topLeft.x,r.topLeft.y);
/*         	if (gHiRes && (vsize > ZoomSize))
         		WinDrawBitmap(blackStoneP,r.topLeft.x,r.topLeft.y);
         	else 
         	{
         		if (fill)
               		WinFillRectangle(&r,Diam);
            	else WinDrawRectangle(&r,Diam);
         	}
*/            break;
      	}
      case WHITE:
      	{
         		WinDrawBitmap(whiteStoneP,r.topLeft.x,r.topLeft.y);
/*         	if (gHiRes && (vsize > ZoomSize))
         		WinDrawBitmap(whiteStoneP,r.topLeft.x,r.topLeft.y);
         	else 
         	{
				if (fill)
            		WinFillRectangle(&r,Diam);
         		else 
         			WinDrawRectangle(&r,Diam);
			        r.topLeft.x ++;
			        r.topLeft.y ++;
			        r.extent.x -= 2;
			        r.extent.y -= 2;
			        if (gSupportColor) 
			        	WinSetBackColor (whiteColor);	//[Jin]
			        WinEraseRectangle(&r,Diam-1);
			        if (gSupportColor) WinSetBackColor(gBoardColor);
         	}
*/         break;
      	}
      case EMPTY:
         {
            Int16 size_plus_one = Size + 1;

            if (x == 0 && viewx == 0) x1 = offset; 
            else x1 = x*width + offset - size_plus_one;
            if (x == (vsize-1) && (vsize+viewx) == size)
               x2 = (vsize-1)*width+offset;
            else x2 = x*width + offset + size_plus_one;
            y1 = y*width + offset;
            if (gHiRes)
            	WinDrawLine(x1*2,y1*2,x2*2,y1*2);
            else
            	//WinFillLine(x1,y1,x2,y1);
            	WinDrawLine(x1,y1,x2,y1);

            if (x1 != offset) x1 += size_plus_one;
            if (y == 0 && viewy == 0) y1 = offset; 
            else y1 -= size_plus_one;
            if (y == (vsize-1) && (vsize+viewy) == size)
               y2 = (vsize-1)*width+offset;
            else y2 = y*width + offset + size_plus_one;
            if (gHiRes)
	            WinDrawLine(x1*2,y1*2,x1*2,y2*2);
            else
            	//WinFillLine(x1,y1,x1,y2);
            	WinDrawLine(x1,y1,x1,y2);
            break;
         }
   }
	WinPopDrawState();
   if (gSupportColor) 
      	  WinSetBackColor (whiteColor);	//[Jin]
   
}

/************************************************
 * Function:	CBoard::DrawPiece()
 * Purpose:		Draw pieces such as stones, territory
 */
void CBoard::DrawPiece(Int16 x, Int16 y, UInt8 color)
{ 
   Int16 Diam = (width >> 1) - 1;
   Int16 color_land_piecemask = color&PIECEMASK;

   //FlashCount = MAXFLASH; // when a stone is drawn, reinit flashes

   if (!scoringMode)       // if no score flags
   {
      DrawOnBoard(x,y,color_land_piecemask,Diam,Diam,0); // draw piece
	  //[Jin]Highlight or number current move
	  if (x == lastx && y == lasty && recentMoveCount > 0 )//&& inBound(x,y))
	  	{
	  		;//DrawNumber(x,y,color,game.movecount-baseMoveCount);
	  	}
	  else if (x == lastx && y == lasty ) //Highlight current move by mark
	  		{
	  			DrawMarker(x,y,color_land_piecemask==BLACK?WHITE:BLACK);
				if ((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && (MYTURN))
				//it's my turn to move
				{
					SHOWCURSOR
				}	
				else
				{
					RESETCURSOR  
				}
	  		}
   }
   else
   {
      DrawOnBoard(x,y,color_land_piecemask,Diam,Diam,color&FLAG); // draw piece
      switch (color&(BLACKTERRITORY|WHITETERRITORY))
      {
         case BLACKTERRITORY:
            DrawMarker(x,y,BLACK);
            break;
         case WHITETERRITORY:
            DrawMarker(x,y,WHITE);
            break;
      }
   }
}
void CBoard::SwitchLabel()
{					
	showMovePosition = !showMovePosition;
	DrawStatus();
}
void CBoard::UpdateButtons()
{
	FormPtr     frm = FrmGetActiveForm();
	RectangleType	recDefaultButton;
	Char		stringBoardNumber[4];
	//RctSetRectangle(&recButton,152,27,7,11);
	if (BoardForm != FrmGetActiveFormID() || this != gBoardPool.GetCurrentBoard())
		return; 
	switch (gameInfo.status)
	{
		case INPROGRESS:
			if (gameInfo.action == PLAY || gameInfo.action == TEACH)
			{
				HideObject(frm,BoardFormBeginButton);
				HideObject(frm,BoardFormFBackwardButton);		
				HideObject(frm,BoardFormBackwardButton);		
				HideObject(frm,BoardFormForwardButton);		
				HideObject(frm,BoardFormFForwardButton);		
				HideObject(frm,BoardFormEndButton);		
				HideObject(frm,BoardFormUndoButton);		
				HideObject(frm,BoardFormDoneButton);
				HideObject(frm,BoardFormMarkButton);
				HideObject(frm,BoardFormUnobserveButton);	
				ShowObject(frm,BoardFormRefreshButton); 
				ShowObject(frm,BoardFormResignButton);		
				ShowObject(frm,BoardFormAdjournButton);		
				ShowObject(frm,BoardFormPassButton);	
				if (gameInfo.action == TEACH)
					ShowObject(frm,BoardFormUndoButton);
				if (gameInfo.action == TEACH && gAccountList[gAccountIdInUsed].serverType == IGS)
					ShowObject(frm,BoardFormMarkButton);	//IGS-specific command
			}
			else if (gameInfo.action == OBSERVE)	//Observe a game
			{
				HideObject(frm,BoardFormResignButton);		
				HideObject(frm,BoardFormAdjournButton);		
				HideObject(frm,BoardFormPassButton);		
				HideObject(frm,BoardFormUndoButton);		
				HideObject(frm,BoardFormDoneButton); 
				HideObject(frm,BoardFormMarkButton);
				ShowObject(frm,BoardFormBeginButton);		
				ShowObject(frm,BoardFormFBackwardButton);		
				ShowObject(frm,BoardFormBackwardButton);		
				ShowObject(frm,BoardFormForwardButton);		
				ShowObject(frm,BoardFormFForwardButton);		
				ShowObject(frm,BoardFormEndButton);						
				ShowObject(frm,BoardFormRefreshButton);
				ShowObject(frm,BoardFormUnobserveButton);	
			}
			break;
		case SCORING:
			HideObject(frm,BoardFormBeginButton);
			HideObject(frm,BoardFormFBackwardButton);		
			HideObject(frm,BoardFormBackwardButton);		
			HideObject(frm,BoardFormForwardButton);		
			HideObject(frm,BoardFormFForwardButton);		
			HideObject(frm,BoardFormEndButton);		
			HideObject(frm,BoardFormMarkButton);
			HideObject(frm,BoardFormUnobserveButton);	
			HideObject(frm,BoardFormRefreshButton); 
			HideObject(frm,BoardFormResignButton);		
			HideObject(frm,BoardFormAdjournButton);		
			HideObject(frm,BoardFormPassButton);		
			ShowObject(frm,BoardFormUndoButton);		
			ShowObject(frm,BoardFormDoneButton);		
			break; 
		case GAMEOVER:
		case INTERRUPTED:
		case LOADED:
			HideObject(frm,BoardFormRefreshButton);
			HideObject(frm,BoardFormResignButton);		
			HideObject(frm,BoardFormAdjournButton);		
			HideObject(frm,BoardFormPassButton);		
			HideObject(frm,BoardFormUndoButton);		
			HideObject(frm,BoardFormDoneButton); 
			HideObject(frm,BoardFormMarkButton);
			ShowObject(frm,BoardFormBeginButton);		
			ShowObject(frm,BoardFormFBackwardButton);		
			ShowObject(frm,BoardFormBackwardButton);		
			ShowObject(frm,BoardFormForwardButton);		
			ShowObject(frm,BoardFormFForwardButton);		
			ShowObject(frm,BoardFormEndButton);		
			ShowObject(frm,BoardFormUnobserveButton);		 	
			break; 
		default:	//Preparing  
			HideObject(frm,BoardFormBeginButton);
			HideObject(frm,BoardFormFBackwardButton);		
			HideObject(frm,BoardFormBackwardButton);		
			HideObject(frm,BoardFormForwardButton);		
			HideObject(frm,BoardFormFForwardButton);		
			HideObject(frm,BoardFormEndButton);		
			HideObject(frm,BoardFormRefreshButton);
			HideObject(frm,BoardFormResignButton);		
			HideObject(frm,BoardFormAdjournButton);		
			HideObject(frm,BoardFormPassButton);		
			HideObject(frm,BoardFormUndoButton);		
			HideObject(frm,BoardFormDoneButton);
			HideObject(frm,BoardFormMarkButton);
			#ifndef _BE_
			ShowObject(frm,BoardFormUnobserveButton);		 	
			#endif
			break;
	} 
	UInt16 focusIndex = FrmGetFocus(frm);
	if (focusIndex!=noFocus)
	{
		FrmGetObjectBounds(frm,focusIndex,&recDefaultButton);
		FrmGlueNavDrawFocusRing(frm,FrmGetObjectId(frm,focusIndex),frmNavFocusRingNoExtraInfo,&recDefaultButton,frmNavFocusRingStyleObjectTypeDefault,true);
	}
	#ifdef _PE_
	ShowObject(frm,BoardFormBoardSelectButton);		 	
	CtlSetLabel((ControlType*)GetObjectPtr(BoardFormBoardSelectButton),StrIToA(stringBoardNumber,gBoardPool.GetBoardIndex(gBoardPool.GetCurrentBoard())));
	CtlDrawControl((ControlType*)GetObjectPtr(BoardFormBoardSelectButton));
	#endif
}
/************************************************ 
 * Function:	CBoard::DrawMoveLable()
 * Purpose:		Draw move lable
 */
void CBoard::DrawMoveLabel()
{
	static Char movedesc[10];
   	TMove move;
   	Char c[10];
   	Int16 x,y;
   	if (displayedMoveCount<0)
   		sprintf(movedesc," ");
   	else
   	{
	   	record.GetMove(displayedMoveCount,move);
	   	x = move.position.x;
	   	y = move.position.y;
	   	if (x <0 || y<0 )
	   	{
	   		if (displayedMoveCount == 0 && sgfInfo.HA != 0)
	   			sprintf(c,"%dHA",sgfInfo.HA);
	   		else
	   			sprintf(c,"Pass");
	   	}
	   	else
	   	{
	   		x = x>='I'-'A'?x+1:x;
	   		y = size - y;
	   		sprintf(c,"%c%d",'A'+x,y);
	   	}
	   	if (showMovePosition)
			sprintf(movedesc,"%s",c);
		else
			sprintf(movedesc,"%d",displayedMoveCount+1);
   	}
	CtlSetLabel((ControlType*)GetObjectPtr(BoardFormMoveLabel),(Char*)movedesc);
	CtlDrawControl((ControlType*)GetObjectPtr(BoardFormMoveLabel));
}

/************************************************ 
 * Function:	CBoard::DrawStatus()
 * Purpose:		Draw game status, such as time, captures
 */
void CBoard::DrawStatus()
{
/*	static Char movedesc[10];
   	TMove move;
   	Char c[10];
   	Int16 x,y;
   	if (displayedMoveCount<0)
   		sprintf(movedesc," ");
   	else
   	{
	   	record.GetMove(displayedMoveCount,move);
	   	x = move.position.x;
	   	y = move.position.y;
	   	if (x <0 || y<0 )
	   	{
	   		if (displayedMoveCount == 0 && sgfInfo.HA != 0)
	   			sprintf(c,"%dHA",sgfInfo.HA);
	   		else
	   			sprintf(c,"Pass");
	   	}
	   	else
	   	{
	   		x = x>='I'-'A'?x+1:x;
	   		y = size - y;
	   		sprintf(c,"%c%d",'A'+x,y);
	   	}
	   	if (showMovePosition)
			sprintf(movedesc,"%s",c);
		else
			sprintf(movedesc,"%d",displayedMoveCount+1);
   	}
	CtlSetLabel((ControlType*)GetObjectPtr(BoardFormMoveLabel),(Char*)movedesc);
	CtlDrawControl((ControlType*)GetObjectPtr(BoardFormMoveLabel));
*/
	DrawMoveLabel();
   	DrawTimer();
	
}

/************************************************
 * Function:	CBoard::ShowBoard()
 * Purpose:		Show board with stones placed
 */
void CBoard::ShowBoard()
{
	
   	Int16 i,j;
   	Int16 x1,x2,y1,y2;
   	Int16 width_halved;

	WinSetPattern(&CustomPtn);
   	width = vsize ? 2*(76/vsize) : 42;
   	width_halved = width >> 1;
	
   	/* recompute due to integer arithmetics */
   	offset = (151 - width*vsize + width) >> 1;
   	y1 = offset;
   	if (viewy)
      	y1 -= width_halved; 
   	y2 = offset+(vsize-1)*width;
   	if (viewy+vsize-size)
      	y2 += width_halved;

   	if (gSupportColor) 
   		WinSetBackColor(gBoardColor);
   	CleanScreen();
	WinPushDrawState();
	if (gHiRes)
   		WinSetCoordinateSystem(kCoordinatesDouble);

   	x1 = offset;
   	for (i = 0; i < vsize; i++)
   	{
   	//Jin
   		if (gHiRes)
	      WinDrawLine(x1*2,y1*2,x1*2,y2*2);
	    else
	      //WinFillLine(x1,y1,x1,y2);
	      WinDrawLine(x1,y1,x1,y2);
	    x1 += width;
   	}

   	x1 = offset;
   	if (viewx) 
      	x1 -= width_halved;


   	x2 = offset+(vsize-1)*width;
   	if (viewx+vsize-size)
      	x2 += width_halved;

   	y1 = offset;
   	for (i = 0; i < vsize; i++)
   	{
   	//Jin
      	if (gHiRes)
      		WinDrawLine(x1*2,y1*2,x2*2,y1*2);
      	else
      		//WinFillLine(x1,y1,x2,y1);
      		WinDrawLine(x1,y1,x2,y1);
      	y1 += width;
   	}
	WinPopDrawState();	//[Jin]Restore standard coordinates
   	DrawMarkers();
   	if (gSupportColor) 
   	  	WinSetBackColor (whiteColor);	
   	for (i=viewx;i<(viewx+vsize); i++)
   	{
      	for(j=viewy;j<(vsize); j++)
      {
         if (p[i][j] != EMPTY)
            DrawPiece(i-viewx,j-viewy,p[i][j]);
         screen[i][j] = p[i][j];
      }
   	}
   	DrawStatus();
	UpdateButtons(); 
}
/************************************************
 * Function:	CBoard::UpdateBoard()
 * Purpose:		Update board with stone placed or removed
 */
void CBoard::UpdateBoard()
{
   	Int16 i,j;
	TMove previouseMove,currentMove;
    Int16 i_bound, j_bound;
    i_bound = viewx+vsize;
    j_bound = viewy+vsize;

	//record.GetMove(displayedMoveCount-1,previouseMove);
	record.GetMove(previouseDisplayedMoveCount,previouseMove);
    for (i=viewx;i<i_bound; i++)
    	for(j=viewy;j<j_bound; j++)
            if (p[i][j] != screen[i][j])
            {
               DrawPiece(i-viewx,j-viewy,p[i][j]);
               screen[i][j] = p[i][j];
            }
       		else
       		{
       			//if (displayedMoveCount>=0 && displayedMoveCount < MAX_MOVECOUNT)	//[Jin] Restore previoue highlighted stone 
       			//[Jin] Restore previoue highlighted stone 
       			if (previouseDisplayedMoveCount >= 0)	
            	{
            		
            		if (i==previouseMove.position.x && j==previouseMove.position.y && recentMoveCount <=0)
               		DrawPiece(i-viewx,j-viewy,p[i][j]);
            	}
       		}
    //Always draw the current move   		
	record.GetMove(displayedMoveCount,currentMove);
	i = currentMove.position.x;
	j = currentMove.position.y;
	DrawPiece(i-viewx,j-viewy,p[i][j]);

    DrawMarkers();
    DrawStatus();
	//displayedMoveCount = record.GetMoveCount()-1;
	if ((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && (MYTURN))
	//it's my turn to move
	{
		SHOWCURSOR
	}	
	else
	{
		RESETCURSOR  
	}
}

/************************************************
 * Function:	CBoard::CleanBoard()
 * Purpose:		Reset board to empty
 */
void CBoard::CleanBoard()
{
	Int16 i,j;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{
			p[i][j] = EMPTY;
		}
	}
	prisoners[WHITE] = prisoners[BLACK] = 0;
	displayedMoveCount = -1;
}
/************************************************
 * Function:	CBoard::SetLoadedPosition()
 * Purpose:		Update stones position based on loaded moves
 */
void CBoard::SetLoadedPosition()
{
	TMove currentMove;
	Int16	i,totalMoves = record.GetMoveCount();
	for (i=0;i<totalMoves;i++)
	{
		record.GetMove(i,currentMove);
		UpdatePosition(currentMove);
	}
	displayedMoveCount = totalMoves - 1;
}
/************************************************
 * Function:	CBoard::UpdatePosition()
 * Purpose:		Update stones position based on moves
 */
Boolean CBoard::UpdatePosition(TMove currentMove)
{
	Int16 currentColor,x,y;
	Int16 oppositeColor;
	Int16 captured = false;;
	currentColor = currentMove.color;
	x = currentMove.position.x;
	y = currentMove.position.y;
	lastx = x;
	lasty = y;
	if (x<0 || y<0)	//Pass
	{
   		if (currentMove.id == 0 && sgfInfo.HA != 0) //Handicap
		switch (sgfInfo.HA)
		{
			case 9:
				p[9][9] = BLACK;
			case 8:
				p[9][3] = p[9][15] = BLACK;
				goto case6;
			case 7:
				p[9][9] = BLACK;
case6:
			case 6:
				p[3][9] = p[15][9] = BLACK;
				goto case4;
			case 5:
				p[9][9] = BLACK;
case4:
			case 4:
				p[15][15] = BLACK;
			case 3:
				p[3][3] = BLACK;
			case 2:
				p[3][15] = BLACK;
			case 1:
				p[15][3] = BLACK;
		}
		return false;
	}
	oppositeColor = currentColor==BLACK?WHITE:BLACK;
   	p[x][y] = (p[x][y]&MARKMASK) | currentColor;
   	if (x>0 && (p[x-1][y]&PIECEMASK)==oppositeColor) 
      	captured |= CheckCapture(x-1,y);
   	if (y>0 && (p[x][y-1]&PIECEMASK)==oppositeColor)
      	captured |= CheckCapture(x,y-1);
   	if (x+1<size && (p[x+1][y]&PIECEMASK)==oppositeColor)
      	captured |= CheckCapture(x+1,y);
   	if (y+1<size && (p[x][y+1]&PIECEMASK)==oppositeColor)
      	captured |= CheckCapture(x,y+1);
   	captured |= CheckCapture(x,y);
   	return captured;
}
/************************************************
 * Function:	CBoard::CheckCapture()
 * Purpose:		Return TRUE if capture occurs
 */
Boolean CBoard::CheckCapture(Int16 x, Int16 y)
{
   	Int16 us = (p[x][y]&PIECEMASK); 
   	Int16 dist = 0;                      
   	Int16 i, j, k, l;
   	Int16 more;
   	p[x][y] |= MARK;
   	do
   	{
    	more = 0;
      	k = x+dist;
      	l = y+dist;
      	for (i=(x-dist<0?0:x-dist);i<=k && i<size;++i)
        	for (j=(y-dist<0?0:y-dist);j<=l && j<size;++j)
            	if ((p[i][j]&PIECEMASK) == MARK)
            	{
					#define CHECKCAPTURE(i,j)                   \
        			if ((p[i][j]&PIECEMASK)==EMPTY)     \
            			goto gotlib;               \
            		else if ((p[i][j]&PIECEMASK)==us)   \
            			p[i][j] |= MARK, more = 1;

               		if (i>0) CHECKCAPTURE(i-1,j);
               		if (j>0) CHECKCAPTURE(i,j-1);
               		if (i+1<size) CHECKCAPTURE(i+1,j);
               		if (j+1<size) CHECKCAPTURE(i,j+1);
            	}
      	++dist;
   	} while (more);
   	// no liberties at all, kill the group
   	--dist;
   	for (i=(x-dist<0?0:x-dist);i<=k && i<size;++i)
      	for (j=(y-dist<0?0:y-dist);j<=l && j<size;++j)
         	if ((p[i][j]&PIECEMASK)==MARK)
         	{
            	p[i][j] = EMPTY;
            	++prisoners[us];
         	}
   	return true;
gotlib:
   	++dist;
   	++k;
   	++l;
   	for (i=(x-dist<0?0:x-dist);i<=k && i<size;++i)
      	for (j=(y-dist<0?0:y-dist);j<=l && j<size;++j)
         	if ((p[i][j]&PIECEMASK)==MARK)
            	p[i][j] = us | (p[i][j]&MARKMASK);
   	return false;
}
/************************************************
 * Function:	CBoard::PenMove()
 * Purpose:		Handle pen move event for board
 */
void CBoard::PenMove(EventPtr e)         
{
	if (!((gameInfo.action == PLAY || gameInfo.action==TEACH) && gameInfo.status == INPROGRESS && (MYTURN)))
		return;	//it's not my turn
	Int16 dummy = (width >> 1) - offset;
	Int16 penX = ((e->screenX + dummy) / width);
	Int16 penY = ((e->screenY + dummy) / width);

	if ((penX != virtualX) || (penY !=virtualY))
	{
   		if (
         	virtualX>=0 && virtualX<vsize &&
         	virtualY>=0 && virtualY<vsize)
   		{
      		InvertZone(virtualX,virtualY);
   		}

   		virtualX = penX;
   		virtualY = penY;

       	if (penX>=0 && penX<vsize && penY>=0 && penY<vsize)
          	InvertZone(penX,penY);
    }
}
/************************************************
 * Function:	CBoard::PenUp()
 * Purpose:		Handle pen up event for board
 */
void CBoard::PenUp(EventPtr e)         
{
	Int16 dummy = (width >> 1) - offset;
	Int16 penX = ((e->screenX + dummy) / width);
	Int16 penY = ((e->screenY + dummy) / width); 
	Char newmove[4],c;
	if (!((gameInfo.action==PLAY || gameInfo.action==TEACH) && (gameInfo.status==INPROGRESS || gameInfo.status== SCORING)))
		return;
	if (penX>=0 && penX<vsize && penY>=0 && penY<vsize)	//tap within board
	{
		c = 'A'+ penX;
		if (c>='I')
			c++;
		sprintf(newmove,"%c%d",c,size-penY);
		//if ( gameInfo.color == play && gameInfo.status== INPROGRESS) //it's my turn
		if ( (MYTURN) && gameInfo.status== INPROGRESS) //it's my turn, I could possible play both color for TEACH
		{
			if (prevPenX == penX && prevPenY == penY)
			{
				InvertZone(penX,penY);
				WriteServer(newmove);
				RESETCURSOR
			}
			else
			{
       			if (virtualX<vsize && virtualY<vsize)
					InvertZone(virtualX,virtualY);
				InvertZone(penX,penY);
				virtualX = penX;
    			virtualY = penY;
				prevPenX = penX;
				prevPenY = penY;
			}
		}
		else if (gameInfo.action==PLAY && gameInfo.status== SCORING)
		{
			//FlagGroup(penX,penY);
			WriteServer(newmove);
			//UpdateBoard();
		}
	}
	else	//outside board
	{
		/*
		if (virtualX<vsize && virtualY<vsize)
		InvertZone(virtualX,virtualY);
		RESETCURSOR
		*/
		HIDECURSOR
	}
}
/************************************************
 * Function:	CBoard::KeyDown()
 * Purpose:		Handle keyDown event for board
 */
Boolean CBoard::KeyDown(EventPtr eventP)         
{
	Boolean handled = false;
	Int16 penX = virtualX;
	Int16 penY = virtualY; 
	Char newmove[4],c;
	//I'm observing a game, reviewing a loaded game or not in the process of playing a game
	//then i'm allowed to review the game (backward or forward the position)
	if (gameInfo.action == OBSERVE || \
	(gameInfo.action == IDLE && gameInfo.status == LOADED) || \
	((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == GAMEOVER) || \
	((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == INTERRUPTED))
	{	
     	switch (eventP->data.keyDown.chr)
     	{
     		//Backward
        	case pageUpChr:
        	case backspaceChr:
        	case BACKWARDCHAR:
        		MoveBackward();
        		handled = true;
        		break;
        	//Forward
       		case pageDownChr:
        	case linefeedChr:
        	case FORWARDCHAR:
        		MoveForward();
        		handled = true;
				break;
			//Fast Backward (5 moves backward)
			case FBACKWARDCHAR:
        		MoveFastBackward();
        		handled = true;
				break;
			//Fast Forward (5 moves forward)
			case FFORWARDCHAR:
        		MoveFastForward();
        		handled = true;
				break;
			//Begining of game
			case BEGINCHAR:
        		MoveBegin();
        		handled = true;
				break;			
			//End of game
			case ENDCHAR:
        		MoveEnd();
        		handled = true;
				break;
			default:
				handled = false;
				break;
     	}
	}	
	//I'm in the process of playing a game
	//and it's my turn to place move and cursor shows up on board
	else if ((gameInfo.action==PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && (MYTURN) \
			&& virtualX>=0 && virtualX<vsize &&	virtualY>=0 && virtualY<vsize) 
			
	{
		//Check if ZERO key was pressed, if yes, hide the cursor
     	switch (eventP->data.keyDown.chr)
     	{
        	case BLANKSPACE:
        	case TOGGLECHAR:
        		HIDECURSOR
        		handled = true;
        		return handled;
        		break;
        	default:
        		break;
     	}
		
		//Handle 5 keys 
		if (NavSelectPressed(eventP))	//Select Key pressed, send current coordinate
		{
			c = 'A'+ penX;
			if (c>='I')
				c++;
			sprintf(newmove,"%c%d",c,size-penY);
			InvertZone(penX,penY);
			WriteServer(newmove);
			RESETCURSOR
			return true;
		}
		if (NavDirectionPalmPressed(eventP, Left) || eventP->data.keyDown.chr == vchrRockerLeft)	//Left key pressed, move cursor one grid left
		{
			penX --;
			if (penX <0)
				penX = 0;
			handled = true;
		}
		if (NavDirectionPalmPressed(eventP, Right) || eventP->data.keyDown.chr == vchrRockerRight)	//Right key pressed, move cursor one grid right
		{
			penX ++;
			if (penX >= vsize)
				penX = vsize - 1;
			handled = true;
		}
		if (NavDirectionPalmPressed(eventP, Up) || eventP->data.keyDown.chr == vchrRockerUp)	//Up key pressed, move cursor one grid up
		{
			penY --;
			if (penY <0)
				penY = 0;
			handled = true;
		}
		if (NavDirectionPalmPressed(eventP, Down) || eventP->data.keyDown.chr == vchrRockerDown)	//Down key pressed, move cursor one grid down
		{
			penY ++;
			if (penY >= vsize)
				penY = vsize - 1;
			handled = true;
		}
		//Update cursor
		//if (!(virtualX==penX && virtualY==penY))
		{
			InvertZone(virtualX,virtualY);
			InvertZone(penX,penY);
			virtualX = penX;
			virtualY = penY;
			prevPenX = penX;
			prevPenY = penY;
		}
	}
	else if ((gameInfo.action==PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && (MYTURN))
			//it's my turn and cursor does not show up on board
	{
		//Check if ZERO key was pressed, if yes, show the cursor
     	switch (eventP->data.keyDown.chr)
     	{
        	case BLANKSPACE:
        		SHOWCURSOR
        		handled = true;
        		break;
        	default:
        		break;
     	}
		
	}
	return handled;
}
/************************************************
 * Function:	CBoard::Resign()
 * Purpose:		Send Resign command to server
 */
void CBoard::Resign()         
{
	if ((gameInfo.action==PLAY || gameInfo.action == TEACH)&& gameInfo.status==INPROGRESS)
		WriteServer("resign");
}
/************************************************
 * Function:	CBoard::Pass()
 * Purpose:		Send Pass command to server
 */
void CBoard::Pass()         
{
	if ((gameInfo.action==PLAY || gameInfo.action == TEACH) && gameInfo.status==INPROGRESS)
		WriteServer("pass");
}
/************************************************
 * Function:	CBoard::Adjourn()
 * Purpose:		Send Adjourn command to server
 */
void CBoard::Adjourn()         
{
	if ((gameInfo.action==PLAY || gameInfo.action == TEACH) && gameInfo.status==INPROGRESS)
		WriteServer("adjourn");
}
/************************************************
 * Function:	CBoard::Undo()
 * Purpose:		Send Undo command to server
 */
void CBoard::Undo()         
{
	if ((gameInfo.action==PLAY && gameInfo.status==SCORING) || (gameInfo.action == TEACH && gameInfo.status==INPROGRESS))
		WriteServer("undo");
}
/************************************************
 * Function:	CBoard::Mrak()
 * Purpose:		Send Mark command to server
 */
void CBoard::Mark()         
{
	if ((gameInfo.action == TEACH && gameInfo.status==INPROGRESS))
		WriteServer("mark");
}
/************************************************
 * Function:	CBoard::Unobserve()
 * Purpose:		Send Unobserve command to server if
 * 				we are currently observing a game
 */
void CBoard::Unobserve()
{
   	Char	cmdString[25],gameidStr[4];
   	UInt16	preGameId;
	if (gameInfo.action == OBSERVE && gameInfo.status == INPROGRESS)	//I'm observing a game
	{
		//unobserve previouse game first
		preGameId = gameInfo.id;	
		StrCopy(cmdString,"unobserve "); 
		StrCat(cmdString,StrIToA(gameidStr,preGameId));
		WriteServer(cmdString);
	}
}
/************************************************
 * Function:	CBoard::Done()
 * Purpose:		Send Done command to server
 */
void CBoard::Done()         
{
	if ((gameInfo.action==PLAY || gameInfo.action == TEACH) && gameInfo.status==SCORING)
		WriteServer("done");
}
void CBoard::InvertZone(Int16 x,Int16 y)
{
   RectangleType r;
   Int16 zone_offset = offset - (width >> 1) + 1;

   r.topLeft.x = x*width + zone_offset;
   r.topLeft.y = y*width + zone_offset;
   r.extent.x  = r.extent.y  = width - 1;

   //WinInvertRectangle(&r,0);
   WinInvertRectangleFrame(simpleFrame,&r);
}

/************************************************
 * Function:	CBoard::DrawTimer()
 * Purpose:		Draw timer on screen
 */
void CBoard::DrawTimer()
{
   	RectangleType	rw,rb;
   	rw.topLeft.x = 9;
   	rb.topLeft.x = 101; 
   	rw.topLeft.y = rb.topLeft.y = 151;
   	rw.extent.x = rb.extent.x = 58;
   	rw.extent.y = rb.extent.y = 9;
   	static UInt8 timerFlashCount = 0;
   	Char whiteStone[10],blackStone[10];
   	Char whiteTimerString[20],blackTimerString[20];
   	WinPushDrawState();
	WinSetTextColorRGB(&white,NULL); 
   	//Display white timer
   	ConvertTimerString(timerInfo.whiteTimeLeft,whiteTimerString);
   	//sprintf(whiteStoneLeft,"%2d",timerInfo.whiteStoneLeft);
   	//StrCat(whiteTimerString,"/");
   	//StrCat(whiteTimerString,whiteStoneLeft);
   	sprintf(whiteStone,"/%d/%d",timerInfo.whiteStoneLeft,timerInfo.whiteCapture);
   	StrCat(whiteTimerString,whiteStone);
   	if ((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && gameInfo.color == WHITE \
   	&& timerInfo.whiteTimeLeft > 0 && timerInfo.whiteTimeLeft <= gByoAlert \
   	&& timerInfo.whiteStoneLeft > 0)
	{
		if (MYTURN)	//it's my turn, flash the timer
		{
			if (timerFlashCount) 
			{
				WinSetBackColorRGB(&red,NULL);
				timerFlashCount = 0;
			}
			else
			{
				WinSetBackColorRGB(&black,NULL);
				timerFlashCount = 1;
			}
		}  
		else 
			WinSetBackColorRGB(&red,NULL); //Keep the timer warning
	}	
	else	//Not in the middle of playing a game
	{
		WinSetBackColorRGB(&black,NULL);
	}
   	WinEraseRectangle(&rw,0); 
   	WinDrawChars(whiteTimerString,StrLen(whiteTimerString),rw.topLeft.x+1,rw.topLeft.y);
	//CtlSetLabel((ControlType*)GetObjectPtr(BoardFormWhiteTimer),whiteTimerString);
   	//Display black timer
   	ConvertTimerString(timerInfo.blackTimeLeft,blackTimerString);
   	//sprintf(blackStoneLeft,"%2d",timerInfo.blackStoneLeft);
   	//StrCat(blackTimerString,"/");
   	//StrCat(blackTimerString,blackStoneLeft);
   	sprintf(blackStone,"/%d/%d",timerInfo.blackStoneLeft,timerInfo.blackCapture);
   	StrCat(blackTimerString,blackStone);
   	if ((gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == INPROGRESS && gameInfo.color == BLACK  \
   	&& timerInfo.blackTimeLeft > 0 && timerInfo.blackTimeLeft <= gByoAlert \
   	&& timerInfo.blackStoneLeft > 0)
	{
		if (MYTURN)	//it's my turn, flash the timer
		{
			if (timerFlashCount)
			{
				WinSetBackColorRGB(&red,NULL);
				timerFlashCount = 0;
			}
			else
			{
				WinSetBackColorRGB(&black,NULL);
				timerFlashCount = 1;
			}
		}
		else
			WinSetBackColorRGB(&red,NULL); //Keep the timer warning
	}	
	else	//Not in the middle of playing a game
	{
		WinSetBackColorRGB(&black,NULL);
	}
   	WinEraseRectangle(&rb,0);
   	WinDrawChars(blackTimerString,StrLen(blackTimerString),rb.topLeft.x+1,rb.topLeft.y);
	//CtlSetLabel((ControlType*)GetObjectPtr(BoardFormBlackTimer),blackTimerString);
	WinPopDrawState();
}
/************************************************
 * Function:	CBoard::UpdateTimer()
 * Purpose:		Update timer when pre-set time out occurs
 */
void CBoard::UpdateTimer(Int16 diff)
{
	if (gameInfo.status==INPROGRESS) 
	{
		//if (BoardForm == FrmGetActiveFormID())
		//	DrawTimer();
   		if (gSupportSound && gPlaySound && tickingAudioP && gameInfo.color == WHITE && play == WHITE \
   		&& timerInfo.whiteTimeLeft > 0 && timerInfo.whiteTimeLeft <= gByoAlert \
   		&& timerInfo.whiteStoneLeft > 0)
			//play clock ticking
			SndPlayResource(tickingAudioP,sndGameVolume,sndFlagAsync);
   		else if (gSupportSound && gPlaySound && tickingAudioP && gameInfo.color == BLACK && play == BLACK \
   		&& timerInfo.blackTimeLeft > 0 && timerInfo.blackTimeLeft <= gByoAlert \
   		&& timerInfo.blackStoneLeft > 0)
			//play clock ticking
			SndPlayResource(tickingAudioP,sndGameVolume,sndFlagAsync);
		if (play == WHITE)
		{   
			timerInfo.whiteTimeLeft-=diff;
			//timerInfo.whiteTimeElapse+=diff;
			sgfInfo.WE+=diff;
			if (timerInfo.whiteTimeLeft<=0)
			{
				if (gameInfo.byoyomi>0)
				{
					timerInfo.whiteTimeLeft += gameInfo.byoyomi*60;
					timerInfo.whiteStoneLeft = (stones>0?stones:25);
				}
				else
					timerInfo.whiteTimeLeft = 0;
				
			}				
		}
		else if (play == BLACK)
			{
				timerInfo.blackTimeLeft-=diff;
				//timerInfo.blackTimeElapse+=diff;
				sgfInfo.BE+=diff;				
				if (timerInfo.blackTimeLeft<=0)
				{
					if (gameInfo.byoyomi>0)
					{
						timerInfo.blackTimeLeft += gameInfo.byoyomi*60;
						timerInfo.blackStoneLeft = (stones>0?stones:25);
					}
				else
					timerInfo.blackTimeLeft = 0;
				} 
			}		
	if (BoardForm == FrmGetActiveFormID() && this == gBoardPool.GetCurrentBoard())
		DrawTimer();
	//if (GameInfoForm == FrmGetActiveFormID())
	//	FrmUpdateForm(GameInfoForm,frmRedrawUpdateCode); 
	}
}
/************************************************
 * Function:	CBoard::UpdateTitle()
 * Purpose:		Update game title
 */
void CBoard::UpdateTitle(Char* title)
{
	if (title) // && !sgfInfo.GN)
	{
		if (sgfInfo.GN)	//Release old GN if there is any
		{
			MemPtrFree(sgfInfo.GN);
			sgfInfo.GN = NULL;
		}
		sgfInfo.GN = (Char*)MemPtrNew(StrLen(title)+1);
		if (sgfInfo.GN)
			StrCopy(sgfInfo.GN,title);
		else
		{
			// Memory Allocation Failed.
			return;
		}
	}
}
/************************************************
 * Function:	CBoard::JumpToMoveN()
 * Purpose:		Change board position to a certain move
 */
void CBoard::JumpToMoveN(Int16 moveIndex)
{
	Int16 i;
	TMove	theMove;
	Int16 latestMoveIndex = record.GetMoveCount()-1;
	//Boundary check
	if (moveIndex <0 || moveIndex> latestMoveIndex)
		return;
	
	CleanBoard();
	displayedMoveCount = moveIndex;	
	
	for (i=0;i<=moveIndex;i++)
	{
		record.GetMove(i,theMove);
		UpdatePosition(theMove);
	}
	//Switch to review mode for this moment, in order to update board
	reviewingMode = true;	
	EndOfMove();		//Draw or update board by calling EndOfMove()
	if (moveIndex == latestMoveIndex)	//To the end
	{
		//Switch back to auto play mode (non-review mode)
		reviewingMode = false;	
	}
	previouseDisplayedMoveCount = displayedMoveCount;   
}
/************************************************
 * Function:	CBoard::MoveBackward()
 * Purpose:		Change board position to previouse move
 */
void CBoard::MoveBackward()
{
	if (displayedMoveCount > 0)
	{
		displayedMoveCount--;
		JumpToMoveN(displayedMoveCount);
	}
}
/************************************************
 * Function:	CBoard::MoveForward()
 * Purpose:		Change board position to previouse move
 */
void CBoard::MoveForward()
{
	Int16 latestMoveIndex = record.GetMoveCount()-1;
	displayedMoveCount++;
	if (displayedMoveCount > latestMoveIndex)
	{
		displayedMoveCount = latestMoveIndex;
	}
	JumpToMoveN(displayedMoveCount);
}
/************************************************
 * Function:	CBoard::MoveFastBackward()
 * Purpose:		Change board position to 5 moves backward
 */
void CBoard::MoveFastBackward()
{
	if (displayedMoveCount > 0)
	{
		displayedMoveCount = displayedMoveCount>=5 ?displayedMoveCount-5: 0;
		JumpToMoveN(displayedMoveCount);
	}
}
/************************************************
 * Function:	CBoard::MoveFastForward()
 * Purpose:		Change board position to 5 moves forward
 */
void CBoard::MoveFastForward()
{
	Int16 latestMoveIndex = record.GetMoveCount()-1;
	displayedMoveCount += 5;
	if (displayedMoveCount > latestMoveIndex)	//To the end
	{
		displayedMoveCount = latestMoveIndex;
	}
	JumpToMoveN(displayedMoveCount);
}
/************************************************
 * Function:	CBoard::MoveEnd()
 * Purpose:		Change board position to the end of game
 */
void CBoard::MoveEnd()
{
	Int16 latestMoveIndex = record.GetMoveCount()-1;
	displayedMoveCount = latestMoveIndex;
	JumpToMoveN(latestMoveIndex);
}
/************************************************
 * Function:	CBoard::MoveBegin()
 * Purpose:		Change board position to the begining of game
 */
void CBoard::MoveBegin()
{
	//if (displayedMoveCount > 0)
	{
		JumpToMoveN(0);
	}
}

/************************************************
 * Function:	ConvertTimerString()
 * Purpose:		Convert timer value to a readable string
 */
void ConvertTimerString(Int16 timeLeft, Char *string)
{
   	UInt16 hh = 0;
   	UInt16 mm = 0;
   	UInt16 ss = 0;
   	
   	ss =  timeLeft % 60;
   	timeLeft /=  60;
	mm = timeLeft % 60;
	timeLeft /= 60;
	
	hh = timeLeft;
	if(mm>99 && ss>9)
		sprintf(string,"%3d:%2d",mm,ss);
	if(mm>9 && ss>9)
		sprintf(string,"0%2d:%2d",mm,ss);
	else if(mm>9 && ss<=9)
		sprintf(string,"0%2d:0%d",mm,ss);
	else if(mm<=9 && ss>9) 
		sprintf(string,"00%d:%2d",mm,ss);
	else if(mm<=9 && ss<=9)
		sprintf(string,"00%d:0%d",mm,ss);
	
   	return;
}
/************************************************
 * Function:	NotifyGameOver()
 * Purpose:		Pop up a alert form to notify that game is over
 */
void NotifyGameOver(UInt16 gameId, Char *reason)
{
	Char idString[10];
	FrmCustomAlert(GameOverAlert,StrIToA(idString,gameId),reason," ");
}
/************************************************
 * Function:	MarkDead()
 * Purpose:		Mark and identify group as dead 
 */
void CBoard::MarkDead(Int16 x, Int16 y)
{
	y = size - y -1; //Mirro y scal
	Int16 us = p[x][y]&PIECEMASK;
   	Int16 dist = 0;
   	Int16 i, j, k, l;
   	Int16 more;
   	if ((p[x][y] & PIECEMASK) == EMPTY)
   		return;
	p[x][y] |= ((p[x][y]&PIECEMASK) == WHITE ? BLACKTERRITORY : WHITETERRITORY);
   	p[x][y] |= FLAG;
   	do
   	{
    	more = 0;
      	k = x+dist;
      	l = y+dist;
      	for (i=(0>x-dist?0:x-dist);i<=k && i<size;++i)
         	for (j=(0>y-dist?0:y-dist);j<=l && j<size;++j)
            	//if ((p[i][j] & PIECEMASK) == MARK)
            	if (p[i][j] & FLAG)
            	{
#define CHECKFORUS(i,j)                   \
               		if ((p[i][j] & PIECEMASK) == us && !(p[i][j] & FLAG))   	\
               		{									\
               			p[i][j] |= ((p[i][j]&PIECEMASK) == WHITE ? BLACKTERRITORY : WHITETERRITORY);	\
               			p[i][j] |= FLAG;	\
               			more = 1;				\
               		};
                  	//p[i][j] |= MARK, more = 1;
					//CHECKFORUS(i,j);
               		if (i>0) CHECKFORUS(i-1,j);
               		if (j>0) CHECKFORUS(i,j-1);
               		if (i+1<size) CHECKFORUS(i+1,j);
               		if (j+1<size) CHECKFORUS(i,j+1);
            	} 
      	++dist;
	} while (more);
   	scoringMode = true;
   	EndOfMove();	//To update board
}
/************************************************
 * Function:	UnmarkDead()
 * Purpose:		Unmark dead group, restore to the status when you begin scoring 
 */
void CBoard::UnmarkDead()
{
   	Int16 i, j;
	for (i = 0; i < size; i++)
		for (j = 0; j < size; j++)
			p[i][j] &= PIECEMASK;	// Keep color bit information
   	scoringMode = true;
   	EndOfMove();	//To update board
}
CBoardPool::CBoardPool()
{
	Int16	i;
	for (i=0; i<MAX_BOARDS; i++)
		boardArray[i] = new CBoard;
	recentUpdatedGameID = 0;
	matchingGameID = 0;
	currentBoardIndex = 0;
	waitNewGameID = false;
	unobservedGameID = 0;
}
CBoardPool::~CBoardPool()
{
	Int16	i;
	for (i=0; i<MAX_BOARDS; i++)
		delete boardArray[i];
}
CBoard*	CBoardPool::GetNewBoard()
{
	Int16	i;
	TGameInfo gameInfo;
	for (i=0; i<MAX_BOARDS; i++)
	{
		boardArray[i]->GetGameInfo(gameInfo);
		if (gameInfo.status == PREPARING)
		{
			return boardArray[i];
		}
		
	}
	return NULL;
}
CBoard*	CBoardPool::GetInactiveBoard()
{
	Int16	i;
	TGameInfo gameInfo;
	for (i=0; i<MAX_BOARDS; i++)
	{
		boardArray[i]->GetGameInfo(gameInfo);
		if (gameInfo.status == INTERRUPTED || gameInfo.status == GAMEOVER || gameInfo.status == LOADED)
		{
			return boardArray[i];
		}
	}
	return NULL;
}
CBoard*	CBoardPool::GetCurrentBoard()
{
	return boardArray[currentBoardIndex];
} 
void CBoardPool::SetCurrentBoard(const CBoard* boardP)
{
	Int16	i=0;
	if (!boardP)
		return;
	else
	{
		for (i=0; i<MAX_BOARDS; i++)
		{
			if (boardArray[i]==boardP)
			{
				currentBoardIndex = i;
				break;
			}
		}
	}
}
void CBoardPool::SetCurrentBoard(const Int16 index)
{
	if (index>=0 && index<MAX_BOARDS)
		currentBoardIndex = index;
}
CBoard*	CBoardPool::GetBoardByID(const UInt16 gameID)
{
	Int16	i;
	TGameInfo gameInfo;
	if (gameID == 0)
		return NULL;
	for (i=0; i<MAX_BOARDS; i++)
	{
		boardArray[i]->GetGameInfo(gameInfo);
		if (gameInfo.id == gameID) // && (gameInfo.status == INPROGRESS || gameInfo.status == SCORING))
		{
			return boardArray[i];
		}
		
	}
	return NULL;
}
CBoard*	CBoardPool::GetBoardByIndex(const Int16 index)
{
	if (index<0 || index>=MAX_BOARDS)
		return NULL;
	else
		return boardArray[index];
}
/*Int16	CBoardPool::GetCurrentBoardIndex()
{
		return currentBoardIndex;
}
*/
Int16	CBoardPool::GetBoardIndex(const CBoard* boardP)
{
	Int16	i=0;
	if (!boardP)
		return -1;
	else
	{
		for (i=0; i<MAX_BOARDS; i++)
		{
			if (boardArray[i]==boardP)
			{
				return i;
			}
		}
	}
	return -1;
}

void CBoardPool::CloseBoard(const UInt16 gameID)
{
	Int16	i=0;
	TGameInfo gameInfo;
	//Look for the particular game id and delete it.
	for (i=0; i<MAX_BOARDS; i++)
	{
		boardArray[i]->GetGameInfo(gameInfo);
		if (gameInfo.id == gameID)
		{
			delete boardArray[i];
			boardArray[i] = new CBoard;
			break;
		} 
	}
	return;
}	
void CBoardPool::CloseBoard(const CBoard* boardP)
{
	Int16	i=0;
	if (!boardP)
		return;
	else
	{
		for (i=0; i<MAX_BOARDS; i++)
		{
			if (boardArray[i]==boardP)
			{
				delete boardArray[i];
				boardArray[i] = new CBoard;
				break;
			}
		}
	}
}
void CBoardPool::DiscardBoard(const CBoard* boardP)
{
	TGameInfo gameInfo;
	Int16	i=0;
	if (!boardP)
		return;
	else
	{
		for (i=0; i<MAX_BOARDS; i++)
		{
			if (boardArray[i]==boardP)
			{
				boardArray[i]->GetGameInfo(gameInfo);
				delete boardArray[i];
				boardArray[i] = new CBoard;
				if (gameInfo.action == OBSERVE && gameInfo.status == INPROGRESS)
					unobservedGameID = gameInfo.id;
				break;
			}
		}
	}
}
void CBoardPool::UpdateTimer(Int16 diff)
{
	Int16	i=0;
	for (i=0; i<MAX_BOARDS; i++)
	{
		boardArray[i]->UpdateTimer(diff);
	}
}
void CBoardPool::InterruptGame()
{
	Int16	i=0;
	CBoard	*workingBoard;
	TGameInfo	info;
	for (i=0; i<MAX_BOARDS; i++)
	{
		workingBoard = boardArray[i];
		if (workingBoard)
		{
			workingBoard->GetGameInfo(info);
			if(info.status == INPROGRESS || info.status == SCORING) 
			{
				unsigned char status = INTERRUPTED;
				workingBoard->UpdateGameInfo(0,NULL,&status,NULL,NULL);
			}
		}
	}
}
//Global function called by network.cpp
CBoard*	OpenNewBoard()
{
	CBoard* workingBoard=NULL;
	workingBoard = gBoardPool.GetNewBoard();
	if (!workingBoard) 
	{
		//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
		workingBoard = gBoardPool.GetInactiveBoard();
		if (workingBoard)
		{
			gBoardPool.CloseBoard(workingBoard); //close the inactive board
		}
		else
		{
			//No inactive board available. All boards are in process, 
			//then we have to force current board to be discarded. 
			workingBoard = gBoardPool.GetCurrentBoard();
			workingBoard->Unobserve();	//unobserve current game in case we are observing a game
			//discard current board,	need to remember the game id currently being observed
			//because it is going to be ovwelayed by a matching game.
			gBoardPool.DiscardBoard(workingBoard);	
		}
		workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
		/*
		if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
			return NULL; 
		*/
	}
	return workingBoard;
} 