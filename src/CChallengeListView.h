#ifndef CCHALLENGELISTVIEW_H
#define CCHALLENGELISTVIEW_H
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"

class CChallengeArray;
class CChallengeListView{
public:

	CChallengeListView() SEC_LIST;
	CChallengeListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	virtual ~CChallengeListView() SEC_LIST;
	void Clear() SEC_LIST;
	Boolean HasNewItem() SEC_LIST;
	Boolean HasItem() SEC_LIST;
	Boolean TimeoutItem(UInt32 currentTime, UInt32 timeout) SEC_LIST;
	void RemoveItem(Char* s) SEC_LIST;
	void SetItemRank(Char *playerName, Char *rank) SEC_LIST;
	CChallengeItem* FindItemByName(Char *playerName) SEC_LIST;
	void UpdateList(CChallengeItem *item) SEC_LIST;
	void EndOfList() SEC_LIST;
	void BeginOfList() SEC_LIST;
	void Flush() SEC_LIST;
	void UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw) SEC_LIST;
    void DisableTable(TablePtr table) SEC_LIST;
	void Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage) SEC_LIST;
	void SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	Boolean HandleEvent( EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean highLighted;
   	Int8	highLightedRow;

protected:

	void InitTable(TablePtr table ) SEC_LIST;
   	void UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar ) SEC_LIST;
   	Boolean HandleRepeatEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleTableEnterEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleKeyDownEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
	Boolean HandleSelectItem(CChallengeItem *item) SEC_LIST;   
   
   	Boolean hasNewItem;
   	Boolean	updatingList;
	CChallengeArray	*list;
   	Int32	skippedItems;
   	void 	(*customDrawFunction)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
};

#endif // CCHALLENGELISTVIEW_H
