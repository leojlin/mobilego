#include "CStringArray.h"

CStringArray::CStringArray()
{
	pArray = NULL;
	size = 0;
}

Boolean CStringArray::Add(Char* node)
{
	Int32	newsize;
	Char*		*pTemp;
	
	//Allocate new array
	newsize = size+1;
	pTemp = new Char*[newsize];
	if (!pTemp)
	{
		//Memory allocation failed
		return false;
	}
	//Copy array bit by bit
	if (pArray)
	{
		MemMove(pTemp,pArray,size * sizeof(Char*));
	}
	pTemp[newsize -1]=node;
	//delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

void CStringArray::Clear()
{
	Int32 i, k;
	if (pArray)
	{
		k = size;
		for (i=0;i<k;i++)
		{
			//*(&temp) = *pArray[i];
			//MemPtrFree(pArray[i]);	//Release dynamic memory allocated for each node
			delete pArray[i];
		}
		delete pArray;
		pArray = NULL;
		size = 0;
	}
}

CStringArray::~CStringArray()
{
	Clear();
}

Int32 CStringArray::FindByName(Char *name)
{
	for (Int32 i=0;i<size;i++)
	{
		if(!StrCompare(pArray[i],name))
		{
			return i;
		}
	}
	//Not found
	return -1;
}

Boolean CStringArray::Insert(Char* node,Int32 index)
{
	Int32	newsize;
	Char*		*pTemp;
	
	if (index<0)
	{
		index = 0;;
	}
	//Make room for insertion
	newsize = size+1;
	pTemp = new Char*[newsize];
	if (!pTemp)
	{
		//Memory allocation failed
		return false;
	}
	if (pArray)
	{
		//Move elements up to the insertion point into new storage
		MemMove(pTemp,pArray,index * sizeof(Char*));
		//Move all remaining elements into new storage
		MemMove(pTemp+index+1,&pArray[index],(newsize-index-1)*sizeof(Char*));
	}
	//Insert the new element
	pTemp[index] = node;
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

Boolean CStringArray::Remove(Int32 index)
{
	Int32	newsize;
	Char*		*pTemp;

	if (index<0 ||index>=size)
	{
		return false;
	}
	//make the array smaller
	newsize = size-1;
	pTemp = new Char*[newsize];
	if (!pTemp)
	{
		//Memory allocation failed
		return false;
	}
	if (pArray)
	{
		MemPtrFree(pArray[index]);	//Release dynamic memory allocated for this node
		delete pArray[index];
		//Move elements up to the deletion point into new storage
		MemMove(pTemp,pArray,index * sizeof(Char*));
		//Move all remaining elements into new storage
		MemMove(pTemp+index,&pArray[index+1],(newsize-index)*sizeof(Char*));
	}
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

Int32 CStringArray::Size()
{
	return size;
}

Char* CStringArray::operator[](Int32 index)
{
	return pArray[index];
}
