#ifndef __MAIN_H__
#define __MAIN_H__
#include <PalmOS.h>
#include "sections.h"

#define appFileCreator			'MCGS'	// register your own at http://www.palmos.com/dev/creatorid/
#define appVersionNum			0x01
#define appPrefID				0x00
#define appPrefVersionNum		0x03

const	Int16	NETCHECKTIMEOUT	= 100; //in system ticks
const 	Int16 	NETCONNECTTIMEOUT = 10;	//in seconds
const 	UInt32  HEARTBEATINTERVAL = 60;	//Idle time, to send AYT when it is out (in seconds)
//const 	UInt32  SERVERRESPONSEDELAY = 15;	//Expected time to receive server response for AYT (in seconds)
//const 	Int16  MAXERRORCOUNT = 3;
const 	Int16 	MINNETLAG =	5;	//in seconds
const 	Int16 	MAXNETLAG =	99;	//in seconds
const 	Int16 	DEFAULTNETLAG =	15;	//in seconds
const 	UInt32  CHALLENGEUPDATEINTERVAL = 10;	//How often to update challenge queue (in seconds)
const 	UInt32  CHALLENGEEXPIRETIME = 120; //How long a challenge will be expired (in seconds)
//const Int16 	GAMETIMERTIMEOUT = 100;
const 	Int16 	IN_BUFFER_SIZE = 1024;
const 	UInt16 	TERMINAL_BUFFER_SIZE = 4096;
const 	Int16  TABLEROWS = 10;
const 	Int16  MAXACCOUNTNUM = 10;
const 	Int16 	MAXCHATCHARACTERS =	90;

typedef struct TGoAccount{
	Char	description[25];
	Char	userName[25];
	Char	password[25];
	Char	serverName[25];
	Char	serverPort[6];
	Int16	serverType;
	Boolean	supportNmatch;	//<- Version 1
	Boolean	useProxy;	
	Char	proxyName[25];
	Char	proxyPort[6];
	Int16	proxyType;		//<-Version 2
};  

enum NETQUALITY {
	HIGHQUALITY	= 0,
	MEDIUMQUALITY,
	LOWQUALITY,
	SUSPENDED,
};
void FlashChatButton() SEC_STRG;
void HideChatButton() SEC_STRG;
void FlashChallengeButton() SEC_STRG;
void ShowChallengeButton() SEC_STRG;
void HideChallengeButton() SEC_STRG;
void Chat(const Char* myAccountName, const Char* chatBuddyName, const Char* chatMessage) SEC_STRG;
void DrawChatBuddyListItem(Int16 itemNum, RectangleType *bounds, Char **itemsText) SEC_STRG;
void RegisterForNotification(UInt32 notifyType);
void UnregisterForNotification(UInt32 notifyType);
void MainFormUpdateTerminal();
void DrawTerminalLine(UInt16 y);
void WriteTerminal(Char*);
void DrawPlayerItem(void* tableP, Int16 row, Int16 column, RectanglePtr bounds) SEC_LIST;
void DrawGameItem(void* tableP, Int16 row, Int16 column, RectanglePtr bounds) SEC_LIST;
void DrawGameItem2(void* tableP, Int16 row, Int16 column, RectanglePtr bounds) SEC_LIST;
void DrawChallengeItem(void* tableP, Int16 row, Int16 column, RectanglePtr bounds) SEC_LIST;
#endif
 