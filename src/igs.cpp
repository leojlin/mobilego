#include <PalmOS.h>
#include <StdIOPalm.h>
#include "CChallengeArray.h" 
#include "CStringArray.h"
#include "nngs.h"
#include "rsc.h"
extern CChallengeItem	gCurrentMatch;
extern CChallengeArray	gChatBuddyList;
extern CStringArray		gStoredGameList;
extern Char				gChatBuddyName[];
Int16 IGS_AutoMatchConfirmed(const Char* s, Char *player)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Prompt must be "35 Using "
	if (StrNCompare(s,"35 Using ",StrLen("35 Using ")))
			return n;
	// Player name
	currentOffset = StrLen("35 Using ");
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	StrCopy(player, token);	
	currentOffset += i+1;
	if (!StrNCompare(s+currentOffset,"\'defs\'",StrLen("\'defs\'")))
		n = 1;
	return n;
}

Int16 IGS_GameAdjourned(const Char* s, UInt16& gameId, Char *result)
{
	Int16 i, n, currentOffset;
	//Int16 whiteScore,blackScore;
	Char token[MAX_WORD_LENGTH];
	//Char *whiteName=NULL;
	//Char *blackName=NULL;
	n = 0;
	currentOffset = 0;
	// Prompt must be "9 Game "
	if (StrNCompare(s,"9 Game ",StrLen("9 Game ")))
			return n;
	if (!StrNCompare(s,"9 Game has been adjourned.",StrLen("9 Game has been adjourned.")))
	{
		gameId = 0;	//My matching game is adjourned.
		StrCopy(result,"Adjourned");
		n = 4;
		return n;
	}
	currentOffset += StrLen("9 Game ");
	// GameID
	if ((i = GetNextWord(s+currentOffset, ':', token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	// White name
	currentOffset += i+2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//if ((whiteName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
	//	StrCopy(whiteName,token);
	//sgfInfoP->PW = whiteName;
	n++;
	// "vs"
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token))!=2 || StrNCompare(token,"vs",2))
		return n;
	// Black Name
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//if ((blackName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
	//	StrCopy(blackName,token);
	//sgfInfoP->PB = blackName;
	n++;
	//has adjourned.
	currentOffset += i+1;
	token[0]=NULL;
	if (StrNCompare(s+currentOffset,"has adjourned.",StrLen("has adjourned.")))
		return n;
	sprintf(token,"Adjourned");
	n++;
	StrCopy(result, token);
	return n;
}
Int16 IGS_MatchResult(const Char* s,Char *player, Char *result)
{
	Int16 i, n, currentOffset;
	Int16 whiteScore=0,blackScore=0;
	Char token[MAX_WORD_LENGTH],whitePlayer[MAX_WORD_LENGTH],blackPlayer[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Prompt must be "20 "
	if (StrNCompare(s,"20 ",3))
			return n;
	//White player
	currentOffset = 3;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	StrCopy(whitePlayer,token);
	n++;
	//White score
	currentOffset += i+1+StrLen("(W:O):");	//skip " (W:O): "
	SkipBlankSpace(s, &currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	whiteScore = FString2Integer(token);
	n++;		
	currentOffset += i+1;	
	if (StrNCompare(s+currentOffset,"to ",StrLen("to ")))
			return n;
	currentOffset += 3;
	//Black player
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	StrCopy(blackPlayer,token);
	n++;
	//Black score
	currentOffset += i+1+StrLen("(B:#):");	//" ( B:# ): "
	SkipBlankSpace(s, &currentOffset);
	if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
		return n;
	blackScore = FString2Integer(token);
	n++;		
	token[0] = NULL;
	if (whiteScore>blackScore)
	{
		StrCopy(player,blackPlayer);
		sprintf(token,"%2d.%d",(whiteScore-blackScore)/10,(whiteScore-blackScore)%10);
	}
	else if (whiteScore<blackScore)
	{
		StrCopy(player,whitePlayer);
		sprintf(token,"%2d.%d",(blackScore-whiteScore)/10,(blackScore-whiteScore)%10);
	}
	else
		sprintf(token,"Over");
	if(StrLen(token))
	{
		StrCopy(result, token);
	} 
	return n;
}			

Int16 IGS_MatchOver(const Char* s, Char *player, Char *result)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Prompt must be "9 "
	if (StrNCompare(s,"9 ",2))
			return n;
	// Player name 
	currentOffset = 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	StrCopy(player,token);	
	currentOffset += i+1;
	if (!StrNCompare(s+currentOffset,"has run out of time.",StrLen("has run out of time.")))
	{
		StrCopy(result,"T");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"has resigned the game.",StrLen("has resigned the game.")))
		{
			StrCopy(result,"R");
			n++;
		}
	return n;
}
Int16 IGS_AutoMatchRequest(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	//UInt8	playerColor;
	Int16	size;
	//Int16	handicap;
	//Int16	komi;
	Int16	main;	//Main time
	Int16	byoyomi;//Byoyomi time
	Int16	stones;	//Byoyomi stones
	//UInt32	timeStamp;//Time stamp when challenge received
	
	n = 0;
	currentOffset = 0;
	// "36 "
	if (StrNCompare(s,"36 ",3))
		return n;
	//Player name
	currentOffset = 3;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"wants ",StrLen("wants ")))	
		return n;
	currentOffset += StrLen("wants ");
	// Size 
	if ((i = GetNextWord(s+currentOffset,'x', token)) <= 0)
		return n;
	size = String2Integer(token);
	challengeItemP->size = size;
	n++;
	currentOffset +=i+1;
	if ((i = GetNextWord(s+currentOffset,BLANKSPACE, token)) <= 0)
		return n;
	currentOffset +=i+1;
	if (StrNCompare(s+currentOffset,"in ",StrLen("in ")))	
		return n;
	currentOffset += StrLen("in ");
	//Main time
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	main = String2Integer(token);
	challengeItemP->main = main;
	n++;
	
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"minutes with ",StrLen("minutes with ")))	
		return n;
	// Byoyomi time
	currentOffset += StrLen("minutes with ");
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	byoyomi = String2Integer(token);
	challengeItemP->byoyomi = byoyomi;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"byo-yomi and ",StrLen("byo-yomi and ")))	
		return 0;
	//Stones
	currentOffset += StrLen("byo-yomi and ");
	if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
		return n;
	stones = String2Integer(token);
	challengeItemP->stones = stones;	
	n++;
	// Timestamp
	challengeItemP->timeStamp = TimGetSeconds();
	//Others, default
	challengeItemP->playerColor = TBD;
	challengeItemP->handicap = 0;
	challengeItemP->komi = 55;
	challengeItemP->matchtype = IGS_AMATCH;
	challengeItemP->offered = true;
	return n;	
}
Int16 IGS_ByoyomiStonesInfoUpdate(const Char* s, UInt16& gameId, Int16& byoyomiStones)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// "15 TIME"
	if (StrNCompare(s,"15 TIME:",StrLen("15 TIME:")))
		return n;
	// GameID
	currentOffset += StrLen("15 TIME:");
	if ((i = GetNextWord(s+currentOffset, ':', token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	// Skip "whitePlayer(W): "
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	n++;
	// Skip Captures
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	n++;
	// Skip main time, i.e. 60/60
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	n++;
	// Skip byoyomi time, i.e. 0/600
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	n++;
	// byoyomi moves left
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, '/', token)) <= 0)
		return n;
	n++;
	// byoyomi stones
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	byoyomiStones = String2Integer(token);
	n++;
	// The rest must be 0/0 0/0 0/0
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"0/0 0/0 0/0",StrLen("0/0 0/0 0/0")))
		return n;
		
	return n;
}
Int16 IGS_GameReloaded(const Char* s)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Prompt must be "9 "
	if (StrNCompare(s,"9 ",2))
			return n;
	// Player name
	currentOffset = 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	currentOffset += i+1;
	if (!StrNCompare(s+currentOffset,"has restarted your game.",StrLen("has restarted your game.")))
		n++;	//IGS style
	else if (!StrNCompare(s+currentOffset,"as restarted your game.",StrLen("as restarted your game.")))
			n++;	//WING style
	return n;
}
Int16 IGS_MatchDispute(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	UInt8	playerColor;
	Int16	size;
	//Int16	handicap;
	//Int16	komi;
	Int16	main;	//Main time
	Int16	byoyomi;//Byoyomi time
	//Int16	stones;	//Byoyomi stones
	//UInt32	timeStamp;//Time stamp when challenge received
	
	n = 0;
	currentOffset = 0;
	// "5   "
	if (StrNCompare(s,"5   ",4))
		return n;
	//Player name
	currentOffset = 4;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"wants ",StrLen("wants ")))	
		return n;
	currentOffset += StrLen("wants ");
	// Challenger's color	
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if (!StrNCompare(token,"Black",5))
		playerColor = BLACK;
	else if (!StrNCompare(token,"White",5))
		playerColor = WHITE;
	else
		return n;
	challengeItemP->playerColor = playerColor;
	n++;
	//
	currentOffset += 6;
	if (StrNCompare(s+currentOffset,"on a ",StrLen("on a ")))	
		return n;
	// Size
	currentOffset += StrLen("on a ");
	if ((i = GetNextWord(s+currentOffset,'x', token)) <= 0)
		return n;
	size = String2Integer(token);
	challengeItemP->size = size;
	n++;
	currentOffset +=i+1;
	if ((i = GetNextWord(s+currentOffset,BLANKSPACE, token)) <= 0)
		return n;
	currentOffset +=i+1;
	if (StrNCompare(s+currentOffset,"in ",StrLen("in ")))	
		return n;
	currentOffset += StrLen("in ");
	//Main time
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	main = String2Integer(token);
	challengeItemP->main = main;
	n++;
	
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"mins (",StrLen("mins (")))	
		return n;
	// Byoyomi time
	currentOffset += StrLen("mins (");
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	byoyomi = String2Integer(token);
	challengeItemP->byoyomi = byoyomi;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"byoyomi)",StrLen("byoyomi)")))	
		return 0;
	// Timestamp
	challengeItemP->timeStamp = TimGetSeconds();
	//Others, default
	challengeItemP->matchtype = MATCH;
	challengeItemP->handicap = 0;
	challengeItemP->komi = 55;
	challengeItemP->stones = 25;
	return n;	
}
Int16 IGS_NmatchDispute(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	UInt8	playerColor;
	Int16	size;
	Int16	handicap;
	Int16	main;	//Main time
	Int16	byoyomi;//Byoyomi time
	Int16	stones;	//Byoyomi stones
	//UInt32	timeStamp;//Time stamp when challenge received
	
	n = 0;
	currentOffset = 0;
	// "5 "
	if (StrNCompare(s,"5 ",2))
		return n;
	//Player name
	currentOffset = 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"request: ",StrLen("request: ")))	
		return n;
	// Challenger's color	
	currentOffset += StrLen("request: ");
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if (token[0] == 'B')
		playerColor = BLACK;
	else if (token[0] == 'W')
		playerColor = WHITE;
	else if (token[0] == 'N')
		playerColor = TBD;
	else
		return n;
	challengeItemP->playerColor = playerColor;
	n++;
	// Handicap
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	handicap = String2Integer(token);
	challengeItemP->handicap = handicap;
	n++;
	// Size
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	size = String2Integer(token);
	//Only 19x19 supported by now
	if (size!=19)
		return n;
	challengeItemP->size = size;
	n++;
	// Main time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	main = String2Integer(token);
	challengeItemP->main = main/60;	//Convert to seconds
	n++;
	// Byoyomi time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	byoyomi = String2Integer(token);
	challengeItemP->byoyomi = byoyomi/60;	//Convert to seconds
	n++;
	// Stones
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	stones = String2Integer(token);
	challengeItemP->stones = stones;
	n++;
	//The rest of string must be 0 0 0>
	//Byoyomi Time System is not supported for now
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"0 0 0",StrLen("0 0 0")))	
		return 0;
	// Timestamp
	challengeItemP->timeStamp = TimGetSeconds();
	//Others, default
	challengeItemP->matchtype = IGS_NMATCH;
	return n;	
}
Int16 IGS_NmatchNotSupport(const Char* s)
{

	if (StrNCompare(s,"5 Opponent's client does not support nmatch.",StrLen("5 Opponent's client does not support nmatch.")))	
		return 0;
	else return 1;
}	
Int16 IGS_NmatchRequest(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	UInt8	playerColor;
	Int16	size;
	Int16	handicap;
	Int16	main;	//Main time
	Int16	byoyomi;//Byoyomi time
	Int16	stones;	//Byoyomi stones
	//UInt32	timeStamp;//Time stamp when challenge received
	
	n = 0;
	currentOffset = 0;

	// The line of new challenge item should begins with "9 Use <nmatch "
	if (StrNCompare(s,"9 Use <nmatch",StrLen("9 Use <nmatch")))	
		return n;
	// Player name
	currentOffset += StrLen("9 Use <nmatch")+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	// Challenger's color	
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if (token[0] == 'B')
		playerColor = WHITE;
	else if (token[0] == 'W')
		playerColor = BLACK;
	else if (token[0] == 'N')
		playerColor = TBD;
	else
		return n;
	challengeItemP->playerColor = playerColor;
	n++;
	// Handicap
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	handicap = String2Integer(token);
	challengeItemP->handicap = handicap;
	n++;
	// Size
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	size = String2Integer(token);
	//Only 19x19 supported by now
	if (size!=19)
		return n;
	challengeItemP->size = size;
	n++;
	// Main time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	main = String2Integer(token);
	challengeItemP->main = main/60;	//Convert to seconds
	n++;
	// Byoyomi time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	byoyomi = String2Integer(token);
	challengeItemP->byoyomi = byoyomi/60;	//Convert to seconds
	n++;
	// Stones
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	stones = String2Integer(token);
	challengeItemP->stones = stones;
	n++;
	//The rest of string must be 0 0 0>
	//Byoyomi Time System is not supported for now
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"0 0 0> ",StrLen("0 0 0> ")))	
		return 0;
	// Timestamp
	challengeItemP->timeStamp = TimGetSeconds();
	//Others, default
	challengeItemP->matchtype = IGS_NMATCH;
	return n;	
}
Int16 IGS_NmatchWithdraw(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	
	n = 0;
	if (StrNCompare(s,"24 *SYSTEM*:",StrLen("24 *SYSTEM*:")))	
		return n;
	currentOffset = StrLen("24 *SYSTEM*:")+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"canceled the nmatch request",StrLen("canceled the nmatch request")))	
		n = 0;
	return n;
}	
Int16 IGS_Seek(const Char* s)
{
	Int16 n;
	n = 0;
	if (!StrNCompare(s,"63 ENTRY ",StrLen("63 ENTRY ")))
		n = 1;	//Seek started
	else if (!StrNCompare(s,"63 ENTRY_CANCEL",StrLen("63 ENTRY_CANCEL")))
		n = 2;	//Seek cancelled
	return n;
}

Int16 IGS_StatsDefs(const Char* s, Char* statsPlayerNameP, TDefsInfo* statsDefsP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Int16	time,size,byoyomi,stones;
	n = 0;
	currentOffset = 0;  
	if (StrNCompare(s, "9 ", StrLen("9 ")))
		return 0; 
	currentOffset = 2;
	if (!StrNCompare(s+currentOffset,"Player: ",StrLen("Player: ")))	
	{	
		currentOffset += StrLen("Player: ");
		SkipBlankSpace(s,&currentOffset);
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		StrCopy(statsPlayerNameP,token);
		n = 1;
	}
	else if (!StrNCompare(s+currentOffset,"Defaults (help defs):",StrLen("Defaults (help defs):")))	
	{	//IGS style
		currentOffset += StrLen("Defaults (help defs):");
		SkipBlankSpace(s,&currentOffset);
		//"time" 
		if (StrNCompare(s+currentOffset,"time ",StrLen("time ")))
			return n;
		currentOffset += StrLen("time ");
		//Get time value
		if ((i = GetNextWord(s+currentOffset, ',', token)) <= 0)
			return n;
		time = String2Integer(token);
		currentOffset += i+1;
		SkipBlankSpace(s,&currentOffset);
		//"size" 
		if (StrNCompare(s+currentOffset,"size ",StrLen("size ")))
			return n;
		currentOffset += StrLen("size ");
		//Get size value
		if ((i = GetNextWord(s+currentOffset, ',', token)) <= 0)
			return n;
		size = String2Integer(token);
		currentOffset += i+1;
		SkipBlankSpace(s,&currentOffset);
		//"byo-yomi time" 
		if (StrNCompare(s+currentOffset,"byo-yomi time ",StrLen("byo-yomi time ")))
			return n;
		currentOffset += StrLen("byo-yomi time ");
		//Get byo-yomi time value
		if ((i = GetNextWord(s+currentOffset, ',', token)) <= 0)
			return n;
		byoyomi = String2Integer(token);
		currentOffset += i+1;
		SkipBlankSpace(s,&currentOffset);
		//"byo-yomi stones" 
		if (StrNCompare(s+currentOffset,"byo-yomi stones ",StrLen("byo-yomi stones ")))
			return n;
		currentOffset += StrLen("byo-yomi stones ");
		//Get byo-yomi time value
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		stones = String2Integer(token);
		statsDefsP->color = TBD;
		statsDefsP->minSize = size;
		statsDefsP->maxSize = size;
		statsDefsP->minHandicap = 0;
		statsDefsP->maxHandicap = 0;
		statsDefsP->minTime = time;
		statsDefsP->maxTime = time;
		statsDefsP->minByoyomi = byoyomi;
		statsDefsP->maxByoyomi = byoyomi;
		statsDefsP->minStones = stones;
		statsDefsP->maxStones = stones;
		statsDefsP->minCount = 0;
		statsDefsP->maxCount = 0;
		statsDefsP->minKoryo = 0;
		statsDefsP->maxKoryo = 0;
		statsDefsP->minPrebyoyomi = 0;
		statsDefsP->maxPrebyoyomi = 0;
		n = DEFS_END;
	}
	return n;	
}

