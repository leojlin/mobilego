#include <PalmOS.h>
#include <StdIOPalm.h>
#include "CChallengeArray.h" 
#include "CStringArray.h"
#include "nngs.h"
#include "rsc.h"
extern CChallengeItem	gCurrentMatch;
extern CChallengeArray	gChatBuddyList;
extern CStringArray		gStoredGameList;
extern Char				gChatBuddyName[];
Int16 String2Integer(const Char* s)
{
	Int16 k;
	Int16 digits;
	Boolean negative = false;
	if ((digits=StrLen(s))<=0)
		return 0;
	for (k=0;k<digits;k++)
	{
		if (TxtCharIsDigit(s[k]))		
			break;
		if (s[k]=='-')
			negative = true;
	}
	if (k>=digits)
		return 0;
	if (negative)
		return -1*StrAToI(s+k);
	else
		return StrAToI(s+k);
}
Int16 FString2Integer(const Char* s)
{
	Int16 dPoint;
	Int16 dInteger,dFraction;
	Char stringInteger[MAX_WORD_LENGTH],stringFraction[MAX_WORD_LENGTH];
	dPoint = GetNextWord(s, '.', stringInteger);
	dInteger = String2Integer(stringInteger);
	if (dPoint<=0)
		return dInteger;
	StrCopy(stringFraction, s+dPoint);
	dFraction = String2Integer(stringFraction);
	if (dInteger>=0)
		return dInteger*10+dFraction;
	else
		return dInteger*10-dFraction;
}
void SkipBlankSpace(const Char* s, Int16 *currentOffsetP)	
{
	Int16 i;
	Int16 len = StrLen(s);
	for (i=*currentOffsetP;i<len;i++)
	{
		if (s[i]!=BLANKSPACE)
			break;
	}
	*currentOffsetP = i;
	return;
}
Int16 GetNextWord(const Char* s, Char delimiter, Char *wordP)
{
	Int16 i;
	Char *position;
	position = (Char*)StrChr(s,delimiter);
	if (!position || position == s)
	{
		StrNCopy(wordP, s, MAX_WORD_LENGTH);
		i = 0;
	}
	else
	{
		i = position - s;
		if (i > MAX_WORD_LENGTH)
			i = -1;
		else
		{
			StrNCopy(wordP, s, i);
			*(wordP + i) = '\0';
		}
	}
	return i;
}
Int16	ConvertRankToNumber(Char *rank)
{
	Int16	numRank = -1000;
	Int32	rankNumber = 0;
	UInt16	k,q;
	//Parse rank
	q = 0;
	//Skip leading blank
	while (rank[q] == BLANKSPACE && q<StrLen(rank))
		q++;
	for (k=q;k<StrLen(rank);k++)
	{
		if (TxtCharIsAlpha(rank[k]))	//Find first alpha 	
			break;
	}

	if (k<StrLen(rank))
	{
		rankNumber = StrAToI(rank+q); 
		switch (rank[k])
		{
			case 'k':
				numRank = -10*rankNumber;
				if (rank[k+1] == '*' || rank[k+1] == '+')
					numRank +=3;	//k* or k+
				else if (rank[k+1] == '?')
					numRank -=3;	//k?
				break;
			case 'd':
				numRank = 10*rankNumber-5;
				if (rank[k+1] == '*' || rank[k+1] == '+')	
					numRank +=3;	//d* or d+
				else if (rank[k+1] == '?')
					numRank -=3;	//d?
				break;
			case 'p':
				numRank = 100*rankNumber;
				break;
			case 'N':
			case 'n':
			case '?':
			default:	//unknown to us, same as NR
				break;
		}
	}
	return numRank;
}
/***************************************************	
 * Function:	RankCompare(Char* rank1, Char* rank2)
 * Return:	0 if rank1 has same rank as rank2
 * 			1 if rank1 has higher rank than rank2
 * 			-1 if rank1 has lower rank then rank2
 ***************************************************/
/*Int16 RankCompare(Char *rank1, Char *rank2)
{
	Int16		ret = 0;
	Int32		rankNumber1 = 0,rankNumber2 = 0;
	RANKCLASS 	rankClass1 = nr,rankClass2 = nr;
	SUBCLASS	subClass1 = basic, subClass2 = basic;
	UInt16	k,q;
	//Parse rank1
	q = 0;
	//Skip blank space
	while (rank1[q] == BLANKSPACE && q<StrLen(rank1))
		q++;
	for (k=q;k<StrLen(rank1);k++)
	{
		if (TxtCharIsAlpha(rank1[k]))	//Find first alpha 	
			break;
	}

	if (k<StrLen(rank1))
	{
		rankNumber1 = StrAToI(rank1+q);
		switch (rank1[k])
		{
			case 'k':
				rankClass1 = kyu;
				if (rank1[k+1] == '*')
					subClass1 = plus;
				if (rank1[k+1] == '?')
					subClass1 = unknown;
				break;
			case 'd':
				rankClass1 = dan;
				if (rank1[k+1] == '*')
					subClass1 = plus;
				if (rank1[k+1] == '?')
					subClass1 = unknown;
				break;
			case 'p':
				rankClass1 = pro;
				if (rank1[k+1] == '*')
					subClass1 = plus;
				if (rank1[k+1] == '?')
					subClass1 = unknown;
				break;
			case 'N':
			case 'n':
			default:	//unknown to us
				break;
		}
	}
	//Parse rank2
	q =0;
	//Skip blank space
	while (rank2[q] == BLANKSPACE && q<StrLen(rank2))
		q++;
	for (k=q;k<StrLen(rank2);k++)
	{
		if (TxtCharIsAlpha(rank2[k]))	//Find first alpha 	
			break;
	}

	if (k<StrLen(rank2))
	{
		rankNumber2 = StrAToI(rank2+q);
		switch (rank2[k])
		{
			case 'k':
				rankClass2 = kyu;
				if (rank2[k+1] == '*')
					subClass2 = plus;
				if (rank2[k+1] == '?')
					subClass2 = unknown;
				break;
			case 'd':
				rankClass2 = dan;
				if (rank2[k+1] == '*')
					subClass2 = plus;
				if (rank2[k+1] == '?')
					subClass2 = unknown;
				break;
			case 'p':
				rankClass2 = pro;
				if (rank2[k+1] == '*')
					subClass2 = plus;
				if (rank2[k+1] == '?')
					subClass2 = unknown;
				break;
			case 'R':
			default:	//unknown to us
				break;
		}
	}
	//Compare rank1 with rank2
	if (rankClass1 > rankClass2)
		ret = 1;
	if (rankClass1 < rankClass2)
		ret = -1;
	if (rankClass1 == rankClass2)//same class, then compare rank number
	{
		if (rankNumber1 > rankNumber2)
			ret = (rankClass1 == kyu?-1:1) ; //the higher number of kyu, the lower ranking it is
		if (rankNumber1 < rankNumber2)
			ret = (rankClass1 == kyu?1:-1) ; //the higher number of kyu, the lower ranking it is
		if (rankNumber1 == rankNumber2) //same rank class and rank number, the compare subclass
		{
			if (subClass1 == subClass2)
				ret = 0;
			else
				ret = (subClass1 > subClass2? 1:-1);
		}
	}
	return ret;		
}
*/

void SetCurrentMatch(CChallengeItem	*matchP)
{
	if (!matchP)
		return ;
	if (!matchP->playerName) 
		return;
	if (gCurrentMatch.playerName)
		MemPtrFree(gCurrentMatch.playerName);	//release previous memory
	if (gCurrentMatch.playerRank)
		MemPtrFree(gCurrentMatch.playerRank);	//release previous memory
		
	gCurrentMatch.playerName = NULL;
	gCurrentMatch.playerRank = NULL;  
	gCurrentMatch.playerName = (Char*) MemPtrNew(StrLen(matchP->playerName)+1);
	if (!gCurrentMatch.playerName)	
  	{
  		// Memory Allocation Failed.
  		return;
  	}
	StrCopy(gCurrentMatch.playerName,matchP->playerName);
	if (matchP->playerRank)
	{
		gCurrentMatch.playerRank = (Char*) MemPtrNew(StrLen(matchP->playerRank)+1);
		if (gCurrentMatch.playerRank)	
			StrCopy(gCurrentMatch.playerRank,matchP->playerRank);
		else
	  	{
	  		// Memory Allocation Failed.
	  		//Continue, just don't copy Rank
	  	}
	}
	gCurrentMatch.playerColor = matchP->playerColor;
	gCurrentMatch.size = matchP->size;
	gCurrentMatch.handicap = matchP->handicap;
	gCurrentMatch.komi = matchP->komi;
	gCurrentMatch.main = matchP->main;	
	gCurrentMatch.byoyomi = matchP->byoyomi;
	gCurrentMatch.stones = matchP->stones;	
	gCurrentMatch.timeStamp = matchP->timeStamp;
	gCurrentMatch.count = matchP->count;
	gCurrentMatch.koryo = matchP->koryo;
	gCurrentMatch.prebyoyomi = matchP->prebyoyomi;
	//Added for V2.4 
	gCurrentMatch.matchtype = matchP->matchtype;
	gCurrentMatch.offered = matchP->offered;
}
Int16	CompareMatchOpponent(const Char* currnetOpponent, const Char* comparedOpponent)
{
	if (!currnetOpponent || !comparedOpponent)
		return -1;
	else
		return StrCompare(currnetOpponent,comparedOpponent);
}

void UpdateChatBuddyList(const Char* playerName)
{
	CChallengeItem* chatBuddyItemP = new CChallengeItem;
	if (!chatBuddyItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	if (!playerName)
  	{
		delete chatBuddyItemP;
		return;
  	}
	//Always insert the latest player to the top of Chat List
	if((chatBuddyItemP->playerName = (Char*)MemPtrNew(StrLen(playerName)+1)))
	{
		StrCopy(chatBuddyItemP->playerName,playerName);
		Int32 index;
		index = gChatBuddyList.FindByName(playerName);	
		if ( index != -1)
		{
			if (!gChatBuddyList.Remove(index))
			{
				// Fail to remove previouse chat buddy
				delete chatBuddyItemP;
				return;
			}
		}
		if (!gChatBuddyList.Insert(chatBuddyItemP,0))
		{
			// Fail to insert current chat buddy
			delete chatBuddyItemP;
			return;
		}
		
	}	
	else
	{
		// Memory Allocation Failed.
		delete chatBuddyItemP;
	}
	if (gChatBuddyList.Size()==1)
	{		
		//There is only one person in my chat buddy list.
		//Set this person as my current chat buddy.
		StrCopy(gChatBuddyName,playerName);	//Update current chat buddy
		FrmUpdateForm(MainForm,frmRedrawUpdateCode);
	}

}
Int16 NNGS_AdjournRequest(const Char* s)
{
	//NNGS style
	if (!StrNCompare(s,"9 Use adjourn to adjourn",StrLen("9 Use adjourn to adjourn")))
		return 1;
	//IGS style
	if (!StrNCompare(s,"9 Use <adjourn> to adjourn",StrLen("9 Use <adjourn> to adjourn")))
		return 1;
	return 0;
} 
Int16 NNGS_MoveInfoUpdate(const Char* s, TMoveInfo *moveP)
{
	Int16 i, n, currentOffset;
	Char c,token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Movelist ends when seeing prompt "1 5", "1 6" or "1 8"
	if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 6",3) || !StrNCompare(s,"1 8",3))
		return MOVE_END;
	// "15" Leading string of move infomation
	if ((i = GetNextWord(s, BLANKSPACE, token)) !=2 || StrNCompare(token,"15",2))
		return n;
	// MoveID 
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, '(', token)) <= 0)
		return n;
	moveP->moveID = String2Integer(token);
	n++;
	// Color
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, ')', token)) <= 0)
		return n;
	if (!StrNCompare(token,"B",1))
	{
		moveP->playerColor = BLACK;
		n++;
	}
	else if(!StrNCompare(token,"W",1))
	{
		moveP->playerColor = WHITE;
		n++;
	}
	else
		return n;
	// New move
	currentOffset += i+3;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) < 0)
		return n;
	if (!StrNCompare(token,"Handicap",8))
	{
		currentOffset += i+1;
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) < 0)
			return n;
		moveP->handicap = String2Integer(token);
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
	moveP->handicap = 0;	
	if (!StrNCompare(token,"Pass",4))
	{
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
		
	c = token[0];
    if ((c>='A')&&(c<='Z'))
      c+='a'-'A';
    if (c>'i')
      c--;
    moveP->x=c-'a';
	moveP->y = String2Integer(token+1) -1;
	n++;
	
	return n;
}
Int16 NNGS_UndoLastMove(const Char* s, TMoveInfo* moveP)
{
	Int16 i, n, currentOffset;
	Char c,token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Movelist ends when seeing prompt "1 5", "1 6" or "1 8"
	if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 6",3) || !StrNCompare(s,"1 8",3))
		return MOVE_END;
	// "28" Leading string of undo infomation
	if ((i = GetNextWord(s, BLANKSPACE, token)) !=2 || StrNCompare(token,"28",2))
		return n;
	// Player 
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	n++;
	// undid the last move
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"undid the last move (",StrLen("undid the last move (")))
		return n;
	currentOffset += StrLen("undid the last move (");
	n++;
	// New move
	if ((i = GetNextWord(s+currentOffset, ')', token)) < 0)
		return n;
	moveP->moveID = MOVEID_NA;
	if (!StrNCompare(token,"Handicap",8))
	{
		currentOffset += i+1;
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) < 0)
			return n;
		moveP->handicap = String2Integer(token);
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
	moveP->handicap = 0;	
	if (!StrNCompare(token,"Pass",4))
	{
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
		
	c = token[0];
    if ((c>='A')&&(c<='Z'))
      c+='a'-'A';
    if (c>'i')
      c--;
    moveP->x=c-'a';
	moveP->y = String2Integer(token+1) -1;
	n++;
	
	return n;
}
Int16 NNGS_UndoInGame(const Char* s, UInt16& gameId, TMoveInfo* moveP)
{
	Int16 i, n, currentOffset;
	Char c,token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Movelist ends when seeing prompt "1 5", "1 6" or "1 8"
	if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 6",3) || !StrNCompare(s,"1 8",3))
		return MOVE_END;
	// "28 undo in game " 
	if (StrNCompare(s,"28 Undo in game " ,StrLen("28 Undo in game ")) && \
		StrNCompare(s,"28 undo in game " ,StrLen("28 undo in game ")))
		return n;
	// GameID
	currentOffset += StrLen("28 undo in game ");
	if ((i = GetNextWord(s+currentOffset, ':', token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	// skip %player vs %player:
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, ':', token)) <= 0)
		return n;
	currentOffset += i+1;
	n++;
	// New move
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, CR, token)) < 0)
		return n;
	moveP->moveID = MOVEID_NA;
	if (!StrNCompare(token,"Handicap",8))
	{
		currentOffset += i+1;
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) < 0)
			return n;
		moveP->handicap = String2Integer(token);
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
	moveP->handicap = 0;	
	if (!StrNCompare(token,"Pass",4))
	{
		moveP->x = -1;
		moveP->y = -1;
		n++;
		return n;
	}
		
	c = token[0];
    if ((c>='A')&&(c<='Z'))
      c+='a'-'A';
    if (c>'i')
      c--;
    moveP->x=c-'a';
	moveP->y = String2Integer(token+1) -1;
	n++;
	
	return n;
}
Char* NNGS_TitleInfoUpdate(const Char* s)
{
	Int16 offset;
	Char *title = NULL;
	
	// "9" Leading string of title infomation
	offset = StrLen("9 Game is titled: ");
	if (StrNCompare(s,"9 Game is titled: ",offset))
		return NULL;
	// Title 
	title = (Char*)MemPtrNew(StrLen(s)-offset+1);
	if (!title)	//Memory allocation fails
	{
  		// Memory Allocation Failed.
		title = NULL;
	}
	else 	//Get the string without ending CR charater
	{
		Int16 i;
		Char *position;
		position = (Char*)StrChr(s,CR);
		i = position - s - offset;
		if (i>0)
		{
			StrNCopy(title, s+offset, i);
			*(title + i) = '\0';
		}
		else
		{
			//for some reason, the string is not ended with CR character
			delete title;
			title = NULL;
		}
	}
	return title;
}
Int16 NNGS_DeadGroup(const Char* s, Int16& x, Int16& y)
{
	Int16 i, n, currentOffset;
	Char c,token[MAX_WORD_LENGTH];
	n = 0;
	//x = -1,
	//y = -1;
	if (StrNCompare(s,"9 Removing @ ",StrLen("9 Removing @ ")))
		return n;
	// dead group's position
	currentOffset = StrLen("9 Removing @ ");
	if ((i = GetNextWord(s+currentOffset, CR, token)) < 0)
		return n;
		
	c = token[0];
    if ((c>='A')&&(c<='Z'))
      c+='a'-'A';
    if (c>'i')
      c--;
    x=c-'a';
	y = String2Integer(token+1) -1;
	n++;
	
	return n;
}
Int16 NNGS_RestoreGroup(const Char* s)
{
	Int16	n = 0;
	//Not implemented yet
	if (StrNCompare(s,"9 Board is restored ",StrLen("9 Board is restored ")))
		return n;
	n++;
	return n;
}

Int16 NNGS_GameOver(const Char* s, UInt16& gameId, Char *result)
{
	Int16 i, n, currentOffset;
	Int16 whiteScore,blackScore,score;
	Char token[MAX_WORD_LENGTH];
	//Char *whiteName=NULL;
	//Char *blackName=NULL;
	n = 0;
	currentOffset = 0;
	// Prompt must be "9 {Game "
	if (StrNCompare(s,"9 {Game ",8))	//IGS style
	{
		if (StrNCompare(s,"21 {Game ",9))	//WING style
			return n;
		else 
			currentOffset = 9;
	}
	else 
		currentOffset = 8;
	// GameID
	if ((i = GetNextWord(s+currentOffset, ':', token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	// White name
	currentOffset += i+2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//if ((whiteName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
	//	StrCopy(whiteName,token);
	//sgfInfoP->PW = whiteName;
	n++;
	// "vs"
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token))!=2 || StrNCompare(token,"vs",2))
		return n;
	// Black Name
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//if ((blackName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
	//	StrCopy(blackName,token);
	//sgfInfoP->PB = blackName;
	n++;
	// ":"	
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,":",1))
		return n;

	currentOffset += 2;
	token[0]=NULL;
	if (!StrNCompare(s+currentOffset,"White resigns.",StrLen("White resigns.")))
	{
		sprintf(token,"B+R");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"White lost by Resign",StrLen("White lost by Resign")))
	{
		sprintf(token,"B+R");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"Black resigns.",StrLen("Black resigns.")))
		{
			sprintf(token,"W+R");
			n++;
		}
	else if (!StrNCompare(s+currentOffset,"Black lost by Resign",StrLen("Black lost by Resign")))  
	{
		sprintf(token,"W+R");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"White forfeits on time.}",StrLen("White forfeits on time.}")))
	{
		sprintf(token,"B+T");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"Black forfeits on time.}",StrLen("Black forfeits on time.}")))
	{
		sprintf(token,"W+T");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"has adjourned.",14))
	{
		sprintf(token,"Adjourned");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"has been adjourned.",StrLen("has been adjourned."))) //WING style
	{
		sprintf(token,"Adjourned");
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"Black wins by",StrLen("Black wins by"))) //WING style
	{
		currentOffset += StrLen("Black wins by")+1;
		if ((i = GetNextWord(s+currentOffset, '}', token)) <= 0)
			return n;
		score = FString2Integer(token);
		sprintf(token,"B+%2d.%d",score/10,score%10);
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"White wins by",StrLen("White wins by"))) //WING style
	{
		currentOffset += StrLen("White wins by")+1;
		if ((i = GetNextWord(s+currentOffset, '}', token)) <= 0)
			return n;
		score = FString2Integer(token);
		sprintf(token,"W+%2d.%d",score/10,score%10);
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"White lost by",StrLen("White lost by"))) //IGS teaching game
	{
		currentOffset += StrLen("White lost by")+1;
		if ((i = GetNextWord(s+currentOffset, '}', token)) <= 0)
			return n;
		score = FString2Integer(token);
		sprintf(token,"B+%2d.%d",score/10,score%10);
		n++;
	}
	else if (!StrNCompare(s+currentOffset,"Black lost by",StrLen("Black lost by"))) //IGS teaching game
	{
		currentOffset += StrLen("Black lost by")+1;
		if ((i = GetNextWord(s+currentOffset, '}', token)) <= 0)
			return n;
		score = FString2Integer(token);
		sprintf(token,"W+%2d.%d",score/10,score%10);
		n++;
	}
	else //"W %2d.%d B %2d.%d"}" //IGS style
	{
		if (StrNCompare(s+currentOffset,"W",1) )
		return n;
		// White score
		currentOffset += 2;
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
			return n;
		whiteScore = FString2Integer(token);
		// "B"
		currentOffset += i+1;
		if (StrNCompare(s+currentOffset,"B",1)) 
			return n;
		// Black score
		currentOffset += 2;
		if ((i = GetNextWord(s+currentOffset, '}', token)) <= 0)	//IGS style
			if((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)	//WING style
				return n;
		blackScore = FString2Integer(token);
		n++;
		token[0] = NULL;
		if (whiteScore>blackScore)
		{
			sprintf(token,"W+%2d.%d",(whiteScore-blackScore)/10,(whiteScore-blackScore)%10);
		}
		else if (whiteScore<blackScore)
		{
			sprintf(token,"B+%2d.%d",(blackScore-whiteScore)/10,(blackScore-whiteScore)%10);
		}
		else
			sprintf(token,"Over");
	}
	if(StrLen(token))
	{
		//sgfInfoP->RE = (Char*)MemPtrNew(StrLen(token)+1);
		StrCopy(result, token);
	}
	return n;
}
Int16 NNGS_AddObservation(const Char* s)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	//IGS style
	if (!StrNCompare(s,"9 Adding game to observation list.",StrLen("9 Adding game to observation list.")))
			return 1;
	//NNGS style
	// Prompt must be "9 Adding game "
	if (StrNCompare(s,"9 Adding game ",StrLen("9 Adding game ")))
			return n;
	currentOffset += StrLen("9 Adding game ");
	// GameID
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//gameId = String2Integer(token);
	n++;
	currentOffset += i+1;
	//Teh rest must be "to observation list."
	if (StrNCompare(s+currentOffset,"to observation list.",StrLen("to observation list.")))
			n = 0;
		
	return n;
}
Int16 NNGS_RemoveObservation(const Char* s, UInt16& gameId)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// Prompt must be "9 Removing game " or "5 Removing game "
	if (StrNCompare(s,"9 Removing game ",StrLen("9 Removing game ")))
		if (StrNCompare(s,"5 Removing game ",StrLen("5 Removing game ")))
			return n;
	currentOffset += StrLen("9 Removing game ");
	// GameID
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	currentOffset += i+1;
	//Teh rest must be "from observation list."
	if (StrNCompare(s+currentOffset,"from observation list.",StrLen("from observation list.")))
			n = 0;
		
	return n;
}

Int16 NNGS_MatchCreate(const Char* s, UInt16& gameId, Char *opponent)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	if (!StrNCompare(s,"9 Creating match [",StrLen("9 Creating match [")))
		currentOffset += StrLen("9 Creating match [");
	else if (!StrNCompare(s,"9 Match [",StrLen("9 Match [")))
		currentOffset += StrLen("9 Match [");
	else 
		return n;
	// GameID
	if ((i = GetNextWord(s+currentOffset, ']', token)) <= 0)
		return n;
	gameId = String2Integer(token);
	n++;
	currentOffset += i+2;
	if (StrNCompare(s+currentOffset,"with ",StrLen("with ")))
			return n;
	currentOffset += StrLen("with ");
	//player name
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		if ((i = GetNextWord(s+currentOffset, '.', token)) <= 0)
			return n;
	StrCopy(opponent,token);
	n++;	
	return n;
}
Int16 NNGS_CheckScore(const Char* s)
{
	Int16 n, currentOffset;
//	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	if (StrNCompare(s,"9 You can check your score",StrLen("9 You can check your score")))
		return n;
	n++;	
	return n;
}
 
Int16 NNGS_ChangeKomi(const Char* s, Int16& komi)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	// "9 Komi is now set to "
	if (!StrNCompare(s,"9 Komi is now set to ",StrLen("9 Komi is now set to ")))
	{
		// new komi
		currentOffset += StrLen("9 Komi is now set to ");
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		komi = FString2Integer(token);
		n++;
	}
	else if (!StrNCompare(s,"9 Komi set to ",StrLen("9 Komi set to ")))
	{
		// new komi
		currentOffset += StrLen("9 Komi set to ");
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		komi = FString2Integer(token);
		n++;
	}
	else if (!StrNCompare(s,"9 Set the komi to ",StrLen("9 Set the komi to ")))
	{
		// new komi
		currentOffset += StrLen("9 Set the komi to ");
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		komi = FString2Integer(token);
		n++;
	}
	return n;
}
Int16 NNGS_TimerInfoUpdate(const Char* s, UInt16& gameId, TTimerInfo *timerInfoP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *whiteName=NULL;
	Char *blackName=NULL;
	n = 0;
	currentOffset = 0;
	timerInfoP->whitePlayer = whiteName;
	timerInfoP->blackPlayer = blackName;
	// "15"
	if ((i = GetNextWord(s, BLANKSPACE, token)) !=2 || StrNCompare(token,"15",2))
		return n;
	// "Game"
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) !=4 || StrNCompare(token,"Game",4))
		return n;
	// GameID
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	timerInfoP->gameID = String2Integer(token);
	gameId = timerInfoP->gameID;
	n++;
	// "I:"
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token))!=2 || StrNCompare(token,"I:",2))
		return n;
	// White name
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((whiteName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(whiteName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}	
	timerInfoP->whitePlayer = whiteName;
	n++;
	// "("
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"(",1))
		return n;
	// White captures
	currentOffset += 1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	timerInfoP->whiteCapture = String2Integer(token);
	n++;
	// White time left
	currentOffset += i+1;
	i = GetNextWord(s+currentOffset, BLANKSPACE, token);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//else if (i>3)
	//{
	//	i = GetNextWord(s+currentOffset, '-', token);
	//	if (i<=0 || i>3)
	//		return n;
	//}
	timerInfoP->whiteTimeLeft = String2Integer(token);
	n++;
	// White stone left
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, ')', token)) <= 0)
		return n;
	timerInfoP->whiteStoneLeft = String2Integer(token);
	n++;
	// "vs"
	currentOffset += i+2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token))!=2 || StrNCompare(token,"vs",2))
		return n;
	// Black Name
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((blackName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(blackName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	timerInfoP->blackPlayer = blackName;
	n++;
	// "("
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"(",1))
		return n;
	// Black captures
	currentOffset += 1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	timerInfoP->blackCapture = String2Integer(token);
	n++;
	// Black time left
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	timerInfoP->blackTimeLeft = String2Integer(token);
	n++;
	// Black stone left
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, ')', token)) <= 0)
		return n;
	timerInfoP->blackStoneLeft = String2Integer(token);
	n++;
		
	return n;
}
Int16 NNGS_GameItemUpdate(const Char* s, CGameItem *gameItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *whiteName=NULL;
	Char *blackName=NULL;
	Char *whiteRank=NULL;
	Char *blackRank=NULL;
	//Int16 komi=0;
	Char *flag=NULL;
	n = 0;
	currentOffset = 0;

	// Game list begins when seeing "7 [##] " at the begining of a line
	if (!StrNCompare(s,"7 [##] ",7))
		return LIST_BEGIN;
	// Game list ends when seeing prompt "1 5" or "1 8" or "1 6"
	if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 8",3) || !StrNCompare(s,"1 6",3))
		return LIST_END;
	// The line of game list item should begin with "7"
	if (StrNCompare(s,"7",1))	
		return n;
	// "["
	currentOffset += 2;
	if (StrNCompare(s+currentOffset,"[",1))
		return n;
	// [##]Game ID
	currentOffset += 1;	
	if ((i = GetNextWord(s+currentOffset, ']', token)) <= 0)
		return n;
	gameItemP->gameID = String2Integer(token);
	n++;
	// White name
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((whiteName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(whiteName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	gameItemP->whitePlayer = whiteName;
	n++;
	// "["
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"[",1))
		return n;
	// White rank
	currentOffset += 1;
	if ((i = GetNextWord(s+currentOffset, ']', token)) <= 0)
		return n;
	if ((whiteRank = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(whiteRank,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	gameItemP->whiteRank = whiteRank;
	gameItemP->whiteNumericalRank = ConvertRankToNumber(whiteRank);
	n++;
	// "vs."
	currentOffset += i+2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) !=3 || StrNCompare(token,"vs.",3))
		return n;
	// Black Name
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((blackName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(blackName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	gameItemP->blackPlayer = blackName;
	n++;
	// "["
	currentOffset += i+1;
	if (StrNCompare(s+currentOffset,"[",1))
		return n;
	// Black rank
	currentOffset += 1;
	if ((i = GetNextWord(s+currentOffset, ']', token)) <= 0)
		return n;
	if ((blackRank = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(blackRank,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	gameItemP->blackRank = blackRank;
	gameItemP->blackNumericalRank = ConvertRankToNumber(blackRank);
	n++;
	// "("
	currentOffset += i+2;
	if (StrNCompare(s+currentOffset,"(",1))
		return n;
	// Move
	currentOffset += 1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	gameItemP->moveCount = String2Integer(token);
	n++;
	// Size
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	i = GetNextWord(s+currentOffset, BLANKSPACE, token);
	gameItemP->size = String2Integer(token);
	n++;
	// Handicap
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	gameItemP->handicap = String2Integer(token);
	n++;
	// Komi
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	//if ((komi = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
	//	StrCopy(komi,token);
	//gameItemP->komi = komi;
	gameItemP->komi = FString2Integer(token);
	n++;
	// byoyomi	
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	gameItemP->byoyomi = String2Integer(token);
	n++;
	// flag
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, ')', token)) <= 0)
		return n;
	if ((flag = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(flag,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	gameItemP->flag = flag;
	n++;
	// "("
	currentOffset += i+2;
	if (StrNCompare(s+currentOffset,"(",1))
		return n;
	// observer
	currentOffset += 1;	
//	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, ')', token)) <= 0)
		return n;
	gameItemP->observer = String2Integer(token);
	n++;
		
	return n;
}
Int16 NNGS_PlayerItemUpdate(const Char* s, CPlayerItem *playerItemP1, CPlayerItem *playerItemP2)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	Char *rank=NULL;
	Char *idle=NULL;
	Int16	numericalRank;
	UInt8	action=IDLE;	//'P'= Playing, 'O'= Observing, 'I'= Idle
	UInt16	gameID=0;
	
	n = 0;
	currentOffset = 0;

	// Player list begins when seeing "27  Info" at the begining of a line
	if (!StrNCompare(s,"27  Info",8))
		return LIST_BEGIN;
	// Player list ends when seeing prompt "1 5" or "1 8"
	if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 8",3) || !StrNCompare(s,"1 6",3))
		return LIST_END;
	// The line of player list item should begins with "27"
	if (StrNCompare(s,"27",2))	
		return n;
	// Info
	currentOffset += 4;
	playerItemP1->status[0] = s[currentOffset];
	playerItemP1->status[1] = s[currentOffset+1];
	n++;
	// Observing game ID
	currentOffset += 2;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) ==2 && !StrNCompare(token,"--",2))
		;
	else 
	{
		if ((gameID = String2Integer(token))!=0)
				action = OBSERVE;
		 else
		 	return n;
	}
	n++;
	// Playing game ID	
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) ==2 && !StrNCompare(token,"--",2))
		;
	else 
	{
		if ((gameID = String2Integer(token))!=0)
				action = PLAY;
		 else
		 	return n;
	}
	n++;
	playerItemP1->action = action;
	playerItemP1->gameID = gameID;
	// Player name
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP1->playerName = playerName;
	n++;
	// Idle
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((idle = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(idle,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP1->idle = idle;
	n++;
	// Rank
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((rank = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(rank,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP1->rank = rank;
	numericalRank = ConvertRankToNumber(rank);
	playerItemP1->numericalRank = numericalRank;
	n++;
	// Reset initial values for playerItem2
	gameID = 0;
	action = IDLE;
	// "|"
	currentOffset = 38;
	if (StrNCompare(s+currentOffset,"|",1))
		return n;
	// Info
	currentOffset += 3;
	playerItemP2->status[0] = s[currentOffset];
	playerItemP2->status[1] = s[currentOffset+1];
	n++;
	// Observing game ID
	currentOffset += 2;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) ==2 && !StrNCompare(token,"--",2))
		;
	else 
	{
		if ((gameID = String2Integer(token))!=0)
				action = OBSERVE;
		 else
		 	return n;
	}
	n++;
	// Playing game ID	
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) ==2 && !StrNCompare(token,"--",2))
		;
	else 
	{
		if ((gameID = String2Integer(token))!=0)
				action = PLAY;
		 else
		 	return n;
	}
	n++;
	playerItemP2->action = action;
	playerItemP2->gameID = gameID;
	// Player name
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP2->playerName = playerName;
	n++;
	// Idle
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((idle = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(idle,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP2->idle = idle;
	n++;
	// Rank
	currentOffset += i+1;
	SkipBlankSpace(s,&currentOffset);
	if ((i = GetNextWord(s+currentOffset, CR, token)) < 0)
		return n;
	if ((rank = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(rank,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	playerItemP2->rank = rank;
	numericalRank = ConvertRankToNumber(rank);
	playerItemP2->numericalRank = numericalRank;
	n++;
	return n;
}
Int16 NNGS_MatchRequest(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	UInt8	playerColor;
	Int16	size;
	//Int16	handicap;
	//Int16	komi;
	Int16	main;	//Main time
	Int16	byoyomi;//Byoyomi time
	//Int16	stones;	//Byoyomi stones
	//UInt32	timeStamp;//Time stamp when challenge received
	
	n = 0;
	currentOffset = 0;

	// Update Challenge list when seeing "9 Match[ " at the begining of a line
	//if (!StrNCompare(s,"9 Match [",StrLen("9 Match [")))
	//	return LIST_BEGIN;
	// Player list ends when seeing prompt "1 5" or "1 8"
	//if (!StrNCompare(s,"1 5",3) || !StrNCompare(s,"1 8",3))
	//	return LIST_END;
	// The line of new challenge item should begins with "9 Use<match "
	if (StrNCompare(s,"9 Use <match ",StrLen("9 Use <match ")))	
		return n;
	// Player name
	currentOffset += 13;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	// Challenger's color	
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if (token[0] == 'B')
		playerColor = WHITE;
	else if (token[0] == 'W')
		playerColor = BLACK;
	else
		return n;
	challengeItemP->playerColor = playerColor;
	n++;
	// Size
	currentOffset += 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	size = String2Integer(token);
	challengeItemP->size = size;
	n++;
	// Main time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	main = String2Integer(token);
	challengeItemP->main = main;
	n++;
	// Byoyomi time
	currentOffset += i+1;
	if ((i = GetNextWord(s+currentOffset, '>', token)) <= 0)
		return n;
	byoyomi = String2Integer(token);
	challengeItemP->byoyomi = byoyomi;
	n++;
	// Timestamp
	challengeItemP->timeStamp = TimGetSeconds();
	//Others, default
	challengeItemP->handicap = 0;
	challengeItemP->komi = 55;
	challengeItemP->stones = 25;
	challengeItemP->matchtype = MATCH;
	return n;	
}
Int16 NNGS_MatchWithdraw(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	
	n = 0;
	if (StrNCompare(s,"9 ",StrLen("9 ")))	
		return n;
	currentOffset = 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (!StrNCompare(s+currentOffset,"withdraws the match offer",StrLen("withdraws the match offer"))) 	
		return 1;
	else 
		return 0;
}	
Int16 NNGS_MatchDecline(const Char* s,CChallengeItem *challengeItemP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	Char *playerName=NULL;
	
	n = 0;
	if (StrNCompare(s,"9 ",StrLen("9 ")))	
		return n;
	currentOffset = 2;
	if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		return n;
	if ((playerName = (Char*)MemPtrNew(StrLen(token)+1))!=NULL)
		StrCopy(playerName,token);
	else
  	{
  		// Memory Allocation Failed.
  		return n;
  	}
	challengeItemP->playerName = playerName;
	n++;
	currentOffset += i+1;
	if (!StrNCompare(s+currentOffset,"declines your request for a match",StrLen("declines your request for a match")))	
		return 1;
	else
		return 0;
}	
Char* NNGS_ServerResponse(const Char* s)
{
	Char *rString = NULL;
	//Filter out some common messages
	if (!StrNCompare(s,"5 There is a dispute regarding your match",StrLen("5 There is a dispute regarding your match")))
		return NULL;
	if (!StrNCompare(s,"5 It does not look as though that player is challenging you.",StrLen("5 It does not look as though that player is challenging you.")))
		return NULL;
	if (!StrNCompare(s,"5 ",2))	//Leading with "5 "
	{
		rString = (Char*)MemPtrNew(StrLen(s)+1);
		if (!rString)	//Memory allocation fails
		{
	  		// Memory Allocation Failed.
			rString = NULL;
		}
		else 	//Get the string without leading "5 " and the ending CR charater
		{
			Int16 i;
			Char *position;
			position = (Char*)StrChr(s,CR);
			i = position - s - 2;
			if (i>0)
			{
				StrNCopy(rString, s+2, i);
				*(rString + i) = '\0';
			}
			else
			{
				//for some reason, the string is not ended with CR character
				delete rString;
				rString = NULL;
			}
		}
	}
	return rString;
} 
Char* NNGS_GetMessage(const Char* s, Char *who)
{
	Char *rString = NULL;
	Int16 i, currentOffset;
	Char token[MAX_WORD_LENGTH];
	//Leading with "24 " (tell) or "19 " (say)
	if (StrNCompare(s,"24 ",3) && StrNCompare(s,"19 ",3))
		return NULL;
	currentOffset = 3;
	if (!StrNCompare(s+currentOffset,"*",1))	
	{	//Get opponent's name
		currentOffset ++;
		if ((i = GetNextWord(s+currentOffset, '*', token)) <= 0)
			return NULL;
		StrCopy(who,token);
		currentOffset += i+3; //skip following "*: "
		rString = (Char*)MemPtrNew(StrLen(s)-currentOffset+1);
		if (!rString)	
		{
			// Memory Allocation Failed.
			rString = NULL;
		}
 		else 	//Get the string without leading "5 " and the ending CR charater
		{
			Int16 i;
			Char *position;
			position = (Char*)StrChr(s,CR);
			i = position - s - currentOffset;
			if (i>0)
			{
				StrNCopy(rString, s+currentOffset, i);
				*(rString + i) = '\0';
			}
			else
			{
				//for some reason, the string is not ended with CR character
				delete rString;
				rString = NULL;
			}
		}
	}
	else //NNGS style
	{
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
			return NULL;
		StrCopy(who,token);
		currentOffset += i+1; 
		if (StrNCompare(s+currentOffset,"-->",3))
			return NULL;
		currentOffset += 4;
		rString = (Char*)MemPtrNew(StrLen(s)-currentOffset+1);
		if (!rString)	
		{
			// Memory Allocation Failed.
			rString = NULL;
		}
		else 	//Get the string without leading "5 " and the ending CR charater
		{
			Int16 i;
			Char *position;
			position = (Char*)StrChr(s,CR);
			i = position - s - currentOffset;
			if (i>0)
			{
				StrNCopy(rString, s+currentOffset, i);
				*(rString + i) = '\0';
			}
			else
			{
				//for some reason, the string is not ended with CR character
				delete rString;
				rString = NULL;
			}
		}
		
	}
	return rString;
} 

Int16 NNGS_AccountName(const Char* s, Char *name)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;
	if (StrNCompare(s, "Your account name is ", StrLen("Your account name is ")))
		return n; 
	// Account name
	currentOffset = StrLen("Your account name is ");
	if ((i = GetNextWord(s+currentOffset, '.', token)) <= 0)
		return n;
	n++;
	StrCopy(name,token);
	return n; 
}
Int16 NNGS_StoredGame(const Char* s)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH],*gameName;
	n = 0;
	currentOffset = 0; //"23 " is NNGS style
	if (StrNCompare(s, "18 ", StrLen("18 ")) && StrNCompare(s, "23 ", StrLen("23 ")))	
		return n; 
	if (!StrNCompare(s, "18 Found ", StrLen("18 Found ")))
		return n;	
	if (!StrNCompare(s, "23 Found ", StrLen("23 Found ")))
		return n;	
	if (!StrNCompare(s, "23 Stored games for ", StrLen("23 Stored games for "))) //LGS prompt
		return n;	
	currentOffset = 3;
	while (currentOffset<(Int16)StrLen(s))
	{
		SkipBlankSpace(s,&currentOffset);
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
		{
			if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
				return n;
		}
		gameName = (Char*)MemPtrNew(StrLen(token)+1);
		if (gameName)
		{
			StrCopy(gameName,token);
			gStoredGameList.Add(gameName);
		}
		else
		{
			// Memory Allocation Failed.
			break;
		}
		n++;
		currentOffset += i+1; 
	}
	return n;
}
Int16 NNGS_StatsRank(const Char* s, Char* statsPlayerNameP, Char* statsRankP)
{
	Int16 i, n, currentOffset;
	Char token[MAX_WORD_LENGTH];
	n = 0;
	currentOffset = 0;  
	if (StrNCompare(s, "9 ", StrLen("9 ")))
		return 0; 
	currentOffset = 2;
	if (!StrNCompare(s+currentOffset,"Player: ",StrLen("Player: ")))	
	{	
		currentOffset += StrLen("Player: ");
		SkipBlankSpace(s,&currentOffset);
		if ((i = GetNextWord(s+currentOffset, CR, token)) <= 0)
			return n;
		StrCopy(statsPlayerNameP,token);
		n = 1;
	}
	else if (!StrNCompare(s+currentOffset,"Rating: ",StrLen("Rating: ")))	
	{	//NNGS, IGS, WING and CWS style
		currentOffset += StrLen("Rating: ");
		SkipBlankSpace(s,&currentOffset);
		if ((i = GetNextWord(s+currentOffset, BLANKSPACE, token)) <= 0)
			return n;
		StrCopy(statsRankP,token);
		n = RANK_END;
	}
	else if (!StrNCompare(s+currentOffset,"Rank/Rated: ",StrLen("Rank/Rated: ")))	
	{	//LGS style
		currentOffset += StrLen("Rank/Rated: ");
		SkipBlankSpace(s,&currentOffset);
		if ((i = GetNextWord(s+currentOffset, '/', token)) <= 0)
			return n;
		StrCopy(statsRankP,token);
		n = RANK_END;
	}
	return n;
	
}

Int16 NNGS_GetAYT(const Char* s, UInt32& getSeconds, Int16& aytWaitCount)
{
	if (!StrNCaselessCompare(s, "9 YES", StrLen("9 YES")))
	{
		getSeconds = TimGetSeconds();
		aytWaitCount = 0;
		return 1; 
	}
	else
		return 0;
}
