#ifndef __SECTIONS_H__
#define __SECTIONS_H__
#define SEC_NET	__attribute__ ((section("SEC_NET")))
#define SEC_NNGS	__attribute__ ((section("SEC_NNGS")))
#define SEC_IGS	__attribute__ ((section("SEC_IGS")))
#define SEC_CMN	__attribute__ ((section("SEC_CMN")))
#define SEC_FRM1 __attribute__ ((section("SEC_FRM1")))
//#define SEC_FRM2 __attribute__ ((section("SEC_FRM2")))
#define SEC_ARR __attribute__ ((section("SEC_ARR")))
#define	SEC_GO	__attribute__ ((section("SEC_GO")))
#define	SEC_LIST	__attribute__ ((section("SEC_LIST")))
#define	SEC_STRG	__attribute__ ((section("SEC_STRG")))

#endif

