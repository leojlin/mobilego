#ifndef CGAMELISTVIEW_H
#define CGAMELISTVIEW_H
#include <PalmOS.h>
#include "nngs.h"

class CGameArray;
class CGameListView{
public:

	CGameListView() SEC_LIST;
	CGameListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	virtual ~CGameListView() SEC_LIST;
	void Clear() SEC_LIST;
	void UpdateList(CGameItem *item) SEC_LIST;
	void BeginOfList() SEC_LIST;
	void EndOfList() SEC_LIST;
	void Flush() SEC_LIST;
	Boolean HasItem() SEC_LIST;
	Boolean FindGame(UInt16 id, CGameItem **item) SEC_LIST;
	void SortList(GAMEFIELD fld, Boolean  ascending) SEC_LIST;
	void DisplayItemTotal() SEC_LIST;
	void UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw) SEC_LIST;
    void DisableTable(TablePtr table) SEC_LIST;
	void Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage) SEC_LIST;
	void SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	Boolean HandleEvent( EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean highLighted;
   	Int8	highLightedRow;

protected:

	void InitTable(TablePtr table ) SEC_LIST;
   	void UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar ) SEC_LIST;
   	Boolean HandleRepeatEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleTableEnterEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleKeyDownEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
	//Int16 CompareItem(CGameItem *itemP1, CGameItem *itemP2, GAMEFIELD fld, Boolean ascending) SEC_LIST;
	Boolean HandleSelectItem(CGameItem *item) SEC_LIST;   
   	Boolean	updatingList;
	CGameArray	*list;
   	Int32	skippedItems;
   	//Boolean filterOn;
   	//UInt32	formID;
   	void 	(*customDrawFunction)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
};

#endif // CGAMELISTVIEW_H
