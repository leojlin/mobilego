#include <VFSMgr.h>
#include "functions.h"
UInt16  gVolRefNum;

#define MAXFILESIZE	0xFFFF
void InitVFS(void)
{
    Err     err;
    UInt32  volIterator = vfsIteratorStart;
    Char    *volName;

    
    err = VFSVolumeEnumerate(&gVolRefNum, &volIterator);
    if (err)
    {
        gVolRefNum = 0;
        //FrmAlert(altNoVolum);
    }
    else
    {
        volName = (Char *)MemPtrNew(256);
        MemSet(volName, 256, 0);
        err = VFSVolumeGetLabel(gVolRefNum, volName, 255);
        ErrFatalDisplayIf(err, "Couldn't retrieve volume label");
        MemPtrFree(volName);
    }
}
Err SgfReadFile(char *name, char **sgf)
{
    Err   err;
    Char  *errString;
    void  *data;
    FileRef  fileRef;
    UInt32  fileSize;
    
	err = 0;
    if (! name)
    {
        //FrmAlert(altNoFileNamed);
        err = 1;
        return err;
    }

    err = VFSFileOpen(gVolRefNum, name, vfsModeRead, &fileRef);
    if (err)
    {
        switch (err)
        {
            case vfsErrBadName:
                //FrmCustomAlert(altReadFileInvalid, name, "", "");
                break;

            case vfsErrFileNotFound:
                // Note: uses the same alert as DeleteFile.
                //FrmCustomAlert(altDeleteFileFileNotFound, name, "", "");
                break;

            default:
                errString = (Char*)MemPtrNew(maxStrIToALen);
                //FrmCustomAlert(altReadFileError, name, StrIToA(errString, err), "");
                MemPtrFree(errString);
            	break;
        }
    }
    else
    {
        err = VFSFileSize(fileRef, &fileSize);
        if (err)
        {
            if (err == vfsErrIsADirectory)
                //FrmCustomAlert(altReadFileIsDirectory, name, "", "");
                ;
            else
            {
                errString = (Char*)MemPtrNew(maxStrIToALen);
                //FrmCustomAlert(altReadFileError, name, StrIToA(errString, err), "");
                MemPtrFree(errString);
            }
            VFSFileClose(fileRef);
            return err;
        }
        else
        {
        	if (fileSize >MAXFILESIZE)
        	{
                //FrmCustomAlert(altReadFileOverSize, name, "", "");
                VFSFileClose(fileRef);
                err = 100;
                return err;     		
        	}
            // Allocate a buffer large enough to store the file, plus an extra
            // byte to hold a terminating null.
            data = (void *)MemPtrNew(fileSize + 1);
            if (! data)
            {
                //FrmCustomAlert(altReadFileOutOfMemory, name, "", "");
                VFSFileClose(fileRef);
                err = 100;
                return err;
            }
            MemSet(data, fileSize + 1, 0);
            err = VFSFileRead(fileRef, fileSize, data, NULL);
            if (err)
            {
                switch (err)
                {
                    case vfsErrFileEOF:
                        //FrmCustomAlert(altReadFileNoData, name, "", "");
                        break;

                    default:
                    	break;
                        errString = (Char*)MemPtrNew(maxStrIToALen);
                        //FrmCustomAlert(altReadFileError, name, StrIToA(errString, err), "");
                        MemPtrFree(errString);
                }
            }

            VFSFileClose(fileRef);
            *sgf = (Char *)data; //Need to convert to ANSI encoding.
        }
    }
    return err;
}
