/******************************************************************************
 *
 * Copyright (c) 1999-2004 PalmSource, Inc. All rights reserved.
 *
 * File: AppMain.c
 * 
 *****************************************************************************/
#include <PalmOS.h>
#include <StdIOPalm.h>
#include <PalmOne_68K.h> 
//#include <68K/hs.h>
#include <FrmGlue.h>
#include "sections.h"
#include "rsc.h"
#include "main.h"
#include "go.h" 
#include "network.h"   
#include "ui.h"   
#include "nngs.h" 
#include "CGameListView.h"
#include "CPlayerListView.h"
#include "CChallengeListView.h"
#include "CChallengeArray.h"
#include "CStringArray.h" 
#include "forms.h" 
#include "CStatistics.h"   
#include "functions.h"
//void DrawGameItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
//void DrawPlayerItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
//void DrawChallengeItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
//void MainFormUpdateTerminal(Char *newStr);
static void MainFormUpdateScrollBar ();
static void MainFormScroll (Int16 linesToScroll, Boolean updateScrollbar);
static void MainFormPageScroll (WinDirectionType direction);
/*
typedef struct {
	Char	description[25];
	Char	userName[25];
	Char	password[25];
	Char	serverName[25];
	Char	serverPort[6];
	Int16	serverType;
	Boolean	supportNmatch;
	Boolean	supportSeek;
} TGoAccount; 
*/
typedef struct TGoPreference{
	Int16	accountNum;
	Int16	usedAccountID;
	IndexedColorType boardColor;
	IndexedColorType textColor;
	IndexedColorType stripColor;
	Int16	challengeTimeOut;	//in seconds
	Boolean	optOpen;
	Boolean optLooking;
	Boolean optQuiet;
	Boolean optKibitz;
	Boolean optShout;
	Int16 	optSize;
	Int16	time;				//in minutes	
	Int16	byoTime;			//in minutes
	Int16	stones;
	Int16	handicap;
	UInt8	playColor;			//BLACK or WHITE 
	Boolean	playerFilterOn;
	Int16	upperRank;
	Int16	lowerRank;
	Boolean	playerOpenOnly;
	Boolean playSound;
	Int16	byoAlert;			//in seconds
	Int16	seekConfigIndex;
	Int16	seekSizeIndex;
	Int16	seekHandicap;
	Int16	netQuality;
	Int16	netLag;
}; 
/***********************************************************************
 *
 *	Entry Points
 *
 ***********************************************************************/
Char 	*gInBuffer = NULL;
Char	*gTerminalBuffer = NULL;
UInt32	gEventTimeOut;
UInt16	gAutoOffTime = 0;
Boolean gAutoLoadList = false;
Boolean gWaitDraw = false;
Boolean gOpen = false;
Boolean	gGuestAccount=false;
Boolean gLooking = false;
UInt32	gAytSendSeconds = 0;
UInt32	gAytGetSeconds = 0;
Int16	gAytWaitCount = 0;
UInt32	gLastSendSeconds = 0;
UInt32	gLastReceiveSeconds = 0;
UInt32	gUpdateChallengeListSeconds = 0;
UInt32  gLastFlashSeconds = 0;
Int16	gGameListTab = 0;
GAMEFIELD	gGameSortedField = WR;
Boolean gGameAscending = false;
PLAYERFIELD	gPlayerSortedField = RK;
Boolean gPlayerAscending = false;
//Char	gConnectedAccountDescription[25];
//Int16 gConnectedAccountServerType;
//Boolean	gProxy = false;
//Boolean	gConnectedAccountSupportNmatch;
Char	gMyAccountName[20];
Char	gChatBuddyName[20];	//The user I'm currenttly chatting with
//Int16	gChatBuddyIndex = 0;
CChallengeArray	gChatBuddyList; //We only care about the playerName
Boolean gChatMode = true;
Boolean gNewMessage = false;
//Char	gDescription[25];
//Char	gUserName[25]; //= "mogo1";
//Char	gPassword[25]; //= "raxsk";
//Char	gServerName[25];
//Char	gServerPort[6];	
UInt16	gReturnToFormID = MainForm;	//the form id that we should return to when exit from challenge list
CGameListView	gGameList(DrawGameItem);	
CPlayerListView	gPlayerList(DrawPlayerItem);	
CChallengeListView gChallengeList(DrawChallengeItem);
CChallengeItem	gCurrentMatch;
Int16 gCurrentMain = 1;	//default main time
Int16 gCurrentByoyomi = 10;	//default byoyomi time

CPlayerItemPtr	gCurrentPlayerP = NULL;
//CBoard			*gBoard = NULL;	   
CBoardPool		gBoardPool;
CStringArray	gStoredGameList;   
BitmapPtr	whiteStoneP,blackStoneP;  
MemPtr	tickingAudioP,moveAudioP,newMessageAudioP;
TGoAccount	gAccount;
Int16	gAccountNum;
Int16	gAccountIdToBeUsed;
Int16	gAccountIdInUsed;
//Int16	gServerType = GENERIC;
//Boolean	gNmatch = false;
Boolean	gSeek = false;
TGoAccount	gAccountList[MAXACCOUNTNUM];
Int16	gUpperRank = 0;
Int16	gLowerRank = 0;
Boolean	gPlayerFilterOn = false;
Boolean	gPlayerOpenOnly = false;
Boolean gPlaySound = true;
Int16	gByoAlert = 120;			//in seconds
Int16	gNetQuality = MEDIUMQUALITY;
Int16	gNetLag = DEFAULTNETLAG;	//in seconds
//Feature-specific check
Boolean	gSupportColor = false;
Boolean gHiRes = false;
Boolean gSupportSound = false; 
Boolean gNavSupport = false;
//UInt32	gNavVersion = 0;
Int16	gSeekConfigIndex = 0;
Int16	gSeekSizeIndex = 0;
Int16	gSeekHandicap = 0;
TDefsInfo gMyStatsDefs;
CStatistics gStatistics;
//Boolean gAutomatch = false;
IndexedColorType gDefaultBackgroundColor=13,gDefaultTextColor=0;
IndexedColorType	gBoardColor,gTextColor,gStripColor;
IndexedColorType	greyColor,blackColor=1,whiteColor=0;
CustomPatternType  CustomPtn = { 0xaa, 0x55, 0xaa, 0x55, 0xaa, 0x55,
   0xaa, 0x55 }; 

Char	gRankString[12][4] =
{
	{"9p"},
	{"9p"},  
	{"1p"},
	{"9d"},
	{"5d"},
	{"1d"},
	{"1k"},
	{"5k"},
	{"10k"},
	{"15k"},
	{"20k"},
	{"30k"},
};
Char	gServerTypeString[2][8] =
{
	{"Generic"},
	{"IGS"},
};
Char	gMatchTypeString[MATCHTYPE_TOTAL][40] =
{
	{"MATCH"},
	{"TEACH"},	
	{"NMATCH"}, 
	{"AUTO"}, 
	{"PMATCH"}, 
};

//Char	gAcceptButtonLabel_C[25] = "Challenge";
//Char	gAcceptButtonLabel_A[25] = "Accept";
//Maximum error counts used in routine HandleNilEvent() in mobilego.cpp
Int16	MAXERRORCOUNT[4]={1,2,3,30};	//index coresponding to NETQUALITY

/***********************************************************************
 *
 *	Internal Constants
 *
 ***********************************************************************/

/* Define the minimum OS version we support */
#define ourMinVersion    sysMakeROMVersion(3,5,0,sysROMStageDevelopment,0)
#define kPalmOS20Version sysMakeROMVersion(2,0,0,sysROMStageDevelopment,0)


/***********************************************************************
 *
 *	Internal Functions
 *
 ***********************************************************************/
void Chat(const Char* myAccountName, const Char* chatBuddyName, const Char* chatMessage)
{
	TSgfInfo	sgf;
	TGameInfo	info;
	Char	cmdString[MAXCHATCHARACTERS];
	Boolean	chatWithOppenent = false;	//To check if I'm chatting with my opponent when I'm playing a game.
	CBoard	*workingBoard = gBoardPool.GetCurrentBoard();
	if (!workingBoard)
		return;
	//if (!gBoard)
	//	return;
	else
	{
		workingBoard->GetGameInfo(info);
		workingBoard->GetSgfInfo(sgf);
		if (info.status == INPROGRESS && info.action == PLAY)	//I'm playing a game
		{
			switch (info.color) {
				case BLACK:
					if (!StrCompare(chatBuddyName, sgf.PW) || *chatBuddyName == NULL)
						chatWithOppenent = true;
					break;
				case WHITE:
					if (!StrCompare(chatBuddyName, sgf.PB) || *chatBuddyName == NULL)
						chatWithOppenent = true;
					break;
				default:
					break;
			}
		}
		//Write message to terminal
		sprintf(cmdString,"%s: %s\n",myAccountName,chatMessage);
		WriteTerminal(cmdString);
		//Form chat command
		if (chatWithOppenent)
		{
			sprintf(cmdString, "say %s",chatMessage);
		}
		else
		{
			sprintf(cmdString, "tell %s %s",chatBuddyName,chatMessage);
		}
		//Send command
		#ifdef _DEBUG_
		WriteTerminal(cmdString);
		WriteTerminal("\n"); 
		#endif
		WriteServer(cmdString);
	}
}
void DrawTerminalLine(UInt16 y)
{
	Coord width, height;
	WinGetDisplayExtent( &width, &height );
	//ColorSet( &grey, NULL, NULL, NULL, NULL );
	WinDrawLine( 0, y, width, y );
   	//ColorUnset();
} 
void DrawChatBuddyListItem(Int16 itemNum, RectangleType *bounds, Char **itemsText)
{
	Char * name = gChatBuddyList[itemNum]->playerName;
	WinDrawChars(name,StrLen(name),bounds->topLeft.x,bounds->topLeft.y);
}

void DrawGameItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)
{
	TablePtr table = (TablePtr)tableP;
	RGBColorType	textColor, backColor;
   	RectangleType tableBounds;
   	TblGetBounds( table, &tableBounds );
   	Int16 tableWidth = tableBounds.extent.x; 

   	RectangleType rowBounds;
   	Int16 x = rowBounds.topLeft.x = bounds->topLeft.x;
   	Int16 y = rowBounds.topLeft.y = bounds->topLeft.y;
   	rowBounds.extent.y = bounds->extent.y;
   	rowBounds.extent.x = tableWidth;
   	WinPushDrawState();
	UIColorGetTableEntryRGB(UIMenuSelectedForeground,&textColor);
	UIColorGetTableEntryRGB(UIMenuSelectedFill,&backColor);

   // Specify the background color of the row	
	if( row % 2 == 0 )
      	WinSetBackColor(gStripColor);
	
	// Draw the background rectangle
	WinEraseRectangle( &rowBounds, 0 ); 

   	CGameItem *item = (CGameItem*)TblGetItemPtr( table, row, column );
   	if( !item )
    {
//      showMessage( "No item in TreeView::drawRecord()." );
    	return;
    }
   	if (gGameList.highLighted && gGameList.highLightedRow  == row)
   	{
   		//Highlight the selected row
   		WinSetTextColorRGB(&textColor,NULL);
   		WinSetBackColorRGB(&backColor,NULL);
   		WinEraseRectangle( &rowBounds, 0 ); 
   	}

	Char *displayTitle;
	Int16 textLen;
	Int16 width;
   	Int16 columnWidth;
   	Boolean fits;
   	//Game ID
   	x = 1;
   	columnWidth = 19;
 
   	fits = false;   
   	Char gameIDStr[4];
   	StrIToA(gameIDStr,item->gameID);
   	displayTitle = gameIDStr;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
   	
   	//White player
	x = 20;
	columnWidth = 44;
   	if( !item->whitePlayer)
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->whitePlayer;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else 
    	WinDrawChars( displayTitle, textLen, x, y );

	//White rank
	x += columnWidth + 1;
	columnWidth = 18;
   	if( !item->whiteRank) 
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->whiteRank;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );

	//Black player
	x = 89;
	columnWidth = 44;
   	if( !item->blackPlayer)
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->blackPlayer;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    
	//Black rank
	x += columnWidth + 1;
	columnWidth = 18;
   	if( !item->blackRank)
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->blackRank;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    WinPopDrawState();

}
void DrawGameItem2(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)
{
	TablePtr table = (TablePtr)tableP;
	RGBColorType	textColor, backColor;
   	RectangleType tableBounds;
   	TblGetBounds( table, &tableBounds );
   	Int16 tableWidth = tableBounds.extent.x; 

   	RectangleType rowBounds;
   	Int16 x = rowBounds.topLeft.x = bounds->topLeft.x;
   	Int16 y = rowBounds.topLeft.y = bounds->topLeft.y;
   	rowBounds.extent.y = bounds->extent.y;
   	rowBounds.extent.x = tableWidth;
   	WinPushDrawState();
	UIColorGetTableEntryRGB(UIMenuSelectedForeground,&textColor);
	UIColorGetTableEntryRGB(UIMenuSelectedFill,&backColor);

   // Specify the background color of the row	
	if( row % 2 == 0 )
      	WinSetBackColor(gStripColor);
	
	// Draw the background rectangle
	WinEraseRectangle( &rowBounds, 0 ); 

   	CGameItem *item = (CGameItem*)TblGetItemPtr( table, row, column );
   	if( !item )
    {
//      showMessage( "No item in TreeView::drawRecord()." );
    	return;
    }
   	if (gGameList.highLighted && gGameList.highLightedRow  == row)
   	{
   		//Highlight the selected row
   		WinSetTextColorRGB(&textColor,NULL);
   		WinSetBackColorRGB(&backColor,NULL);
   		WinEraseRectangle( &rowBounds, 0 ); 
   	}


	Char *displayTitle;
	Int16 textLen;
	Int16 width;
   	Int16 columnWidth;
   	Boolean fits;
   	//Game ID
   	x = 1;
   	columnWidth = 19;
 
   	fits = false;   
   	Char gameIDStr[4];
   	StrIToA(gameIDStr,item->gameID);
   	displayTitle = gameIDStr;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
   	
   	//Move
	x = 20;
	columnWidth = 25;
	Char colString[20];
   	fits = false;   
   	StrIToA(colString,item->moveCount);
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );

	//Size
	x += columnWidth + 1;
	columnWidth = 18;

   	fits = false;   
   	StrIToA(colString,item->size);
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );

	//Handicap
	x = 65;
	columnWidth = 8;

   	fits = false;   
   	StrIToA(colString,item->handicap);
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    
	//Komi
	x += columnWidth + 1;
	columnWidth = 24;

   	fits = false;   
	if(item->komi<0)
		sprintf(colString,"%d.%d",item->komi/10,-item->komi%10);
	else
		sprintf(colString,"%d.%d",item->komi/10,item->komi%10);
   	
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
	//BY
	x += columnWidth + 1;
	columnWidth = 15;

   	fits = false;   
   	StrIToA(colString,item->byoyomi);
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
	//FR
	x += columnWidth + 1;
	columnWidth = 13;

   	fits = false;   
	if (item->flag)
		StrCopy(colString,item->flag);
	else
		StrCopy(colString, "");
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
	//Obs
	x += columnWidth + 1;
	columnWidth = 21;

   	fits = false;   
   	StrIToA(colString,item->observer);
	displayTitle = colString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    WinPopDrawState();

}

void DrawPlayerItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)
{
	TablePtr table = (TablePtr)tableP;
	RGBColorType	textColor, backColor;
   	RectangleType tableBounds;
   	TblGetBounds( table, &tableBounds );
   	Int16 tableWidth = tableBounds.extent.x;

   	RectangleType rowBounds;
   	Int16 x = rowBounds.topLeft.x = bounds->topLeft.x;
   	Int16 y = rowBounds.topLeft.y = bounds->topLeft.y;
   	rowBounds.extent.y = bounds->extent.y;
   	rowBounds.extent.x = tableWidth;
   	WinPushDrawState();
	UIColorGetTableEntryRGB(UIMenuSelectedForeground,&textColor);
	UIColorGetTableEntryRGB(UIMenuSelectedFill,&backColor);

   // Specify the background color of the row	
	if( row % 2 == 0 )
		WinSetBackColor(gStripColor);
	
	// Draw the background rectangle
	WinEraseRectangle( &rowBounds, 0 );

   	CPlayerItem *item = (CPlayerItem*)TblGetItemPtr( table, row, column );
   	if( !item )
    {
//      showMessage( "No item in TreeView::drawRecord()." );
    	return;
    }
   	if (gPlayerList.highLighted && gPlayerList.highLightedRow  == row)
   	{
   		//Highlight the selected row
   		WinSetTextColorRGB(&textColor,NULL);
   		WinSetBackColorRGB(&backColor,NULL);
   		WinEraseRectangle( &rowBounds, 0 ); 
   	}


	Char *displayTitle;
	Int16 textLen;
	Int16 width;
   	Int16 columnWidth;
   	Boolean fits;
   	//Player
   	x = 1;
   	columnWidth = 54;
   	if( !item->playerName)
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->playerName;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
 
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
   	
   	//Rank
	x = 56;
	columnWidth = 24;
   	if(item->rank)
    {
	   	fits = false;   
		displayTitle = item->rank;
		textLen = StrLen( displayTitle);
		width = columnWidth;
		FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	    // if the full filename does not fit, then we shorten it a little
	   //  more so that we can draw an elipis after it.
	   	if( !fits )
	    {
	    	fits = false;
	      	textLen = StrLen( displayTitle);
	      	width = columnWidth - 5;
	      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	      	WinDrawChars( displayTitle, textLen, x, y );
	      	WinDrawChars( "...", 3, x+width, y );
	    }
	   	else
	    	WinDrawChars( displayTitle, textLen, x, y );
    }


	//Status
	Char	status[3];
	x = 81;
	columnWidth = 20;
   	fits = false;   
	status[0] = item->status[0];
	status[1] = item->status[1];
	status[2] = '\0';
	
	textLen = 2;
	width = columnWidth;
	//FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	//if( !fits )
    //{
    //	fits = false;
    //  	textLen = StrLen( displayTitle);
    //  	width = columnWidth - 5;
    //  	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
    //  	WinDrawChars( displayTitle, textLen, x, y );
    //  	WinDrawChars( "...", 3, x+width, y );
    //}
   	//else
    	WinDrawChars( status, textLen, x, y );
	//Idle
	x = 101;
	columnWidth = 24;
   	if( item->idle)
    {
	   	fits = false;   
		displayTitle = item->idle;
		textLen = StrLen( displayTitle);
		width = columnWidth;
		FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	   
	   // if the full filename does not fit, then we shorten it a little
	   //  more so that we can draw an elipis after it.
	   	if( !fits )
	    {
	    	fits = false;
	      	textLen = StrLen( displayTitle);
	      	width = columnWidth - 5;
	      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	      	WinDrawChars( displayTitle, textLen, x, y );
	      	WinDrawChars( "...", 3, x+width, y );
	    }
	   	else
	    	WinDrawChars( displayTitle, textLen, x, y );
	}

    
	//GameID
	x += columnWidth + 1;
	columnWidth = 24;
	if (item->gameID)
	{
		Char gameIDStr[4];
	   	Char title[6];
	   	switch (item->action)
	   	{
	   		case IDLE:
	   			StrCopy(title," ");
	   			break;
	   		case PLAY:
	   			StrCopy(title,"P");
	   			break;
	   		case OBSERVE:
	   			StrCopy(title,"O");
	   			break;
	   		default:
	   			StrCopy(title," ");
	   			break;
	   	}
	   	StrIToA(gameIDStr,item->gameID);
		StrCopy(title+1,gameIDStr);
		textLen = StrLen( title);
		width = columnWidth;
		FntCharsInWidth( title, &width, &textLen, &fits );
	    //MemPtrFree(title);
	    //MemPtrFree(gameIDStr);
	   
	   // if the full filename does not fit, then we shorten it a little
	   //  more so that we can draw an elipis after it.
	   	if( !fits )
	    {
	    	fits = false;
	      	textLen = StrLen( title);
	      	width = columnWidth - 5;
	      	FntCharsInWidth( title, &width, &textLen, &fits );
	      	WinDrawChars( title, textLen, x, y );
	      	WinDrawChars( "...", 3, x+width, y );
	    }
	   	else
	    	WinDrawChars( title, textLen, x, y );
	}
	WinPopDrawState();
}
void DrawChallengeItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)
{
	TablePtr table = (TablePtr)tableP;
	RGBColorType	textColor, backColor;
   	RectangleType tableBounds;
   	TblGetBounds( table, &tableBounds );
   	Int16 tableWidth = tableBounds.extent.x;

   	RectangleType rowBounds;
   	Int16 x = rowBounds.topLeft.x = bounds->topLeft.x;
   	Int16 y = rowBounds.topLeft.y = bounds->topLeft.y;
   	rowBounds.extent.y = bounds->extent.y;
   	rowBounds.extent.x = tableWidth;
   	WinPushDrawState();
	UIColorGetTableEntryRGB(UIMenuSelectedForeground,&textColor);
	UIColorGetTableEntryRGB(UIMenuSelectedFill,&backColor);

   // Specify the background color of the row	
	if( row % 2 == 0 )
		WinSetBackColor(gStripColor);
	
	// Draw the background rectangle
	WinEraseRectangle( &rowBounds, 0 );

   	CChallengeItem *item = (CChallengeItem*)TblGetItemPtr( table, row, column );
   	if( !item )
    {
//      showMessage( "No item in TreeView::drawRecord()." );
    	return;
    }
   	if (gChallengeList.highLighted && gChallengeList.highLightedRow  == row)
   	{
   		//Highlight the selected row
   		WinSetTextColorRGB(&textColor,NULL);
   		WinSetBackColorRGB(&backColor,NULL);
   		WinEraseRectangle( &rowBounds, 0 ); 
   	}


	Char *displayTitle;
	Int16 textLen;
	Int16 width;
   	Int16 columnWidth;
   	Boolean fits;
   	//Color
   	x = 1;
   	if( item->playerColor == BLACK)
   		WinDrawBitmap(blackStoneP,x,y+2);
   	else if( item->playerColor == WHITE)
   		WinDrawBitmap(whiteStoneP,x,y+2);
   	
   	//Player
   	x = 10;
   	columnWidth = 46;
   	if( !item->playerName)
    {
    	return;
    }

   	fits = false;   
	displayTitle = item->playerName;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   
 
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
	//Rank
	x += columnWidth + 1;
	columnWidth = 21;
   	if( item->playerRank) 
    {

	   	fits = false;   
		displayTitle = item->playerRank;
		textLen = StrLen( displayTitle);
		width = columnWidth;
		FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	   
	   // if the full filename does not fit, then we shorten it a little
	   //  more so that we can draw an elipis after it.
	   	if( !fits )
	    {
	    	fits = false;
	      	textLen = StrLen( displayTitle);
	      	width = columnWidth - 5;
	      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
	      	WinDrawChars( displayTitle, textLen, x, y );
	      	WinDrawChars( "...", 3, x+width, y );
	    }
	   	else
	    	WinDrawChars( displayTitle, textLen, x, y );
    }
    //Match setting	
    Char	timeSetting[10] = "";
    if (item->matchtype == IGS_NMATCH)
    //Should show an indicator for NMATCH
    	sprintf(timeSetting,"%3d-%2d",item->main,item->byoyomi);
    else
    	sprintf(timeSetting,"%3d-%2d",item->main,item->byoyomi);
   	x = 82;
   	columnWidth = 32;
   	fits = false;   
	displayTitle = timeSetting;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    //When	
    Char	whenString[10] = "";
    Char	ssString[3] = "";
    DateTimeType dateTime;
    //unsigned int ss = 0;
    TimSecondsToDateTime(item->timeStamp,&dateTime);
    TimeToAscii(dateTime.hour,dateTime.minute,tfColon24h,whenString);
    //ss = item->timeStamp % 60;
    StrIToA(ssString,dateTime.second);
    if (dateTime.second>=10)
    	StrCat(whenString,":");
    else
    	StrCat(whenString,":0");
    StrCat(whenString,ssString);
   	x = 115;
   	columnWidth = 36;
   	fits = false;   
	displayTitle = whenString;
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y ); 
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
    
	WinPopDrawState();

}
void FlashChatButton() 
{
	static Boolean visible = true;
	FormPtr     frm = FrmGetActiveForm();
	switch (FrmGetActiveFormID()){
		case BoardForm:
			ShowObject(frm,BoardFormChatButton);				
			if (visible)
				CtlSetLabel((ControlType*)GetObjectPtr(BoardFormChatButton),"C");
			else
				CtlSetLabel((ControlType*)GetObjectPtr(BoardFormChatButton)," "); 
			visible = !visible; 
			break;
		default:
			break;
	}
				
}
void HideChatButton()
{ 
	FormPtr     frm = FrmGetActiveForm();
	switch (FrmGetActiveFormID()){
		case BoardForm:
			HideObject(frm,BoardFormChatButton);				
			break;
		default:
			break;
	}
}

void FlashChallengeButton()
{
	static Boolean visible = true;
	FormPtr     frm = FrmGetActiveForm();
	switch (FrmGetActiveFormID()){
		case MainForm:  
			ShowObject(frm,MainFormChallengeButton);				
			if (visible)
				CtlSetLabel((ControlType*)GetObjectPtr(MainFormChallengeButton),"?");
			else
				CtlSetLabel((ControlType*)GetObjectPtr(MainFormChallengeButton)," ");
			visible = !visible; 
			break;
		case BoardForm:
			ShowObject(frm,BoardFormChallengeButton);				
			if (visible)
				CtlSetLabel((ControlType*)GetObjectPtr(BoardFormChallengeButton),"?");
			else
				CtlSetLabel((ControlType*)GetObjectPtr(BoardFormChallengeButton)," ");
			visible = !visible; 
			break; 
		case PlayerListForm:
			ShowObject(frm,PlayerListFormChallengeButton);				
			if (visible)
				CtlSetLabel((ControlType*)GetObjectPtr(PlayerListFormChallengeButton),"?");
			else
				CtlSetLabel((ControlType*)GetObjectPtr(PlayerListFormChallengeButton)," ");
			visible = !visible; 
			break;
		case GameListForm:
			ShowObject(frm,GameListFormChallengeButton);				
			if (visible)
				CtlSetLabel((ControlType*)GetObjectPtr(GameListFormChallengeButton),"?");
			else
				CtlSetLabel((ControlType*)GetObjectPtr(GameListFormChallengeButton)," ");
			visible = !visible; 
			break;
		default:
			break;
	} 
				
}
void ShowChallengeButton()
{
	FormPtr     frm = FrmGetActiveForm();
	switch (FrmGetActiveFormID()){
		case MainForm:  
			ShowObject(frm,MainFormChallengeButton);				
			CtlSetLabel((ControlType*)GetObjectPtr(MainFormChallengeButton),"?");
			break;
		case BoardForm:
			ShowObject(frm,BoardFormChallengeButton);				
			CtlSetLabel((ControlType*)GetObjectPtr(BoardFormChallengeButton),"?");
			break;
		case PlayerListForm:
			ShowObject(frm,PlayerListFormChallengeButton);				
			CtlSetLabel((ControlType*)GetObjectPtr(PlayerListFormChallengeButton),"?");
			break;
		case GameListForm:
			ShowObject(frm,GameListFormChallengeButton);				
			CtlSetLabel((ControlType*)GetObjectPtr(GameListFormChallengeButton),"?");
			break;
		default:
			break;
	}
}
void HideChallengeButton()
{
	FormPtr     frm = FrmGetActiveForm();
	switch (FrmGetActiveFormID()){
		case MainForm:  
			HideObject(frm,MainFormChallengeButton);				
			break;
		case BoardForm:
			HideObject(frm,BoardFormChallengeButton);				
			break;
		case PlayerListForm:
			HideObject(frm,PlayerListFormChallengeButton);				
			break;
		case GameListForm:
			HideObject(frm,GameListFormChallengeButton);				
			break;
		default:
			break;
	}
}
void MainFormUpdateTerminal()
{
	UInt16	scrollPos,textHeight,fieldHeight;
	RectangleType rect;
	FieldPtr	fld; 

	if ((FrmGetActiveFormID() == MainForm) && !gWaitDraw) 
	{  
		fld = (FieldPtr)GetObjectPtr (MainFormTerminalField);
		FldGetBounds(fld,&rect);
		SetFieldText(fld,gTerminalBuffer);
		FldGetScrollValues(fld,&scrollPos,&textHeight,&fieldHeight);
		if (textHeight>fieldHeight)
		{	/*
			UInt16	scrollLines;
			for (scrollLines =0;scrollLines<=textHeight-fieldHeight;scrollLines++)
			{
				UInt16 newLength = FldWordWrap(newStr,rect.extent.x);
				if (newLength<StrLen(newStr))
					newStr+=newLength;
			}
			//FldSetScrollPosition(fld,newStr-gTerminalBuffer);  
			*/
			FldScrollField(fld,textHeight-fieldHeight,winDown);
		}
		/*else
		{
			//FldSetScrollPosition(fld,0); 
			FldDrawField(fld);
		}
		
		FldSetScrollPosition(fld,newStr-gTerminalBuffer);
		*/
		FldDrawField(fld);
		MainFormUpdateScrollBar();
	}

}
void WriteTerminal(Char *inString)
{
	UInt16 inLen = StrLen(inString);
	Char	*str,*newStr;
	UInt16	length = StrLen(gTerminalBuffer);
	if (length>0) 
	{
		if (length+inLen+1<TERMINAL_BUFFER_SIZE)
		{
			str = (Char*)MemPtrNew(length+inLen+1);
			if (!str)
			{
				// Memory Allocation Failed.
				return;
			}
			StrCopy(str,gTerminalBuffer);
			StrCat(str,inString);
			newStr = inString;
		}
		else 
		{
			str = (Char*)MemPtrNew(TERMINAL_BUFFER_SIZE);
			if (!str)
			{
				// Memory Allocation Failed.
				return;
			}
			if (inLen<TERMINAL_BUFFER_SIZE)
			{
				StrCopy(str,inLen+gTerminalBuffer);
				StrCat(str,inString);
				newStr = inString;
			}
			else
			{
				StrCopy(str,inString+inLen-TERMINAL_BUFFER_SIZE);
				newStr = inString+inLen-TERMINAL_BUFFER_SIZE;
			}
		}
	}
	else
	{
		str = (Char*)MemPtrNew(inLen+1);
		if (!str)
		{
			// Memory Allocation Failed.
			return;
		}
		StrCopy(str,inString);
		newStr = inString;
	}
	MemPtrFree(gTerminalBuffer);
	gTerminalBuffer = str;
	MainFormUpdateTerminal();
} 

static void HandleNilEvent()
{
	static UInt32 lastSeconds = 0;
	static Boolean connected = false;	//status of connect button
	static Int16	errCount = 0;
	Err		error = errNone;
	UInt32	currentSeconds;
	currentSeconds = TimGetSeconds();
	//TGameInfo	info;
	Char	msg[100];
	#ifdef _DEBUG_NET_ERROR_
	Char	errorString[50];
	#endif
	CBoard	*workingBoard = gBoardPool.GetCurrentBoard();
	if (currentSeconds>lastSeconds && workingBoard)
	{
		/*
		TGameInfo info;
		workingBoard->GetGameInfo(info);
		if (info.status==INPROGRESS)
			workingBoard->UpdateTimer(currentSeconds - lastSeconds);
		*/
		gBoardPool.UpdateTimer(currentSeconds - lastSeconds);
		lastSeconds = currentSeconds;
	}
	if (IsLoggedIn())
	{
		if (!gAutoOffTime)
			gAutoOffTime = SysSetAutoOffTime(0);	//Disable Aoto Timeoff 
		switch (FrmGetActiveFormID()){
			case BoardForm:
				if (!CtlGetValue((ControlType*)GetObjectPtr(BoardFormConnected)))
					CtlSetValue((ControlType*)GetObjectPtr(BoardFormConnected), 1);
				break;
			case MainForm:
				if (!gSeek)
					CtlSetValue((ControlType*)GetObjectPtr(MainFormSeekPushButton), 0);
				else
					CtlSetValue((ControlType*)GetObjectPtr(MainFormSeekPushButton), 1);
				if (!gWaitDraw && !connected) 
				{
					CtlSetValue((ControlType*)GetObjectPtr(MainFormLoginButton),1);
					CtlSetLabel((ControlType*)GetObjectPtr(MainFormLoginButton),"Logout");
					connected = true;
					if (gNavSupport)
						FrmGlueNavObjectTakeFocus(FrmGetActiveForm(),MainFormBoardButton);
				}
				break;
		}
	}
	else 
	{
		//Reset Seek flag
		gSeek = false;
		//Update game status to INTERRUPTED if it's in the middlw of processing or scoring
		/*
		if (workingBoard)
		{
			workingBoard->GetGameInfo(info);
			//if(info.status != GAMEOVER && info.status != PREPARING) 
			if(info.status == INPROGRESS || info.status == SCORING) 
			{
				unsigned char status = INTERRUPTED;
				workingBoard->UpdateGameInfo(0,NULL,&status,NULL,NULL);
			}
		}
		*/
		gBoardPool.InterruptGame();
		//restore previous Aoto Timeoff setting
		if (gAutoOffTime)
			gAutoOffTime = SysSetAutoOffTime(gAutoOffTime);	
		//reset errCount for network connection checking
		errCount = 0;
		switch (FrmGetActiveFormID()){
			case BoardForm:
				if (CtlGetValue((ControlType*)GetObjectPtr(BoardFormConnected)))
				{
					CtlSetValue((ControlType*)GetObjectPtr(BoardFormConnected), 0);
					FrmAlert(ConnectionLostAlert);
				}
				break;
			case MainForm:
				if (!gSeek)
					CtlSetValue((ControlType*)GetObjectPtr(MainFormSeekPushButton), 0);
				if (!gWaitDraw && connected) 
				{
					StrCopy(msg,"Disconnected with server.\n");
					//StrCat(msg,gConnectedAccountDescription);
					//StrCat(msg,gAccountList[gAccountIdInUsed].description);
					//StrCat(msg,"\n");
					WriteTerminal((Char*)msg);
					CtlSetValue((ControlType*)GetObjectPtr(MainFormLoginButton),0);
					CtlSetLabel((ControlType*)GetObjectPtr(MainFormLoginButton),"Login");
					connected = false;
					if (gNavSupport)
						FrmGlueNavObjectTakeFocus(FrmGetActiveForm(),MainFormLoginButton);
				}
				break;
		}
	} 
		/*
		if (!EvtEventAvail())
	   	{
	   		ProcessNetworkData();
	   	}
	   	*/
	if (!EvtEventAvail() && IsConnected())
   	{
   		ProcessNetworkData();
   		if (IsLoggedIn())
			gStatistics.UpdateDuration(currentSeconds);
   	}
	
   	//Check network connection
	//Check error count
	if (errCount > MAXERRORCOUNT[gNetQuality] && IsLoggedIn())	
	{	//Reach the maxum error count, we should disconnect it,
		//and give user the chance to re connect.
		DisconnectNetwork();
		errCount = 0;
	}
	//currentSeconds = TimGetSeconds();
	/* Old approach to check if connection was broken
	if(gLastSendSeconds>0 && (currentSeconds-gLastSendSeconds>HEARTBEATINTERVAL+SERVERRESPONSEDELAY) && IsConnected())	//Idle for over 2 minutes
		DisconnectServer();
	else if(gLastSendSeconds>0 && currentSeconds-gLastSendSeconds>HEARTBEATINTERVAL && IsConnected())	//Idle for over 1 minutes
			WriteServer("AYT");	//Send hart beat AYT
	*/ 
	if(gLastSendSeconds>0 && currentSeconds-gLastSendSeconds>HEARTBEATINTERVAL*(1+errCount) && IsLoggedIn())	//Idle for over 1 heartbeat interval
	//if(gLastSendSeconds>0 && currentSeconds-gLastSendSeconds>HEARTBEATINTERVAL && IsLoggedIn())	
	{	//Idle for over 1 heartbeat interval
		#ifdef _DEBUG_NET_ERROR_
		sprintf(errorString,"gNetQuality=%d, MaxErrorCount=%d",gNetQuality,MAXERRORCOUNT[gNetQuality]);
		WriteTerminal(errorString);
		WriteTerminal("\n"); 
		sprintf(errorString,"errCount=%d",errCount);
		WriteTerminal(errorString);
		WriteTerminal("\n");
		WriteTerminal("AYT"); 
		WriteTerminal("\n");
		#endif
		error = WriteServer("AYT");	//Send heart beat AYT
		//Save statistics data after every time sending a heart beat
		gStatistics.SavePrefs();
		//gAytSendSeconds = gLastSendSeconds;
		gAytSendSeconds = TimGetSeconds();
		gAytWaitCount++;
		if (error != errNone)	
		{
			if (error == netErrSocketClosedByRemote)
			{
				DisconnectServer();
				errCount = 0;
			}
			else
			{
				//Unable to write to server for other reasons, increase error count
				#ifdef _DEBUG_NET_ERROR_
				WriteTerminal("AYT:: Failed To Send");
				WriteTerminal("\n");
				#endif 
			}
		}
		/***********************************************************************
		else	//No problem to write to server
		{
			//We have to check if there is response from server since we sent ATY
			if ((gLastSendSeconds > gLastReceiveSeconds) \
			&& (gLastSendSeconds - gLastReceiveSeconds >= HEARTBEATINTERVAL+gNetLag))
			{
				//It's over one heartbeat interval time that there is no response from server 
				errCount++;
				#ifdef _DEBUG_NET_ERROR_
				WriteTerminal("AYT:: No Response");
				WriteTerminal("\n");
				#endif
			}
			else
			{
				errCount = 0;
			}
		}
		if (errCount >= (1+gNetQuality) && IsConnected())	
		{	//Reach the maxum error count, we should disconnect it,
			//and give user the chance to re connect.
			DisconnectServer();
			errCount = 0;
			//gLastSendSeconds = currentSeconds;
		}
		***********************************************************************/
	}	//Check Idle
	//Check if we got ATY response
	if (IsLoggedIn()) 
	{
		//if (gAytWaitCount>0 && gAytSendSeconds>gAytGetSeconds && (TimGetSeconds()-gAytSendSeconds)> (UInt32)gNetLag)
		if (gAytWaitCount>= gNetLag)
		{
			//We didn't get an AYT response within expected time (with net lag being considerred)
			errCount++;
			gAytWaitCount = 1;
			//It's very IMPORTANT to update gAytGetSeconds to gAytSendSeconds.
			//If don't do this, errCount will be increased every time this function is called
			gAytGetSeconds = gAytSendSeconds;	
			#ifdef _DEBUG_NET_ERROR_
			WriteTerminal("AYT:: No Response");
			WriteTerminal("\n");
			sprintf(errorString,"errCount=%d",errCount);
			WriteTerminal(errorString);
			WriteTerminal("\n");
			#endif
		}
		else if ((gAytWaitCount>0 && gAytSendSeconds<gAytGetSeconds) ||gAytWaitCount<=0)
		{
			//We got an AYT response after we had sent one AYT
			//Looks like network is fine
			//Clear error count
			errCount = 0;
			gAytWaitCount = 0;
			#ifdef _DEBUG_NET_ERROR_
			sprintf(errorString,"errCount=%d\n",errCount);
			WriteTerminal(errorString);
			WriteTerminal("\n");
			#endif
		}
		#ifdef _DEBUG_NET_ERROR_
		else
		{
			sprintf(errorString,"gAytWaitCount=%d",gAytWaitCount);
			WriteTerminal(errorString);
			WriteTerminal("\n");	
		}
		#endif		
	}
	 
	
	//Check and remove old challenge once every 10 seconds
	if ((currentSeconds - gUpdateChallengeListSeconds) > CHALLENGEUPDATEINTERVAL)
	{	
		if (gChallengeList.TimeoutItem(currentSeconds, CHALLENGEEXPIRETIME))
			FrmUpdateForm(ChallengeListForm,frmReloadUpdateCode);
			
		gUpdateChallengeListSeconds = currentSeconds;
	}
	//Update challenge button based on challenge list status
	if (gChallengeList.HasNewItem())
	{
		//if (currentSeconds - gLastFlashSeconds > 1)
		//{
			FlashChallengeButton();
			gLastFlashSeconds = currentSeconds;
		//}
	}
	else if(gChallengeList.HasItem())
		ShowChallengeButton();
	else 
		HideChallengeButton();
	//Update chat button based on whether we got new message 	
	if (gNewMessage)
		FlashChatButton();
	else
		HideChatButton();
	//Update Statistics Form every seconds
	if (FrmGetActiveFormID()==StatisticsForm)
		FrmUpdateForm(StatisticsForm,frmReloadUpdateCode);
	if (GameInfoForm == FrmGetActiveFormID())
		FrmUpdateForm(GameInfoForm,frmRedrawUpdateCode); 
		
	return;
} 


/***********************************************************************
 *
 * FUNCTION:    MainFormDoCommand
 *
 * DESCRIPTION: This routine performs the menu command specified.
 *
 * PARAMETERS:  command  - menu item id
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormDoCommand(UInt16 command)
{
	Boolean handled = false;
	//FormType * pForm;

	switch (command) {
		case MainOptionsAboutStarterApp:
		/*
			pForm = FrmInitForm(AboutForm);
			FrmDoDialog(pForm);					// Display the About Box.
			FrmDeleteForm(pForm);
		*/
			FrmPopupForm(AboutForm);
			handled = true;
			break;
		case MainOptionsAccounts:
			FrmGotoForm(AccountListForm);
			handled = true;
			break;
		case MainCommandLogin:
			Login();
			FrmUpdateForm(MainForm,frmReloadUpdateCode);
			handled = true;
			break;
		case MainCommandLogout:
			Logout();
			FrmUpdateForm(MainForm,frmReloadUpdateCode);
			handled = true;
			break;
		case MainCommandStats:
			//if (IsConnected())
			if (IsConnected() && !gGuestAccount)
				WriteServer("stats");
			break;
		case MainCommandStored:
			gStoredGameList.Clear();
			//if (IsConnected())
			if (IsConnected() && !gGuestAccount)
				WriteServer("stored");
			break;
		case MainOptionsColors:
            FrmPopupForm(ColorPrefForm);
			handled = true;
			break;
		case MainOptionsSound:
            FrmPopupForm(SoundPrefForm);
			handled = true;
			break;
		case MainOptionsDisconnect:
            DisconnectNetwork();
            FrmUpdateForm(MainForm,frmReloadUpdateCode);
			handled = true;
			break;
		case MainOptionsNetwork:
            FrmPopupForm(NetworkPrefForm);
			handled = true;
			break;
		case MainCommandDefault:
			//This menu is only available when logged into IGS
			if (IsLoggedIn() && gAccountList[gAccountIdInUsed].serverType == IGS && !gGuestAccount) 
            	FrmPopupForm(DefaultForm);
			handled = true;
			break;
		case MainCommandTeach:
			//This menu is only available for Prime Edition and when logged in			
			#ifndef	_PE_
			FrmAlert(FeatureUnavailableAlert);
			#else
			if (IsLoggedIn() && !gGuestAccount)  
			//Submit TEACH command & initial a new game
			{
				WriteServer("teach 19");
				UInt8 action = TEACH;
				UInt8 status = PREPARING;
		  		CBoard	*workingBoard; 
		  		workingBoard = gBoardPool.GetNewBoard();
		  		if (!workingBoard) 
		  		{
		  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
		  			workingBoard = gBoardPool.GetInactiveBoard();
		  			if (!workingBoard)
		  			{
		  				//No inactive board available. All boards are in process, 
		  				//then we have to force current board to be closed. 
		  				workingBoard = gBoardPool.GetCurrentBoard();
						workingBoard->Unobserve();	//unobserve current game in case we are observing a game
		  			}
					//close the board, no need to remember the game id currently being observed
					//because it is not going to be ovwelayed by a matching game.
					gBoardPool.CloseBoard(workingBoard);	
		  			workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
		  			if (!workingBoard)	//For unknown reason, we are not able to get a new board, maybe my program goes wrong
		  			{
						handled = true;
						break;		  			
		  			}	 
		  		}
		  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
		  		workingBoard->UpdateGameInfo(0,&action,&status,NULL,NULL); //Init working board, don't know game id yet for this moment
				gBoardPool.matchingGameID = 0;
				gBoardPool.waitNewGameID = true;
				if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
					FrmGotoForm(BoardForm);
				else
					FrmUpdateForm(BoardForm,frmReloadUpdateCode);
			}	
			else
			//	Offline Replay	
			{
				CBoard	*workingBoard = gBoardPool.GetNewBoard();
				if (workingBoard)
				{
			  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
					workingBoard->Load("/test.sgf");
			  		//workingBoard->UpdateGameInfo(0,&action,&status,NULL,NULL); //Init working board, don't know game id yet for this moment
					//gBoardPool.matchingGameID = 0;
					//gBoardPool.waitNewGameID = true;
					if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
						FrmGotoForm(BoardForm);
					else
						FrmUpdateForm(BoardForm,frmReloadUpdateCode);
				}
			}		
			#endif
			handled = true;
			break;
		case MainStatisticsOnline:
			FrmGotoForm(StatisticsForm);
			handled = true;
			break;
	} 
	
	return handled;
}
/***********************************************************************
 *
 * FUNCTION:    MainFormUpdateScrollBar
 *
 * DESCRIPTION: This routine update the scroll bar.
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *			Name	Date		Description
 *			----	----		-----------
 *			art	07/01/96	Initial Revision
 *			gap	11/02/96	Fix case where field and scroll bars get out of sync
 *
 ***********************************************************************/
static void MainFormUpdateScrollBar ()
{
	UInt16 scrollPos;
	UInt16 textHeight;
	UInt16 fieldHeight;
	Int16 maxValue;
	FieldPtr fld;
	ScrollBarPtr bar;

	fld = (FieldPtr)GetObjectPtr (MainFormTerminalField);
	bar = (ScrollBarPtr)GetObjectPtr (MainFormTerminalScrollBar);

	FldGetScrollValues (fld, &scrollPos, &textHeight,  &fieldHeight);

	if (textHeight > fieldHeight)
	{
		// On occasion, such as after deleting a multi-line selection of text,
		// the display might be the last few lines of a field followed by some
		// blank lines.  To keep the current position in place and allow the user
		// to "gracefully" scroll out of the blank area, the number of blank lines
		// visible needs to be added to max value.  Otherwise the scroll position
		// may be greater than maxValue, get pinned to maxvalue in SclSetScrollBar
		// resulting in the scroll bar and the display being out of sync.
		maxValue = (textHeight - fieldHeight) + FldGetNumberOfBlankLines (fld);
	}
	else if (scrollPos)
		maxValue = scrollPos;
	else
		maxValue = 0;

	SclSetScrollBar (bar, scrollPos, 0, maxValue, fieldHeight-1);
}


/***********************************************************************
 *
 * FUNCTION:    MainFormScroll
 *
 * DESCRIPTION: This routine scrolls the memo edit view a page or a
 *              line at a time.
 *
 * PARAMETERS:  linesToScroll - the number of lines to scroll,
 *						positive for winDown,
 *						negative for winUp
 *					 updateScrollbar - force a scrollbar update?
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *			Name	Date		Description
 *			----	----		-----------
 *			art	7/1/96	Initial Revision
 *			grant 2/4/99	Use MainFormUpdateScrollBar()
 *
 ***********************************************************************/
static void MainFormScroll (Int16 linesToScroll, Boolean updateScrollbar)
{
	UInt16				blankLines;
	FieldPtr			fld;

	fld = (FieldPtr)GetObjectPtr (MainFormTerminalField);
	blankLines = FldGetNumberOfBlankLines (fld);

	if (linesToScroll < 0)
		FldScrollField (fld, -linesToScroll, winUp);
	else if (linesToScroll > 0)
		FldScrollField (fld, linesToScroll, winDown);

	// If there were blank lines visible at the end of the field
	// then we need to update the scroll bar.
	if (blankLines || updateScrollbar)
	{
		ErrNonFatalDisplayIf(blankLines && linesToScroll > 0, "blank lines when scrolling winDown");

		MainFormUpdateScrollBar();
	}
}


static void MainFormPageScroll (WinDirectionType direction)
{
	UInt16 linesToScroll;
	FieldPtr fld;

	fld = (FieldPtr)GetObjectPtr (MainFormTerminalField);

	if (FldScrollable (fld, direction))
	{
		linesToScroll = FldGetVisibleLines (fld) - 1;

		if (direction == winUp)
			linesToScroll = -linesToScroll;

		MainFormScroll(linesToScroll, true);

		return;
	}

/*
	// Move to the next or previous memo.
	if (direction == winUp)
	{
		seekDirection = dmSeekBackward;
		EditScrollPosition = maxFieldTextLen;
	}
	else
	{
		seekDirection = dmSeekForward;
		EditScrollPosition = 0;
	}
*/

}


/***********************************************************************
 *
 * FUNCTION:    MainFormHandleEvent
 *
 * DESCRIPTION: This routine is the event handler for the 
 *              "MainForm" of this application.
 *
 * PARAMETERS:  pEvent  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormHandleEvent(EventType* pEvent)
{
	Boolean 	handled = false;
	FormType* 	pForm;
	FieldType*  pField;
	//Char		*newFormTitle; 
   	MemHandle	TextH;
   	Char*		h1=NULL;
	Char	cmdOptionString[20],cmdString[MAXCHATCHARACTERS];
	int	numChoices = 0;
	pForm = FrmGetActiveForm(); 
	//TGoAccount	account = gAccountList[gAccountIdToBeUsed];
	switch (pEvent->eType) {
		case menuEvent:
			return MainFormDoCommand(pEvent->data.menu.itemID);
      	case winEnterEvent:
   			if (pEvent->data.winEnter.enterWindow == (WinHandle) FrmGetFormPtr(MainForm))
            	gWaitDraw = false; // we can update button
         	break;
      	case winExitEvent:
         	if (pEvent->data.winExit.exitWindow == (WinHandle) FrmGetFormPtr(MainForm))
            	gWaitDraw = true; // stop updating button
         	break;
      	case keyDownEvent:
         	switch (pEvent->data.keyDown.chr)
         	{
				case vchrPageUp:
					MainFormPageScroll (winUp);
					handled = true;
					break;
				case vchrPageDown:
					MainFormPageScroll (winDown);
					handled = true;
					break;
            	case LF:
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(MainFormInputField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get field's text
						if (h1)
						{
		 					//Talk to opponent or send command depending on Talk push button
		 					if (CtlGetValue((ControlType*)GetObjectPtr(MainFormTalkCheckBox)))
		 					{	//Chat with current chat buddy
		 						if (*h1)	//Type something in Input field
		 						{
		 						/*
			 						sprintf(cmdString,"%s: %s\n",gMyAccountName,h1);
			 						WriteTerminal(cmdString);
			 						sprintf(cmdString,"tell %s %s",gChatBuddyName,h1);
		 						*/
		 							Chat(gMyAccountName,gChatBuddyName,h1);
		 						}
		 					}       
		 					else
		 					{	//Send command
		 						if (*h1) 
		 							sprintf(cmdString,"%s",h1);
		 						else
			 						sprintf(cmdString,"%s","\n");	//Input LF only
								#ifdef _DEBUG_
								WriteTerminal(cmdString);
								WriteTerminal("\n"); 
								#endif
			 					WriteServer(cmdString);
		 					}
		 					//Clear input field
							cmdString[0] = NULL;
			 				MemHandleUnlock(TextH);
							DrawFieldText((FieldType*)GetObjectPtr(MainFormInputField),cmdString);
							FrmSetFocus(pForm,FrmGetObjectIndex(pForm,MainFormInputField));			
						}
					}
					handled = true;
            		break;
				default:
					FieldAttrType	attr;
					pField = (FieldType*)GetObjectPtr(MainFormInputField);
					FldGetAttributes (pField, &attr);
					//Auto focus on input field whenever user type a valid character
					if (!attr.hasFocus && TxtCharIsValid(pEvent->data.keyDown.chr))
					{
						FrmSetFocus(pForm,FrmGetObjectIndex(pForm,MainFormInputField));
					}
         			break;
         	}
         	break;					
		case frmOpenEvent:
			gNewMessage = false;
		case frmUpdateEvent:
			//pForm = FrmGetActiveForm();
			FrmDrawForm(pForm);
			//Set ChatBuddy list
			LstSetDrawFunction((ListType*)GetObjectPtr(MainFormChatBuddyList),DrawChatBuddyListItem);
			numChoices = gChatBuddyList.Size();
			LstSetListChoices((ListType*)GetObjectPtr(MainFormChatBuddyList),NULL,numChoices);
			//Set option push button
		#ifndef	_BE_
			if (gLooking)
				CtlSetValue((ControlType*)GetObjectPtr(MainFormLookingPushButton),1);
			else if (gOpen)
				CtlSetValue((ControlType*)GetObjectPtr(MainFormOpenPushButton),1);
			else
				CtlSetValue((ControlType*)GetObjectPtr(MainFormClosePushButton),1);
			if (gChatMode)
				CtlSetValue((ControlType*)GetObjectPtr(MainFormTalkCheckBox),1);
			else   
				CtlSetValue((ControlType*)GetObjectPtr(MainFormTalkCheckBox),0);
		#else
			HideObject( pForm, MainFormLookingPushButton);
			HideObject( pForm, MainFormOpenPushButton);
			HideObject( pForm, MainFormClosePushButton);
		#endif
			
			CtlSetLabel((ControlType*)GetObjectPtr(MainFormChatBuddyTrigger),gChatBuddyName);
			 
			//Clear input field
			//cmdString[0] = NULL; 
			//DrawFieldText((FieldType*)GetObjectPtr(MainFormInputField),cmdString);
			//FrmSetFocus(pForm,FrmGetObjectIndex(pForm,MainFormInputField));			
			//if (!IsConnected())
			if (!IsLoggedIn()) 
			{
				//Push the login button and change button title to "Login"
				CtlSetValue((ControlType*)GetObjectPtr(MainFormLoginButton),0);
				CtlSetLabel((ControlType*)GetObjectPtr(MainFormLoginButton),"Login");
				//Change form title to the description of default account
				//StrCopy(gConnectedAccountDescription,gAccountList[gAccountIdToBeUsed].description);
				//gConnectedAccountServerType = gAccountList[gAccountIdToBeUsed].serverType;
				//gConnectedAccountSupportNmatch = gAccountList[gAccountIdToBeUsed].supportNmatch;
				gAccountIdInUsed = gAccountIdToBeUsed;
				if (gNavSupport)
					FrmGlueNavObjectTakeFocus(FrmGetActiveForm(),MainFormLoginButton);
			}
			else
			{
				//Push the login button and change button title to "Disconnect"
				CtlSetValue((ControlType*)GetObjectPtr(MainFormLoginButton),1); 
				CtlSetLabel((ControlType*)GetObjectPtr(MainFormLoginButton),"Logout");
				if (gNavSupport)
					FrmGlueNavObjectTakeFocus(FrmGetActiveForm(),MainFormBoardButton);
			}
			//Set Seek push button 	
			#ifndef _BE_
			if (gAccountList[gAccountIdInUsed].serverType == IGS && gAccountList[gAccountIdInUsed].supportNmatch)
			{
				ShowObject( pForm, MainFormSeekPushButton );
				if (gSeek)
					CtlSetValue((ControlType*)GetObjectPtr(MainFormSeekPushButton),1);
				else
					CtlSetValue((ControlType*)GetObjectPtr(MainFormSeekPushButton),0);
			}
			else
				HideObject( pForm, MainFormSeekPushButton );
			#endif
	  		/*
	  		newFormTitle = (Char*) MemPtrNew(StrLen(gConnectedAccountDescription)+1);
	  		if (newFormTitle)
	  		{
		  		StrCopy(newFormTitle,gConnectedAccountDescription);
		  		FrmCopyTitle(FrmGetActiveForm(),newFormTitle);
		  		MemPtrFree(newFormTitle);
	  		}
	  		*/
	  		FrmCopyTitle(FrmGetActiveForm(),gAccountList[gAccountIdInUsed].description);
			MainFormUpdateTerminal();
			DrawTerminalLine(28);
			handled = true;
			break;
		case ctlSelectEvent:  
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case MainFormLoginButton:  
					if (!IsLoggedIn())	 
						Login();
					else
						Logout();
					FrmUpdateForm(MainForm,frmReloadUpdateCode);
					break;
				case MainFormGamesButton:
					if (gAutoLoadList || !gGameList.HasItem())
					{
						gGameList.Flush();
						WriteServer("games");
					} 
					FrmGotoForm(GameListForm);
					break; 
				case MainFormPlayersButton:
				
					if (gAutoLoadList || !gPlayerList.HasItem())
					{
						if (gLowerRank == 0 && gUpperRank == 0)	//No rank specified
							StrCopy(cmdOptionString,"all");
						else
						{
							StrCopy(cmdOptionString,gRankString[gLowerRank]);
							StrCat(cmdOptionString,"-");
							StrCat(cmdOptionString,gRankString[gUpperRank]);
						}
						if (gPlayerOpenOnly)
							StrCat(cmdOptionString," o");
						StrCopy(cmdString,"who ");
						if (gPlayerFilterOn)
							StrCat(cmdString,cmdOptionString);
						gPlayerList.Flush();
						WriteServer(cmdString);
					}
						FrmGotoForm(PlayerListForm);
					break;
				case MainFormBoardButton:
					FrmGotoForm(BoardForm);
					break;
				case MainFormChallengeButton:
					gReturnToFormID = MainForm;
					FrmGotoForm(ChallengeListForm);
					break;	
				case MainFormClosePushButton:
					WriteServer("toggle open off");
					gOpen = false;
					gLooking = false;
					break;
				case MainFormOpenPushButton:
					WriteServer("toggle looking off");
					WriteServer("toggle open on");
					gOpen = true;
					gLooking = false;
					break;
				case MainFormLookingPushButton: 
					WriteServer("toggle looking on");
					gOpen = true;
					gLooking = true;
					break;
				case MainFormTalkCheckBox: 
					gChatMode = !gChatMode;
					break;
         		case MainFormChatBuddyTrigger:
         			LstSetListChoices((ListType*)GetObjectPtr(MainFormChatBuddyList),NULL,gChatBuddyList.Size());
         			handled = false;
         			break;
         		case MainFormSeekPushButton:
		            FrmPopupForm(SeekForm);
					handled = false;
					break;
/*
         			//gSeek = !gSeek;
         			if (!gSeek)
         			{
         				sprintf(cmdString,"seek entry %d 19 %d %d 0",gSeekConfigIndex,gSeekHandicap,gSeekHandicap);
         				WriteServer(cmdString);
							#ifdef _DEBUG_
							WriteTerminal(cmdString);
							WriteTerminal("\n"); 
							#endif
         			}
         			else
         			{
         				sprintf(cmdString,"seek entry_cancel",gSeekConfigIndex);
         				WriteServer(cmdString);
							#ifdef _DEBUG_
							WriteTerminal(cmdString);
							WriteTerminal("\n"); 
							#endif
         			}
         			handled = false;
         			break;
*/         		
				default:
					break;
			} 
			break;
        case popSelectEvent:
        	switch (pEvent->data.popSelect.listID)
        	{
        		Int16	itemNum;
        		case MainFormChatBuddyList:
        			itemNum = pEvent->data.popSelect.selection;
					StrCopy(gChatBuddyName,gChatBuddyList[itemNum]->playerName);	//Update current chat buddy
        			CtlSetLabel((ControlType*)GetObjectPtr(MainFormChatBuddyTrigger),gChatBuddyName);
        			handled = true;
        			break;
        		default:
        			break;
        	}
        	break;
		case sclRepeatEvent:
			MainFormScroll (pEvent->data.sclRepeat.newValue -
						pEvent->data.sclRepeat.value, false);
			handled = true;
			break;
		case frmObjectFocusTakeEvent:
			switch(pEvent->data.ctlSelect.controlID) {
				case MainFormBoardButton:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,MainFormBoardButton));
					handled = true;
				case MainFormLoginButton:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,MainFormLoginButton));
					handled = true;
				default:
					break;
			}
			break;
		case fldChangedEvent:
			if (pEvent->data.fldChanged.fieldID == MainFormTerminalField)
			{
				MainFormUpdateScrollBar();
				handled = true;
			}
			break;
		default: 
			break; 
	}
	 
	return handled;
}


/***********************************************************************
 *
 * FUNCTION:    AppHandleEvent
 *
 * DESCRIPTION: This routine loads form resources and set the event
 *              handler for the form loaded.
 *
 * PARAMETERS:  event  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean AppHandleEvent(EventType* pEvent)
{
	UInt16 		formId;
	FormType* 	pForm;
	Boolean		handled = false;

	if (pEvent->eType == frmLoadEvent) {
		// Load the form resource.
		formId = pEvent->data.frmLoad.formID;
		
		pForm = FrmGetFormPtr(formId);
		if (!pForm)
			pForm = FrmInitForm(formId);
		FrmSetActiveForm(pForm);

		// Set the event handler for the form.  The handler of the currently
		// active form is called by FrmHandleEvent each time is receives an
		// event.
		switch (formId) {
			case MainForm:
				FrmSetEventHandler(pForm, MainFormHandleEvent);
				break;
			case GameListForm:
				FrmSetEventHandler(pForm, GameListFormHandleEvent);
				break;
			case PlayerListForm:
				FrmSetEventHandler(pForm, PlayerListFormHandleEvent);
				break;
			case ChallengeListForm:
				FrmSetEventHandler(pForm, ChallengeListFormHandleEvent);
				break;
			case BoardForm:
				FrmSetEventHandler(pForm, BoardFormHandleEvent);
				break;
			case AccountListForm:
				FrmSetEventHandler(pForm, AccountListFormHandleEvent);
				break;
			case AccountEditForm:
				FrmSetEventHandler(pForm, AccountEditFormHandleEvent);
				break;
			case GameInfoForm:
				FrmSetEventHandler(pForm, GameInfoFormHandleEvent);
				break;
         	case ColorPrefForm:
            	FrmSetEventHandler(pForm, ColorPrefFormHandleEvent);
            	break;
         	case SoundPrefForm:
            	FrmSetEventHandler(pForm, SoundPrefFormHandleEvent);
            	break;
         	case PlayerFilterForm:
            	FrmSetEventHandler(pForm, PlayerFilterFormHandleEvent);
            	break;
         	case MatchForm:
            	FrmSetEventHandler(pForm, MatchFormHandleEvent);
            	break;
         	case StoredForm:
            	FrmSetEventHandler(pForm, StoredFormHandleEvent);
            	break;
         	case PlayerViewForm:
            	FrmSetEventHandler(pForm, PlayerViewFormHandleEvent);
            	break;
         	case AboutForm:
            	FrmSetEventHandler(pForm, AboutFormHandleEvent);
            	break;
         	case RegisterForm:
            	FrmSetEventHandler(pForm, RegisterFormHandleEvent);
            	break;
         	case SeekForm:
            	FrmSetEventHandler(pForm, SeekFormHandleEvent);
            	break;
         	case ProxyForm:
            	FrmSetEventHandler(pForm, ProxyFormHandleEvent);
            	break;
         	case NetworkPrefForm:
            	FrmSetEventHandler(pForm, NetworkPrefFormHandleEvent);
            	break;
         	case PasswordForm:
            	FrmSetEventHandler(pForm, PasswordFormHandleEvent);
            	break;
         	case TitleForm:
            	FrmSetEventHandler(pForm, TitleFormHandleEvent);
            	break;
         	case DefaultForm:
            	FrmSetEventHandler(pForm, DefaultFormHandleEvent);
            	break;
         	case StatisticsForm:
            	FrmSetEventHandler(pForm, StatisticsFormHandleEvent);
            	break;
			default:
				break;
		}
		handled = true;
	}
	
	return handled;
}


/***********************************************************************
 *
 * FUNCTION:     AppStart
 *
 * DESCRIPTION:  Get the current application's preferences.
 *
 * PARAMETERS:   nothing
 *
 * RETURNED:     Err value errNone if nothing went wrong
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Err AppStart(void)
{
	TGoPreference	goPrefs;
	TGoAccount accountPrefs;
	UInt16	prefsSize = sizeof(TGoPreference);
	UInt16	accountPrefsSize = sizeof(TGoAccount);
    Int16 prefsVersion;
    UInt32 version;
	UInt32  density,depth;
    //Boolean enableColor;
	// 5 Way Navigation support?
	if (FtrGet(hsFtrCreator, hsFtrIDNavigationSupported, &version) == 0)
	{
		gNavSupport = true;
	}		
	//[Jin Lin] color support?
	FtrGet (sysFtrCreator, sysFtrNumROMVersion, &version);
	if (version > 0x03503000)
	{
		RGBColorType		rgb;
	 	WinScreenMode (winScreenModeGet, NULL, NULL, NULL, &gSupportColor);
	 	if (gSupportColor)
	 	{
		    //Black and white
		    rgb.r = 0;
		    rgb.g = 0;
		    rgb.b = 0;
		    blackColor = WinRGBToIndex(&rgb);
		    rgb.r = 255;
		    rgb.g = 255;
		    rgb.b = 255;
		    whiteColor = WinRGBToIndex(&rgb);
		    rgb.r = 119;
		    rgb.g = 119;
		    rgb.b = 119;
		    greyColor = WinRGBToIndex(&rgb);
			//Default board color
		    rgb.r = 204;
		    rgb.g = 153;
		    rgb.b = 51;            
		    gDefaultBackgroundColor = WinRGBToIndex(&rgb);
			//Default text color on transparent chatting window
	        rgb.r = 255;
	        rgb.g = 255;
	        rgb.b = 0;  
	        gDefaultTextColor = WinRGBToIndex(&rgb);         
		} 
	}
	//sample Sound support?
	if (version > 0x05000000)	
		gSupportSound = true;
	//[Jin Lin] hi-res support?
	FtrGet (sysFtrCreator, sysFtrNumWinVersion, &version);
	if (version >= 4)
	{
        WinScreenGetAttribute(winScreenDensity,&density);
        WinScreenGetAttribute(winScreenDepth,&depth);
        if (density>=144 && depth>=8)
        	gHiRes = true;
	}
	if (gHiRes) 
	{
		if (depth!=8)	//Set screen depth to 8 if it isn't.
		{
			depth = 8;
			WinScreenMode(winScreenModeSet,NULL,NULL,&depth,NULL);
		}
	}
	
	//Get preferences
	prefsVersion = PrefGetAppPreferences (appFileCreator, appPrefID, &goPrefs, &prefsSize, true);
    if (prefsVersion>appPrefVersionNum)
         prefsVersion = noPreferenceFound;
    if (prefsVersion<2)
	{
		goPrefs.accountNum = 1;
		goPrefs.usedAccountID = 0;
	    goPrefs.boardColor = gDefaultBackgroundColor;
	    goPrefs.textColor = gDefaultTextColor;
	    goPrefs.stripColor = greyColor;
	    goPrefs.optLooking = false;
	    goPrefs.optOpen = false;
		goPrefs.playerFilterOn = false;
		goPrefs.upperRank = 0;
		goPrefs.lowerRank = 0;
		goPrefs.playerOpenOnly = false;
		goPrefs.playSound = true;
		goPrefs.byoAlert = 120;
		goPrefs.seekConfigIndex = 0;
		goPrefs.seekSizeIndex = 0;	//default size 19 x 19
		goPrefs.seekHandicap = 0;
		goPrefs.netQuality = MEDIUMQUALITY;
		goPrefs.netLag = DEFAULTNETLAG;
  	}
    if (prefsVersion < 3)
    {
    	goPrefs.time = 1;
    	goPrefs.byoTime = 10;
    }
 	gAccountNum = goPrefs.accountNum>=1?goPrefs.accountNum:1;
 	gAccountIdToBeUsed = goPrefs.usedAccountID>=0?goPrefs.usedAccountID:0;
 	gBoardColor = goPrefs.boardColor;
 	gTextColor = goPrefs.textColor;
 	gStripColor = goPrefs.stripColor;
 	gLooking = goPrefs.optLooking;
 	gOpen = goPrefs.optOpen;
 	gPlayerFilterOn = goPrefs.playerFilterOn;
 	gUpperRank = goPrefs.upperRank>=0?goPrefs.upperRank:0;
 	gLowerRank = goPrefs.lowerRank>=0?goPrefs.lowerRank:0;
 	gPlayerOpenOnly = goPrefs.playerOpenOnly;
 	gPlaySound = goPrefs.playSound;
 	gByoAlert = goPrefs.byoAlert>=0?goPrefs.byoAlert:120;
	gSeekConfigIndex = goPrefs.seekConfigIndex>=0?goPrefs.seekConfigIndex:0;
	gSeekSizeIndex = goPrefs.seekSizeIndex>=0?goPrefs.seekSizeIndex:0;
	gSeekHandicap = goPrefs.seekHandicap>=0?goPrefs.seekHandicap:0;
	gNetQuality = goPrefs.netQuality>=0?goPrefs.netQuality:MEDIUMQUALITY;
	gNetLag = goPrefs.netLag>=MINNETLAG?goPrefs.netLag:DEFAULTNETLAG;
	gCurrentMain = goPrefs.time;
	gCurrentByoyomi = goPrefs.byoTime;
	//Get accounts 
	Int16	i;
	for (i=0;i<gAccountNum;i++)
	{
    	prefsVersion = PrefGetAppPreferences (appFileCreator, appPrefID+1+i, &accountPrefs, &accountPrefsSize, true);
    	if (prefsVersion>appPrefVersionNum)
        	prefsVersion = noPreferenceFound;
    	if (prefsVersion<1)
    	{
        	StrCopy(accountPrefs.description, "guest/IGS");
#ifdef _DEBUG_
        	StrCopy(accountPrefs.description, "guest/VSB");
#endif	
         	StrCopy(accountPrefs.userName,"guest");
         	StrCopy(accountPrefs.password,"");
         	StrCopy(accountPrefs.serverName,"igs.joyjoy.net");
#ifdef _DEBUG_
         	StrCopy(accountPrefs.serverName,"172.16.102.126");
#endif	
         	StrCopy(accountPrefs.serverPort,"6969");
         	accountPrefs.serverType = GENERIC;
         	accountPrefs.supportNmatch = false;
      	}
    	if (prefsVersion<2)
		{
         	accountPrefs.useProxy = false;
         	StrCopy(accountPrefs.proxyName,"");
         	StrCopy(accountPrefs.proxyPort,"");
         	accountPrefs.proxyType = 0;
		}
		gAccountList[i] = accountPrefs;
	}
 	/*
 	StrCopy(gConnectedAccountDescription,gAccountList[gAccountIdToBeUsed].description);
	gConnectedAccountServerType = gAccountList[gAccountIdToBeUsed].serverType;
	gConnectedAccountSupportNmatch = gAccountList[gAccountIdToBeUsed].supportNmatch;
	*/
	//gProxy = gAccountList[gAccountIdToBeUsed].useProxy;
	//Initialize bitmaps 
	MemHandle	resH;
	resH = DmGetResource( bitmapRsc, WhiteStoneBitmap );
	ErrFatalDisplayIf( !resH, "Missing bitmap" );
	whiteStoneP = (BitmapType*)MemHandleLock( resH );
	MemPtrUnlock( whiteStoneP );
	DmReleaseResource( resH );
	resH = DmGetResource( bitmapRsc, BlackStoneBitmap );
	ErrFatalDisplayIf( !resH, "Missing bitmap" );
	blackStoneP = (BitmapType*)MemHandleLock( resH );
	MemPtrUnlock( blackStoneP );
	DmReleaseResource( resH );
	//Initialize wave file
	resH = DmGetResource( 'wave', ClockTickingWav );
	ErrFatalDisplayIf( !resH, "Missing wave" );   
	tickingAudioP = MemHandleLock(resH);
	MemPtrUnlock(tickingAudioP);
	DmReleaseResource( resH );
	resH = DmGetResource( 'wave', PlaceStoneWav );
	ErrFatalDisplayIf( !resH, "Missing wave" );
	moveAudioP = MemHandleLock(resH);
	MemPtrUnlock(moveAudioP);
	DmReleaseResource( resH );
	resH = DmGetResource( 'wave', NewMessageWav );
	ErrFatalDisplayIf( !resH, "Missing wave" );
	newMessageAudioP = MemHandleLock(resH);
	MemPtrUnlock(newMessageAudioP);
	DmReleaseResource( resH );
	if (!gInBuffer) 
		gInBuffer = (Char*) MemPtrNew(IN_BUFFER_SIZE);
	if (gInBuffer)
		MemSet(gInBuffer,IN_BUFFER_SIZE,NULL);
	else
		//Allocating memory fails
		return memErrNotEnoughSpace;
	if (!gTerminalBuffer) 
		gTerminalBuffer = (Char*) MemPtrNew(TERMINAL_BUFFER_SIZE);
	if (gTerminalBuffer)
		MemSet(gTerminalBuffer,TERMINAL_BUFFER_SIZE,NULL);
	else
		//Allocating memory fails
		return memErrNotEnoughSpace;
	InitVFS();
	//if (!gBoard)
	//	gBoard = new CBoard;
	//gBoard->Load();
	CBoard	*workingBoard = gBoardPool.GetCurrentBoard();
	if (workingBoard)
		workingBoard->Load(NULL);
	//if (!gMatchList)
	//	gMatchList = new CChallengeArray;	
	FrmGotoForm(MainForm);
	return errNone;
}


/***********************************************************************
 *
 * FUNCTION:    AppStop
 *
 * DESCRIPTION: Save the current state of the application.
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppStop(void)
{
	TGoPreference	goPrefs;
	TGoAccount accountPrefs;
	//Save preferences
	goPrefs.accountNum = gAccountNum;
	goPrefs.usedAccountID = gAccountIdToBeUsed;
 	goPrefs.boardColor = gBoardColor;
 	goPrefs.textColor = gTextColor; 
 	goPrefs.stripColor = gStripColor;
 	goPrefs.optLooking = gLooking;
 	goPrefs.optOpen = gOpen;
 	goPrefs.playerFilterOn = gPlayerFilterOn;
 	goPrefs.upperRank = gUpperRank;
 	goPrefs.lowerRank = gLowerRank;
 	goPrefs.playerOpenOnly = gPlayerOpenOnly;
 	goPrefs.playSound = gPlaySound;
 	goPrefs.byoAlert = gByoAlert;
	goPrefs.seekConfigIndex = gSeekConfigIndex;
	goPrefs.seekSizeIndex = gSeekSizeIndex;
	goPrefs.seekHandicap = gSeekHandicap;
	goPrefs.netQuality = gNetQuality;
	goPrefs.netLag = gNetLag;
	goPrefs.time = gCurrentMain;
	goPrefs.byoTime = gCurrentByoyomi;
    PrefSetAppPreferences (appFileCreator, appPrefID, appPrefVersionNum, &goPrefs, sizeof(TGoPreference), true);

/*	if (whiteStoneP)
		MemPtrFree(whiteStoneP);
	if (blackStoneP)
		MemPtrFree(blackStoneP);

	if (tickingAudioP)
		//MemPtrUnlock(tickingAudioP);
		MemPtrFree(tickingAudioP);
	if (moveAudioP)
		//MemPtrUnlock(moveAudioP);
		MemPtrFree(moveAudioP);
	if (newMessageAudioP)
		//MemPtrUnlock(newMessageAudioP);
		MemPtrFree(newMessageAudioP);
*/
	//Save accounts info
	Int16 i;
	for (i=0;i<gAccountNum;i++)
	{
		accountPrefs = gAccountList[i];
    	PrefSetAppPreferences (appFileCreator, appPrefID+1+i, appPrefVersionNum, &accountPrefs, sizeof(TGoAccount), true);
	}
   	FrmSaveAllForms();
   	FrmCloseAllForms();
	//if (IsConnected())
	//	DisconnectServer();
	if (IsLoggedIn())  
		Logout();
	if (gInBuffer) 
		MemPtrFree(gInBuffer);
	if (gTerminalBuffer) 
		MemPtrFree(gTerminalBuffer);
	//if (gBoard)
	//{
	//	gBoard->Save();
	//	delete gBoard;
	//}
	CBoard	*workingBoard = gBoardPool.GetCurrentBoard();
	if (workingBoard)
		workingBoard->Save();	
	//if (gCurrentPlayerP)	//will be released by gPlayerList
	//	delete gCurrentPlayerP;

	if (gAutoOffTime)
		gAutoOffTime = SysSetAutoOffTime(gAutoOffTime);	//restore previous Aoto Off setting
		
}


/***********************************************************************
 *
 * FUNCTION:    AppEventLoop
 *
 * DESCRIPTION: This routine is the event loop for the application.  
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppEventLoop(void)
{
	Err			error;
	EventType	event;

	do {
		gEventTimeOut = NETCHECKTIMEOUT;
		EvtGetEvent(&event, gEventTimeOut);
		//Skip auto off event if it's logged in
		//if (event.eType == keyDownEvent && event.data.keyDown.chr == vchrAutoOff && IsLoggedIn())
		//{
		//	EvtResetAutoOffTimer(); 
		//	continue;
		//}
      	if (event.eType == nilEvent)
			HandleNilEvent();
			
		if (SysHandleEvent(&event))
			continue;
			
		if (MenuHandleEvent(0, &event, &error))
			continue;
			
		if (AppHandleEvent(&event))
			continue;

		FrmDispatchEvent(&event);

	} while (event.eType != appStopEvent || IsLoggedIn()); //Don't exits Mobile Go if it's logged in.
	
	
}

/*********************************************************************
 * FUNCTION: RomVersionCompatible
 *
 * DESCRIPTION: 
 *
 * This routine checks that a ROM version is meet your minimum 
 * requirement.
 *
 * PARAMETERS:
 *
 * requiredVersion
 *     minimum rom version required
 *     (see sysFtrNumROMVersion in SystemMgr.h for format)
 *
 * launchFlags
 *     flags that indicate if the application UI is initialized
 *     These flags are one of the parameters to your app's PilotMain
 *
 * RETURNED:
 *     error code or zero if ROM version is compatible
 **********************************************************************/

Err RomVersionCompatible(UInt32 requiredVersion, UInt16 launchFlags)
{
	UInt32 romVersion;

	/* See if we're on in minimum required version of the ROM or later. */
	FtrGet(sysFtrCreator, sysFtrNumROMVersion, &romVersion);
	if (romVersion < requiredVersion)
	{
		if ((launchFlags & 
			(sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp)) ==
			(sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp))
		{
			FrmAlert (RomIncompatibleAlert);

			/* Palm OS versions before 2.0 will continuously relaunch this
			 * app unless we switch to another safe one. */
			if (romVersion < kPalmOS20Version)
			{
				AppLaunchWithCommand(
					sysFileCDefaultApp, 
					sysAppLaunchCmdNormalLaunch, NULL);
			}
		}

		return sysErrRomIncompatible;
	}

	return errNone;
}

/***********************************************************************
 *
 * FUNCTION:    PilotMain
 *
 * DESCRIPTION: This is the main entry point for the application.
 *
 * PARAMETERS:  cmd - word value specifying the launch code. 
 *              cmdPB - pointer to a structure that is associated with the launch code. 
 *              launchFlags -  word value providing extra information about the launch.
 * RETURNED:    Result of launch
 *
 * REVISION HISTORY: 
 *
 *
 ***********************************************************************/
UInt32 PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{   
	Err error = errNone;    
	Boolean appIsActive = launchFlags & sysAppLaunchFlagSubCall;
	
	error = RomVersionCompatible (ourMinVersion, launchFlags);
	if (error) return (error);
	
	switch (cmd) {     
		case sysAppLaunchCmdNormalLaunch:
			if ((error = AppStart()) == 0) {	
				AppEventLoop(); 
				AppStop();
			} 
			break;
		case sysAppLaunchCmdNotify :  
        	if (!appIsActive)
        	{
                //ErrNonFatalDisplay("App is not active but is accessing globals");
                return 1;
            }
			//Go to REM sleep (turn screen off)
   			if (((SysNotifyParamType*)cmdPBP)->notifyType == hsNotifyRemSleepRequestEvent && IsLoggedIn()) 
   			{ 
   				//Boolean	screenOn;
   				//error = HsAttrGet(hsAttrDisplayOn,0,(UInt32 *)&screenOn);
   				//if (screenOn)
      				((SleepEventParamType *)((SysNotifyParamType*)cmdPBP)->notifyDetailsP)->deferSleep++;
   			}
			break; 
		default:
			break;
	}
	 
	return error;
}


