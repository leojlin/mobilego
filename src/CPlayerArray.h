#ifndef CPLAYERARRAY_H
#define CPLAYERARRAY_H
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"

class CPlayerArray{
public:

	CPlayerArray() SEC_ARR;
	virtual ~CPlayerArray() SEC_ARR;
	//void Swap(Int32, Int32) SEC_ARR;
	void HeapSort(Int32 count,PLAYERFIELD fld, Boolean  ascending) SEC_ARR;	
	Boolean Add(CPlayerItemPtr) SEC_ARR;
	void Clear() SEC_ARR;
	Boolean Insert(CPlayerItemPtr,Int32) SEC_ARR;
	//Int32 Find(CPlayerItemPtr) SEC_ARR;
	Int32 FindByName(const Char*) SEC_ARR;
	Boolean Remove(Int32) SEC_ARR;
	Int32 Size() SEC_ARR;
	CPlayerItemPtr operator[](Int32) SEC_ARR;
protected:
	//void QuickSort(Int32 low, Int32 high,PLAYERFIELD fld, Boolean  ascending) SEC_ARR;
	Int16 Compare(const CPlayerItem *itemP1, const CPlayerItem *itemP2, PLAYERFIELD fld, Boolean ascending) SEC_ARR;
	CPlayerItemPtr	*pArray;
	Int32	size;	

};

#endif // CPLAYERARRAY_H
