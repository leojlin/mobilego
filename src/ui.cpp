#include <PalmOS.h>
#include <System/DLServer.h>
#include <PalmOSGlue.h>
#include "ui.h"


//RGBColorType		blue = 	{ 0, 51, 0, 153 };
RGBColorType		white = 	{ 0, 255, 255, 255 };
RGBColorType		black = 	{ 0, 0, 0, 0 };
//RGBColorType		lightBlue = { 0, 0, 153, 255 };
RGBColorType		grey = 	{ 0, 51, 51, 51 };
//RGBColorType		lightGrey = { 0, 100, 100, 100 };
RGBColorType		red = 	{ 0, 255, 0, 0 };
//RGBColorType		green	= 	{ 0, 0, 154, 0 };
//RGBColorType		purple = { 0, 100, 0, 100 };
//RGBColorType		olive	= 	{ 0, 101, 101, 0 };
//RGBColorType		maroon = { 0, 136, 0, 0 };
//RGBColorType		paleBlue = { 0, 222, 237, 255 };
//RGBColorType		bgColor = { 0, 102, 153, 204 };

/*
void DisableControl( FormPtr frm, UInt16 objectID )
   {
   ControlPtr		ctl;

	ctl = (ControlType*)GetObjectPtr( objectID );	
   CtlSetEnabled( ctl, false );
   }
   

void EnableControl( FormPtr frm, UInt16 objectID )
   {
   ControlPtr		ctl;

	ctl = (ControlType*)GetObjectPtr( objectID );	
   CtlSetEnabled( ctl, true);
   }
   

void DisableField( FormPtr frm, UInt16 objectID )
   {
   FieldPtr		fld;

	fld = (FieldType*)GetObjectPtr( objectID );	
   FldSetUsable( fld, false );
   }
   

void EnableField( FormPtr frm, UInt16 objectID )
   {
   FieldPtr		fld;

	fld = (FieldType*)GetObjectPtr( objectID );	
   FldSetUsable( fld, true );
   }
*/   

/**
 * Show an object (enable and draw).
 *
 * @param frm the object's form
 * @param objectID the object's id
 */
void ShowObject( FormPtr frm, UInt16 objectID )
   {
	FrmShowObject( frm, FrmGetObjectIndex( frm, objectID ));
   }
   

/**
 * Hide an object (disable and erase).
 *
 * @param frm the object's form
 * @param objectID the object's id
 */
void HideObject( FormPtr frm, UInt16 objectID )
   {
	FrmHideObject( frm, FrmGetObjectIndex( frm, objectID ));
   }


/**
 * Give the focus to an object.
 *
 * @param frm the object's form
 * @param objectID the object's id
 */
/*
void SetFocus( FormPtr frm, UInt16 objectID )
{
	UInt16 index;
	index = FrmGetObjectIndex( frm, objectID );
	if (index!=frmInvalidObjectId )
		FrmSetFocus(frm,index);
	//FrmSetFocus( frm, FrmGetObjectIndex( frm, objectID ));
}
*/	

/**
 * Draws a bitmap resource right to the screen.
 *
 * @param resID the bitmap's resource id
 * @param x the x coordinate
 * @param y the y coordinate
 */
/*
void DrawBitmapSimple( Int16 resID, Int16 x, Int16 y )
	{
  	MemHandle	resH;
	BitmapPtr		resP;

	resH = DmGetResource( bitmapRsc, resID );
	ErrFatalDisplayIf( !resH, "Missing bitmap" );
	resP = (BitmapType*)MemHandleLock( resH );
	WinDrawBitmap( resP, x, y );
	MemPtrUnlock( resP );
	DmReleaseResource( resH );
   }
*/

/**
 * Draws a bitmap resource to the screen, but does so using a mask.  If
 * you call this function, make sure that for whatever resID you use,
 * there needs to be a b/w mask for it that has resource id resID + 1;
 *
 * @param resID the bitmap's resource id
 * @param x the x coordinate
 * @param y the y coordinate
 */
/*
void DrawBitmap( Int16 resID, Int16 x, Int16 y )
	{
	MemHandle	dataH, maskH;
	BitmapPtr	data, mask;
   WinHandle   oldWinH, winH;

	dataH = DmGetResource( bitmapRsc, resID );
	maskH = DmGetResource( bitmapRsc, resID+1 );
	ErrFatalDisplayIf( !dataH || !maskH, "Missing bitmap" );
	data = (BitmapPtr)MemHandleLock( dataH );
	mask = (BitmapPtr)MemHandleLock( maskH );

   Coord width, height;
   UInt16 err=errNone;
   
   BmpGetDimensions( data, &width, &height, NULL );
   
   winH = WinCreateOffscreenWindow( width, height, nativeFormat, &err );
   ErrFatalDisplayIf( err != errNone, "Unable to create offscreen window" );

   oldWinH = WinSetDrawWindow( winH );
   WinDrawBitmap( mask, 0, 0 );

   WinSetDrawWindow( oldWinH );
   
   RectangleType srcRect;
   srcRect.topLeft.x = srcRect.topLeft.y = 0;
   srcRect.extent.x = width;
   srcRect.extent.y = height;
   
   WinCopyRectangle( winH, oldWinH, &srcRect, x, y, winMask );

   oldWinH = WinSetDrawWindow( winH );

   WinDrawBitmap( data, 0, 0 );
   WinSetDrawWindow( oldWinH );
   
   WinCopyRectangle( winH, oldWinH, &srcRect, x, y, winOverlay );
   WinSetDrawWindow( oldWinH );
   WinDeleteWindow( winH, false );

	MemPtrUnlock( data );
	MemPtrUnlock( mask );
	DmReleaseResource( dataH );
	DmReleaseResource( maskH );
   }
*/

/**
 * Deallocates the memory used by a control.
 *
 * @param id the control's id
 */
/* 
void CtlFreeMemory( UInt16 id )
	{
   Err err = 0;
   
	const Char *label = CtlGetLabel( (ControlType*)GetObjectPtr( id ) );
	if( label )
      {
      err = MemPtrFree( (void*)label );
      //checkError( err, "CtlFreeMemory", 0 );
      }
	}
*/

/**
 * Allocates a small (1 byte) of memory to a control.
 *
 * @param id the control's id
 */
/*
void CtlInit( UInt16 id )
	{
	ControlPtr		ctl;
	Char 		*label;

	ctl = (ControlType*)GetObjectPtr( id );
	label = (char*)MemPtrNew( 1 );
	label[0] = 0;
	CtlSetLabel( ctl, label );
	}
*/	

/**
 * Returns a pointer to the field that has the focus, or NULL if no field
 * has the focus.
 *
 * @return a pointer to the field with the focus
 */
/* 
void* GetFocusObjectPtr()
{
	FormPtr           frm;
	UInt16            focusIndex;    // the index, not the id of the object

	frm = FrmGetActiveForm();
	focusIndex = FrmGetFocus( frm );
	if( focusIndex == noFocus )
		return NULL;
	else
        return GetObjectPtr( FrmGetObjectId( frm, focusIndex ));
}
*/

/**
 * Sets the label of a control, and frees any memory already used by it.
 *
 * @param ctlID control id to set
 * @param str the string to set it to
 * @param allowWidth the max width in pixels that the label can be
 * @return a pointer to the control
 */
/*
ControlPtr SetControlLabel( UInt16 ctlID, Char* str, Int16 allowWidth )
	{
	ControlPtr		ctl;
	const Char		*oldStr;
	Char				*p;
	Int16				drawLen;
	Boolean			fits;
	FontID			curFont = FntSetFont( stdFont );						// save current font
   Err            err = 0;
	
	ctl = (ControlType*)GetObjectPtr( ctlID );										// make sure we can get the control
	ErrNonFatalDisplayIf( !ctl, "Incorrect Control Specified." );				// some debugging stuff
	ErrNonFatalDisplayIf( !str, "No string passed to SetControlLabel." );

	drawLen = StrLen( str );											// how many to draw
	FntCharsInWidth( str, &allowWidth, &drawLen, &fits );					// see how much fits

	if( allowWidth != 0 )												// if we only have so much room
		{
		p = (char*)MemPtrNew( drawLen + 1 );				// allocate new memory for the label
		StrNCopy( p, str, drawLen );							// copy in the new string
		p[drawLen] = 0;											// have to terminate it!!
		}
	else															// if we've got lots of room for it
		{
		p = (char*)MemPtrNew( StrLen( str ) + 1 );							// allocate the full amount
		ErrFatalDisplayIf( !p, "Unable to allocate memory" );
      StrCopy( p, str );											// copy in the new string
		}

	oldStr = CtlGetLabel( (ControlType*)GetObjectPtr( ctlID ) );							// save the old stuff
	CtlSetLabel( ctl, p );												// set the new value
	if( oldStr )
      {
      err = MemPtrFree( (void*)oldStr );								// free the old stuff
      //checkError( err, "SetControlLabel()", 0 );
      oldStr = 0;
      }
   
	FntSetFont( curFont );											// restore previous font

	return ctl;														// return a pointer to the control
	}
*/

/**
 * Sets a field's text to a string, and frees any memory previously used
 * by it. It also draws the field, so it must be called after the form has
 * been drawn.
 *
 * @param fldID field id to set
 * @param txtH the string to set it to
 * @return a pointer to the field
 */
/*
FieldPtr SetFieldTextFromHandle( UInt16 fldID, MemHandle txtH, Boolean draw )
	{
	MemHandle	oldH;
	FieldPtr		fld;
	
	fld = (FieldType*)GetObjectPtr( fldID );
	ErrNonFatalDisplayIf( !fld, "Incorrect Field Specified." );
	ErrNonFatalDisplayIf( !txtH, "No string passed to SetFieldTextFromHandle." );
	
	oldH = FldGetTextHandle( fld );
	FldSetTextHandle( fld, txtH );
	if( oldH )	MemHandleFree( oldH );

   if( draw )
      FldDrawField( fld );
   
	return fld;
	}
*/
void SetFieldText(FieldType	*pField, Char* s)
{
	MemHandle	textH,oldtextH;
	Char*		h1;
	int			len;
	//
	oldtextH=FldGetTextHandle(pField);
	if (oldtextH)
	{
		FldSetTextHandle(pField,NULL);
		MemHandleFree(oldtextH);
	}
	len = StrLen(s);
	textH = MemHandleNew(len+1);
	h1 = (Char *)MemHandleLock(textH);
	MemMove(h1,s,len);
	h1[len]=NULL;
	MemHandleUnlock(textH);
	FldSetTextHandle(pField,textH);
	//FldDrawField(pField);	
}
void DrawFieldText(FieldType	*pField, Char* s)
{
	SetFieldText(pField,s);
	FldDrawField(pField);	
}


// table rows are always either 10 or 16, depending on the bounds of the table
/*
UInt16 calcTableRows( TablePtr table, Boolean isChooser )
   {
   if( isChooser )
      return 9;
   
   RectangleType  bounds;
   TblGetBounds( table, &bounds );
   
   UInt16 rows = bounds.extent.y / 10;
   if( rows > 16 )
      rows = 16;

   if( rows < 16 )
      rows = 10;

   return rows;
   }
*/

void* GetObjectPtr( UInt16 id )
   {
	FormType * frmP;

	frmP = FrmGetActiveForm();
	return 	FrmGetObjectPtr(frmP, FrmGetObjectIndex(frmP, id));
   }
Int32 GenerateRCode()
{
	
	Char *username = (Char*)MemPtrNew(dlkUserNameBufSize * sizeof(Char)); 
	Int32	code,code1,code2,code3;
	UInt16 i;
  
	// Allocate a buffer for the user name 
	  
	// Obtain the user's name 
	DlkGetSyncInfo(NULL, NULL, NULL, username, NULL, NULL); 
#ifdef _DEBUG_
	StrCopy(username,"leojlin");  
#endif
	code = 0;  
	for (i=0;(i<dlkUserNameBufSize) && (username[i] != NULL); i++)
	{
		#ifdef	_BE_
		code1 = ((Int32)username[i] & 0x000000cc) << 16;
		code2 = ((Int32)username[i] & 0x000000aa) << 8;
		code3 = (Int32)username[i] & 0x00000055;
		#endif
		#ifdef	_SE_
		code1 = ((Int32)username[i] & 0x00000055) << 16;
		code2 = ((Int32)username[i] & 0x000000cc) << 8;
		code3 = (Int32)username[i] & 0x000000aa;
		#endif
		#ifdef	_PE_
		code1 = ((Int32)username[i] & 0x000000aa) << 16;
		code2 = ((Int32)username[i] & 0x00000055) << 8;
		code3 = (Int32)username[i] & 0x000000cc;
		#endif
		code = code+ (code1 | code2 | code3);
		if (code > 1000000000)
    		code = code - 1000000000;
	}	  
	code += 1000000000;
	// Now that we're done with the user name, free the buffer 
	MemPtrFree(username); 
	return code;
}	
Boolean CheckRegistered(UInt8& remainingDay)
{
	Boolean result = false;
	TKey key;
	Int16 prefsVersion;
	UInt16	prefsSize = sizeof(TKey);
	prefsVersion = PrefGetAppPreferences (keyFileCreator, keyPrefID, &key, &prefsSize, true);
    if (prefsVersion>keyPrefVersionNum)
         prefsVersion = noPreferenceFound;
	if (prefsVersion<keyPrefMiniVersionNum)	//no key
	{
		key.data = TimGetSeconds();
		key.value = 30;
		key.code = 0;
	}
	else if (prefsVersion<keyPrefVersionNum)	//old key
	{
		//Extend trial period to other 15 days
		key.data = TimGetSeconds() - 0x00278D00 / 2;
	}
	// Basic Edition always return true
	#ifdef _BE_
		return true;
	#endif
	//else //do checking
	{
		if (key.code == GenerateRCode())
			result = true;
		else
		{
			UInt32 diff = TimGetSeconds() - key.data;
			//30 days is over?
			if (diff >= 0x00278D00)
				key.value = 0; //0x00278D00 = 30 days
			else
				key.value = 30 - (diff / 0x00015180); //0x00015180 = 1 day
		}
	}
	//save the key
    PrefSetAppPreferences (keyFileCreator, keyPrefID, keyPrefVersionNum, &key, sizeof(TKey), true);
	remainingDay = key.value;
    return result;
}
void SaveRegisterCode(UInt32 registerCode)
{
	TKey key;
	Int16 prefsVersion;
	UInt16	prefsSize = sizeof(TKey);
	prefsVersion = PrefGetAppPreferences (keyFileCreator, keyPrefID, &key, &prefsSize, true);
    if (prefsVersion>keyPrefVersionNum)
         prefsVersion = noPreferenceFound;
	if (prefsVersion<1)	//no key
	{
		key.data = TimGetSeconds();
		key.value = 30;
		key.code = registerCode;
	}
	else
		key.code = registerCode;
	//save the key
    PrefSetAppPreferences (keyFileCreator, keyPrefID, keyPrefVersionNum, &key, sizeof(TKey), true);
}
void RegisterForNotification(UInt32 notifyType)
{
	UInt16 cardNo	= 0;
	LocalID dbID	= 0;

	SysCurAppDatabase(&cardNo, &dbID);
	SysNotifyRegister(cardNo, dbID, notifyType, 0, sysNotifyNormalPriority, 0);

    return;
}

void UnregisterForNotification(UInt32 notifyType)
{
	UInt16 cardNo	= 0;
	LocalID dbID	= 0;

	SysCurAppDatabase(&cardNo, &dbID);
	SysNotifyUnregister(cardNo, dbID, notifyType, sysNotifyNormalPriority);

    return;
}

