#ifndef CCHALLENGE_H
#define CCHALLENGE_H
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"

class CChallengeArray{
public:

	CChallengeArray() SEC_ARR;
	virtual ~CChallengeArray() SEC_ARR;
	Boolean Add(CChallengeItemPtr) SEC_ARR;
	void Clear() SEC_ARR;
	Boolean Insert(CChallengeItemPtr,Int32) SEC_ARR;
	Int32 FindByName(const Char*) SEC_ARR;
	Boolean Remove(Int32) SEC_ARR;
	Int32 Size() SEC_ARR;
	CChallengeItemPtr operator[](Int32) SEC_ARR;

protected:
	CChallengeItemPtr	*pArray;
	Int32	size;	

};

#endif // CCHALLENGE_H
