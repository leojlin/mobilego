#include <PalmOS.h>
#include <StdIOPalm.h>
#include <FrmGlue.h>
#include <PalmOne_68K.h> 
#include "network.h"
#include "main.h"
#include "rsc.h"
#include "ui.h"
#include "forms.h"
#include "nngs.h"
#include "go.h"
#include "CGameListView.h"
#include "CPlayerListView.h"
#include "CChallengeListView.h"
#include "CStringArray.h"
#include "CStatistics.h"
extern UInt16	gReturnToFormID;
extern CGameListView	gGameList;
extern CPlayerListView	gPlayerList;
extern CChallengeListView	gChallengeList;
extern CChallengeArray	*gMatchList;
extern CChallengeItem	gCurrentMatch;
extern Int16			gCurrentMain;
extern Int16			gCurrentByoyomi;
extern CPlayerItemPtr	gCurrentPlayerP;
extern CStringArray		gStoredGameList;
//extern CBoard	*gBoard;
extern CBoardPool		gBoardPool;
extern TGoAccount	gAccount;
extern Int16	gAccountNum;
extern Int16	gAccountIdToBeUsed;
extern Int16	gAccountIdInUsed;
extern Boolean	gProxy;
extern Boolean	gGuestAccount;
//extern Int16 	gConnectedAccountServerType;
extern Char	gChatBuddyName[];	
extern Boolean gChatMode;
extern TGoAccount	gAccountList[];
extern Int16	gGameListTab;
extern GAMEFIELD	gGameSortedField;
extern Boolean gGameAscending;
extern PLAYERFIELD	gPlayerSortedField;
extern Boolean gPlayerAscending;
extern Int16	gUpperRank;
extern Int16	gLowerRank;
extern Boolean	gPlayerFilterOn;
extern Boolean	gPlayerOpenOnly;
extern Boolean 	gPlaySound;
extern Int16	gByoAlert;			//in seconds
extern Int16	gNetQuality;
extern Int16	gNetLag;	//in seconds
extern Int16	gSeekConfigIndex;
extern Int16	gSeekSizeIndex;
extern Int16	gSeekHandicap;
extern Boolean	gSeek;
extern Boolean 	gNavSupport;
extern Boolean	gWaitDraw;
extern TDefsInfo gMyStatsDefs;
extern CStatistics gStatistics;
//extern Char	*gTerminalBuffer;
extern IndexedColorType gDefaultBackgroundColor,gDefaultTextColor;
extern IndexedColorType	gBoardColor,gTextColor,gStripColor;
extern IndexedColorType	greyColor,blackColor,whiteColor;
extern BitmapPtr	whiteStoneP,blackStoneP;
extern Char	gRankString[12][4];
extern Char gMatchTypeString[MATCHTYPE_TOTAL][40];
//extern Char gAcceptButtonLabel_C[25];	//Challenge
//extern Char gAcceptButtonLabel_A[25];	//Accept
Int16	gCurrentAccountID;
static Boolean  useAutomatch = false;
//UInt16  gHWRefNum;   
void ShowMatchFields(FormPtr frm, Int16	m_time, Int16 b_time, Int16 m_type)
{
	FieldAttrType attr;	
	Char	string[10];
	//ShowObject( frm, MatchFormMatchTypeField );
	HideObject( frm, MatchFormHandicapField );
	ShowObject( frm, MatchFormBlackPushButton );
	ShowObject( frm, MatchFormWhitePushButton );
	HideObject( frm, MatchFormStonesField );
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	attr.editable = 1;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	attr.editable = 1;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	StrIToA(string,m_time); 
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMainTimeField),string);	
	//StrIToA(string,gCurrentMatch.byoyomi);
	StrIToA(string,b_time);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormBYTimeField),string);	
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMatchTypeField),gMatchTypeString[m_type]);	
}
void ShowAutoMatchFields(FormPtr frm, Int16	m_time, Int16 b_time, Int16 b_stones, Int16 m_type)
{
	FieldAttrType attr;	
	Char	string[10];
	//HideObject( frm, MatchFormMatchTypeField );
	HideObject( frm, MatchFormHandicapField );
	HideObject( frm, MatchFormBlackPushButton );
	HideObject( frm, MatchFormWhitePushButton );
	ShowObject( frm, MatchFormStonesField );
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	attr.editable = 0;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	attr.editable = 0;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormStonesField),&attr); 
	attr.editable = 0;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormStonesField),&attr); 
	StrIToA(string,m_time); 
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMainTimeField),string);	
	//StrIToA(string,gCurrentMatch.byoyomi);
	StrIToA(string,b_time);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormBYTimeField),string);	
	//StrIToA(string,gCurrentMatch.stones);
	StrIToA(string,b_stones);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormStonesField),string);	
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMatchTypeField),gMatchTypeString[m_type]);	
}
void ShowAllMatchFields(FormPtr frm, Int16	m_time, Int16 b_time, Int16 b_stones, Int16 handicap, Int16 m_type)
{
	FieldAttrType attr;	
	Char	string[10];
	//ShowObject( frm, MatchFormMatchTypeField );
	ShowObject( frm, MatchFormHandicapField );
	ShowObject( frm, MatchFormBlackPushButton );
	ShowObject( frm, MatchFormWhitePushButton );
	ShowObject( frm, MatchFormStonesField );
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	attr.editable = 1;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormMainTimeField),&attr); 
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	attr.editable = 1;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormBYTimeField),&attr); 
	FldGetAttributes((FieldType*)GetObjectPtr(MatchFormStonesField),&attr); 
	attr.editable = 1;
	FldSetAttributes((FieldType*)GetObjectPtr(MatchFormStonesField),&attr); 
	StrIToA(string,m_time); 
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMainTimeField),string);	
	//StrIToA(string,gCurrentMatch.byoyomi);
	StrIToA(string,b_time);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormBYTimeField),string);	
	//StrIToA(string,gCurrentMatch.stones);
	StrIToA(string,b_stones);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormStonesField),string);	
	StrIToA(string,handicap);
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormHandicapField),string);	
	DrawFieldText((FieldType*)GetObjectPtr(MatchFormMatchTypeField),gMatchTypeString[m_type]);	
}
void DrawLine() 
{
	Coord width, height;
	WinGetDisplayExtent( &width, &height );
	//ColorSet( &grey, NULL, NULL, NULL, NULL );
	WinDrawLine( 0, 29, width, 29 );
   	//ColorUnset(); 
} 
void DrawStoredGameListItem(Int16 itemNum, RectangleType *bounds, Char **itemsText)
{
	Char * name = gStoredGameList[itemNum];
	WinDrawChars(name,StrLen(name),bounds->topLeft.x,bounds->topLeft.y);
}

void DrawGameListHeader()
{
	Int16	x,y=17;
   	RectangleType	r;
   	r.topLeft.x = 17;
   	r.topLeft.y = 17;
   	r.extent.x = 132;
   	r.extent.y = 10;
	DrawLine();
	if (gGameListTab)
	{	//Display header of second tab
		//CtlSetValue((ControlType*)GetObjectPtr(GameListFormRightButton),1);
		WinEraseRectangle(&r,0);
		x = 20;
      	WinPaintChars("Move",StrLen("Move"),x,y);
      	x = 46;
      	WinPaintChars("Size",StrLen("Size"),x,y);
      	x = 65;
      	WinPaintChars("H",StrLen("H"),x,y);
      	x = 74;
      	WinPaintChars("Komi",StrLen("Komi"),x,y);
      	x = 99;
      	WinPaintChars("BY",StrLen("BY"),x,y);
      	x = 115;
      	WinPaintChars("FR",StrLen("FR"),x,y);
      	x = 129;
      	WinPaintChars("Obs",StrLen("Obs"),x,y);
	}
	else
	{	//Display header of first tab
		//CtlSetValue((ControlType*)GetObjectPtr(GameListFormLeftButton),1);
		WinEraseRectangle(&r,0);
		WinDrawBitmap(whiteStoneP,19,19);
		x = 27;
      	WinPaintChars("Name",StrLen("Name"),x,y);
		x = 65;
      	WinPaintChars("Rank",StrLen("Rank"),x,y);
		WinDrawBitmap(blackStoneP,88,19);
		x = 96;
      	WinPaintChars("Name",StrLen("Name"),x,y);
      	x = 134; 
      	WinPaintChars("Rank",StrLen("Rank"),x,y);
	}
}	
void DisplayPassword(Char *password)
{
	Char		*resource;
	MemHandle	resourceH = NULL;
	if (password == NULL || password[0] == NULL)	//Empty password
	{
		resourceH = DmGetResource(strRsc, StringUnassigned);
		if (resourceH)
		{
			resource = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormPassword),resource);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH); 
		} 
		else	//In case resource missing
			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormPassword),"-Unassigned-");
	}
	else
	{
		resourceH = DmGetResource(strRsc, StringAssigned);
		if (resourceH)
		{
			resource = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormPassword),resource);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH);
		}
		else	//In case resource missing
			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormPassword),"-Assigned-");
	}
}

void DisplayGameTitle(Char *title)
{
	Boolean fits = false;
	static Char displayTitle[40];
	Int16 textLen = StrLen(title);
	Int16 width = 120;
	MemSet(displayTitle,40,NULL);
	//if (displayTitle)
	//{
		FntCharsInWidth( title, &width, &textLen, &fits );
		// if the full title does not fit, then we shorten it a little
		//  more so that we can draw an elipis after it.
	   	if( !fits )
	    {
	    	fits = false;
	      	textLen = StrLen(title);
	      	width = 120 - FntCharsWidth ("...", 3);
	      	FntCharsInWidth( title, &width, &textLen, &fits );
	      	if (textLen > 36)
	      		textLen = 36;
	      	StrNCopy(displayTitle, title, textLen);
	      	StrCat(displayTitle, "...");
			//WinDrawChars( displayTitle, textLen, x, y );
			//WinDrawChars( "...", 3, x+width, y );
	    }
	    else
	    {
	    	StrCopy(displayTitle,title);
	    	//WinDrawChars( displayTitle, textLen, x, y );
	    }
		//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormEV),title);
		CtlSetLabel((ControlType*)GetObjectPtr(GameInfoFormGN),displayTitle);
		//MemPtrFree(displayTitle);
	//}
}
Boolean GameListFormHandlePenDown(EventPtr pEvent)
{
	Int16	x,y,w,h;
   	RectangleType	r;
   	r.topLeft.x = 1;
   	r.topLeft.y = 17;
   	r.extent.x = 151;
   	r.extent.y = 10;
   	Boolean handled = false;
    if (!(pEvent->screenX>r.topLeft.x-1 && pEvent->screenX< r.topLeft.x +r.extent.x+1 &&
    	pEvent->screenY>r.topLeft.y-1 && pEvent->screenY< r.topLeft.y +r.extent.y +1 ))
    	return handled;
    //tap on #
    x = 2;
    y = r.topLeft.y;
    w = FntCharsWidth ("#",1);
    h = r.extent.y;
    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    {
    	if (gGameSortedField == ID)	//same sorting filed
    		gGameAscending = !gGameAscending;	//toggle ascending
    	else
    	{
    		gGameSortedField = ID;
    		gGameAscending = true;
    	}
    	return true;
    }
	if (gGameListTab == 0) //First tab
	{
		//tap on white player
		x = 27;
    	w = FntCharsWidth ("Name",StrLen("Name"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == PW)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = PW;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap white rank
		x = 65;
    	w = FntCharsWidth ("Rank",StrLen("Rank"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == WR)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = WR;
	    		gGameAscending = false;
	    	}
	    	return true;
    	}
		//tap black player
		x = 96;
    	w = FntCharsWidth ("Name",StrLen("Name"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == PB)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = PB;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap black rank
		x = 134;
    	w = FntCharsWidth ("Rank",StrLen("Rank"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == BR)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = BR;
	    		gGameAscending = false;
	    	}
	    	return true;
    	}
	}
	else //Second tab
	{
		//tap on Move
		x = 20;
    	w = FntCharsWidth ("Move",StrLen("Move"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == MV)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = MV;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap on Size
		x = 46;
    	w = FntCharsWidth ("Size",StrLen("Size"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == SZ)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = SZ;
	    		gGameAscending = false;
	    	}
	    	return true;
    	}
		//tap on H
		x = 65;
    	w = FntCharsWidth ("H",StrLen("H"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == HA)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = HA;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap on Komi
		x = 74;
    	w = FntCharsWidth ("Komi",StrLen("Komi"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == KM)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = KM;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap on BY
		x = 99;
    	w = FntCharsWidth ("BY",StrLen("BY"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == BY)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = BY;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap on FR
		x = 115;
    	w = FntCharsWidth ("FR",StrLen("FR"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == FR)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = FR;
	    		gGameAscending = true;
	    	}
	    	return true;
    	}
		//tap on Obs
		x = 129;
    	w = FntCharsWidth ("Obs",StrLen("Obs"));
	    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
	    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    	{
	    	if (gGameSortedField == OB)	//same sorting filed
	    		gGameAscending = !gGameAscending;	//toggle ascending
	    	else
	    	{
	    		gGameSortedField = OB;
	    		gGameAscending = false;
	    	}
	    	return true;
    	}
		
	}
    return handled;

}
Boolean PlayerListFormHandlePenDown(EventPtr pEvent)
{
	Int16	x,y,w,h;
   	RectangleType	r;
   	r.topLeft.x = 1;
   	r.topLeft.y = 17;
   	r.extent.x = 151;
   	r.extent.y = 10;
   	Boolean handled = false;
    if (!(pEvent->screenX>r.topLeft.x-1 && pEvent->screenX< r.topLeft.x +r.extent.x+1 &&
    	pEvent->screenY>r.topLeft.y-1 && pEvent->screenY< r.topLeft.y +r.extent.y +1 ))
    	return false;
    //tap on Name
    x = 2;
    y = r.topLeft.y;
   	w = FntCharsWidth ("Name",StrLen("Name"));
    h = r.extent.y;
    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
    {
    	if (gPlayerSortedField == NM)	//same sorting filed
    		gPlayerAscending = !gPlayerAscending;	//toggle ascending
    	else
    	{
    		gPlayerSortedField = NM;
    		gPlayerAscending = true;
    	}
    	return true;
    }
	//tap Rank
	x = 56;
	w = FntCharsWidth ("Rank",StrLen("Rank"));
    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
	{
    	if (gPlayerSortedField == RK)	//same sorting filed
    		gPlayerAscending = !gPlayerAscending;	//toggle ascending
    	else
    	{
    		gPlayerSortedField = RK;
    		gPlayerAscending = false;
    	}
    	return true;
	}
	//tap on Game
	x = 126;
	w = FntCharsWidth ("Game",StrLen("Game"));
    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
	{
    	if (gPlayerSortedField == GM)	//same sorting filed
    		gPlayerAscending = !gPlayerAscending;	//toggle ascending
    	else
    	{
    		gPlayerSortedField = GM;
    		gPlayerAscending = true;
    	}
    	return true;
	}
	//tap on ST
	x = 81;
	w = FntCharsWidth ("Sta.",StrLen("Sta."));
    if (pEvent->screenX>x-1 && pEvent->screenX< x+w+1 &&
    	pEvent->screenY>y-1 && pEvent->screenY< y+h+1 )
	{
    	if (gPlayerSortedField == ST)	//same sorting filed
    		gPlayerAscending = !gPlayerAscending;	//toggle ascending
    	else
    	{
    		gPlayerSortedField = ST;
    		gPlayerAscending = true;
    	}
    	return true;
	}
    return handled;

}
Boolean GameListFormHandleFormOpen()
{
	FrmDrawForm( FrmGetActiveForm() );
	gGameList.highLighted = false;
	gGameList.highLightedRow = 0;
	FrmUpdateForm(GameListForm,frmReloadUpdateCode);
	return true;
}
Boolean PlayerListFormHandleFormOpen()
{
	FrmDrawForm( FrmGetActiveForm() );
	gPlayerList.highLighted = false;
	gPlayerList.highLightedRow = 0;
	FrmUpdateForm(PlayerListForm,frmReloadUpdateCode);
	return true;
}
Boolean ChallengeListFormHandleFormOpen()
{
/*
	FormPtr     frm = FrmGetActiveForm();
	FrmDrawForm( frm );
	DrawLine();
   //drawBackground();

	ScrollBarPtr scrollBar = (ScrollBarType*)GetObjectPtr( ChallengeListFormScrollBar );
	TablePtr table = (TableType*)GetObjectPtr( ChallengeListFormTable );
	gChallengeList.UpdateTable( table, scrollBar, true );
*/
	FrmDrawForm( FrmGetActiveForm() );
	gChallengeList.highLighted = false;
	gChallengeList.highLightedRow = 0;
	FrmUpdateForm(PlayerListForm,frmReloadUpdateCode);
	return true;
}
Boolean MatchFormDisplay()
{
   	Char	*resource;
	MemHandle	resourceH = NULL;
	Char	newTitle[30];
	Char	*buttonLabel;  
	FormPtr frm = FrmGetActiveForm();
    if (FrmGetActiveFormID() != MatchForm)
    	return true;
	FrmDrawForm( frm );
    ShowObject( frm, MatchFormAcceptButton );
    DrawFieldText((FieldType*)GetObjectPtr(MatchFormMatchTypeField),gMatchTypeString[gCurrentMatch.matchtype]);	
	//Display player's name in form title
	resourceH = DmGetResource(strRsc, StringVersus);
	if (resourceH)
	{
		resource = (Char*)MemHandleLock(resourceH);
		StrCopy(newTitle,resource);
		MemHandleUnlock(resourceH);
		DmReleaseResource(resourceH);
	}
	if (gCurrentMatch.playerName)
	{
  		StrCat(newTitle,gCurrentMatch.playerName);
	}
	if (gCurrentMatch.playerRank)
	{
  		StrCat(newTitle," ");
  		StrCat(newTitle,gCurrentMatch.playerRank);
	}
	FrmCopyTitle(FrmGetActiveForm(),newTitle);
	//Set color
	if (gCurrentMatch.playerColor == BLACK)
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),1);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),0);
	}
	else if (gCurrentMatch.playerColor == WHITE)
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),0);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),1);
	}
	else //No color specified
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),0);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),0);
	}
	switch (gCurrentMatch.matchtype){
		case IGS_AMATCH:
			ShowAutoMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.stones,gCurrentMatch.matchtype);
			break;
		case IGS_NMATCH:
			ShowAllMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.stones,gCurrentMatch.handicap,gCurrentMatch.matchtype);
			break;
		default:	//MATCH
			ShowMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.matchtype); 
			break;
	}
	//Show Challenge if match is not offered by opponent (I initiated the match)
	if (!gCurrentMatch.offered) 
	{
	    //Display Automatch check box for IGS
	    //if (gAccount.serverType == IGS)
	    if (gAccountList[gAccountIdToBeUsed].serverType == IGS)
		    ShowObject( frm, MatchFormAutoMatchCheckBox);
		else
			HideObject( frm, MatchFormAutoMatchCheckBox);
		
	    //Show Challenge as button's label
		resourceH = DmGetResource(strRsc, StringChallenge);
		if (resourceH)
		{
			buttonLabel = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(MatchFormAcceptButton),buttonLabel);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH);
		}		   		    
    	HideObject( frm, MatchFormDeclineButton );		
	} 
	else
	{
		ShowObject( frm, MatchFormMatchTypeField );
		HideObject( frm, MatchFormAutoMatchCheckBox);
		resourceH = DmGetResource(strRsc, StringAccept);
		if (resourceH)
		{
			buttonLabel = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(MatchFormAcceptButton),buttonLabel);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH);
		}		   		    
    	ShowObject( frm, MatchFormDeclineButton );		
	} 
	FrmSetFocus(frm,FrmGetObjectIndex(frm,MatchFormCancelButton));
	return true;
}
/*
Boolean MatchFormHandleFormOpen()
{
   	Char	*resource;
	MemHandle	resourceH = NULL;
	Char	newTitle[30];
	Char	*buttonLabel;  
	FormPtr frm = FrmGetActiveForm();
    if (FrmGetActiveFormID() != MatchForm)
    	return true;
	FrmDrawForm( frm );
    ShowObject( frm, MatchFormAcceptButton );
    DrawFieldText((FieldType*)GetObjectPtr(MatchFormMatchTypeField),gMatchTypeString[gCurrentMatch.matchtype]);	
	//Display player's name in form title
	resourceH = DmGetResource(strRsc, StringVersus);
	if (resourceH)
	{
		resource = (Char*)MemHandleLock(resourceH);
		StrCopy(newTitle,resource);
		MemHandleUnlock(resourceH);
		DmReleaseResource(resourceH);
	}
	if (gCurrentMatch.playerName)
	{
  		StrCat(newTitle,gCurrentMatch.playerName);
	}
	if (gCurrentMatch.playerRank)
	{
  		StrCat(newTitle," ");
  		StrCat(newTitle,gCurrentMatch.playerRank);
	}
	FrmCopyTitle(FrmGetActiveForm(),newTitle);
	//Set color
	if (gCurrentMatch.playerColor == BLACK)
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),1);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),0);
	}
	else if (gCurrentMatch.playerColor == WHITE)
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),0);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),1);
	}
	else //No color specified
	{
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton),0);
		CtlSetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton),0);
	}
	switch (gCurrentMatch.matchtype){
		case IGS_AMATCH:
			ShowAutoMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.stones);
			break;
		case IGS_NMATCH:
			ShowAllMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.stones,gCurrentMatch.handicap);
			break;
		default:	//MATCH
			ShowMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi); 
			break;
	}
	//Show Challenge if match is not offered by opponent (I initiated the match)
	if (!gCurrentMatch.offered) 
	{
	    //Display Automatch check box
	    ShowObject( frm, MatchFormAutoMatchCheckBox);
	    //Show Challenge as button's label
		resourceH = DmGetResource(strRsc, StringChallenge);
		if (resourceH)
		{
			buttonLabel = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(MatchFormAcceptButton),buttonLabel);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH);
		}		   		    
    	HideObject( frm, MatchFormDeclineButton );		
	} 
	else
	{
		ShowObject( frm, MatchFormMatchTypeField );
		HideObject( frm, MatchFormAutoMatchCheckBox);
		resourceH = DmGetResource(strRsc, StringAccept);
		if (resourceH)
		{
			buttonLabel = (Char*)MemHandleLock(resourceH);
			CtlSetLabel((ControlType*)GetObjectPtr(MatchFormAcceptButton),buttonLabel);
			MemHandleUnlock(resourceH);
			DmReleaseResource(resourceH);
		}		   		    
    	ShowObject( frm, MatchFormDeclineButton );		
	} 
	return true;
}
*/
void MatchFormHandleDecline()
{
	Char	matchString[50];
	StrCopy(matchString,"decline ");
	StrCat(matchString, gCurrentMatch.playerName);
	WriteServer(matchString);
	//removed from challenge list
	gChallengeList.RemoveItem(gCurrentMatch.playerName);
	gCurrentMatch.Release();
}
void MatchFormHandleAccept()
{
	CChallengeItem *matchP;
	FormPtr frm = FrmGetActiveForm();
	Char	matchString[50],sizeString[10],timeString[10];
	FrmDrawForm( frm );
	matchP = new CChallengeItem;
	if (!matchP)
	{
		// Memory Allocation Failed.
		return;
	}
   	MemHandle	TextH;
   	Char*		h1=NULL,*name=NULL;
   	matchP->matchtype = gCurrentMatch.matchtype;
   	matchP->offered = gCurrentMatch.offered;
   	matchP->stones = gCurrentMatch.stones;
   	matchP->size = gCurrentMatch.size;
   	matchP->handicap = gCurrentMatch.handicap;
   	matchP->main = gCurrentMatch.main;
   	matchP->byoyomi = gCurrentMatch.byoyomi;
   	if (gCurrentMatch.playerName)
   		name = (Char*)MemPtrNew(StrLen(gCurrentMatch.playerName)+1);
	else
	{	
		delete matchP;
		return;	//Memory allocation failed
	}
   	if (name)
   	{
		StrCopy(name,gCurrentMatch.playerName);
		matchP->playerName = name;
   	}
	else
	{	
		delete matchP;
		return;	//Memory allocation failed
	}
	//Save fields' value;
	//if (CtlGetValue((ControlType*)GetObjectPtr(MatchFormAutoMatchCheckBox)))
	if (useAutomatch)
		matchP->matchtype = IGS_AMATCH;
	if (CtlGetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton)) && \
		!CtlGetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton)))
		matchP->playerColor = BLACK;
	else if (CtlGetValue((ControlType*)GetObjectPtr(MatchFormBlackPushButton)) && \
		!CtlGetValue((ControlType*)GetObjectPtr(MatchFormWhitePushButton)))
		matchP->playerColor = WHITE;
	else //No color specified
		matchP->playerColor = TBD;
	//Get Handicap
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(MatchFormHandicapField));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	
		if (h1)
		matchP->handicap = StrAToI(h1);
		MemHandleUnlock(TextH);
	}
	//Get Main time
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(MatchFormMainTimeField));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	
		if (h1)
			matchP->main = StrAToI(h1);
		MemHandleUnlock(TextH);
	}
	//Get Byoyomi time
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(MatchFormBYTimeField));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	
		if (h1)
			matchP->byoyomi = StrAToI(h1);
		MemHandleUnlock(TextH);
	}
	//Get Byoyomi stones
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(MatchFormStonesField));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	
		if (h1)
			matchP->stones = StrAToI(h1);
		MemHandleUnlock(TextH);
	}
	//save current Main and Byoyomi as default	
	gCurrentMain = matchP->main;
	gCurrentByoyomi = matchP->byoyomi;
	
	//Build match command 
	if (matchP->matchtype == IGS_NMATCH)
	{	//Build IGS NMATCH command
		Char	c;
		switch (matchP->playerColor)
		{
			case WHITE:
				c = 'B';
				break;
			case BLACK:
				c = 'W';
				break;
			default:
				c = 'N';
				break;
		}
		sprintf(matchString,"nmatch %s %c %d 19 %d %d %d 0 0 0",\
		matchP->playerName, c, matchP->handicap, matchP->main * 60, matchP->byoyomi * 60, matchP->stones);
	}
	else if (matchP->matchtype == IGS_AMATCH)
	{	//Build IGS AUTOMATCH command
		sprintf(matchString,"automatch %s",matchP->playerName);
	}
	else //Build MATCH command
	{
	StrCopy(matchString,"match ");
	StrCat(matchString, matchP->playerName);
	if (matchP->playerColor == BLACK)
		StrCat(matchString, " w ");
	else	//otherwise, I always take BLACK
		StrCat(matchString, " b ");
	//Hardcode size 19
	//StrIToA(sizeString, matchP->size);
	StrCopy (sizeString, "19");
	StrCat(matchString,sizeString);      
	StrCat(matchString, " ");
	StrIToA(timeString,matchP->main);
	StrCat(matchString,timeString);
	StrCat(matchString, " ");
	StrIToA(timeString,matchP->byoyomi);
	StrCat(matchString,timeString);
	}
	WriteServer(matchString);
#ifdef _DEBUG_	
	StrCat(matchString, "\n");
	WriteTerminal(matchString);
#endif
	//removed from challenge list
	//gChallengeList.RemoveItem(gCurrentMatch.playerName);
	//Add to Match List
	//Int32 index;
	//index = gMatchList->FindByName(gCurrentMatch.playerName);	//Has this player challenged me already?
	//if ( index != -1)
	//	gMatchList->Remove(index); //Yes, delete previous match item			
	//gMatchList->Insert(matchP,0);	//Insert the new match to the top of match list
	delete matchP;
}

Boolean BoardFormHandleFormOpen()
{
	FormPtr     frm = FrmGetActiveForm();
	UInt8	days = 30;
	Boolean registered = false;
	Char	StringDays[6];
	registered = CheckRegistered(days);
	if (!registered)
	{
		if (days==0)
		{
			FrmAlert(TrialExpiredAlert);
			FrmGotoForm(MainForm);
			return true;
		}
		StrIToA(StringDays,days);
		FrmCustomAlert(RegisterPleaseAlert,StringDays," "," ");
	}
	FrmDrawForm( frm );
	FrmSetFocus(frm,FrmGetObjectIndex(frm,BoardFormCancelButton));
	//if (gBoard)
	//{
	//	gBoard->ShowBoard();
	//}
	gBoardPool.GetCurrentBoard()->ShowBoard(); 
	return true;
}
Boolean AccountListFormHandleFormOpen()
{
	FormPtr     frm = FrmGetActiveForm();
	FrmDrawForm( frm );
	DrawLine();
   //drawBackground();
	gCurrentAccountID = gAccountIdToBeUsed;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( AccountListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( AccountListFormTable );
	AccountListFormUpdateTable( table, scrollBar, true );
	if (!IsConnected())
		ShowObject( frm, AccountListFormConnectButton );
	else
		HideObject( frm, AccountListFormConnectButton );
	return true;
}
Boolean GameListFormHandleFormUpdate( EventPtr event )
{
	RectangleType	recTable;
	FormPtr	frm = FrmGetActiveForm();
	Boolean handled = false;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( GameListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( GameListFormTable );
	switch( event->data.frmUpdate.updateCode )
	{
		case frmReloadUpdateCode:
			//Reset skipped items 
		case frmRedrawUpdateCode:
   			gGameList.UpdateTable( table, scrollBar, true );
			FrmDrawForm(frm);
			gGameList.DisplayItemTotal();
			DrawGameListHeader();			
         	handled = true;
         	break;
		default:
			break;
	}    
	//Set focus to table if there is item
	if (gGameList.HasItem())
	{
		FrmSetFocus(frm,FrmGetObjectIndex(frm,GameListFormTable));
		FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,GameListFormTable),&recTable);
		FrmGlueNavDrawFocusRing(frm,GameListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
	}		
	return true;
}
Boolean PlayerListFormHandleFormUpdate( EventPtr event )
{
	RectangleType	recTable;
	FormPtr	frm = FrmGetActiveForm();
	Boolean handled = false;
	ScrollBarPtr scrollBar = (ScrollBarType*)GetObjectPtr( PlayerListFormScrollBar );
	TablePtr table = (TableType*)GetObjectPtr( PlayerListFormTable );
	switch( event->data.frmUpdate.updateCode )
	{
		case frmReloadUpdateCode:
			//Reset skipped items 
		case frmRedrawUpdateCode:
   			gPlayerList.UpdateTable( table, scrollBar, true );
			FrmDrawForm(frm);
			gPlayerList.DisplayItemTotal();
			DrawLine();
         	handled = true;
         	break;
		default:
			break;
	}    
	//Set focus to table if there is item
	if (gPlayerList.HasItem())
	{
		FrmSetFocus(frm,FrmGetObjectIndex(frm,PlayerListFormTable));
		FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,PlayerListFormTable),&recTable);
		FrmGlueNavDrawFocusRing(frm,PlayerListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
	}		
	return handled; 
}
Boolean ChallengeListFormHandleFormUpdate( EventPtr event )
{
	RectangleType	recTable;
	FormPtr frm = FrmGetActiveForm();
	Boolean handled = false;
	ScrollBarPtr scrollBar = (ScrollBarType*)GetObjectPtr( ChallengeListFormScrollBar );
	TablePtr table = (TableType*)GetObjectPtr( ChallengeListFormTable );
    if (FrmGetActiveFormID() != ChallengeListForm)
    	return true;
	switch( event->data.frmUpdate.updateCode )
	{
		case frmReloadUpdateCode:
			//Reset skipped items 
		case frmRedrawUpdateCode:
   			gChallengeList.UpdateTable( table, scrollBar, true );
			FrmDrawForm(frm);
			DrawLine();
         	handled = true;
         	break;
		default:
			break;
	}    
	//Set focus to table if there is item
	if (gChallengeList.HasItem())
	{
		FrmSetFocus(frm,FrmGetObjectIndex(frm,ChallengeListFormTable));
		FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,ChallengeListFormTable),&recTable);
		FrmGlueNavDrawFocusRing(frm,ChallengeListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
	}		
    //Auto close challenger form and return to previouse form if there is no challenger
	else
	{
		FrmGotoForm(gReturnToFormID);
	}
	return handled;
}
Boolean MatchFormHandleFormUpdate( EventPtr event )
{
	Boolean handled = false;	
	switch( event->data.frmUpdate.updateCode )
	{
		case frmReloadUpdateCode:		
			handled = MatchFormDisplay();
			break;
		case frmRedrawUpdateCode:
			FrmDrawForm( FrmGetActiveForm());
         	handled = true;
         	break;
		default:
			break;
	}    
	return handled;
}
Boolean BoardFormHandleFormUpdate(EventPtr event)
{
	FormPtr     frm = FrmGetActiveForm();
	FrmDrawForm( frm );
	switch( event->data.frmUpdate.updateCode )
	{
		case frmChangeBoardUpdateCode:
			//FrmDrawForm( frm );
			FrmSetFocus(frm,FrmGetObjectIndex(frm,BoardFormBoardSelectButton));
			gBoardPool.GetCurrentBoard()->ShowBoard(); 
			break;
		default:
			FrmSetFocus(frm,FrmGetObjectIndex(frm,BoardFormCancelButton));
			//FrmDrawForm( frm );
			gBoardPool.GetCurrentBoard()->ShowBoard(); 
			break;
	}
	//if (gBoard)
	//	gBoard->ShowBoard();
	return true;
}
void DrawAccountItem(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)
{
	TablePtr table = (TablePtr)tableP;
	RGBColorType	textColor, backColor;
   	RectangleType tableBounds;
   	TblGetBounds( table, &tableBounds );
   	Int16 tableWidth = tableBounds.extent.x;

   	RectangleType rowBounds;
   	Int16 x = rowBounds.topLeft.x = bounds->topLeft.x;
   	Int16 y = rowBounds.topLeft.y = bounds->topLeft.y;
   	rowBounds.extent.y = bounds->extent.y;
   	rowBounds.extent.x = tableWidth;
   	WinPushDrawState();
	UIColorGetTableEntryRGB(UIMenuSelectedForeground,&textColor);
	UIColorGetTableEntryRGB(UIMenuSelectedFill,&backColor);

   	TGoAccount *item = (TGoAccount*)TblGetItemPtr( table, row, column );
   	if( !item )
    {
//      showMessage( "No item in TreeView::drawRecord()." );
    	return;
    }
   	//Used Mark
   	//x= 1;
   	if (gCurrentAccountID  == row)
   	{
   		//Highlight the selected account
   		WinSetTextColorRGB(&textColor,NULL);
   		WinSetBackColorRGB(&backColor,NULL);
   		WinEraseRectangle( &rowBounds, 0 ); 
   	}
   	//Mark as Used Account
   	//if (gAccountIdToBeUsed  == row)
	//	WinDrawChars( ">", 1, 1, y );   	
	// Figure out what color this file should be drawn in
//	RGBColorType	color;
//	color = determineColor( item );
//	Set color red
//	textColor = &blue;
//	ColorSet( 0, backColor, textColor, 0, 0 );

	Char displayTitle[25];
	Int16 textLen;
	Int16 width;
   	Int16 columnWidth;
   	Boolean fits;
   	//Description
   	x = 12;
   	columnWidth = 78;
 
   	fits = false;   
   	StrCopy(displayTitle,item->description);
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
   	//Login ID
   	x = 91;
   	columnWidth = 60;
 
   	fits = false;   
   	StrCopy(displayTitle,item->userName);
	textLen = StrLen( displayTitle);
	width = columnWidth;
	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
   // if the full filename does not fit, then we shorten it a little
   //  more so that we can draw an elipis after it.
   	if( !fits )
    {
    	fits = false;
      	textLen = StrLen( displayTitle);
      	width = columnWidth - 5;
      	FntCharsInWidth( displayTitle, &width, &textLen, &fits );
      	WinDrawChars( displayTitle, textLen, x, y );
      	WinDrawChars( "...", 3, x+width, y );
    }
   	else
    	WinDrawChars( displayTitle, textLen, x, y );
   	
    WinPopDrawState();
	//ColorUnset();

}

void AccountListFormInitTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
	{		
		TblSetItemStyle( table, row, 0, customTableItem );
		TblSetItemFont( table, row, 0, stdFont );
		TblSetRowUsable( table, row, false );
	}
	TblSetColumnUsable( table, 0, true );
   	TblSetCustomDrawProcedure( table, 0, DrawAccountItem );
}

void AccountListFormUpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw)
{
	Int16	row;
	Int16	totalItems;
	Int16	tableRows = TABLEROWS;
	TGoAccount *itemP;
	totalItems = gAccountNum;
   	AccountListFormInitTable(table);
	for( row=0; row<tableRows; row++ )
    {
    	if( row >= totalItems )
        	TblSetRowUsable( table, row, false );			// so it won't get drawn
		else
        {
        	itemP = &gAccountList[row];
			TblSetRowUsable( table, row, true );			// so it will get drawn
			TblMarkRowInvalid( table, row );					// so it will get updated
			TblSetItemPtr( table, row, 0, (void *)itemP);
			itemP = (TGoAccount*)TblGetItemPtr( table, row, 0 );
		}
	}
	if( redraw )
    	TblDrawTable( table );
	//UpdateScrollers( table, scrollBar );   
}

Boolean AccountListFormHandleFormUpdate( EventPtr event )
{
	Boolean handled = false;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( AccountListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( AccountListFormTable );
   
	switch( event->data.frmUpdate.updateCode )
	{
		case frmReloadUpdateCode:
			//Reset skipped items 
		case frmRedrawUpdateCode:
   			AccountListFormUpdateTable( table, scrollBar, true );
   			
			FrmDrawForm( FrmGetActiveForm());
			DrawLine();
			
         	handled = true;
         	break;
		default:
			break;
	}    
	return true;
}


Boolean GameListFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( GameListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( GameListFormTable );
	
    //Handle table events first
    handled = gGameList.HandleEvent( pEvent, table, scrollBar );      
    if( handled ) return handled;
	//Then other events
	switch (pEvent->eType) {
		case menuEvent:
			break;
		case frmOpenEvent:
			handled = GameListFormHandleFormOpen();
			break;
		case frmUpdateEvent:
			handled = GameListFormHandleFormUpdate( pEvent );
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case GameListFormRefreshButton:
					gGameList.Flush();
					WriteServer("game");
					break;
				case GameListFormCancelButton:
					FrmGotoForm(MainForm);
					break;
				case GameListFormChallengeButton:
					gReturnToFormID = GameListForm;
					FrmGotoForm(ChallengeListForm);
					break;	
				case GameListFormLeftButton:
					gGameListTab = 0;
					gGameList.SetTableDrawProcedure(DrawGameItem);
					FrmUpdateForm(GameListForm,frmReloadUpdateCode);
					break;
				case GameListFormRightButton:
					gGameListTab = 1;
					gGameList.SetTableDrawProcedure(DrawGameItem2);
					FrmUpdateForm(GameListForm,frmReloadUpdateCode);
					break;
				default:
					break;
			}
		case keyDownEvent:
         	/*
         	switch (pEvent->data.keyDown.chr)
         	{
				case vchrNavChange: //[Jin] 5-way navigation
					if(pEvent->data.keyDown.keyCode & navBitRight) 
					//Right Key, same as pressing the right arrow button
					{
						gGameListTab = 1;
						gGameList.SetTableDrawProcedure(DrawGameItem2);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormLeftButton),0);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormRightButton),1);
						FrmUpdateForm(GameListForm,frmReloadUpdateCode);
						handled = true;
					}
					else if(pEvent->data.keyDown.keyCode & navBitLeft)
					//Left Key, same as pressing the left arrow button
					{
						gGameListTab = 0;
						gGameList.SetTableDrawProcedure(DrawGameItem);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormLeftButton),1);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormRightButton),0);
						FrmUpdateForm(GameListForm,frmReloadUpdateCode);
						handled = true;
					}
					break;   
				default:
					break;
         	}
         	*/
         	if (!gNavSupport)	//not support 5-way navigation 
         	{
         		if (NavDirectionPressed(pEvent, Right))
					//Right Key, same as pressing the right arrow button
					{
						gGameListTab = 1;
						gGameList.SetTableDrawProcedure(DrawGameItem2);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormLeftButton),0);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormRightButton),1);
						FrmUpdateForm(GameListForm,frmReloadUpdateCode);
						handled = true;
						break;
					}
				if (NavDirectionPressed(pEvent, Left))
					//Left Key, same as pressing the left arrow button
					{
						gGameListTab = 0;
						gGameList.SetTableDrawProcedure(DrawGameItem);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormLeftButton),1);
						//CtlSetValue((ControlType*)GetObjectPtr(GameListFormRightButton),0);
						FrmUpdateForm(GameListForm,frmReloadUpdateCode);
						handled = true;
						break;
					}
         	}
         	break;
        case penDownEvent:
        	handled = GameListFormHandlePenDown(pEvent);
        	if (handled)
        	{
        		gGameList.SortList(gGameSortedField,gGameAscending);
        		FrmUpdateForm(GameListForm,frmReloadUpdateCode);
        	}
        	break;
		default:
			break;
	}
	
	return handled;
}

Boolean PlayerListFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( PlayerListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( PlayerListFormTable );
	//FormPtr	pForm = FrmGetActiveForm();
	Char	cmdOptionString[30],cmdString[50];
    //Handle table events first
    handled = gPlayerList.HandleEvent( pEvent, table, scrollBar );      
    if( handled ) return handled;
	//Then other events
	switch (pEvent->eType) {
		case menuEvent:
			handled = true;
			switch(pEvent->data.menu.itemID){
				case PlayerListOptionsFilter:
					FrmPopupForm(PlayerFilterForm);
					break;
				default:
					break;
			}
			break; 

		case frmOpenEvent:
			handled = PlayerListFormHandleFormOpen();
			break;
		case frmUpdateEvent:
			handled = PlayerListFormHandleFormUpdate( pEvent );
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case PlayerListFormRefreshButton:
					if (gLowerRank == 0 && gUpperRank == 0)	//No rank specified
						StrCopy(cmdOptionString,"all");
					else
					{
						StrCopy(cmdOptionString,gRankString[gLowerRank]);
						StrCat(cmdOptionString,"-");
						StrCat(cmdOptionString,gRankString[gUpperRank]);
					}
					if (gPlayerOpenOnly)
						StrCat(cmdOptionString," o");
					StrCopy(cmdString,"who ");
					if (gPlayerFilterOn)
						StrCat(cmdString,cmdOptionString);
					gPlayerList.Flush();
					WriteServer(cmdString);
					break;
				case PlayerListFormCancelButton:
					FrmGotoForm(MainForm);
					break;
				case PlayerListFormChallengeButton:
					gReturnToFormID = PlayerListForm;
					FrmGotoForm(ChallengeListForm);
					break;	
				default:
					break;
			}
        case penDownEvent:
        	handled = PlayerListFormHandlePenDown(pEvent);
        	if (handled)
        	{
        		gPlayerList.SortList(gPlayerSortedField,gPlayerAscending);
        		FrmUpdateForm(PlayerListForm,frmReloadUpdateCode);
        	}
        	break;
		default:
			break;
	} 
	  
	return handled;
}

Boolean ChallengeListFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( ChallengeListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( ChallengeListFormTable );

    //Handle table events first
    handled = gChallengeList.HandleEvent( pEvent, table, scrollBar );      
    if( handled ) return handled;
	//Then other events
	switch (pEvent->eType) {
		case menuEvent:
			return true; 

		case frmOpenEvent:
			handled = ChallengeListFormHandleFormOpen();
			break;
		case frmUpdateEvent:
			handled = ChallengeListFormHandleFormUpdate( pEvent );
			break; 
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case ChallengeListFormSelectButton:
				case ChallengeListFormCancelButton:
					FrmGotoForm(gReturnToFormID);
					break;
				default:
					break;
			}
		default:
			break;
	}
	
	return handled;
}
Boolean ColorPrefFormHandleEvent(EventPtr e)
{
   Boolean handled = false;
   FormPtr frm;
   static IndexedColorType selectedBoardColor,selectedStripColor; //selectedTextColor, 
   RectangleType	rBoard,rStrip;
   rBoard.topLeft.x = 100;
   rStrip.topLeft.x = 100;
   rBoard.topLeft.y = 17;
   rStrip.topLeft.y = 45; 
   rBoard.extent.x = 40;
   rStrip.extent.x = 40;
   rBoard.extent.y = 10;
   rStrip.extent.y = 10;

   frm = FrmGetActiveForm();
   
   

   switch (e->eType) 
   { 
      case frmOpenEvent:
   			FrmDrawForm(frm);
   			WinPushDrawState();
   			selectedBoardColor = gBoardColor;
   			//selectedTextColor = gTextColor;
   			selectedStripColor = gStripColor;
      		WinSetBackColor(selectedBoardColor);
      		WinEraseRectangle(&rBoard,0);
      		//WinSetBackColor(greyColor);
      		//WinSetTextColor(selectedTextColor);
      		//WinPaintChars(" Let's go GO! ",StrLen(" Let's go GO! "),rBoard.topLeft.x,31);
      		WinSetBackColor(selectedStripColor);
      		WinEraseRectangle(&rStrip,0);
         	handled = true;
         	WinPopDrawState();
         	break;
      case ctlSelectEvent:
         switch (e->data.ctlSelect.controlID)
         {
         	case ColorPrefOKButton:
               	FrmReturnToForm(0);
               	gBoardColor = selectedBoardColor;
               	//gTextColor = selectedTextColor;
               	gStripColor = selectedStripColor;
         		handled = true;
         		break;
         	case ColorPrefDefaultButton:
         		WinPushDrawState();
        		selectedBoardColor = gDefaultBackgroundColor;
  				WinSetBackColor(selectedBoardColor);
  				WinEraseRectangle(&rBoard,0);
        		//selectedTextColor = gDefaultTextColor;
  				//WinSetBackColor(greyColor);
  				//WinSetTextColor(selectedTextColor);
  				//WinPaintChars(" Let's go GO! ",StrLen(" Let's go GO! "),rBoard.topLeft.x,31);
        		selectedStripColor = greyColor;
  				WinSetBackColor(selectedStripColor);
  				WinEraseRectangle(&rStrip,0);
      			WinPopDrawState();
         		break;
         	case ColorPrefCancelButton:
               	FrmReturnToForm(0);
         		handled = true;
         		break;
         	default:
         		break;
         }
         break;
      case penDownEvent: 
            WinPushDrawState();
            if (e->screenX>rBoard.topLeft.x-1 && e->screenX< rBoard.topLeft.x +rBoard.extent.x+1 &&
            	e->screenY>rBoard.topLeft.y-1 && e->screenY< rBoard.topLeft.y +rBoard.extent.y +1 )
            	{
            		UIPickColor(  &selectedBoardColor, NULL, NULL, "Select Board Color", NULL );
      				WinSetBackColor(selectedBoardColor);
      				WinEraseRectangle(&rBoard,0);
            	}
/*            else if (e->screenX>rBoard.topLeft.x-1 && 
            		e->screenX< rBoard.topLeft.x +FntCharsWidth(" Let's go GO! ",StrLen(" Let's go GO! ")+1) &&
            		e->screenY> 31-1 && e->screenY< 31 + FntCharHeight() +1)
            	{
            		UIPickColor(  &selectedTextColor, NULL, NULL, "Select Text Color", NULL );
      				WinSetBackColor(greyColor);
      				WinSetTextColor(selectedTextColor);
      				WinPaintChars(" Let's go GO! ",StrLen(" Let's go GO! "),rBoard.topLeft.x,31);
            	}*/
            else if (e->screenX>rStrip.topLeft.x-1 && e->screenX< rStrip.topLeft.x +rStrip.extent.x+1 &&
            	e->screenY>rStrip.topLeft.y-1 && e->screenY< rStrip.topLeft.y +rStrip.extent.y +1 )
            	{
            		UIPickColor(  &selectedStripColor, NULL, NULL, "Select Strip Color", NULL );
      				WinSetBackColor(selectedStripColor);
      				WinEraseRectangle(&rStrip,0);
            	}
            WinPopDrawState(); 
            break;
      
      default: 
         break;
   }
   
   
   return handled;
}
Boolean SoundPrefFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
	FieldType	*fldP;
   	FormPtr frm;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	static int byAlert;
	Char	byAlertString[4];
	
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
      		CtlSetValue((ControlType*)GetObjectPtr(SoundPrefPlaySoundCheckBoxButton),gPlaySound);
      		byAlert = gByoAlert;
      		StrIToA(byAlertString,byAlert);
			fldP = (FieldType*)GetObjectPtr(SoundPrefAlertValueField);
			DrawFieldText(fldP,byAlertString);
         	handled = true; 
         	break;
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case SoundPrefOKButton:
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(SoundPrefAlertValueField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get byo alert
						if (h1)
							byAlert = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Save new values
					gByoAlert = byAlert;
					gPlaySound = CtlGetValue((ControlType*)GetObjectPtr(SoundPrefPlaySoundCheckBoxButton));					
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case SoundPrefCancelButton:
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	default:
	         		break;
         	}
         	break;
       	case ctlRepeatEvent: 
         	switch (e->data.ctlSelect.controlID)
         	{
         		case SoundPrefScrollUpButton:
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(SoundPrefAlertValueField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get byo alert
						if (h1)
							byAlert = StrAToI(h1);
						MemHandleUnlock(TextH);
					} 
         			byAlert++;
         			if (byAlert>999)
         				byAlert = 999;
      				StrIToA(byAlertString,byAlert);
					fldP = (FieldType*)GetObjectPtr(SoundPrefAlertValueField);
					DrawFieldText(fldP,byAlertString);
	         		handled = true;
         			break;
         		case SoundPrefScrollDownButton:
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(SoundPrefAlertValueField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get byo alert
						if (h1)
							byAlert = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
         			byAlert--;
         			if (byAlert<10)
         				byAlert = 10;
      				StrIToA(byAlertString,byAlert);
					fldP = (FieldType*)GetObjectPtr(SoundPrefAlertValueField);
					DrawFieldText(fldP,byAlertString);
	         		handled = true;
         			break;
         		default:
         			break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
Boolean NetworkPrefFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
	FieldType	*fldP;
   	FormPtr frm;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	static Int16 	netLag;
   	static Int16	netQuality;
	Char	netLagString[4];
	
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
   			netQuality = gNetQuality;
			CtlSetLabel((ControlType*)GetObjectPtr(NetworkPrefNetQualityTrigger),\
			LstGetSelectionText((ListType*)GetObjectPtr(NetworkPrefNetQualityList),netQuality));
      		netLag = gNetLag;
      		StrIToA(netLagString,netLag);
			fldP = (FieldType*)GetObjectPtr(NetworkPrefNetLagField);
			DrawFieldText(fldP,netLagString);
         	handled = true;
         	break;
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case NetworkPrefOKButton:
         			gNetQuality = netQuality;
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(NetworkPrefNetLagField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get Max. Network Lag
						if (h1)
							netLag = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Save new values
					gNetLag = netLag;
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
         		case NetworkPrefDefaultButton:
         			netQuality = MEDIUMQUALITY;
					CtlSetLabel((ControlType*)GetObjectPtr(NetworkPrefNetQualityTrigger),\
					LstGetSelectionText((ListType*)GetObjectPtr(NetworkPrefNetQualityList),netQuality));
         			netLag = DEFAULTNETLAG;
      				StrIToA(netLagString,netLag);
					fldP = (FieldType*)GetObjectPtr(NetworkPrefNetLagField);
					DrawFieldText(fldP,netLagString);
	         		handled = true;
         			break;
	         	case NetworkPrefCancelButton:
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case NetworkPrefNetQualityTrigger:
	         		LstSetSelection((ListType*)GetObjectPtr(NetworkPrefNetQualityList),netQuality);
	       			LstMakeItemVisible((ListType*)GetObjectPtr(NetworkPrefNetQualityList),netQuality);
		           	handled = false;
	         		break;
	         	default:
	         		break;
         	}
         	break;
       	case ctlRepeatEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case NetworkPrefScrollUpButton:
      				//Get Max. Network Lag
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(NetworkPrefNetLagField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							netLag = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
         			netLag++;
         			if (netLag>MAXNETLAG)
         				netLag = MAXNETLAG;
      				StrIToA(netLagString,netLag);
					fldP = (FieldType*)GetObjectPtr(NetworkPrefNetLagField);
					DrawFieldText(fldP,netLagString);
	         		handled = true;
         			break;
         		case NetworkPrefScrollDownButton:
      				//Get Max. Network Lag
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(NetworkPrefNetLagField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							netLag = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
         			netLag--;
         			if (netLag<MINNETLAG)
         				netLag = MINNETLAG;
      				StrIToA(netLagString,netLag);
					fldP = (FieldType*)GetObjectPtr(NetworkPrefNetLagField);
					DrawFieldText(fldP,netLagString);
	         		handled = true;
         			break;
         		default:
         			break;
         	}
         	break;
        case popSelectEvent:
        	switch (e->data.popSelect.listID)
        	{
        		case NetworkPrefNetQualityList:
        			netQuality = e->data.popSelect.selection;
        			CtlSetLabel((ControlType*)GetObjectPtr(NetworkPrefNetQualityTrigger),\
        			LstGetSelectionText((ListType*)GetObjectPtr(NetworkPrefNetQualityList),netQuality));
        			handled = true;
        			break;
        		default:
        			break;
        	}
        	break;
         default:
         	break;
   	}
   	return handled;
}

Boolean SeekFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
   	FormPtr frm;
   	static Int16	seekConfigIndex = 0, seekHandicap = 0;
	Char	cmdString[90];
	
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
      		if (gSeek)
      			ShowObject(frm,SeekFormStopButton);
      		else
      			HideObject(frm,SeekFormStopButton);
   			seekConfigIndex = gSeekConfigIndex;
   			seekHandicap = (gSeekHandicap==0?0:gSeekHandicap-1);
   			switch (seekConfigIndex) {
   				case 0:
       				CtlSetValue((ControlType*)GetObjectPtr(SeekFormConfig0),1);
       				break;
   				case 1:
       				CtlSetValue((ControlType*)GetObjectPtr(SeekFormConfig1),1);
       				break;
   				case 2:
       				CtlSetValue((ControlType*)GetObjectPtr(SeekFormConfig2),1);
       				break;
   				case 3:
       				CtlSetValue((ControlType*)GetObjectPtr(SeekFormConfig3),1);
       				break;
   			}
			CtlSetLabel((ControlType*)GetObjectPtr(SeekFormHandicapTrigger),\
			LstGetSelectionText((ListType*)GetObjectPtr(SeekFormHandicapList),seekHandicap));
   			FrmDrawForm(frm);
         	handled = true;
         	break; 
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case SeekFormOKButton:
         			gSeekConfigIndex = seekConfigIndex;
         			gSeekHandicap = (seekHandicap==0?0:seekHandicap+1);
     				sprintf(cmdString,"seek entry %d 19 %d %d 0",gSeekConfigIndex,gSeekHandicap,gSeekHandicap);
     				WriteServer(cmdString);
						#ifdef _DEBUG_
						WriteTerminal(cmdString);
						WriteTerminal("\n"); 
						#endif
	               	FrmReturnToForm(MainForm);
	               	FrmUpdateForm(MainForm,frmReloadUpdateCode);
	         		handled = true;
	         		break;
	         	case SeekFormStopButton:
     				sprintf(cmdString,"seek entry_cancel %d",gSeekConfigIndex);
     				WriteServer(cmdString); 
						#ifdef _DEBUG_
						WriteTerminal(cmdString);
						WriteTerminal("\n"); 
						#endif
	               	FrmReturnToForm(MainForm);
	               	FrmUpdateForm(MainForm,frmReloadUpdateCode);
	         		handled = true;
	         		break;
	         	case SeekFormCancelButton:  
	               	FrmReturnToForm(MainForm);
	               	FrmUpdateForm(MainForm,frmReloadUpdateCode);
	         		handled = true; 
	         		break;
	         	case SeekFormConfig0:
	         		seekConfigIndex = 0;
	         		break;
	         	case SeekFormConfig1:
	         		seekConfigIndex = 1;
	         		break;
	         	case SeekFormConfig2:
	         		seekConfigIndex = 2;
	         		break;
	         	case SeekFormConfig3:
	         		seekConfigIndex = 3;
	         		break;
	         	case SeekFormHandicapTrigger:
	         		LstSetSelection((ListType*)GetObjectPtr(SeekFormHandicapList),seekHandicap);
	         		LstMakeItemVisible((ListType*)GetObjectPtr(SeekFormHandicapList),seekHandicap);
	         		handled = false;
	         		break;
	         	default:
	         		break;
         	}
         	break;
        case popSelectEvent:
        	switch (e->data.popSelect.listID)
        	{
        		case SeekFormHandicapList:
        	 		seekHandicap = e->data.popSelect.selection;
        			CtlSetLabel((ControlType*)GetObjectPtr(SeekFormHandicapTrigger),\
        			LstGetSelectionText((ListType*)GetObjectPtr(SeekFormHandicapList),seekHandicap));
        			
        			handled = true;
        			break;
        		default:
        			break;
        	}
        	break;
         default:
         	break;
   	} 
   	return handled;
}
Boolean ProxyFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
   	FormPtr frm;
	FieldType	*fldP;
	//TGoAccount	account;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	
   	frm = FrmGetActiveForm();
   	//account = gAccountList[gCurrentAccountID];

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
   			//Boundry check
   			if (StrLen(gAccount.proxyName)>25)
         		StrCopy(gAccount.proxyName,"");
   			if (StrLen(gAccount.proxyPort)>5)
         		StrCopy(gAccount.proxyPort,"");
			fldP = (FieldType*)GetObjectPtr(ProxyFormName);
			DrawFieldText(fldP,gAccount.proxyName);
			fldP = (FieldType*)GetObjectPtr(ProxyFormPort);
			DrawFieldText(fldP,gAccount.proxyPort);
         	handled = true;
         	break; 
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case ProxyFormOKButton:
					//Save fields' value;
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(ProxyFormName));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get proxy address
						if (h1)
							StrCopy(gAccount.proxyName,h1);
						MemHandleUnlock(TextH);
					}
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(ProxyFormPort));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get proxy port
						if (h1)
							StrCopy(gAccount.proxyPort,h1);
						MemHandleUnlock(TextH);
					}
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case ProxyFormCancelButton:  
	               	FrmReturnToForm(0);
	         		handled = true; 
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	} 
   	return handled;
}
Boolean PasswordFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
	FieldType	*fldP;
   	FormPtr frm;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
			fldP = (FieldType*)GetObjectPtr(PasswordFormPasswordField);
			DrawFieldText(fldP,gAccount.password);
         	handled = true;
         	break;
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case PasswordFormOKButton:
					//Save fields' value;
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(PasswordFormPasswordField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get password string
						if (h1)
							StrCopy(gAccount.password,h1);
						MemHandleUnlock(TextH);
					}
					FrmUpdateForm(AccountEditForm,frmRedrawUpdateCode);	//Update Password selector trigger
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case PasswordFormCancelButton:  
	               	FrmReturnToForm(0);
	         		handled = true; 
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
Boolean TitleFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
	FieldType	*fldP;
   	FormPtr frm;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	TSgfInfo	sgf;
	CBoard		*workingBoard = gBoardPool.GetCurrentBoard(); 
	Char		cmdString[90];
	
	cmdString[0] = NULL;
	if (!workingBoard)
		return true;
	
	workingBoard->GetSgfInfo(sgf);
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
			fldP = (FieldType*)GetObjectPtr(TitleFormTitleField);
			if (sgf.EV)
				DrawFieldText(fldP,sgf.EV);
         	handled = true;
         	break;
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case TitleFormOKButton:
					//Save fields' value;
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(TitleFormTitleField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	//Get title string
						if (h1)
						{
							sprintf(cmdString,"title %s",h1);
							WriteServer(cmdString);	//Set title
							workingBoard->UpdateTitle(h1);
							FrmUpdateForm(GameInfoForm,frmRedrawUpdateCode);	//Update Game Info
						}
						MemHandleUnlock(TextH);
					}
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case TitleFormCancelButton:  
	               	FrmReturnToForm(0);
	         		handled = true; 
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
Boolean AboutFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
   	FormPtr frm;
	UInt8	days;
	MemHandle	resourceH = NULL;
	Char		*resource;
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			#ifdef _PE_
				resourceH = DmGetResource(strRsc, StringProfessionalEdition);
			#endif
   			#ifdef _BE_
				resourceH = DmGetResource(strRsc, StringBasicEdition);
			#endif
			#ifdef _SE_
   			resourceH = DmGetResource(strRsc, StringStandardEdition);
			#endif
			if (resourceH)
			{
				resource = (Char*)MemHandleLock(resourceH);
				//StrCopy(str,resource);
				MemHandleUnlock(resourceH);
				DmReleaseResource(resourceH);
				CtlSetLabel((ControlType*)GetObjectPtr(AboutEditionLabel),resource);
				//DrawFieldText((FieldType*)GetObjectPtr(AboutEditionField),str);
			}
   			FrmDrawForm(frm);
   			if (!CheckRegistered(days))
   				ShowObject(frm,AboutFormRegisterButton);
         	handled = true;    
         	break;
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case AboutFormOKButton:
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case AboutFormRegisterButton:
	         		FrmPopupForm(RegisterForm);
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
Boolean RegisterFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
   	FormPtr frm;
   	frm = FrmGetActiveForm();
   	MemHandle	TextH;
   	Char*		h1=NULL;
	UInt32	RCode = 0;
	UInt32	code1 = 0, code2 = 0;
	MemHandle	resourceH = NULL;
	Char		*resource;

   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			#ifdef _PE_
				resourceH = DmGetResource(strRsc, StringProfessionalEdition);
			#endif
   			#ifdef _BE_
				resourceH = DmGetResource(strRsc, StringBasicEdition);
			#endif
			#ifdef _SE_
   			resourceH = DmGetResource(strRsc, StringStandardEdition);
			#endif
			if (resourceH)
			{
				resource = (Char*)MemHandleLock(resourceH);
				MemHandleUnlock(resourceH);
				DmReleaseResource(resourceH);
				CtlSetLabel((ControlType*)GetObjectPtr(RegisterFormEditionLabel),resource);
			} 
   			FrmDrawForm(frm);
			FrmSetFocus(frm,FrmGetObjectIndex(frm,RegisterFormCode1Field));			
         	handled = true; 
         	break; 
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case RegisterFormOKButton:
         			//Get code1
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(RegisterFormCode1Field));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							code1 = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Get code2
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(RegisterFormCode2Field));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							code2 = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Get register by combined code1 and code2
					RCode = code1;
					RCode = code1 * 100000 + code2;
					SaveRegisterCode(RCode);
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	case RegisterFormCancelButton:
	               	FrmReturnToForm(0);
	         		handled = true;
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
Boolean PlayerFilterFormHandleEvent(EventPtr e)
{
   Boolean handled = false;
   FormPtr frm;
   static Int16	upperRank = 0,lowerRank = 0;
   static Boolean	filterOn = false,openOnly = false;
   frm = FrmGetActiveForm();
   switch (e->eType) 
   { 
      case frmOpenEvent:
      		filterOn = gPlayerFilterOn;
      		upperRank = gUpperRank;
      		lowerRank = gLowerRank;
      		openOnly = gPlayerOpenOnly;
      		CtlSetValue((ControlType*)GetObjectPtr(PlayerFilterFormApplyCheckBox),filterOn);
			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormUpperRankTrigger),\
			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormUpperRankList),upperRank));
			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormLowerRankTrigger),\
			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormLowerRankList),lowerRank));
			if (openOnly)
			{
	      		CtlSetValue((ControlType*)GetObjectPtr(PlayerFilterFormOpenPushButton),true);
	      		CtlSetValue((ControlType*)GetObjectPtr(PlayerFilterFormAllPushButton),false);
			}
			else
			{
	      		CtlSetValue((ControlType*)GetObjectPtr(PlayerFilterFormOpenPushButton),false);
	      		CtlSetValue((ControlType*)GetObjectPtr(PlayerFilterFormAllPushButton),true);
			}
   			FrmDrawForm(frm);
         	handled = true;
         	break;
      case ctlSelectEvent:
         switch (e->data.ctlSelect.controlID)
         {
         	case PlayerFilterFormCancelButton:
               	FrmReturnToForm(0);
         		handled = true;
         		break;
         	case PlayerFilterFormOKButton:
         		gPlayerFilterOn = CtlGetValue((ControlType*)GetObjectPtr(PlayerFilterFormApplyCheckBox));
         		gUpperRank = upperRank;
         		gLowerRank = lowerRank;
         		gPlayerOpenOnly = CtlGetValue((ControlType*)GetObjectPtr(PlayerFilterFormOpenPushButton));
               	FrmReturnToForm(0);
         		handled = true;
         		break;
         	case PlayerFilterFormUpperRankTrigger:
         		LstSetSelection((ListType*)GetObjectPtr(PlayerFilterFormUpperRankList),upperRank);
       			LstMakeItemVisible((ListType*)GetObjectPtr(PlayerFilterFormUpperRankList),upperRank);
	           	handled = false;
         		break;
         	case PlayerFilterFormLowerRankTrigger:
         	  	LstSetSelection((ListType*)GetObjectPtr(PlayerFilterFormLowerRankList),lowerRank);
       			LstMakeItemVisible((ListType*)GetObjectPtr(PlayerFilterFormLowerRankList),lowerRank);
         		handled = false;
         		break;
         	default:
         		break;
         }
         break;
        case popSelectEvent:
        	switch (e->data.popSelect.listID)
        	{
        		case PlayerFilterFormUpperRankList:
        			upperRank = e->data.popSelect.selection;
        			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormUpperRankTrigger),\
        			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormUpperRankList),upperRank));
        			if (upperRank>lowerRank)	//upper rank should be always higher than lower rank
        			{
        				lowerRank = upperRank;
	        			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormLowerRankTrigger),\
	        			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormLowerRankList),lowerRank));
        			}
        			handled = true;
        			break;
        		case PlayerFilterFormLowerRankList:
        			lowerRank = e->data.popSelect.selection;
        			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormLowerRankTrigger),\
        			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormLowerRankList),lowerRank));
        			if (upperRank>lowerRank)	//upper rank should be always higher than lower rank
        			{
        				upperRank = lowerRank;
	        			CtlSetLabel((ControlType*)GetObjectPtr(PlayerFilterFormUpperRankTrigger),\
	        			LstGetSelectionText((ListType*)GetObjectPtr(PlayerFilterFormUpperRankList),upperRank));
        			}
        			handled = true; 
        			break; 
        	}
        	break;
      default: 
         break;
   }
   return handled;
}

Boolean MatchFormHandleEvent(EventPtr e)
{
   Boolean handled = false;
   FormPtr frm;
   frm = FrmGetActiveForm();
   switch (e->eType) 
   { 
      case frmOpenEvent:
      		//handled = MatchFormHandleFormOpen();
      		handled = MatchFormDisplay();
      		break;
      case frmUpdateEvent:
	      	handled = MatchFormHandleFormUpdate( e);
      		break;
      		
      case ctlSelectEvent:
         switch (e->data.ctlSelect.controlID)
         {
         	case MatchFormAcceptButton:
         		MatchFormHandleAccept();
               	//FrmReturnToForm(0);
               	HideObject( frm, MatchFormAcceptButton );
               	HideObject( frm, MatchFormAutoMatchCheckBox );
               	useAutomatch = false;
         		handled = true;
         		break;
         	case MatchFormDeclineButton:
         		MatchFormHandleDecline();
         		handled = true;
               	FrmReturnToForm(0);
				FrmUpdateForm(ChallengeListForm,frmReloadUpdateCode);	
         		break;
         	case MatchFormCancelButton:
         		handled = true;
               	FrmReturnToForm(0);
				FrmUpdateForm(ChallengeListForm,frmReloadUpdateCode);	
         		break;
         	case MatchFormAutoMatchCheckBox:
         		useAutomatch = false;
         		if (CtlGetValue((ControlType*)GetObjectPtr(MatchFormAutoMatchCheckBox)))
         		{
			    	ShowAutoMatchFields(frm,gMyStatsDefs.maxTime,gMyStatsDefs.maxByoyomi,gMyStatsDefs.maxStones,IGS_AMATCH);
			    	useAutomatch = true;			    					    	
         		}
               	else if (gCurrentMatch.matchtype == IGS_NMATCH)
               	{
					ShowAllMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.stones,gCurrentMatch.handicap,gCurrentMatch.matchtype);
               	}
				else
					ShowMatchFields(frm,gCurrentMatch.main,gCurrentMatch.byoyomi,gCurrentMatch.matchtype);
         		handled = true;
         		break;
         	default:
         		break;
         }
		break;
	 default:
	 	break;
   }
   return handled;
}    
Boolean StoredFormHandleEvent(EventPtr e)
{
   Boolean handled = false;
   FormPtr frm;
   frm = FrmGetActiveForm();
   static Char	storedGameName[25];
   Char	cmdString[30];
   switch (e->eType) 
   { 
      case frmOpenEvent:
      		FrmDrawForm(frm);
      		if (gAccountList[gAccountIdToBeUsed].serverType != IGS)
      		{
      			HideObject(frm,StoredFormDeleteButton);
      			HideObject(frm,StoredFormResignButton);
      		}
			//Set Stored Game List
			LstSetDrawFunction((ListType*)GetObjectPtr(StoredFormSelectGameList),DrawStoredGameListItem);
			LstSetListChoices((ListType*)GetObjectPtr(StoredFormSelectGameList),NULL,gStoredGameList.Size());
			if (gStoredGameList.Size()>0)
				StrCopy(storedGameName,gStoredGameList[0]);
			else
				storedGameName[0]='\0';
        	CtlSetLabel((ControlType*)GetObjectPtr(StoredFormSelectGameTrigger),storedGameName);
      		handled = true;
      		break;
      		
      case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
	         	case StoredFormLoadButton:
	         		sprintf(cmdString,"load %s",storedGameName);
					WriteServer(cmdString);
					if (gAccountList[gAccountIdToBeUsed].serverType != IGS)
						WriteServer("unpause");	//In case the game clock is paused
					/*
					{
						unsigned char action = PLAY;
						unsigned char status = PREPARING;
						if (gBoard)
						{
							delete gBoard;	//Abandon the old game
							gBoard = NULL;
						}
						gBoard = new CBoard;	//Create an new game
						if (!gBoard)
						{
							// Memory Allocation Failed.
							return false;
						}
						gBoard->UpdateGameInfo(0,&action,&status,NULL,NULL);
						if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
							FrmGotoForm(BoardForm);
						else
							FrmUpdateForm(BoardForm,frmReloadUpdateCode);
					}
					*/
	         		handled = true;
	         		break;
	         	case StoredFormResignButton:
	         		sprintf(cmdString,"resign %s",storedGameName);
					WriteServer(cmdString);
	         		handled = true;
	         		break;
	         	case StoredFormDeleteButton:
	         		sprintf(cmdString,"delete %s",storedGameName);
					WriteServer(cmdString);
	         		handled = true;
	         		break;
	         	case StoredFormCancelButton:
	         		handled = true;
	               	FrmGotoForm(MainForm);
	         		break;
	     		case StoredFormSelectGameTrigger:
	     			LstSetListChoices((ListType*)GetObjectPtr(StoredFormSelectGameList),NULL,gStoredGameList.Size());
	     			handled = false;
	     			break;
	         	default:
	         		break;
	         }
			break;
        case popSelectEvent:
        	switch (e->data.popSelect.listID)
        	{
        		Int16	itemNum;
        		case StoredFormSelectGameList:
        			itemNum = e->data.popSelect.selection;
					StrCopy(storedGameName,gStoredGameList[itemNum]);	
        			CtlSetLabel((ControlType*)GetObjectPtr(StoredFormSelectGameTrigger),storedGameName);
        			handled = true;
        			break;
        		default:
        			break;
        	}
        	break;
	 default:
	 	break;
   }
   return handled;
}    
Boolean PlayerViewFormHandleEvent(EventPtr e)
{
   	Boolean handled = false;
   	FormPtr frm;
   	frm = FrmGetActiveForm();
   	Char	newTitle[25] = "Player view Title";
   	Char	*resource;
	MemHandle	resourceH = NULL;
	//UInt8	action;	//'P'= Playing, 'O'= Observing, 'I'= Idle
	//UInt16	gameID; 
	Char	actionString[50], gameidStr[4];
	CChallengeItem *matchP;
	Char *name,*rank;
   switch (e->eType) 
   { 
      case frmOpenEvent:
      		FrmDrawForm(frm);
      		if (gGuestAccount)
      		{
      			HideObject( frm, PlayerViewFormMatchButton );
      			HideObject( frm, PlayerViewFormTellButton );
      		}
      		/*
	  		if (gCurrentMatch.playerName)
	  		{
		  		StrCopy(newTitle,gCurrentMatch.playerName);
	  		}
	  		if (gCurrentMatch.playerRank)
	  		{
		  		StrCat(newTitle," ");
		  		StrCat(newTitle,gCurrentMatch.playerRank);
	  		}
	  		*/
	  		if (gCurrentPlayerP->playerName)
	  		{
		  		StrCopy(newTitle,gCurrentPlayerP->playerName);
	  		}
	  		if (gCurrentPlayerP->rank)
	  		{
		  		StrCat(newTitle," ");
		  		StrCat(newTitle,gCurrentPlayerP->rank);
	  		}
	  		FrmCopyTitle(FrmGetActiveForm(),newTitle);
	  		//Check if the player is playing or observing a game
	  		switch (gCurrentPlayerP->action)
	  		{
				case PLAY:
					resourceH = DmGetResource(strRsc, StringPlayingGame);
					if (resourceH)
					{
						resource = (Char*)MemHandleLock(resourceH);
						StrCopy(actionString,resource);
						MemHandleUnlock(resourceH);
						DmReleaseResource(resourceH);
					
						//StrCopy(actionString,"is playing game ");
						StrIToA(gameidStr,gCurrentPlayerP->gameID);
						StrCat(actionString,gameidStr);
						WinDrawChars(actionString, StrLen(actionString), 5, 17 );
					}
					HideObject( frm, PlayerViewFormMatchButton );
					break;
				case OBSERVE:
					resourceH = DmGetResource(strRsc, StringObservingGame);
					if (resourceH)
					{
						resource = (Char*)MemHandleLock(resourceH);
						StrCopy(actionString,resource);
						MemHandleUnlock(resourceH);
						DmReleaseResource(resourceH);
					
						//StrCopy(actionString,"is observing game ");
						StrIToA(gameidStr,gCurrentPlayerP->gameID);
						StrCat(actionString,gameidStr);
						WinDrawChars(actionString, StrLen(actionString), 5, 17 );
					}
					break;
	  			case IDLE:
	  			default:
					HideObject( frm, PlayerViewFormObserveButton );
	  				break;
	  		}
      		handled = true;
      		break;
      		
      case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
	         	case PlayerViewFormMatchButton:
	         		#ifndef	_BE_
	         		matchP = new CChallengeItem;
	         		if (!matchP)
					{
						// Memory Allocation Failed.
						return false;
					}
					//It's not offered by opponent (I initiate this match)
					matchP->offered = false;	
					//Set default match type 
					if (gAccountList[gAccountIdToBeUsed].serverType == IGS && \
						gAccountList[gAccountIdToBeUsed].supportNmatch)	
						matchP->matchtype = IGS_NMATCH;										
			   		name = (Char*)MemPtrNew(StrLen(gCurrentPlayerP->playerName)+1);
				   	if (name)
				   	{
						StrCopy(name,gCurrentPlayerP->playerName);
						matchP->playerName = name;
				   	}
				   	else	//Memory allocation failed
				   	{
						delete matchP;
						return false;	
				   	}
			   		rank = (Char*)MemPtrNew(StrLen(gCurrentPlayerP->rank)+1);
				   	if (rank)
				   	{
						StrCopy(rank,gCurrentPlayerP->rank);
						matchP->playerRank = rank;
				   	}
				   	else	//Memory allocation failed
				   	{
						delete matchP;
						return false;	
				   	}
				   	SetCurrentMatch(matchP);
					delete matchP;
	         		FrmPopupForm(MatchForm);
	         		FrmReturnToForm(0);
	         		#endif
	         		handled = true;
	         		break;
	         	case PlayerViewFormOKButton:
	         		handled = true;
	               	FrmReturnToForm(0);
	         		break;
	         	case PlayerViewFormTellButton:
			  		if (gCurrentPlayerP->playerName)
			  		{
				  		StrCopy(gChatBuddyName,gCurrentPlayerP->playerName);
	         			UpdateChatBuddyList(gChatBuddyName);
	         			gChatMode = true;
			  		}
	         		handled = true;
	               	FrmReturnToForm(0);
	               	FrmGotoForm(MainForm);
	         		break;
	         	case PlayerViewFormObserveButton:
	         		ObserveGame(gCurrentPlayerP->gameID);
	         		handled = true;
					FrmReturnToForm(0);
	         	default:
	         		break;
	         }
			break;
	 default:
	 	break;
   }
   return handled;
}    


Boolean BoardFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	RectangleType	recDefaultButton;
	FormPtr     frm = FrmGetActiveForm();
	CBoard		*workingBoard = gBoardPool.GetCurrentBoard(); 
	Int16		boardIndex = gBoardPool.GetBoardIndex(gBoardPool.GetCurrentBoard());
	Char	stringBoardNumber[4];
	TGameInfo	gameInfo;
	if (!workingBoard)
		return true;
	switch (pEvent->eType) {
		case menuEvent:
			handled = true;
			switch(pEvent->data.menu.itemID){
				default:
					break;
			} 
			break; 

		case frmOpenEvent:
			handled = BoardFormHandleFormOpen();
			break;
		case frmUpdateEvent:
			handled = BoardFormHandleFormUpdate( pEvent );
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case BoardFormInfoButton:
					FrmGotoForm(GameInfoForm);
					break;
				case BoardFormCancelButton:
				case BoardFormChatButton:
					FrmGotoForm(MainForm);
					break;
				case BoardFormChallengeButton:
					gReturnToFormID = BoardForm;
					FrmGotoForm(ChallengeListForm);
					break;	
				case BoardFormResignButton:
					if (FrmAlert(GameResignAlert) == 0)
						workingBoard->Resign();
					break;
				case BoardFormPassButton:
					workingBoard->Pass();
					break;
				case BoardFormAdjournButton:
					workingBoard->Adjourn();
					break;
				case BoardFormUndoButton:
					workingBoard->Undo();
					break;
				case BoardFormDoneButton:
					workingBoard->Done();
					break;
				case BoardFormMarkButton:
					workingBoard->Mark();
					break;
				case BoardFormMoveLabel:
					workingBoard->SwitchLabel(); 
					if (FrmGetFocus(frm)==noFocus || FrmGetObjectId(frm,FrmGetFocus(frm))!=BoardFormMoveLabel)
					{
						//Move label did not have focus ring previously. It needs to draw focus ring when it is tapped
						break;
					}
					else
					{
						//FrmSetFocus(frm,FrmGetObjectIndex(frm,BoardFormMoveLabel)); 
						//No need to draw focus ring, because it has the ring already
						return true;
					}
				case BoardFormRefreshButton:
					workingBoard->Refresh(); 
					break;
				case BoardFormUnobserveButton:
					workingBoard->GetGameInfo(gameInfo);
					if (gameInfo.action == OBSERVE && gameInfo.status == INPROGRESS)
					{
						if (FrmAlert(ConfirmUnobserveAlert) == 0)
						{
							workingBoard->Unobserve();
							FrmGotoForm(MainForm);
						}
					}
					else if (gameInfo.action == PLAY && (gameInfo.status == INPROGRESS || gameInfo.status == SCORING))
					{
						//Do nothing if we are playing or scoring
						break;
					}
					else
					{
						//Close current(delete) board
						gBoardPool.CloseBoard(gameInfo.id);
						FrmGotoForm(MainForm);
					}
					break;
				case BoardFormBeginButton:
					workingBoard->MoveBegin(); 
					break;
				case BoardFormEndButton:
					workingBoard->MoveEnd(); 
					break;
				case BoardFormFBackwardButton:
					workingBoard->MoveFastBackward(); 
					break;
				case BoardFormFForwardButton:
					workingBoard->MoveFastForward(); 
					break;
				case BoardFormBackwardButton:
					workingBoard->MoveBackward(); 
					break;
				case BoardFormForwardButton:
					workingBoard->MoveForward(); 
					break;
				case BoardFormBoardSelectButton:
				{
					//Dial board number
					//Only show non-empty boards which status are not PREPARING
					Int16	newBoardIndex = boardIndex;
					do 
					{
						newBoardIndex++;
						newBoardIndex = newBoardIndex>=MAX_BOARDS?0:newBoardIndex;
						gBoardPool.GetBoardByIndex(newBoardIndex)->GetGameInfo(gameInfo);
						if (gameInfo.status != PREPARING && newBoardIndex!=boardIndex)
						{
							gBoardPool.SetCurrentBoard(newBoardIndex);
							FrmUpdateForm(BoardForm,frmChangeBoardUpdateCode);
							return true;
						}
					} while (newBoardIndex!=boardIndex);
					
					if (newBoardIndex==boardIndex)
					{
						//Do not change board
						//Only redraw button to avoid BoardSelectorButton being massed up
						FrmSetFocus(frm,FrmGetObjectIndex(frm,pEvent->data.ctlSelect.controlID)); 
						FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,pEvent->data.ctlSelect.controlID),&recDefaultButton);
						FrmGlueNavDrawFocusRing(frm,pEvent->data.ctlSelect.controlID,frmNavFocusRingNoExtraInfo,&recDefaultButton,frmNavFocusRingStyleObjectTypeDefault,true);
						CtlSetLabel((ControlType*)GetObjectPtr(BoardFormBoardSelectButton),StrIToA(stringBoardNumber,boardIndex));
						CtlDrawControl((ControlType*)GetObjectPtr(BoardFormBoardSelectButton));
					}
					
					return true; 
				}
				//To turn display off 
				/*
				case BoardFormJumpButton:
					{
						HWUOpen(gHWRefNum); 
						HWUEnableDisplay(gHWRefNum,false);
						
					}
					break;
				*/
				default:
					break; 
			}
			FrmSetFocus(frm,FrmGetObjectIndex(frm,pEvent->data.ctlSelect.controlID)); 
			FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,pEvent->data.ctlSelect.controlID),&recDefaultButton);
			FrmGlueNavDrawFocusRing(frm,pEvent->data.ctlSelect.controlID,frmNavFocusRingNoExtraInfo,&recDefaultButton,frmNavFocusRingStyleObjectTypeDefault,true);
			
			break;
			
		case frmObjectFocusTakeEvent:
			switch(pEvent->data.frmObjectFocusTake.objectID) {
				case BoardFormBoardSelectButton:
					CtlSetLabel((ControlType*)GetObjectPtr(BoardFormBoardSelectButton),StrIToA(stringBoardNumber,boardIndex));
					CtlDrawControl((ControlType*)GetObjectPtr(BoardFormBoardSelectButton));
					FrmSetFocus(frm,FrmGetObjectIndex(frm,pEvent->data.frmObjectFocusTake.objectID)); 
					FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,BoardFormBoardSelectButton),&recDefaultButton);
					FrmGlueNavDrawFocusRing(frm,BoardFormBoardSelectButton,frmNavFocusRingNoExtraInfo,&recDefaultButton,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
				default:
					FrmSetFocus(frm,FrmGetObjectIndex(frm,pEvent->data.frmObjectFocusTake.objectID)); 
					FrmGetObjectBounds(frm,FrmGetObjectIndex(frm,pEvent->data.frmObjectFocusTake.objectID),&recDefaultButton);
					FrmGlueNavDrawFocusRing(frm,pEvent->data.frmObjectFocusTake.objectID,frmNavFocusRingNoExtraInfo,&recDefaultButton,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
			}
			break;
		
		case frmObjectFocusLostEvent:
			switch(pEvent->data.frmObjectFocusLost.objectID) {
				case BoardFormBoardSelectButton:
					CtlSetLabel((ControlType*)GetObjectPtr(BoardFormBoardSelectButton),StrIToA(stringBoardNumber,boardIndex));
					CtlDrawControl((ControlType*)GetObjectPtr(BoardFormBoardSelectButton));
					handled = true;
					break;
				default:
					break;
			}
			break;
      	case keyDownEvent:
/*         	switch (pEvent->data.keyDown.chr)
         	{
         		case vchrRockerUp:
         			if((pEvent->data.keyDown.modifiers & shiftKeyMask) == 0)
         				break;
            	case pageUpChr:
            	case backspaceChr:
            		gBoard->MoveBackward();
            		handled = true;
            		break;
         		case vchrRockerDown:
         			if((pEvent->data.keyDown.modifiers & shiftKeyMask) == 0)
         				break;
            	case pageDownChr:
            	case linefeedChr:
            		gBoard->MoveForward();
            		handled = true;
					break;
				default:
					handled = false;
					break;
         	}
*/
			handled = workingBoard->KeyDown(pEvent);
         	break;
         case penMoveEvent:
         	workingBoard->PenMove(pEvent);
         	handled = true;
         	break;
         case penUpEvent:
         	workingBoard->PenUp(pEvent);
         	handled = true;
         	break;
      	case winEnterEvent:
   			if (pEvent->data.winEnter.enterWindow == (WinHandle) FrmGetFormPtr(BoardForm))
   			{
   				EvtFlushPenQueue();	//Absorb following pen event
   				if (gWaitDraw)	//It was previously hold
   				{ 
            		gWaitDraw = false; // we can draw or move now
            		FrmUpdateForm(BoardForm,frmRedrawUpdateCode);
   				}
   			}
         	//handled = false;
         	break;
      case winExitEvent: 
         	if (pEvent->data.winExit.exitWindow == (WinHandle) FrmGetFormPtr(BoardForm))
            	gWaitDraw = true; //Stop draw or move
         	break;
		default:
			break;
	} 
	
	return handled;
}
Boolean HandleAccountListSelectItem(TGoAccount* item, UInt16 row)
{
	if (!item)
		return false;
	else 
	{
		gAccount = *item;
		gAccountIdToBeUsed = row;
		gCurrentAccountID = row;
		FrmGotoForm(AccountEditForm);
		return true;
	}
}
Boolean AccountListFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	UInt16   	row;
	TGoAccount *item;
	//Boolean 	checkUsed = false;
	//UInt16   	x = pEvent->screenX;
	//UInt16		checkWidth = 10;
	//ScrollBarPtr scrollBar = (ScrollBarPtr)GetObjectPtr( AccountListFormScrollBar );
	TablePtr table = (TablePtr)GetObjectPtr( AccountListFormTable );
	FormPtr	pForm = FrmGetActiveForm();
	RectangleType	recTable;

    //Handle table events first
/*
    if (pEvent->eType == tblEnterEvent)
    {
		row = pEvent->data.tblSelect.row;
		item = (TGoAccount*)TblGetItemPtr( table, row, 0 );
		if (!item)
			handled = false;
		else 
		{
			handled = true;
			gAccount = *item;
			gAccountIdToBeUsed = row;
			gCurrentAccountID = row;
			FrmGotoForm(AccountEditForm);
		}
    }
    if( handled ) return handled;
*/
	//Then other events
	switch (pEvent->eType) {
		case menuEvent:
			return true; 
		case tblEnterEvent:
			row = pEvent->data.tblSelect.row;
			item = (TGoAccount*)TblGetItemPtr( table, row, 0 );
			return HandleAccountListSelectItem(item, row);
		case keyDownEvent:
			if (!gNavSupport || (gNavSupport && ObjectHasFocus(pForm, AccountListFormTable)))
		  	{
		  		//in Object focus mode, only handle key down envent when table gets focus
				if (NavSelectPressed(pEvent))
				{
					row = gCurrentAccountID;
					item = (TGoAccount*)TblGetItemPtr( table, row, 0 );
					/*
					gAccount = *item;
					gAccountIdToBeUsed = row;
					FrmGotoForm(AccountEditForm);
					handled = true;
					*/
					//return handled;
					handled = HandleAccountListSelectItem(item, row);
				}					
				if (NavDirectionPressed(pEvent, Up))
				{
					gCurrentAccountID --;
					if (gCurrentAccountID < 0)
						gCurrentAccountID = gAccountNum - 1;
					gAccountIdToBeUsed = gCurrentAccountID;
					FrmUpdateForm(AccountListForm,frmReloadUpdateCode);
					handled = true;
				}
				if (NavDirectionPressed(pEvent, Down))
				{
					gCurrentAccountID ++;
					if (gCurrentAccountID >= gAccountNum)
						gCurrentAccountID = 0;
					gAccountIdToBeUsed = gCurrentAccountID;
					FrmUpdateForm(AccountListForm,frmReloadUpdateCode);
					handled = true;
				}
		  	}
			break;
		case frmObjectFocusTakeEvent:
			switch(pEvent->data.frmObjectFocusTake.objectID) {
				case AccountListFormTable:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,AccountListFormTable)); 
					FrmGetObjectBounds(pForm,FrmGetObjectIndex(pForm,AccountListFormTable),&recTable);
					FrmGlueNavDrawFocusRing(pForm,AccountListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
				default:
					break;
			}
			break;
		case frmOpenEvent:
			handled = AccountListFormHandleFormOpen();
			break;
		case frmUpdateEvent:
			handled = AccountListFormHandleFormUpdate( pEvent );
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case AccountListFormOKButton:
					FrmGotoForm(MainForm);
					break;
				case AccountListFormNewButton:
					if (gAccountNum >= MAXACCOUNTNUM)
					{
						char MaxString[10];
						FrmCustomAlert(MaxAccountAlert,StrIToA(MaxString,MAXACCOUNTNUM)," "," ");
						break;
					}
						
					//Initialize account info
					StrCopy(gAccount.description,"");
					StrCopy(gAccount.userName,"");
					StrCopy(gAccount.password,"");
					StrCopy(gAccount.serverName,"");
					StrCopy(gAccount.serverPort,"");
					gAccount.useProxy = false;
					StrCopy(gAccount.proxyName,"");
					StrCopy(gAccount.proxyPort,"");
					gAccount.proxyType = 0;
					gCurrentAccountID = gAccountNum;
					FrmGotoForm(AccountEditForm);
					break;
				case AccountListFormConnectButton:
					Login();
					FrmGotoForm(MainForm);
					break;
				default:
					break;
			}
		default:
			break;
	}
	
	return handled;
}
Boolean AccountFieldChanged()
{
	FieldType	*fldP;
	Boolean		modified = false;
	//Check if any field is changed
	fldP = (FieldType*)GetObjectPtr(AccountEditFormDescription);
	modified |= FldDirty(fldP);
	fldP = (FieldType*)GetObjectPtr(AccountEditFormUserName);
	modified |= FldDirty(fldP);
	//fldP = (FieldType*)GetObjectPtr(AccountEditFormPassword);
	//modified |= FldDirty(fldP);
	fldP = (FieldType*)GetObjectPtr(AccountEditFormServerName);
	modified |= FldDirty(fldP);
	fldP = (FieldType*)GetObjectPtr(AccountEditFormServerPort);
	modified |= FldDirty(fldP);
	return modified;
}  
void SaveAccount()
{
	TGoAccount	account;
   	MemHandle	TextH;
   	Char*		h1=NULL;
	//Save fields' value;
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(AccountEditFormDescription));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	//Get description
		if (h1)
			StrCopy(account.description,h1);
		MemHandleUnlock(TextH);
	}
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(AccountEditFormUserName));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	//Get login ID
		if (h1)
			StrCopy(account.userName,h1);
		MemHandleUnlock(TextH);
	}
	//TextH=FldGetTextHandle((FieldType*)GetObjectPtr(AccountEditFormPassword));
	//h1 = (Char *)MemHandleLock(TextH);	//Get password
	//if (h1)
	//	StrCopy(account.password,h1);
	//MemHandleUnlock(TextH);
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(AccountEditFormServerName));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	//Get server address
		if (h1)
			StrCopy(account.serverName,h1);
		MemHandleUnlock(TextH);
	}
	TextH=FldGetTextHandle((FieldType*)GetObjectPtr(AccountEditFormServerPort));
	if (TextH)
	{
		h1 = (Char *)MemHandleLock(TextH);	//Get server port
		if (h1)
			StrCopy(account.serverPort,h1);
		MemHandleUnlock(TextH);
	}
	account.serverType = LstGetSelection((ListType*)GetObjectPtr(AccountEditFormServerTypeList));
	account.supportNmatch = CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormNmatch));
	account.useProxy = CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormProxy));
	account.password = gAccount.password;
	account.proxyName = gAccount.proxyName;
	account.proxyPort = gAccount.proxyPort;
	account.proxyType = gAccount.proxyType;
	gAccountList[gCurrentAccountID] = account;
	if (gCurrentAccountID == gAccountNum)
		gAccountNum++;
} 
Boolean AccountEditFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	FieldType	*fldP;
	static 	Int16 	serverType;
	
   	//MemHandle	TextH;
   	//Char*		h1=NULL;
	FormPtr     frm = FrmGetActiveForm();
	//TGoAccount	account;
	FormType *form;
	Int16 buttonHit;
	switch (pEvent->eType) {
		case menuEvent:
			return true; 
		case frmUpdateEvent:
			if (pEvent->data.frmUpdate.updateCode==frmRedrawUpdateCode)
				//Redraw Password selector trigger
				DisplayPassword(gAccount.password);
			break;
		case frmOpenEvent:  
			FrmDrawForm( frm );
			serverType = gAccount.serverType;
			//Boundary check for old version's preference 
			if (serverType < GENERIC || serverType > IGS)
				serverType = GENERIC; 
			if (gCurrentAccountID>=gAccountNum || gAccountNum == 1) 	
				//Delete Button is not applicable to new account or
				//when there is only one account
				HideObject( frm, AccountEditFormDeleteButton );
			fldP = (FieldType*)GetObjectPtr(AccountEditFormDescription);
			DrawFieldText(fldP,gAccount.description);
			fldP = (FieldType*)GetObjectPtr(AccountEditFormUserName);
			DrawFieldText(fldP,gAccount.userName);
			//fldP = (FieldType*)GetObjectPtr(AccountEditFormPassword);
			DisplayPassword(gAccount.password);
			fldP = (FieldType*)GetObjectPtr(AccountEditFormServerName); 
			DrawFieldText(fldP,gAccount.serverName);
			fldP = (FieldType*)GetObjectPtr(AccountEditFormServerPort);
			DrawFieldText(fldP,gAccount.serverPort);
			if (gAccount.useProxy)
			{
				CtlSetValue((ControlType*)GetObjectPtr(AccountEditFormProxy),1); 
				ShowObject( frm, AccountEditFormProxyButton );
			}
			else
			{
				CtlSetValue((ControlType*)GetObjectPtr(AccountEditFormProxy),0); 
				HideObject( frm, AccountEditFormProxyButton );
			}
			if (gCurrentAccountID == gAccountIdToBeUsed) 
			//Current account is being used, check on Used Mark, and hide it
			{
				CtlSetValue((ControlType*)GetObjectPtr(AccountEditFormUsed),1); 
				HideObject(frm, AccountEditFormUsed);
				if (!IsConnected())
					ShowObject( frm, AccountEditFormConnectButton );
				else
					HideObject( frm, AccountEditFormConnectButton );
			}
			else
				HideObject( frm, AccountEditFormConnectButton );
//handle Server Type
			LstSetSelection((ListType*)GetObjectPtr(AccountEditFormServerTypeList),serverType);
			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormServerTypeTrigger),\
			LstGetSelectionText((ListType*)GetObjectPtr(AccountEditFormServerTypeList),serverType));
			#ifndef	_BE_
			if (serverType == IGS)
			{
				CtlSetValue((ControlType*)GetObjectPtr(AccountEditFormNmatch),gAccount.supportNmatch);
				//CtlSetValue((ControlType*)GetObjectPtr(AccountEditFormSeek),gAccount.supportSeek);
				ShowObject( frm, AccountEditFormNmatch);
				//ShowObject( frm, AccountEditFormSeek);
			} 
			else
 			{
				HideObject( frm, AccountEditFormNmatch);
				//HideObject( frm, AccountEditFormSeek);
			}
			#endif
			//SetFocus(frm, AccountEditFormSaveButton);
			handled = true;
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case AccountEditFormCancelButton:
					FrmGotoForm(AccountListForm);
					break;
				case AccountEditFormDeleteButton:
					if (gAccountNum<=1)
					{
						FrmAlert(MinAccountAlert);	//Can't delete the last account
						break; 
					} 
					form = FrmInitForm(AccountDeleteConfirmForm);
					buttonHit = FrmDoDialog(form);
					FrmDeleteForm(form);
					if (buttonHit == AccountDeleteConfirmFormOKButton)
					{
						//Delete current account
						for (Int16 i = gCurrentAccountID;i<gAccountNum-1;i++)
							gAccountList[i] = gAccountList[i+1];
						gAccountNum--;
						//If the deleted account was used to connect, then
						//use the first account to connect 
			 			if (gCurrentAccountID == gAccountIdToBeUsed)	
							gAccountIdToBeUsed = 0;
						FrmUpdateForm(AccountListForm,frmReloadUpdateCode);
						FrmGotoForm(AccountListForm);
					}
					break;
				case AccountEditFormSaveButton:
					//if (AccountFieldChanged())
					{
						SaveAccount();
					}
					if (gCurrentAccountID < gAccountNum && CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormUsed)))
						//Check if current account will be used to connect
						gAccountIdToBeUsed = gCurrentAccountID;
					FrmUpdateForm(AccountListForm,frmReloadUpdateCode);
					FrmGotoForm(AccountListForm);
					break;
				case AccountEditFormConnectButton:
					//Save setting first
					//if (AccountFieldChanged())
					{
						SaveAccount();
					}
					if (gCurrentAccountID < gAccountNum && CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormUsed)))
						//Check if current account will be used to connect
						gAccountIdToBeUsed = gCurrentAccountID;
					Login();
					FrmGotoForm(MainForm);
					break;
				case AccountEditFormUsed:
					if (CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormUsed))&&(!IsConnected()))
						ShowObject( frm, AccountEditFormConnectButton );
					else
						HideObject( frm, AccountEditFormConnectButton );
					break;
				case AccountEditFormProxy: 
					if (CtlGetValue((ControlType*)GetObjectPtr(AccountEditFormProxy)))
						ShowObject( frm, AccountEditFormProxyButton );
					else
						HideObject( frm, AccountEditFormProxyButton );
					break;
				case AccountEditFormProxyButton:
					FrmPopupForm(ProxyForm);
					break;
         		case AccountEditFormServerTypeTrigger:
         			LstSetSelection((ListType*)GetObjectPtr(AccountEditFormServerTypeList),serverType);
         			handled = false;
         			break;
				case AccountEditFormPassword:
					FrmPopupForm(PasswordForm);
					break;
 				default:
					break;
			}
        case popSelectEvent:
        	switch (pEvent->data.popSelect.listID)
        	{
        		case AccountEditFormServerTypeList:
        			serverType = pEvent->data.popSelect.selection;
        			CtlSetLabel((ControlType*)GetObjectPtr(AccountEditFormServerTypeTrigger),\
        			LstGetSelectionText((ListType*)GetObjectPtr(AccountEditFormServerTypeList),serverType));
					#ifndef	_BE_
        			if (serverType == IGS)
        			{
        				ShowObject( frm, AccountEditFormNmatch);
        				//ShowObject( frm, AccountEditFormSeek);
        			}
        			else
         			{
        				HideObject( frm, AccountEditFormNmatch);
        				//HideObject( frm, AccountEditFormSeek);
        			}
        			#endif	 		
        			handled = true;
        			break;
        		default:
        			break;
        	}
		default:
			break;
	}
	
	return handled;
}
Boolean GameInfoFormHandleEvent(EventPtr pEvent)
{
	Boolean 	handled = false;
	FormType     *frm;
	TSgfInfo	sgf;
	TGameInfo 	game;
	//TTimerInfo	timer;
	DateTimeType	dt;
	Char		str[30], *resource, *newFormTitle;
	RectangleType	r;
	r.topLeft.x = 123;
	r.topLeft.y = 97;
	r.extent.x = 35;
	r.extent.y = 28;
	MemHandle	resourceH = NULL;
	frm = FrmGetActiveForm();
	CBoard		*workingBoard = gBoardPool.GetCurrentBoard(); 
	if (!workingBoard)
		return true;
	
	workingBoard->GetSgfInfo(sgf);
	workingBoard->GetGameInfo(game);
	//workingBoard->GetTimerInfo(timer);
	switch (pEvent->eType) {
		case menuEvent:
			handled = true;
			break; 
		case frmUpdateEvent:
			if (pEvent->data.frmUpdate.updateCode==frmRedrawUpdateCode)
			{
				if (game.status==INPROGRESS)
				{	
					/*
					//Draw Elapse
					WinEraseRectangle( &r, 0 );	
					//if (timer.whiteTimeElapse>0)
					{ 
						TimSecondsToDateTime(timer.whiteTimeElapse,&dt);
						sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
						//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormWE),str);
						WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y);
					}
					//if (timer.blackTimeElapse>0)
					{
						TimSecondsToDateTime(timer.blackTimeElapse,&dt);
						sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
						//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormBE),str);
						WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y+16);
					}
					*/
					WinEraseRectangle( &r, 0 );	
					TimSecondsToDateTime(sgf.WE,&dt);
					sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
					//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormWE),str);
					WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y);
					TimSecondsToDateTime(sgf.BE,&dt);
					sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
					//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormBE),str);
					WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y+16);
					if (sgf.GN)
					{
						DisplayGameTitle(sgf.GN);
					}					
				}				
				handled = true;
				break;
			}
				//Redraw Game Title selector trigger
			//	DisplayGameTitle(sgf.EV); 
			//break;
		case frmOpenEvent:
			handled = true;
			FrmDrawForm( frm );
	  		newFormTitle = (Char*) MemPtrNew(10);
	  		if (newFormTitle)
	  		{	  			
	  			StrCopy(newFormTitle,"Game ");
	  			/*
	  			StrIToA(str,game.id);
	  			StrCat(newFormTitle,str);
	  			*/
	  			if (sgf.ID)
	  			{
	  				if (StrLen(sgf.ID)<5)
	  					StrCat(newFormTitle,sgf.ID);
	  				else
	  					StrCat(newFormTitle,"####");
			  		FrmCopyTitle(FrmGetActiveForm(),newFormTitle);
	  			}
		  		MemPtrFree(newFormTitle);
	  		}
			if (sgf.GN)
			{
				DisplayGameTitle(sgf.GN);
			}
			if (sgf.DT)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormDT),sgf.DT);
			if (sgf.PC)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormPC),sgf.PC);
			if (sgf.RE)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormRE),sgf.RE);
			if (sgf.TM)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormTM),sgf.TM);
			if (sgf.PW)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormPW),sgf.PW);
			if (sgf.WR)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormWR),sgf.WR);
			if (sgf.PB)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormPB),sgf.PB);
			if (sgf.BR)
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormBR),sgf.BR);
			if(sgf.KM<0)
				sprintf(str,"%d.%d",sgf.KM/10,-sgf.KM%10);
			else
				sprintf(str,"%d.%d",sgf.KM/10,sgf.KM%10);
			DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormKM),str);
			if (sgf.HA != 0)
			{
				sprintf(str,"%d",sgf.HA);
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormHA),str);
			}
			/*
				//Draw Elapse
				WinEraseRectangle( &r, 0 );	
				//if (timer.whiteTimeElapse>0)
				{
					TimSecondsToDateTime(timer.whiteTimeElapse,&dt);
					sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
					//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormWE),str);
					WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y);
				}
				//if (timer.blackTimeElapse>0)
				{
					TimSecondsToDateTime(timer.blackTimeElapse,&dt);
					sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
					//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormBE),str);
					WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y+16);
				}
			*/
			WinEraseRectangle( &r, 0 );	
			TimSecondsToDateTime(sgf.WE,&dt);
			sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
			//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormWE),str);
			WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y);
			TimSecondsToDateTime(sgf.BE,&dt);
			sprintf(str,"%d:%d:%d",dt.hour,dt.minute,dt.second);
			//DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormBE),str);
			WinDrawChars(str,StrLen(str),r.topLeft.x,r.topLeft.y+16);
			switch 	(game.status){
				case PREPARING:
					resourceH = DmGetResource(strRsc, StringGamePreparing);
					//StrCopy(str,"Preparing");
					break;
				case INPROGRESS:
					resourceH = DmGetResource(strRsc, StringGameInprogress);
					//StrCopy(str,"In Progress");
					break;
				case GAMEOVER:
					resourceH = DmGetResource(strRsc, StringGameOver);
					//StrCopy(str,"Game Over");
					break;
				case INTERRUPTED:
					resourceH = DmGetResource(strRsc, StringGameInterrupted);
					//StrCopy(str,"Interrupted");
					break;
				case SCORING:
					resourceH = DmGetResource(strRsc, StringGameScoring);
					//StrCopy(str,"Scoring");
					break;
				case LOADED:
					resourceH = DmGetResource(strRsc, StringGameLoaded);
					break;
				default:
					break;
			}
			if (resourceH)
			{
				resource = (Char*)MemHandleLock(resourceH);
				StrCopy(str,resource);
				MemHandleUnlock(resourceH);
				DmReleaseResource(resourceH);
				DrawFieldText((FieldType*)GetObjectPtr(GameInfoFormStatus),str);
			}
			break;
		case ctlSelectEvent:
			handled = true;
			switch(pEvent->data.ctlSelect.controlID) {
				case GameInfoFormOKButton:
					FrmGotoForm(BoardForm);
					break;
				case GameInfoFormGN:
				#ifdef	_PE_
					if (gAccountList[gAccountIdInUsed].serverType == IGS && game.action == TEACH && game.status == INPROGRESS) 
		            	FrmPopupForm(TitleForm);
		            else
						if (sgf.GN)
							FrmCustomAlert(GameTitleAlert,sgf.GN,"","");		            
					handled = true;
					break;
				#else	
					if (sgf.GN)
						FrmCustomAlert(GameTitleAlert,sgf.GN,"","");
					//FrmUpdateForm(GameInfoForm,frmRedrawUpdateCode);
					handled = true;
					break;
				#endif
				default:
					break;
			}
		default:
			break;
	}  
	return handled;
}

Boolean ObserveGame(UInt16 gameid)
{
   	Char	cmdString[25],gameidStr[4];
   	UInt16	preGameId;
	TGameInfo	info;
	CBoard		*workingBoard = gBoardPool.GetCurrentBoard(); 
	if (!workingBoard)
		return false;	//safty check
	workingBoard->GetGameInfo(info);	//Get current game info
	if (info.action == PLAY && (info.status == INPROGRESS || info.status == SCORING)) //I'm playing a game
	{
		FrmAlert(GamePlayingAlert);
		return true;
	}
	if (info.action == OBSERVE && info.status == INPROGRESS)	//I'm observing a game
	{
		if (FrmAlert(GameUnobserveAlert) == 0)	//Yes
		{	//unobserve previouse game first
			preGameId = info.id;	
			StrCopy(cmdString,"unobserve ");
			StrCat(cmdString,StrIToA(gameidStr,preGameId));
			WriteServer(cmdString);
		}
		else
			return true;
	}
	//Observe current game
	StrCopy(cmdString,"observe ");
	StrCat(cmdString,StrIToA(gameidStr,gameid));
	WriteServer(cmdString);
	return true;
}

/*
void UpdateSgfByGameList(UInt16 gameId)
{
  	CGameItem	*gameP;
	if  (gGameList.FindGame(gameId,&gameP))
	{
		TSgfInfo	sgf;
		sgf.Init();
		//Set SGF infomation for the game
		sgf.id = gameP->gameID;
		if(gameP->whitePlayer)
		{
			if ((sgf.PW = (char*)MemPtrNew(StrLen(gameP->whitePlayer)+1)))
				StrCopy(sgf.PW,gameP->whitePlayer);
		}
		if(gameP->whiteRank)
		{
			if ((sgf.WR = (char*)MemPtrNew(StrLen(gameP->whiteRank)+1)))
				StrCopy(sgf.WR,gameP->whiteRank);
		}
		if(gameP->blackPlayer)
		{
			if ((sgf.PB = (char*)MemPtrNew(StrLen(gameP->blackPlayer)+1)))
				StrCopy(sgf.PB,gameP->blackPlayer);
		}
		if(gameP->blackRank)
		{
			if ((sgf.BR = (char*)MemPtrNew(StrLen(gameP->blackRank)+1)))
				StrCopy(sgf.BR,gameP->blackRank);
		}
		sgf.HA = gameP->handicap;
		sgf.SZ = gameP->size;
		sgf.KM = gameP->komi;
		gBoard->UpdateSgfInfo(sgf);
		sgf.Release(); 
	}
}
*/ 
Boolean DefaultFormHandleEvent(EventPtr e)
{
   	Boolean handled = false;
   	FormPtr frm;
   	frm = FrmGetActiveForm();
   	MemHandle	TextH;
   	Char*		h1=NULL;
   	Char	string[80];
	Int16	size,time,byoyomi,stones;
	size = gMyStatsDefs.maxSize;
	time = gMyStatsDefs.maxTime;
	byoyomi = gMyStatsDefs.maxByoyomi;
	stones = gMyStatsDefs.maxStones;
	Boolean defsChanged = false;
   	switch (e->eType) 
   	{ 
      	case frmOpenEvent:
   			FrmDrawForm(frm);
   			//Display size
			StrIToA(string,size);
			DrawFieldText((FieldType*)GetObjectPtr(DefaultFormSizeField1),string);	
			DrawFieldText((FieldType*)GetObjectPtr(DefaultFormSizeField2),string);	
   			//Display main time
			StrIToA(string,time);
			DrawFieldText((FieldType*)GetObjectPtr(DefaultFormMainTimeField),string);	
   			//Display byoyomi time
			StrIToA(string,byoyomi);
			DrawFieldText((FieldType*)GetObjectPtr(DefaultFormBYTimeField),string);	
   			//Display byoyomi stones
			StrIToA(string,stones);
			DrawFieldText((FieldType*)GetObjectPtr(DefaultFormStonesField),string);	
         	handled = true; 
         	break; 
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case DefaultFormOkButton:
         			//Get main time
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(DefaultFormMainTimeField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							time = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Get byo-yomi time
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(DefaultFormBYTimeField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							byoyomi = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Get byo-yomi stones
					TextH=FldGetTextHandle((FieldType*)GetObjectPtr(DefaultFormStonesField));
					if (TextH)
					{
						h1 = (Char *)MemHandleLock(TextH);	
						if (h1)
							stones = StrAToI(h1);
						MemHandleUnlock(TextH);
					}
					//Verify defs values
					if (time>0 && time<=360 && byoyomi>0 && byoyomi <=30 && stones >0 && stones<=25)
					{
					//Set defs if it's changed
						if (time != gMyStatsDefs.maxTime)
						{
							defsChanged = true;
							sprintf(string,"defs time %d",time);
							WriteServer(string);						
						}
						if (byoyomi != gMyStatsDefs.maxByoyomi)
						{
							defsChanged = true;
							sprintf(string,"defs byotime %d",byoyomi);
							WriteServer(string);						
						}
						if (stones != gMyStatsDefs.maxStones)
						{
							defsChanged = true;
							sprintf(string,"defs stones %d",stones);
							WriteServer(string);						
						}
						//Retrieval my defs by issuing a STATS command
						if (defsChanged)
							WriteServer("stats");
						FrmReturnToForm(MainForm);
		         		handled = true;
						}
					else
					{
						FrmAlert(InvalidDefsAlert);
		         		handled = false;
					}
	         		break;
	         	case DefaultFormCancelButton:
	         		FrmReturnToForm(MainForm);
	         		handled = true;
	         		break;
	         	default:
	         		break; 
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}    
Boolean StatisticsFormHandleEvent(EventPtr e) 
{
   	Boolean handled = false;
	//FieldType	*fldP;
   	FormPtr frm;
   	//MemHandle	TextH;
   	//Char*		h1=NULL;
	
   	frm = FrmGetActiveForm();

   	switch (e->eType) 
   	{ 
		case menuEvent:
			handled = true;
			switch(e->data.menu.itemID){
				case StatCommandReset:
					if (FrmAlert(BytesTimeAlert) == 0)
						gStatistics.ResetAll(); 
					FrmUpdateForm(StatisticsForm,frmReloadUpdateCode);
					break;
				default:
					break; 
			}
			break; 

      	case frmOpenEvent:
   			FrmDrawForm(frm);
   			gStatistics.UpdateForm();
         	handled = true;
         	break;
		case frmUpdateEvent:
   			gStatistics.UpdateForm();
         	handled = true;
         	break;		
      	case ctlSelectEvent:
         	switch (e->data.ctlSelect.controlID)
         	{
         		case StatisticsFormOkButton:
	               	FrmGotoForm(MainForm);
	         		handled = true;
	         		break;
	         	case StatisticsFormResetButton:  
	               	FrmGotoForm(MainForm);
	         		handled = true; 
	         		break;
	         	default:
	         		break;
         	}
         	break;
         default:
         	break;
   	}
   	return handled;
}
