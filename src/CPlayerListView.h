#ifndef CPLAYERLISTVIEW_H
#define CPLAYERLISTVIEW_H
#include <PalmOS.h>
#include "nngs.h"

class CPlayerArray;
class CPlayerListView{
public:

	CPlayerListView() SEC_LIST;
	CPlayerListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	virtual ~CPlayerListView() SEC_LIST;
	void Clear() SEC_LIST;
	void UpdateList(CPlayerItem *item) SEC_LIST;
	void SortList(PLAYERFIELD fld, Boolean  ascending) SEC_LIST;
	void BeginOfList() SEC_LIST;
	void EndOfList() SEC_LIST;
	void Flush() SEC_LIST;
	Boolean GetItemRank(Char *player, Char *rank) SEC_LIST;
	Boolean HasItem() SEC_LIST;
	void DisplayItemTotal() SEC_LIST;
	void UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw) SEC_LIST;
    void DisableTable(TablePtr table) SEC_LIST;
	void Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage) SEC_LIST;
	void SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds)) SEC_LIST;
	Boolean HandleEvent( EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean highLighted;
   	Int8	highLightedRow;

protected:

	void InitTable(TablePtr table ) SEC_LIST;
   	void UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar ) SEC_LIST;
   	Boolean HandleRepeatEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleTableEnterEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
   	Boolean HandleKeyDownEvent(EventPtr event, TablePtr t, ScrollBarPtr s ) SEC_LIST;
	//Int16 CompareItem(CPlayerItem *itemP1, CPlayerItem *itemP2, PLAYERFIELD fld, Boolean ascending) SEC_LIST;
	Boolean HandleSelectItem(CPlayerItem *item) SEC_LIST;   
   	Boolean	updatingList;
	CPlayerArray	*list;
   	Int32	skippedItems;
   	//Boolean filterOn;
   	//UInt32	formID;
   	void 	(*customDrawFunction)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds);
};

#endif // CPLAYERLISTVIEW_H
