#ifndef __NNGS_H__
#define __NNGS_H__
#include "sections.h"

const	Int16	MAX_WORD_LENGTH = 25;
const	Char	BLANKSPACE = ' ';
const	Char	LF	= '\n';
const	Char	CR	= '\r';
const	Char	ZEROCHAR ='0';
const	Char	TOGGLECHAR = '0';
const	Char	BEGINCHAR = '1';
const	Char	ENDCHAR = '7';
const	Char	BACKWARDCHAR = '2';
const	Char	FORWARDCHAR = '8';
const	Char	FBACKWARDCHAR = '3';
const	Char	FFORWARDCHAR = '9';
//Player's color
const	UInt8	TBD = 0x00;	//To Be Determined
const	UInt8	EMPTY = 0x00;
const	UInt8	WHITE =	0x01;
const	UInt8	BLACK = 0x02;
const	UInt8	BOTH = WHITE | BLACK;
//Actions
const	UInt8	IDLE = 0x00;
const	UInt8	PLAY = 0x01;
const	UInt8	OBSERVE	= 0x02;
const	UInt8	TEACH = 0x03;
//Status
const	UInt8	PREPARING =	0x00;
const	UInt8	INPROGRESS = 0x01;
const	UInt8	SCORING	= 0x02;
const	UInt8	GAMEOVER = 0x03;
const	UInt8	INTERRUPTED	= 0x04;
const	UInt8	LOADED = 0x05;
//Indicator of begining and ending of lists
const	Int16	LIST_BEGIN = -1;
const	Int16	LIST_END = -2;
const	Int16	MOVE_END = -2;
const	Int16	RANK_END = -2;
const	Int16	DEFS_END = -3;
//Indicator of that move id is N/A
const	UInt16	MOVEID_NA = 0xFFFF;
//Server type, server specific feature
enum SERVERTYPE {
	GENERIC,	
	IGS,
	WING,
	LGS,
	CWS,
};
//Match type, server specific match commands
enum MATCHTYPE {
	MATCH,	//Generic MATCH
	NNGS_TEACH,	//Generic TEACH
	IGS_NMATCH, //IGS NMATCH
	IGS_AMATCH, //IGS AutoMatch
	IGS_PMATCH, //IGS PMATCH
	MATCHTYPE_TOTAL,
}; 
enum GAMEFIELD {
	ID,	//id
	PW,	//White player
	WR, //White rank
	PB, //Black player
	BR, //Black rank
	MV, //Moves
	SZ, //Size
	HA, //Handicap
	KM, //Komi
	BY, //Byo-yomi
	FR, //Flag
	OB, //Observer
};
enum PLAYERFIELD {
	NM,	//Player name
	RK,	//Player rank
	ST,	//Status
	GM,	//Game ID
	IL,	//Idle time
	IF,	//info
	CN,	//Country
	WN,	//Won
	LS,	//Lost
};
enum RANKCLASS {
	nr, //No rank
	kyu,
	dan,
	pro,
};
enum SUBCLASS {
	unknown, //?
	basic,
	plus,	//* or +
};
typedef struct TGameInfo
{
	UInt16 id;
	UInt8 action; //IDLE, PLAY or OBSERVE
	UInt8 status; 
	Int16 byoyomi;
	Int16 color;	//my color
};
typedef struct TSgfInfo
{
	UInt16 id;
	Char*	GN;
	Char*	EV;
	Char*	RO;
	Char*	PB;
	Char*	BR;
	Char*	PW;
	Char*	WR;
	Char*	RE;
	Char*	PC;
	Char*	DT;
	Char*	TM;
	Char*	ID;
	Int16 	KM;
	Int16 	HA;
	Int16	SZ;
	Int16	WE;	//White Elapsed, Mobile Go specific
	Int16	BE; //Black Elapsed, Mobile Go specific
	void Init()
	{
		//Init sgfInfo fields
		id = 0;
		GN = NULL;
		EV = NULL;
		RO = NULL;
		PB = NULL;
		BR = NULL;
		PW = NULL;
		WR = NULL;
		RE = NULL;
		PC = NULL;
		DT = NULL;
		TM = NULL;
		ID = NULL;
		KM = 65;
		HA = 0;
		SZ = 19;
		WE = 0;
		BE = 0;
	}
	void Release()
	{
		if (GN)
		{
			MemPtrFree(GN);
			GN = NULL;
		}
		if (EV)
		{
			MemPtrFree(EV);
			EV = NULL;
		}
		if (RO)
		{
			MemPtrFree(RO);
			RO = NULL;
		}
		if (PB)
		{
			MemPtrFree(PB);
			PB = NULL;
		}
		if (BR)
		{
			MemPtrFree(BR);
			BR = NULL;
		}
		if (PW)
		{
			MemPtrFree(PW);
			PW = NULL;
		}
		if (WR)
		{
			MemPtrFree(WR);
			WR = NULL;
		}
		if (RE)
		{
			MemPtrFree(RE);
			RE = NULL;
		}
		if (PC)
		{
			MemPtrFree(PC);
			PC = NULL;
		}
		if (DT)
		{
			MemPtrFree(DT);
			DT = NULL;
		}
		if (TM)
		{
			MemPtrFree(TM);
			TM = NULL;
		}
		if (ID)
		{
			MemPtrFree(ID);
			ID = NULL;
		}
		return;
	}
};
typedef struct TTimerInfo
{
	UInt16	gameID;
	Char	*whitePlayer;
	Int16	whiteCapture;
	Int16	whiteTimeLeft;
	Int16	whiteStoneLeft;
	//Int16	whiteTimeElapse;
	Char	*blackPlayer;
	Int16	blackCapture;
	Int16	blackTimeLeft;
	Int16	blackStoneLeft;
	//Int16	blackTimeElapse;
	void	Release()  
	{
		if (whitePlayer)
			MemPtrFree(whitePlayer);
		if (blackPlayer)
			MemPtrFree(blackPlayer);
		return;
	}
};
typedef struct TMoveInfo
{
	UInt16	moveID;
	UInt8	playerColor;
	Int16	x;
	Int16	y;
	Int16	handicap;
};
typedef struct TDefsInfo{
	UInt8	color;	//Preferred color
	Int16	minSize;	//Preferred minimum board size
	Int16	maxSize;	//Preferred maximum board size
	Int16	minHandicap;	//
	Int16	maxHandicap;	//
	Int16	minTime;	//Main time (in minute)
	Int16	maxTime;	//Main time (in minute)
	Int16	minByoyomi;//Byoyomi time (in minute)
	Int16	maxByoyomi;//Byoyomi time (in minute)
	Int16	minStones;	//Byoyomi stones
	Int16	maxStones;	//Byoyomi stones
	Int16	minCount;	//Koryo count 
	Int16	maxCount;	//Koryo count 
	Int16	minKoryo;	//Koryo time (in second)
	Int16	maxKoryo;	//Koryo time (in second)
	Int16	minPrebyoyomi; //Prebyoyomi time (in second)
	Int16	maxPrebyoyomi; //Prebyoyomi time (in second)
};
class CGameItem{
public:
	CGameItem() SEC_NNGS;
	virtual ~CGameItem() SEC_NNGS;
	Boolean operator==(const CGameItem&)  SEC_NNGS;
	void	Release();
	UInt16	gameID;
	Char	*whitePlayer;
	Char	*whiteRank;
	Int16	whiteNumericalRank;
	Char	*blackPlayer;
	Int16	blackNumericalRank;
	Char	*blackRank;
	UInt16	moveCount;
	Int16	size;
	Int16	handicap;
	Int16	komi;
	Int16	byoyomi;
	Char	*flag;
	Int16	observer;
};
typedef	CGameItem*	CGameItemPtr;

class	CPlayerItem{
public:
	CPlayerItem() SEC_NNGS;
	virtual ~CPlayerItem() SEC_NNGS;
	Boolean operator==(const CPlayerItem&) SEC_NNGS;
	void	Release();
	Char*	playerName;
	Char*	rank;
	Int16	numericalRank;
	Char	status[2];
	UInt8	action;	//'P'= Playing, 'O'= Observing, 'I'= Idle
	UInt16	gameID;
	Char*	idle;
	Char*	info;
	Char*	country;
	UInt16	won;
	UInt16	lost;
};
typedef CPlayerItem*	CPlayerItemPtr;
class CChallengeItem{
public:
	CChallengeItem() SEC_NNGS;
	virtual ~CChallengeItem() SEC_NNGS;
	Boolean operator==(const CChallengeItem&) SEC_NNGS;
	void	Release();
	MATCHTYPE	matchtype;
	Boolean	offered;	//Match is offered by opponent
	Char*	playerName;
	Char*	playerRank;
	UInt8	playerColor;
	Int16	size;
	Int16	handicap;
	Int16	komi;
	Int16	main;	//Main time (in minute)
	Int16	byoyomi;//Byoyomi time (in minute)
	Int16	stones;	//Byoyomi stones
	Int16	count;	//Koryo count 
	Int16	koryo;	//Koryo time (in second)
	Int16	prebyoyomi; //Prebyoyomi time (in second)
	UInt32	timeStamp;//Time stamp when challenge received
};
typedef CChallengeItem*	CChallengeItemPtr;
Int16 String2Integer(const Char* s) SEC_NNGS;
Int16 FString2Integer(const Char* s) SEC_NNGS;
void SkipBlankSpace(const Char* s, Int16 *currentOffsetP) SEC_NNGS;
Int16 GetNextWord(const Char* s, Char delimiter, Char *wordP) SEC_NNGS;
//Int16 RankCompare(const Char* s1, const Char* s2) SEC_NNGS;
Int16	ConvertRankToNumber(Char* rank) SEC_NNGS;
void SetCurrentMatch(CChallengeItem* matchP) SEC_NNGS;
Int16	CompareMatchOpponent(const Char* currnetOpponent, const Char* comparedOpponent) SEC_NNGS;
void UpdateChatBuddyList(const Char* playerName) SEC_NNGS;
Int16 NNGS_AdjournRequest(const Char* ) SEC_NNGS;
Int16 NNGS_GameOver(const Char* s, UInt16& gameIdP, Char* result) SEC_NNGS;
Int16 NNGS_RemoveObservation(const Char* s, UInt16& gameId) SEC_NNGS;
Int16 NNGS_AddObservation(const Char* s) SEC_NNGS;
Int16 NNGS_ChangeKomi(const Char* s,Int16& komi) SEC_NNGS;
Int16 NNGS_MatchRequest(const Char* s,CChallengeItem* challengeItemP) SEC_NNGS;
Int16 NNGS_MatchWithdraw(const Char* s,CChallengeItem* challengeItemP) SEC_NNGS;
Int16 NNGS_MatchCreate(const Char* s, UInt16& gameId, Char* name) SEC_NNGS;
Int16 NNGS_MatchDecline(const Char* s,CChallengeItem* challengeItemP) SEC_NNGS;
Int16 NNGS_MoveInfoUpdate(const Char* s, TMoveInfo* moveP) SEC_NNGS;
Int16 NNGS_UndoLastMove(const Char* s, TMoveInfo* moveP) SEC_NNGS;
Int16 NNGS_UndoInGame(const Char* s, UInt16& gameId, TMoveInfo* moveP) SEC_NNGS;
Int16 NNGS_DeadGroup(const Char* s, Int16& x, Int16& y) SEC_NNGS;
Int16 NNGS_RestoreGroup(const Char* s) SEC_NNGS;
Int16 NNGS_TimerInfoUpdate(const Char* s, UInt16& gameId, TTimerInfo* timerInfoP) SEC_NNGS;
Char* NNGS_TitleInfoUpdate(const Char* s) SEC_NNGS;
Int16 NNGS_GameItemUpdate(const Char* s, CGameItem* gameItemP) SEC_NNGS;
Int16 NNGS_PlayerItemUpdate(const Char* s, CPlayerItem* playerItemP1, CPlayerItem* playerItemP2) SEC_NNGS;
Char* NNGS_ServerResponse(const Char* s) SEC_NNGS;
Char* NNGS_GetMessage(const Char* s, Char* who) SEC_NNGS;
Int16 NNGS_CheckScore(const Char* s) SEC_NNGS;
Int16 NNGS_AccountName(const Char* s, Char* name) SEC_NNGS;
Int16 NNGS_StoredGame(const Char* s) SEC_NNGS;
Int16 NNGS_StatsRank(const Char* s, Char* statsPlayerNameP, Char* statsRankP) SEC_NNGS;
Int16 NNGS_GetAYT(const Char* s, UInt32& getSeconds, Int16& aytWaitCount) SEC_NNGS;
Int16 IGS_GameReloaded(const Char* s) SEC_IGS;
Int16 IGS_AutoMatchConfirmed(const Char* s, Char *player) SEC_IGS;
Int16 IGS_GameAdjourned(const Char* s, UInt16& gameId, Char* result) SEC_IGS;
Int16 IGS_MatchOver(const Char* s, Char* player, Char* result) SEC_IGS;
Int16 IGS_MatchResult(const Char* s, Char* player, Char* result) SEC_IGS;
Int16 IGS_MatchDispute(const Char* s,CChallengeItem* challengeItemP) SEC_IGS;
Int16 IGS_AutoMatchRequest(const Char* s,CChallengeItem* challengeItemP) SEC_IGS;
Int16 IGS_NmatchRequest(const Char* s,CChallengeItem* challengeItemP) SEC_IGS;
Int16 IGS_NmatchWithdraw(const Char* s,CChallengeItem* challengeItemP) SEC_IGS;
Int16 IGS_NmatchNotSupport(const Char* s) SEC_IGS;
Int16 IGS_NmatchDispute(const Char* s,CChallengeItem* challengeItemP) SEC_IGS;
Int16 IGS_ByoyomiStonesInfoUpdate(const Char* s, UInt16& gameId, Int16& byoyomiStones) SEC_IGS;
Int16 IGS_Seek(const Char* s) SEC_IGS;
Int16 IGS_StatsDefs(const Char* s, Char* statsPlayerNameP, TDefsInfo *statsDefsP) SEC_IGS;
#endif