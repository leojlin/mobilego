//#include <TblGlue.h>
#include <FrmGlue.h>
#include <PalmOne_68K.h>
#include "main.h"
#include "ui.h"
#include "forms.h"
#include "CPlayerArray.h"
#include "nngs.h"
#include "CPlayerListView.h"
#include "CChallengeListView.h"
#include "network.h"
#include "rsc.h"

extern PLAYERFIELD	gPlayerSortedField;
extern Boolean gPlayerAscending;
extern CChallengeItemPtr		gCurrentMatch;
extern CPlayerItemPtr	gCurrentPlayerP;
extern Char gCurrentOpponent[];
extern CChallengeListView	gChallengeList;
extern Boolean gNavSupport;

void CPlayerListView::SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	customDrawFunction = f;
}
CPlayerListView::CPlayerListView()
{
	skippedItems=0; 
	//filterOn = false;
	updatingList = false;
	list = new CPlayerArray;
	//Check memory allocation here
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
}
CPlayerListView::CPlayerListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	skippedItems=0; 
	//filterOn = false;
	updatingList = false;
	list = new CPlayerArray;
	//Check memory allocation here
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
	customDrawFunction = f;
}
CPlayerListView::~CPlayerListView()
{
	Clear(); 
}
void CPlayerListView::Clear()
{
	delete list; 
}
Boolean CPlayerListView::HasItem()
{
	if (list->Size())
		return true;
	else
		return false;
}

void CPlayerListView::UpdateList(CPlayerItem *item)
{
	UInt32 size;
	
	if (updatingList)
	{
		if (!list->Add(item))
			return;					//fail to add
		size = list->Size();
		if ((FrmGetActiveFormID() == PlayerListForm))
		{
			switch (size % 4)
			{
				case 0:
				case 1:
					WinDrawChars("Loading\\",StrLen("Loading\\"),70,1);
					break;
				case 2:
				case 3:
					WinDrawChars("Loading/",StrLen("Loading/"),70,1);
					break;
			}
		}
	}
	return ;
}
void CPlayerListView::SortList(PLAYERFIELD fld, Boolean  ascending)
{
	/*
	//Sort list by using insertion sort algorithm
	CPlayerItem *temp;
	Int32	i,j,sz;
	sz = list->Size();
	if (!sz)	
		return;		//Empty list
	for (i = 1; i < sz; i++)
	{
		if ((FrmGetActiveFormID() == PlayerListForm))
		{
			switch (i % 2)
			{
				case 0:
					WinDrawChars("\\",1,80,1);
					break;
				case 1:
					WinDrawChars("/",1,80,1);
					break;
			}
		}
		j = i;
		temp = (*list)[i];
		while (j>0 && CompareItem((*list)[j-1],temp,fld,ascending)==-1)
		{
			list->Swap(j-1,j);
			j--;
		}
	}
	*/
	//list->QuickSort(0,list->Size()-1,fld,ascending);
	if ((FrmGetActiveFormID() == PlayerListForm))
		WinDrawChars("Sorting...",StrLen("Sorting..."),70,1);
	list->HeapSort(list->Size(),fld,ascending);
}		

void CPlayerListView::EndOfList()
{
	if (updatingList)
	{
		updatingList = false;
		SortList(gPlayerSortedField,gPlayerAscending);
		FrmUpdateForm(PlayerListForm,frmReloadUpdateCode);
	}
}
void CPlayerListView::BeginOfList()
{
	skippedItems=0; 
	updatingList = true;
   	highLighted = false;
   	highLightedRow = 0;
}
void CPlayerListView::Flush()
{
	if (list) 
		delete list;
	list = new CPlayerArray;
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
	skippedItems=0; 
   	highLighted = false;
   	highLightedRow = 0;
	//updatingList = true;
}
Boolean CPlayerListView::GetItemRank(Char *playerName, Char *rank)
{
	Int32 index;
	index = list->FindByName(playerName);
	if ( index != -1)
	{
		CPlayerItem *item;
		item = (*list)[index]; 
		StrCopy(rank,item->rank);
		return true;
	}
	else
		return false;
}
void CPlayerListView::DisplayItemTotal()
{
	Int16	totalItems;
	totalItems = list->Size();
	//Display item total
	if ((FrmGetActiveFormID() == PlayerListForm))
	{
	   	RectangleType	r;
	   	r.topLeft.x = 54;
	   	r.topLeft.y = 145;
	   	r.extent.x = 65;
	   	r.extent.y = 11;
		Char	totalString[10];
		Char	displayTitle[20];
		StrIToA(totalString,totalItems);
		StrCopy(displayTitle,"Total: ");
		StrCat(displayTitle,totalString);
		WinEraseRectangle(&r,0);
		WinDrawChars(displayTitle,StrLen(displayTitle),55,145);
	}
}

void CPlayerListView::UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw)
{
	Int16	row;
	Int16	totalItems;
	Int16	tableRows = TABLEROWS;
	CPlayerItem *itemP;
	totalItems = list->Size();
	if( skippedItems < 0 )     // if we page down, this could end up negative
    	skippedItems = 0;

   	if( skippedItems + tableRows > totalItems && totalItems >= tableRows )
      	skippedItems = totalItems - tableRows;

   	if( skippedItems > 0 && totalItems <= tableRows )
      	skippedItems = 0;
   	InitTable(table);
	for( row=0; row<tableRows; row++ )
    {
    	if( row >= totalItems )
        	TblSetRowUsable( table, row, false );			// so it won't get drawn
		else
        {
        	itemP = (*list)[row + skippedItems ];
			TblSetRowUsable( table, row, true );			// so it will get drawn
			TblMarkRowInvalid( table, row );					// so it will get updated
			TblSetItemPtr( table, row, 0, (void *)itemP);
			itemP = (CPlayerItem*)TblGetItemPtr( table, row, 0 );
		}
	}
	if( redraw )
    	TblDrawTable( table );
	UpdateScrollers( table, scrollBar );   
}

void CPlayerListView::Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage)
{
	Int16	tableRows = TABLEROWS;

	if( fullPage )
	{
    	dir *= tableRows;
		skippedItems += dir;
	}
	else
	{
		highLightedRow +=dir;
		if (skippedItems+highLightedRow >= list->Size()) //the last item is highlighted
			highLightedRow = highLightedRow -1;
		if (highLightedRow >= tableRows)
		{
			highLightedRow = highLightedRow -1;
			skippedItems += dir;
		}
		else if (highLightedRow < 0)
		{
			highLightedRow = 0;
			skippedItems += dir;
		}
	}
	UpdateTable( table, scrollBar, true );
}

void CPlayerListView::InitTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
	{		
		TblSetItemStyle( table, row, 0, customTableItem );
		TblSetItemFont( table, row, 0, stdFont );
		TblSetRowUsable( table, row, false );
	}
	TblSetColumnUsable( table, 0, true );
   	TblSetCustomDrawProcedure( table, 0, customDrawFunction );
}

void CPlayerListView::DisableTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
		TblSetRowUsable( table, row, false );
}
/*Int16 CPlayerListView::CompareItem(CPlayerItem *itemP1, CPlayerItem *itemP2, PLAYERFIELD fld, Boolean ascending)
{
	Int16	ret = 0;
	switch (fld){
		case GM:
			if (itemP1->gameID == itemP2->gameID)
				ret = 0;
			else 
			{
				if (itemP1->gameID < itemP2->gameID)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case NM:
			if (StrCompare(itemP1->playerName,itemP2->playerName) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->playerName,itemP2->playerName) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case RK:
			if (RankCompare(itemP1->rank,itemP2->rank) == 0)
				ret = 0;
			else 
			{
				if (RankCompare(itemP1->rank,itemP2->rank) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case CN:
			if (StrCompare(itemP1->country,itemP2->country) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->country,itemP2->country) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case WN:
			if (itemP1->won == itemP2->won)
				ret = 0;
			else 
			{
				if (itemP1->won < itemP2->won)
					ret = ascending ? 1 : -1 ;
				else
					ret = (ascending?-1:1);
			}
			break;
		case LS:
			if (itemP1->lost == itemP2->lost)
				ret = 0;
			else 
			{
				if (itemP1->lost < itemP2->lost)
					ret = ascending ? 1 : -1 ;
				else
					ret = (ascending?-1:1);
			}
			break;
		case ST:
			Char	s1[3],s2[3];
			s1[0] = itemP1->status[0];
			s1[1] = itemP1->status[1];
			s2[0] = itemP2->status[0];
			s2[1] = itemP2->status[1];
			s1[2] = s2[2] = NULL;	//Null terminal
			if (StrNCompare(s1,s2,2) == 0)
				ret = 0;
			else 
			{
				if (StrNCompare(s1,s2,2) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		default:
			break;
	}
	return ret;		
}
*/
void CPlayerListView::UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar)
{
	Int16	totalItems;
   	Int16 	tableRows = TABLEROWS;
	
	totalItems = list->Size();
   	if( totalItems <= tableRows )
		SclSetScrollBar( scrollBar, 0, 0, 0, tableRows-1 );
	else	
		SclSetScrollBar( scrollBar, skippedItems, 0, totalItems - tableRows, tableRows-1 );
}

Boolean CPlayerListView::HandleEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	FormPtr	pForm = FrmGetActiveForm();
	RectangleType	recTable;
	//RctSetRectangle(&recTable,1,30,157,110);
	switch( event->eType )
	{
		case sclRepeatEvent:
			handled = HandleRepeatEvent( event, table, scrollBar );
			break;
		case tblEnterEvent:
			handled = HandleTableEnterEvent( event, table, scrollBar );
			break;
		case keyDownEvent:
			if (!gNavSupport || (gNavSupport && ObjectHasFocus(pForm, PlayerListFormTable)))
		  	{
		  		//in Object focus mode, only handle key down envent when table gets focus
				handled = HandleKeyDownEvent( event, table, scrollBar );
		  	}
			break;
		case frmObjectFocusTakeEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case PlayerListFormTable:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,PlayerListFormTable)); 
					FrmGetObjectBounds(pForm,FrmGetObjectIndex(pForm,PlayerListFormTable),&recTable);
					FrmGlueNavDrawFocusRing(pForm,PlayerListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
				default:
					break;
			}
			break;
		case frmObjectFocusLostEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case PlayerListFormTable:
					highLighted = false;
					highLightedRow = 0;
					UpdateTable(table, scrollBar, true );
					handled = true;
					break;
				default:
					break;
			}
			break;
		default:
			break;
	} 
	return handled;
}

Boolean CPlayerListView::HandleKeyDownEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	if (NavSelectPressed(event))
	{
		if (!highLighted)
		{
			highLighted = true;
			highLightedRow = 0; //top item on table
			UpdateTable( table, scrollBar, true );
			handled = true;
		}
		else	//highlighted, same as tap on the highlighted item
		{
			CPlayerItem *item = (CPlayerItem*)TblGetItemPtr( table, highLightedRow, 0 );
			handled = HandleSelectItem(item);
		}
		//return handled;
	}
	if (NavDirectionPressed(event, Up))
	{
		Scroll( table, scrollBar, -1, highLighted?false:true );
		handled = true;
	}
	if (NavDirectionPressed(event, Down))
	{
		Scroll( table, scrollBar, 1, highLighted?false:true );
		handled = true;
	}
/*
	switch( event->data.keyDown.chr )
    {
    	case vchrRockerUp:
		case vchrPageUp:
			Scroll( table, scrollBar, -1, highLighted?false:true );
			handled = true;
         	break;
        case vchrRockerDown: 
		case vchrPageDown:
			Scroll( table, scrollBar, 1, highLighted?false:true );
			handled = true;
			break;
		default:	
			break;
	}
*/	
    return handled;
}


/**
 * Handles repeat events (i.e. the scrollers).
 */
Boolean CPlayerListView::HandleRepeatEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Scroll( table, scrollBar, event->data.sclRepeat.newValue - event->data.sclRepeat.value, false );
	return false;
}

/**
 * Handles what happens when the user taps on an item in the tree/table.
 */
Boolean CPlayerListView::HandleTableEnterEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	UInt16   row = event->data.tblSelect.row;
	CPlayerItem *item = (CPlayerItem*)TblGetItemPtr( table, row, 0 );
	return HandleSelectItem(item);
}
CPlayerItem::CPlayerItem()
{
	playerName = NULL;
	rank = NULL;
	numericalRank = -1000;
	status[0] = status[1] = BLANKSPACE;
	action = IDLE;	
	gameID = 0;
	idle = NULL;
	info = NULL;
	country = NULL;
	won = lost = 0;
}
CPlayerItem::~CPlayerItem()
{
	Release();
}

Boolean CPlayerItem::operator==(const CPlayerItem& pi)
{	
	return (!StrCompare(playerName, pi.playerName));
}
void CPlayerItem::Release()
{
	if (playerName)
	{
		MemPtrFree(playerName);
		playerName = NULL;
	}
	if (rank)
	{
		MemPtrFree(rank);
		rank = NULL;
	}
	if (idle)
	{
		MemPtrFree(idle);
		idle = NULL;
	}
	if (info)
	{
		MemPtrFree(info);
		idle = NULL;
	}
	if (country)
	{
		MemPtrFree(country);
		idle = NULL;
	}
	return;
}
Boolean CPlayerListView::HandleSelectItem(CPlayerItem *item)
{
/*	CHALLENGETYPE *matchP;
	Char *name,*rank;

	if( !item )
	{
		return false;
	}
	else
	{
		matchP = new CHALLENGETYPE;
   		name = (Char*)MemPtrNew(StrLen(item->playerName)+1);
	   	if (name)
	   	{
			StrCopy(name,item->playerName);
			matchP->playerName = name;
	   	}
   		rank = (Char*)MemPtrNew(StrLen(item->rank)+1);
	   	if (rank)
	   	{
			StrCopy(rank,item->rank);
			matchP->playerRank = rank;
	   	}
	   	
	   	else
	   	{
			delete matchP;
			return false;	//Memory allocation failed
	   	}
	   	SetCurrentMatch(matchP);
		delete matchP;
		//FrmPopupForm(MatchForm);
		FrmPopupForm(PlayerViewForm);
		return true;
	}
*/
	if (item)
	{
		gCurrentPlayerP = item;
		FrmPopupForm(PlayerViewForm);
		return true;
	}
	else
		return false;
}
