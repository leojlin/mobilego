#include <PalmOS.h>
#include "CGameArray.h"
CGameArray::CGameArray()
{
	pArray = NULL;
	size = 0;
}

//template <class CGameItemPtr>
Boolean CGameArray::Add(CGameItemPtr node)
{
	Int32	newsize;
	CGameItemPtr		*pTemp;
	
	//Allocate new array
	newsize = size+1;
	pTemp = new CGameItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	//Copy array bit by bit
	if (pArray)
	{
		MemMove(pTemp,pArray,size * sizeof(CGameItemPtr));
	}
	pTemp[newsize -1]=node;
	//delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

//template <class CGameItemPtr>
void CGameArray::Clear()
{
	CGameItem	temp;
	Int32 i, k;
	if (pArray)
	{
		k = size;
		for (i=0;i<k;i++)
		{
				delete pArray[i]; //we need to free the item as well
		}
		delete pArray;
		pArray = NULL;
		size = 0;
	}
}

//template <class CGameItemPtr>
CGameArray::~CGameArray()
{
	Clear();
}
Int32 CGameArray::FindById(UInt16 id)
{
	for (Int32 i=0;i<size;i++)
	{
		if(pArray[i]->gameID == id)
		{
			return i;
		}
	}
	//Not found
	return -1;
}

//template <class CGameItemPtr>
Boolean CGameArray::Insert(CGameItemPtr node,Int32 index)
{
	Int32	newsize;
	CGameItemPtr		*pTemp;
	
	if (index<0)
	{
		index = 0;
	}
	//Make room for insertion
	newsize = size+1;
	pTemp = new CGameItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		//Move elements up to the insertion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CGameItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index+1,&pArray[index],(newsize-index-1)*sizeof(CGameItemPtr));
	}
	//Insert the new element
	pTemp[index] = node;
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}
/*
void CGameArray::Swap(Int32 index1, Int32 index2)
{
	CGameItemPtr		temp;
	if (index1==index2 || index1<0 || index1>=size || index2<0 || index2>=size)
		return;
	temp = pArray[index1];
	pArray[index1] = pArray[index2];
	pArray[index2] = temp;
}
*/
//template <class CGameItemPtr>
Boolean CGameArray::Remove(Int32 index)
{
	Int32	newsize;
	CGameItemPtr		*pTemp;

	if (index<0 ||index>=size)
	{
		return false;
	}
	//make the array smaller
	newsize = size-1;
	pTemp = new CGameItemPtr[newsize];
	if (!pTemp)
	{
		// Memory Allocation Failed.
		return false;
	}
	if (pArray)
	{
		pArray[index]->Release();	//Release dynamic memory allocated for this node
		delete pArray[index];
		//Move elements up to the deletion point into new storage
		MemMove(pTemp,pArray,index * sizeof(CGameItemPtr));
		//Move all remaining elements into new storage
		MemMove(pTemp+index,&pArray[index+1],(newsize-index)*sizeof(CGameItemPtr));
	}
	//Delete old array and point to new array
	if (pArray)
	{
		delete pArray;
	}
	pArray = pTemp;
	size = newsize;
	return true;
}

//template <class CGameItemPtr>
Int32 CGameArray::Size()
{
	return size;
}

//template <class CGameItemPtr>
CGameItemPtr CGameArray::operator[](Int32 index)
{
	return pArray[index];
}
void CGameArray::HeapSort(Int32 count,GAMEFIELD fld, Boolean  ascending)
{
	Int32	parent,child,n,i;
	CGameItemPtr	temp;
	n = count;
	i = n/2;
	for(;;)
	{
		if (i>0)
		{
			i--;
			temp = pArray[i];
		}
		else
		{
			n--;
			if (n<=0)
				return;
			temp = pArray[n];
			pArray[n] = pArray[0];
		}
		
		parent = i;
		child = i*2 + 1;
		
		while(child<n)
		{
			if (child+1<n && Compare(pArray[child+1],pArray[child],fld,ascending)<0)
			{
				child++;
			}
			if (Compare(pArray[child],temp,fld,ascending)<0)
			{
				pArray[parent] = pArray[child];
				parent = child;
				child = parent*2 + 1;
			}
			else
				break;
		}
		pArray[parent] = temp;
	}
}	
Int16 CGameArray::Compare(CGameItem *itemP1, CGameItem *itemP2, GAMEFIELD fld, Boolean ascending)
{
	Int16	ret = 0;
	switch (fld){
		case ID:
			if (itemP1->gameID == itemP2->gameID)
				ret = 0;
			else 
			{
				if (itemP1->gameID < itemP2->gameID)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case MV:
			if (itemP1->moveCount == itemP2->moveCount)
				ret = 0;
			else 
			{
				if (itemP1->moveCount < itemP2->moveCount)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case SZ:
			if (itemP1->size == itemP2->size)
				ret = 0;
			else 
			{
				if (itemP1->size < itemP2->size)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case HA:
			if (itemP1->handicap == itemP2->handicap)
				ret = 0;
			else 
			{
				if (itemP1->handicap < itemP2->handicap)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case KM:
			if (itemP1->komi == itemP2->komi)
				ret = 0;
			else 
			{
				if (itemP1->komi < itemP2->komi)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case BY:
			if (itemP1->byoyomi == itemP2->byoyomi)
				ret = 0;
			else 
			{
				if (itemP1->byoyomi < itemP2->byoyomi)
					ret = ascending ? 1 : -1 ;
				else
					ret = (ascending?-1:1);
			}
			break;
		case OB:
			if (itemP1->observer == itemP2->observer)
				ret = 0;
			else 
			{
				if (itemP1->observer < itemP2->observer)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case FR:
			if (StrCompare(itemP1->flag,itemP2->flag) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->flag,itemP2->flag) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case PW:
			if (StrCompare(itemP1->whitePlayer,itemP2->whitePlayer) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->whitePlayer,itemP2->whitePlayer) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case PB:
			if (StrCompare(itemP1->blackPlayer,itemP2->blackPlayer) == 0)
				ret = 0;
			else 
			{
				if (StrCompare(itemP1->blackPlayer,itemP2->blackPlayer) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case WR:
/*			if (RankCompare(itemP1->whiteRank,itemP2->whiteRank) == 0)
				ret = 0;
			else 
			{
				if (RankCompare(itemP1->whiteRank,itemP2->whiteRank) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}*/
			if (itemP1->whiteNumericalRank == itemP2->whiteNumericalRank)
				ret = 0;
			else 
			{
				if (itemP1->whiteNumericalRank < itemP2->whiteNumericalRank)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		case BR:
/*			if (RankCompare(itemP1->blackRank,itemP2->blackRank) == 0)
				ret = 0;
			else 
			{
				if (RankCompare(itemP1->blackRank,itemP2->blackRank) < 0)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}*/
			if (itemP1->blackNumericalRank == itemP2->blackNumericalRank)
				ret = 0;
			else 
			{
				if (itemP1->blackNumericalRank < itemP2->blackNumericalRank)
				 	ret = ascending ? 1 : -1 ;
				else
					ret = ascending ? -1 : 1 ;
			}
			break;
		default:
			break;
	}
	return ret;		
}
