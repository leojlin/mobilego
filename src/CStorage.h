#ifndef CSTORAGE_H
#define CSTORAGE_H
#include <PalmOS.h>
#include "sections.h"
#include "nngs.h"
#include "go.h"
#define TAGLEN 3
class CStorage{
public:

	CStorage() SEC_STRG;
	virtual ~CStorage() SEC_STRG;
	void Save(const TSgfInfo& sgfInfo, CMoveRecord& record) SEC_STRG;
//	Err Load(TSgfInfo &sgfInfo, CMoveRecord &record) SEC_STRG;
	Err Load(TSgfInfo &sgfInfo, CMoveRecord &record, Char* sgfFileName) SEC_STRG;
	void AllowSave() SEC_STRG;
	void AllowLoad() SEC_STRG;
private:

	void GetHeadString(Char*& headString, const TSgfInfo& sgfInfo) SEC_STRG;
	void GetHeadStruct(Char* headBegin, Char* headEnd, TSgfInfo& sgfInfo) SEC_STRG;
	void GetMoveString(Char*& moveString, CMoveRecord& record) SEC_STRG;
	void GetMoveStruct(Char* moveBegin, Char* moveEnd, CMoveRecord& record) SEC_STRG;
	Int16 GetNode(Char* begin, Char* &end) SEC_STRG;
	UInt16 GetTag(Char *sgf, UInt16 offset, Char *tag, Char*& valBegin, Char*&valEnd) SEC_STRG;
	Boolean	autoSave;
	Boolean autoLoad;
	UInt16	storageNum;
};

#endif // CSTORAGE_H
