#ifndef __NETEORK_H__
#define __NETEORK_H__
#include <PalmOS.h>
#include "sections.h"

Err WriteServer(Char*) SEC_NET;
Err ReadServer() SEC_NET;
Boolean IsConnected() SEC_NET;
Boolean IsLoggedIn() SEC_NET;
void DisconnectServer() SEC_NET;
void DisconnectNetwork() SEC_NET;
Boolean ConnectServer() SEC_NET;
void ProcessNetworkData() SEC_NET;
void Login() SEC_NET;
void Logout() SEC_NET;
void ProcessLine(Char* s) SEC_NET;
void ProcessNetworkData() SEC_NET;
#endif
