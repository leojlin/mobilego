#include <PalmOS.h>
#include <StdIOPalm.h>
#include <PalmOne_68K.h> 
#include "rsc.h"
#include "main.h"
#include "network.h"
#include "ui.h"
#include "nngs.h"
#include "go.h"
#include "forms.h"
#include "CGameListView.h"
#include "CPlayerListView.h"
#include "CChallengeListView.h"
#include "CStatistics.h"
/***********************************************************************
 *
 *	Entry Points 
 *
 ***********************************************************************/

static UInt16			gNetRefNum; 
static NetSocketRef		gSocket;  
static Int32			gConTimeOut = NETCONNECTTIMEOUT*SysTicksPerSecond();
static Boolean			gConnected = false;
static Boolean			gLoggedIn = false;
//static Int16			gDefaultStones = 0;
//static Boolean			gWaitLogin = false;
//static Boolean			gWaitPassword = false;
extern Boolean				gGuestAccount;
//Boolean			gValidPassword = true;
//#ifdef _DEBUG_
//extern Boolean			gProxy;
//#endif
extern Boolean 			gOpen;
extern Boolean 			gLooking;
extern Boolean			gSeek;
//extern UInt32			gAytSendSeconds;
extern UInt32			gAytGetSeconds ;
extern Int16			gAytWaitCount;
extern UInt32			gLastSendSeconds;
extern UInt32			gLastReceiveSeconds;
//extern Char				gConnectedAccountDescription[];
//extern Int16 			gConnectedAccountServerType;
//extern Boolean			gConnectedAccountSupportNmatch;
extern Char*			gInBuffer;
extern Char*			gTerminalBuffer;
extern CGameListView	gGameList;
extern CPlayerListView	gPlayerList;
extern CChallengeListView	gChallengeList;
//extern CChallengeArray	*gMatchList;
extern CChallengeItem	gCurrentMatch;
//extern CBoard			*gBoard;
extern CBoardPool		gBoardPool;
extern Int16			gAccountIdToBeUsed;
extern Int16			gAccountIdInUsed;
extern TGoAccount		gAccountList[];
extern Char				gMyAccountName[];
extern Boolean 			gPlaySound;
extern Boolean 			gSupportSound;
extern Boolean			gChatMode;
extern Boolean 			gNewMessage;
extern MemPtr			newMessageAudioP;
extern TDefsInfo 		gMyStatsDefs;		
extern CStatistics 		gStatistics;		
void WriteTerminal(Char *inString);
void SetOptions();
Boolean IsConnected()
{
	return gConnected;
}
Boolean IsLoggedIn()
{
	return gConnected && gLoggedIn;
}
void DisconnectServer()
{
	Err error = errNone;    
 	//LocalID	dbID;
 	//dbID = DmFindDatabase(0, "MobileGo");
	//Err	error = 0;
	NetLibSocketClose(gNetRefNum,gSocket,gConTimeOut,&error);
	NetLibClose(gNetRefNum,false);
	gConnected = false;
	gLoggedIn = false;
	gLastSendSeconds = 0;
	gSeek = false;
	gStatistics.SavePrefs();
	//Unregister for notification
	//SysNotifyUnregister(0,dbID,hsNotifyRemSleepRequestEvent,sysNotifyNormalPriority);			
	UnregisterForNotification(hsNotifyRemSleepRequestEvent);
}
void DisconnectNetwork()
{
	UInt16	libOpenCount = 0;
	NetLibOpenCount(gNetRefNum,&libOpenCount);
	//Force to close all Net Library
	do 
	{
		DisconnectServer();
		NetLibOpenCount(gNetRefNum,&libOpenCount);
	} while (libOpenCount > 0);
	NetLibFinishCloseWait(gNetRefNum);
}
Boolean ConnectServer()
{
	UInt16	ifErrs;
 	//LocalID	dbID;
 	//dbID = DmFindDatabase(0, "MobileGo");
	TGoAccount	account = gAccountList[gAccountIdToBeUsed];
	Char	errorString[20],proxyConnectString[80];
	NetIPAddr	serverIPAddr,*serverIPAddrP;
	NetSocketAddrType	addr;
	NetSocketAddrINType	*inetAddrP;
	NetHostInfoBufType	hostInfoBuf;
	Err	error = SysLibFind("Net.lib", &gNetRefNum);
	if (error)
	{
		FrmCustomAlert(NetworkFailureAlert,"SysLibFind()"," "," ");
		return false;	//No access to net library
	}
	error = NetLibOpen(gNetRefNum, &ifErrs);
	if (!error)
	{
		//No errors
	}
	else if (error == netErrAlreadyOpen)
	{
		//No errors, the library was already open.
	}
	else
	{
		StrIToA(errorString,error);
		FrmCustomAlert(NetworkFailureAlert,"SysLibOpen()",errorString," ");
		return false;
	}
	inetAddrP = (NetSocketAddrINType*) &addr;
	//Convert host name to IP
	if (account.useProxy)
		NetLibGetHostByName(gNetRefNum,account.proxyName,&hostInfoBuf,gConTimeOut,&error);
	else
		NetLibGetHostByName(gNetRefNum,account.serverName,&hostInfoBuf,gConTimeOut,&error);
	if (error)
	{
		StrIToA(errorString,error);
		FrmCustomAlert(NetworkFailureAlert,"NetLibGetHostByName()",errorString," ");
		return false;		
	}
	serverIPAddrP = (NetIPAddr*)hostInfoBuf.hostInfo.addrListP[0];
	serverIPAddr = *serverIPAddrP;
	if (account.useProxy)
		inetAddrP->port = NetHToNS(StrAToI(account.proxyPort));
	else
		inetAddrP->port = NetHToNS(StrAToI(account.serverPort));
	inetAddrP->family = netSocketAddrINET;
	inetAddrP->addr = NetHToNL(serverIPAddr);
	gSocket = NetLibSocketOpen(gNetRefNum, netSocketAddrINET, netSocketTypeStream,0,gConTimeOut,&error);
	if (error)
	{
		StrIToA(errorString,error);
		FrmCustomAlert(NetworkFailureAlert,"NetLibSocketOpen()",errorString," ");
		return false;		
	}
	
	if (NetLibSocketConnect(gNetRefNum,gSocket,&addr, sizeof(addr),gConTimeOut,&error))
	{
		StrIToA(errorString,error);
		FrmCustomAlert(NetworkFailureAlert,"NetLibSocketConnect()",errorString," ");
		gConnected = false;
	}
	else
	{
		gConnected = true;
		//Reset Last Session
		gStatistics.ResetLastSession(TimGetSeconds());		
	}
//#ifdef _DEBUG_
//	if (gProxy)
	if (account.useProxy)
	{
		sprintf(proxyConnectString,"CONNECT %s:%s HTTP/1.0\n",account.serverName,account.serverPort);
		WriteServer(proxyConnectString);
		//WriteServer("CONNECT http://cws.weiqi.net:9696 login\n");
	}
//#endif	
	gLastSendSeconds = 0;
	//Register for notification
	//SysNotifyRegister(0,dbID,hsNotifyRemSleepRequestEvent,NULL,sysNotifyNormalPriority,NULL); 
	RegisterForNotification(hsNotifyRemSleepRequestEvent);
	gAccountIdInUsed = gAccountIdToBeUsed;
	return gConnected;
}
/**************************************************************************
 * Function:	ReadServer()
 * Purpose:		Read a line from sokect into gInBuffer
 */
Err ReadServer()
{
	Err error = errNone;
	UInt16	bytesRead=0,i=0;
	Char	c='\0';
//Use NetLibSelect to check if there is data available for reading, to avoid simulator problem
  	NetFDSetType    readFDs,writeFDs,exceptFDs; 
  	UInt16       width; 
  	// Create the descriptor sets 
  	netFDZero(&readFDs); 
  	netFDZero(&writeFDs); 
  	netFDZero(&exceptFDs); 
  	netFDSet(gSocket, &readFDs); 
	width = gSocket; 
	#ifdef _DEBUG_
	Char	errorString[20];
	#endif
	while ((!error) && (c!='\n') && (i<(IN_BUFFER_SIZE-1)) && ((bytesRead > 0) || (i==0)))
	{
		if(NetLibSelect(gNetRefNum,width+1,&readFDs,&writeFDs,&exceptFDs,0,&error) <= 0)
			return errNone; // nothing to read
		bytesRead = NetLibReceive(gNetRefNum,gSocket,&c,1,0,NULL,0,gConTimeOut,&error);
		gLastReceiveSeconds = TimGetSeconds();	
		if ((bytesRead > 0) && (!error) )
		{
			gInBuffer[i]=c;
			i++;
		}
		if (error)
		{
			#ifdef _DEBUG_
			StrIToA(errorString,error);
			FrmCustomAlert(NetworkFailureAlert,"SysLibReceive()",errorString," ");
			//DisconnectServer();
			#endif
			break;
		}
		if (bytesRead == 0)
		{
			//Connection was closed by remote host.
			#ifdef _DEBUG_
			StrIToA(errorString,0);
			FrmCustomAlert(NetworkFailureAlert,"Closed By Remote (R)",errorString," ");
			#endif
			error = netErrSocketClosedByRemote;
			break;
		}
	}
	gInBuffer[i]='\0';
	gStatistics.UpdateDuration(gLastReceiveSeconds);
	gStatistics.UpdateBytes(DIRECTION_READ,i);
	return error;
}

Err WriteServer(Char* string)
{
	Err error = errNone;
	UInt16	bytesToSend,bytesSent = 0;
	Char	*dataP;
//Use NetLibSelect to check if there is data available for reading, to avoid simulator problem
  	NetFDSetType    readFDs,writeFDs,exceptFDs; 
  	UInt16       width; 
  	// Create the descriptor sets 
  	netFDZero(&readFDs); 
  	netFDZero(&writeFDs); 
  	netFDZero(&exceptFDs); 
  	netFDSet(gSocket, &writeFDs); 
	width = gSocket; 
	#ifdef _DEBUG_
	Char	errorString[20];
	#endif
	if (!gConnected)
		return error;
	bytesToSend = StrLen(string);
	dataP = string;
	while((bytesToSend > 0) && (!error)){
		if(NetLibSelect(gNetRefNum,width+1,&readFDs,&writeFDs,&exceptFDs,0,&error) <= 0)
			return errNone; // nothing to read
		bytesSent = NetLibSend(gNetRefNum,gSocket,dataP,bytesToSend,0,NULL,0,gConTimeOut,&error);
//		gLastSendSeconds = TimGetSeconds();	
		if (!error) 
		{
			dataP += bytesSent;
			bytesToSend -= bytesSent;
			gLastSendSeconds = TimGetSeconds();	
			gStatistics.UpdateDuration(gLastSendSeconds);
			gStatistics.UpdateBytes(DIRECTION_SENT,bytesSent);
			
		}
		else
		{
			if (bytesSent == 0)
			{
				//Connection was closed by remote host.
				#ifdef _DEBUG_
				StrIToA(errorString,0);
				FrmCustomAlert(NetworkFailureAlert,"Closed By Remote (W)",errorString," ");
				//DisconnectServer();
				#endif
				//error = netErrSocketClosedByRemote;
				break;
			}
			#ifdef _DEBUG_
			StrIToA(errorString,error);
			FrmCustomAlert(NetworkFailureAlert,"SysLibSend()",errorString," ");
			//DisconnectServer();
			#endif
			break;
		}
	}
	if (!error && bytesSent)
	{
		//Send padding "\n"	
		bytesSent = NetLibSend(gNetRefNum,gSocket,(void*)"\n",1,0,NULL,0,gConTimeOut,&error);
		if (bytesSent == 0)
		{
			#ifdef _DEBUG_
			StrIToA(errorString,0);
			FrmCustomAlert(NetworkFailureAlert,"Closed By Remote (WP)",errorString," ");
			//DisconnectServer();
			#endif
			error = netErrSocketClosedByRemote;
		}
		else if (error)
		{
			#ifdef _DEBUG_
			StrIToA(errorString,error);
			FrmCustomAlert(NetworkFailureAlert,"SysLibSend()",errorString," ");
			//DisconnectServer();
			#endif
		}
	}
	//Save statistics data whenever we sent to server
	gStatistics.SavePrefs();
	return error;	
}
void ProcessLine(Char* s) 
{
	//TSgfInfo		sgfInfo;
	static Char	statsPlayerName[20]="\0"; 	//Must be static to hold value through whole live cycle
	static Char	statsRank[10]="\0";			//Must be static to hold value through whole live cycle
	TDefsInfo statsDefs;				//Must be static to hold value through whole live cycle
	TTimerInfo	timerInfo; 
	TMoveInfo	moveInfo;
	//TGameInfo	gameInfo;
	CGameItem	*gameItemP;
	CPlayerItem	*playerItemP1,*playerItemP2;
	Char	cmdString[30];
	CChallengeItem	*challengeItemP,challengeItem;
  	UInt16 gameId;
	Int16 newKomi = 0;
	Int16 byoyomiStones = 0;
	Int16 ret;
	CBoard	*workingBoard; 
	Char opponent[20];
	opponent[0] = '\0';
	//Handle Time Info Update
  	ret = NNGS_TimerInfoUpdate(s,gameId,&timerInfo);
  	if (ret==9)
  	{
  		//gBoard->NewTimerInfo(timerInfo);
  		gBoardPool.recentUpdatedGameID = gameId;
  		workingBoard = gBoardPool.GetBoardByID(gameId);
		if (workingBoard)
		{
	  		workingBoard->NewTimerInfo(timerInfo);
 		}
 		else if (gameId!= gBoardPool.unobservedGameID && gBoardPool.waitNewGameID) //This is for restarted or observed game which does not have game id yet for this moment
  		{
  			workingBoard = gBoardPool.GetCurrentBoard();
  			gBoardPool.waitNewGameID = false;	
	  		workingBoard->NewTimerInfo(timerInfo);
  		}
		timerInfo.Release();
 		return;
  	}
	else
		timerInfo.Release();
	//Handle Byoyomi Stones Info Update
  	ret = IGS_ByoyomiStonesInfoUpdate(s,gameId,byoyomiStones);
  	if (ret==7)
  	{
  		workingBoard = gBoardPool.GetBoardByID(gameId);
  		if (workingBoard)
		{
	  		workingBoard->UpdateByoyomiStones(gameId,byoyomiStones);
		}
  		return;
  	}
	
	//Handle Move Info Update
	ret = NNGS_MoveInfoUpdate(s,&moveInfo);
  	if (ret==3)
  	{
  		//gBoard->NewMove(moveInfo);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.recentUpdatedGameID);
  		if (workingBoard)
	  		workingBoard->NewMove(moveInfo);
  		return;
  	}
  	else if (ret == MOVE_END)
  	{
  		//gBoard->EndOfMove();
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.recentUpdatedGameID);
  		if (workingBoard)
	  		workingBoard->EndOfMove();
	}
	//Handle Undo -- I'm teaching
	ret = NNGS_UndoLastMove(s,&moveInfo);
  	if (ret==3)
  	{
  		//gBoard->NewMove(moveInfo);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.recentUpdatedGameID);
  		if (workingBoard)
	  		workingBoard->UndoMove(moveInfo);
  		return;
  	}
  	else if (ret == MOVE_END)
  	{
  		//gBoard->EndOfMove();
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.recentUpdatedGameID);
  		if (workingBoard)
	  		workingBoard->EndOfMove();
	}
	//Handle Undo -- I'm observing
	ret = NNGS_UndoInGame(s,gameId,&moveInfo);
  	if (ret==3)
  	{
  		//gBoard->NewMove(moveInfo);
  		workingBoard = gBoardPool.GetBoardByID(gameId);
  		if (workingBoard)
	  		workingBoard->UndoMove(moveInfo);
  		return;
  	}
  	else if (ret == MOVE_END)
  	{
  		//gBoard->EndOfMove();
  		workingBoard = gBoardPool.GetBoardByID(gameId);
  		if (workingBoard)
	  		workingBoard->EndOfMove();
	}	
	//Handle title update
	Char	*title = NULL;
	title = NNGS_TitleInfoUpdate(s); 
	if (title)
	{
		//gBoard->UpdateTitle(title);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.recentUpdatedGameID);
		workingBoard->UpdateTitle(title);
		MemPtrFree(title);
		title = NULL;
		return;
	}
  	if (!StrNCompare(s, "Login:", StrLen("Login:")) && StrNCompare(s, "Login: Sorry ", StrLen("Login: Sorry"))) 
  	{
  		//gWaitLogin = true;
  		WriteServer(gAccountList[gAccountIdToBeUsed].userName);
  		return;
  	}
  	if (!IsLoggedIn() && (!StrNCompare(s, "1 1", StrLen("1 1")) || !StrNCompare(s, "Password:", StrLen("Password:"))))
  	{	//Only send password when not logged in. Fix for CWS and WING
  		//gWaitPassword = true;
		WriteServer(gAccountList[gAccountIdToBeUsed].password);
  		return;
  	}
  	if (!StrNCompare(s, "Invalid password!", StrLen("Invalid password!")) || !StrNCompare(s, "5 Invalid password!", StrLen("5 Invalid password!"))) 
  	{
  		DisconnectServer();
  		FrmAlert(LoginFailureAlert);
  		return;
  	}
  	if (NNGS_AccountName(s,gMyAccountName)==1)
  	{
  		if (!StrNCompare(gMyAccountName,"guest",5))
  			gGuestAccount = true;
  		else
  			gGuestAccount = false;
  		return;
  	}
//  	if (!StrNCompare(s, "Your account name is ", StrLen("Your account name is "))) 
//  	{
//  		gGuestAccount = true;
//  		gValidPassword = true;
//  		return;
//  	}
  	if (!StrNCompare(s, "1 5", StrLen("1 5")) || !StrNCompare(s, "#>", StrLen("#>")) \
  		|| !StrNCompare(s+3, "1 5", StrLen("1 5")) || !StrNCompare(s+3, "#>", StrLen("#>"))) 
  	{
  		if (!gLoggedIn)	//Just logged in
		{
			SetOptions();
	  		gLoggedIn = true; 
	  		//UpdateChatBuddyList(gMyAccountName);
	  		return;
		}
  	}
  	//Handle Stats for Rank
  	ret = NNGS_StatsRank(s, statsPlayerName,statsRank);
  	if (ret !=0)
  	{
  		if (ret==RANK_END)
  		{
  			//Update challenge list with rank info
  			gChallengeList.SetItemRank(statsPlayerName,statsRank);
  			FrmUpdateForm(ChallengeListForm,frmReloadUpdateCode);
  		}
  		return;
  	}
  	//Handle Stats for Defs
  	
  	ret = IGS_StatsDefs(s, statsPlayerName,&statsDefs);
  	if (ret !=0)
  	{
  		//Update my defs
  		if (ret==DEFS_END && !StrCompare(statsPlayerName,gMyAccountName))
  		{
  			gMyStatsDefs = statsDefs;
  		}
  		return;
  	}
  	
  	//Handle remove observation
	ret = NNGS_RemoveObservation(s, gameId);
	if (ret == 1)
	{
		gBoardPool.CloseBoard(gameId);
		return;
	}
	//Handle Change Komi
  	ret = NNGS_ChangeKomi(s, newKomi);
  	if (ret == 1) 
  	{
  		//gBoard->ChangeKomi(newKomi);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
	  		workingBoard->ChangeKomi(newKomi);
  		return;
  	}
  	//Handle Game Over
  	static Char result[20];
  	ret = NNGS_GameOver(s,gameId,result);
  	if (ret==4)
  	{
  		//gBoard->EndOfGame(gameId,result);
  		workingBoard = gBoardPool.GetBoardByID(gameId);
  		if (workingBoard)
  			workingBoard->EndOfGame(gameId,result);
  		return;
  	}
  	ret = IGS_GameAdjourned(s,gameId,result);
  	if (ret==4)
  	{
  		//gBoard->EndOfGame(gameId,result);
  		if (!gameId)
  			gameId = gBoardPool.matchingGameID;
  		workingBoard = gBoardPool.GetBoardByID(gameId);
  		if (workingBoard)
  			workingBoard->EndOfGame(gameId,result);
  		return;
  	}
  	Char player[20];
  	ret = IGS_MatchResult(s,player,result);
  	if (ret==4)
  	{
  		//gBoard->EndOfMatch(player,result);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
  			workingBoard->EndOfMatch(player,result);
  		return;
  	}
  	ret = IGS_MatchOver(s,player,result);
  	if (ret==1)
  	{
  		//gBoard->EndOfMatch(player,result);	//current playing game
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
  			workingBoard->EndOfMatch(player,result);
  		return;
  	}
  	//Handle add observation
  	
	ret = NNGS_AddObservation(s);
	if (ret == 1)
	{
		UInt8 action = OBSERVE;
		UInt8 status = PREPARING;
  		workingBoard = gBoardPool.GetNewBoard();
  		if (!workingBoard) 
  		{
  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
  			workingBoard = gBoardPool.GetInactiveBoard();
  			if (!workingBoard)
  			{
  				//No inactive board available. All boards are in process, 
  				//then we have to force current board to be closed. 
  				workingBoard = gBoardPool.GetCurrentBoard();
				workingBoard->Unobserve();	//unobserve current game in case we are observing a game
  			}
			//close the board, no need to remember the game id currently being observed
			//because it is not going to be ovwelayed by a matching game.
			gBoardPool.CloseBoard(workingBoard);	
  			workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
  			if (!workingBoard)	//For unknown reason, we are not able to get a new board, maybe my program goes wrong
  				return; 
  		}
  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
  		workingBoard->UpdateGameInfo(0,&action,&status,NULL,NULL); //Init working board, don't know game id yet for this moment
		gBoardPool.matchingGameID = 0;
		gBoardPool.waitNewGameID = true;
		if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
			FrmGotoForm(BoardForm);
		else
			FrmUpdateForm(BoardForm,frmReloadUpdateCode);
		return;
	}
	
	//Handle game list
  	gameItemP = new CGameItem;
  	if (!gameItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = NNGS_GameItemUpdate(s,gameItemP);
  	if (ret==12)
  	{
  		//Handle Game Item Update
  		gGameList.UpdateList(gameItemP);
  		//gBoard->InitGameInfo(gameItemP);	//Get game info for current playing game
  		workingBoard = gBoardPool.GetCurrentBoard();
  		if (workingBoard)
  			workingBoard->InitGameInfo(gameItemP);	//Get game info for current playing game
  		return;
  	}
  	else
  	{ 
  		if (ret == LIST_BEGIN) //Begining of game list
  			gGameList.BeginOfList();
  		if (ret == LIST_END) //End of game list
  			//temp = gGameList.updatingList;
  			gGameList.EndOfList();
  		delete gameItemP;
  	}

	//Handle player list
	playerItemP1 = new CPlayerItem;
  	if (!playerItemP1)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
	playerItemP2 = new CPlayerItem;
  	if (!playerItemP2)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = NNGS_PlayerItemUpdate(s,playerItemP1,playerItemP2);
  	if (ret==12)	//2 players in 1 line
  	{
  		gPlayerList.UpdateList(playerItemP1);
  		gPlayerList.UpdateList(playerItemP2);
  		return;
  	}
  	else if (ret>=6) //only 1 player in 1 line
  	{
  		gPlayerList.UpdateList(playerItemP1);
  		delete playerItemP2;
  		return;
  	}
  	else
  	{
  		if (ret == LIST_BEGIN) //Begining of player list
  			gPlayerList.BeginOfList();
  		if (ret == LIST_END) //End of player list
  			gPlayerList.EndOfList();
  		delete playerItemP1;
  		delete playerItemP2;
  	}

  	//Following functions are not available for Basic Edition
  	#ifndef	_BE_
  	//Handle ADJOURN request
  	ret = NNGS_AdjournRequest(s);
  	if (ret == 1)
  	{
		//if (FrmAlert(GameAdjournAlert) == 0 && gBoard)
		//	gBoard->Adjourn();
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
		if (FrmAlert(GameAdjournAlert) == 0 && workingBoard)
			workingBoard->Adjourn();
		return;
  	}
	//Handle MATCH request
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = NNGS_MatchRequest(s,challengeItemP);
  	if (ret==5)
  	{
  		//Update challenge queue
  		gChallengeList.BeginOfList();
  		gChallengeList.UpdateList(challengeItemP);
  		gChallengeList.EndOfList();
  		//Search player list for rank info.
  		if (gPlayerList.GetItemRank(challengeItemP->playerName,statsRank))
  		{
  			//Update challenge list with rank info if it is found in player list
  			gChallengeList.SetItemRank(challengeItemP->playerName,statsRank);
  		}
  		else
  		{
  			//Send a STATS command to server to get rank info
  			StrCopy(cmdString, "stats ");
  			StrCat(cmdString,challengeItemP->playerName);
  			WriteServer(cmdString);
  		}
  		//Update Match window
  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
		{
			SetCurrentMatch(challengeItemP);
			if (FrmGetActiveFormID()== MatchForm)
				FrmUpdateForm(MatchForm,frmReloadUpdateCode);
			else
				FrmPopupForm(MatchForm);
		}
  		return;
  	}
  	else
  	{
  		delete challengeItemP;
  	}
	//Handle IGS NMATCH request
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = IGS_NmatchRequest(s,challengeItemP);
  	if (ret==7)
  	{
  		//Handle new challenge 
  		gChallengeList.BeginOfList();
  		gChallengeList.UpdateList(challengeItemP);
  		gChallengeList.EndOfList();
  		//Search player list for rank info.
  		if (gPlayerList.GetItemRank(challengeItemP->playerName,statsRank))
  		{
  			//Update challenge list with rank info if it is found in player list
  			gChallengeList.SetItemRank(challengeItemP->playerName,statsRank);
  		}
  		else
  		{
  			//Send a STATS command to server to get rank info
  			StrCopy(cmdString, "stats ");
  			StrCat(cmdString,challengeItemP->playerName);
  			WriteServer(cmdString);
  		}
  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
		{
			SetCurrentMatch(challengeItemP);
			if (FrmGetActiveFormID()== MatchForm)
				FrmUpdateForm(MatchForm,frmReloadUpdateCode);
			else
				FrmPopupForm(MatchForm);
		}
  		return;
  	}
  	else
  	{
  		delete challengeItemP;
  	}
	//Handle IGS AutoMatch request
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = IGS_AutoMatchRequest(s,challengeItemP);
  	if (ret==5 && CompareMatchOpponent(gMyAccountName,challengeItemP->playerName))
  	//if (ret==5)
  	{
  		//if (!CompareMatchOpponent(gMyAccountName,challengeItemP->playerName))
  		//	challengeItemP->offered = false; //I initiated the automatch
  		{
	  		//Set defs stones
	  		//gDefaultStones = challengeItemP->stones;
	  		//Handle new challenge 
	  		gChallengeList.BeginOfList();
	  		gChallengeList.UpdateList(challengeItemP);
	  		gChallengeList.EndOfList();
	  		//Search player list for rank info.
	  		if (gPlayerList.GetItemRank(challengeItemP->playerName,statsRank))
	  		{
	  			//Update challenge list with rank info if it is found in player list
	  			gChallengeList.SetItemRank(challengeItemP->playerName,statsRank);
	  		}
	  		else
	  		{
	  			//Send a STATS command to server to get rank info
	  			StrCopy(cmdString, "stats ");
	  			StrCat(cmdString,challengeItemP->playerName);
	  			WriteServer(cmdString);
	  		}
	  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
			{
				SetCurrentMatch(challengeItemP);
				if (FrmGetActiveFormID()== MatchForm)
					FrmUpdateForm(MatchForm,frmReloadUpdateCode);
				else
					FrmPopupForm(MatchForm);
			}
	  		return;
  		}
  	}
  	else
  		delete challengeItemP;
	//Handle IGS style MATCH dispute
	static Int16	disputedMatch = 0;
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = IGS_MatchDispute(s,challengeItemP);
  	if (ret==5)	
  	{
  		if (disputedMatch==0)	
  		{
			disputedMatch = 1;	//count 
			delete challengeItemP;
			return;
  		}
  		disputedMatch = 0; 	
  		//Only handle the second disputed match condition disputedMatch == 1
  		if (StrCompare(gCurrentMatch.playerName,challengeItemP->playerName))	//if it's sent by myself,ignore it
  			delete challengeItemP;
  		else
  		{
	  		gChallengeList.BeginOfList();
	  		gChallengeList.UpdateList(challengeItemP);
	  		gChallengeList.EndOfList();
	  		//Search player list for rank info.
	  		if (gPlayerList.GetItemRank(challengeItemP->playerName,statsRank))
	  		{
	  			//Update challenge list with rank info if it is found in player list
	  			gChallengeList.SetItemRank(challengeItemP->playerName,statsRank);
	  		}
	  		else
	  		{
	  			//Send a STATS command to server to get rank info
	  			StrCopy(cmdString, "stats ");
	  			StrCat(cmdString,challengeItemP->playerName);
	  			WriteServer(cmdString);
	  		}
  			if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
			{
				SetCurrentMatch(challengeItemP);
				if (FrmGetActiveFormID()== MatchForm)
					FrmUpdateForm(MatchForm,frmReloadUpdateCode);
				else
					FrmPopupForm(MatchForm);
			}
  		}
  		return;
  	}
  	else
  	{
  		delete challengeItemP;
  	}
	//Handle IGS style NMATCH dispute
	static Int16	disputedNmatch = 0;
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
  	ret = IGS_NmatchDispute(s,challengeItemP);
  	if (ret==7)	
  	{
  		if (disputedNmatch==0)	
  		{
  		//Only handle the first disputed match condition disputedNmatch == 0
  			if (StrCompare(gCurrentMatch.playerName,challengeItemP->playerName))	//if it's sent by myself,ignore it
  				delete challengeItemP;
  			else
  			{
		  		gChallengeList.BeginOfList();
		  		gChallengeList.UpdateList(challengeItemP);
		  		gChallengeList.EndOfList();
		  		//Search player list for rank info.
		  		if (gPlayerList.GetItemRank(challengeItemP->playerName,statsRank))
		  		{
		  			//Update challenge list with rank info if it is found in player list
		  			gChallengeList.SetItemRank(challengeItemP->playerName,statsRank);
		  		}
		  		else
		  		{
		  			//Send a STATS command to server to get rank info
		  			StrCopy(cmdString, "stats ");
		  			StrCat(cmdString,challengeItemP->playerName);
		  			WriteServer(cmdString);
		  		}
	  			if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
				{
					SetCurrentMatch(challengeItemP);
					if (FrmGetActiveFormID()== MatchForm)
						FrmUpdateForm(MatchForm,frmReloadUpdateCode);
					else
						FrmPopupForm(MatchForm);
				}
  			}
			disputedNmatch = 1;	//count 
			return;
  		}
  		else
  		{
  			disputedNmatch = 0; 	
			delete challengeItemP;
  			return;
  		}
  	}
  	else
  	{
  		delete challengeItemP;
  	}

	//handle MATCH withdraw
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
	ret = NNGS_MatchWithdraw(s,challengeItemP);
	if (ret == 1)
	{
  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
  		{
			gCurrentMatch.Release();
			if (FrmGetActiveFormID()== MatchForm)
			{
				FrmCustomAlert(WithdrawMatchAlert,challengeItemP->playerName,"","");	
				FrmReturnToForm(0);
			}
  		}
  		gChallengeList.BeginOfList();
		gChallengeList.RemoveItem(challengeItemP->playerName);
  		gChallengeList.EndOfList();
  		delete challengeItemP;
  		return; 
	}
	else
		delete challengeItemP;	
	//handle IGS NMATCH withdraw
  	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
	ret = IGS_NmatchWithdraw(s,challengeItemP);
	if (ret == 1) 
	{
  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
  		{
			gCurrentMatch.Release();
			if (FrmGetActiveFormID()== MatchForm)
			{
				FrmCustomAlert(WithdrawMatchAlert,challengeItemP->playerName,"","");	
				FrmReturnToForm(0);
			}
  		}
  		gChallengeList.BeginOfList();
		gChallengeList.RemoveItem(challengeItemP->playerName);
  		gChallengeList.EndOfList();
  		delete challengeItemP;
  		return;
	}
	else
		delete challengeItemP;	
	//Handle IGS NMATCH Not Support
	ret = IGS_NmatchNotSupport(s);
	if (ret == 1) 
	{
	Char	*responseString = NULL;
	responseString = NNGS_ServerResponse(s);
	if (responseString)
	{
		FrmCustomAlert(ServerResponseAlert,responseString,"","");
		MemPtrFree(responseString);
//		return;
	}
		//if NMATCH is not supported, then use MATCH instead
		gCurrentMatch.matchtype = MATCH;
		if (FrmGetActiveFormID()== MatchForm)
			FrmUpdateForm(MatchForm,frmReloadUpdateCode);
		return;	
	}
	//handle MATCH decline
	challengeItemP = new CChallengeItem;
  	if (!challengeItemP)
  	{
  		// Memory Allocation Failed.
  		return;
  	}
	ret = NNGS_MatchDecline(s,challengeItemP);
	if (ret == 1)
	{
		//Int32 index;
		//index = gMatchList->FindByName(challengeItemP->playerName);	
		//if ( index != -1)
		//	gMatchList->Remove(index); 	
		FrmCustomAlert(DeclineMatchAlert,challengeItemP->playerName,"","");	
  		if (!CompareMatchOpponent(gCurrentMatch.playerName,challengeItemP->playerName))	//we are currently negotiating with this player
  		{
			gCurrentMatch.Release();
			if (FrmGetActiveFormID()== MatchForm)
				FrmReturnToForm(0);
  		}
  		gChallengeList.BeginOfList();
		gChallengeList.RemoveItem(challengeItemP->playerName);
  		gChallengeList.EndOfList();
		delete challengeItemP;
  		return;
	}
	else
		delete challengeItemP;

	//handle RESTART a game
	ret = IGS_GameReloaded(s);
	if (ret == 1)
	{
		UInt8 action = IDLE;	//It could be PLAY or TEACH later
		UInt8 status = PREPARING;
		/*
  		workingBoard = gBoardPool.GetNewBoard();
  		if (!workingBoard) 
  		{
  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
  			workingBoard = gBoardPool.GetInactiveBoard();
  			if (workingBoard)
  			{
  				gBoardPool.CloseBoard(workingBoard); //close the inactive board
  			}
  			else
  			{
  				//No inactive board available. All boards are in process, 
  				//then we have to force current board to be discarded. 
  				workingBoard = gBoardPool.GetCurrentBoard();
				workingBoard->Unobserve();	//unobserve current game in case we are observing a game
				//discard current board,	need to remember the game id currently being observed
				//because it is going to be ovwelayed by a matching game.
  				gBoardPool.DiscardBoard(workingBoard);	
  			}
  			workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
  			if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  				return; 
  		}
  		*/
  		workingBoard = OpenNewBoard();
  		if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  			return;
  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
  		workingBoard->UpdateGameInfo(0,&action,&status,NULL,NULL); //Init working board, don't know game id yet for this moment
		gBoardPool.matchingGameID = 0;
		gBoardPool.waitNewGameID = true;
		if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
			FrmGotoForm(BoardForm);
		else
			FrmUpdateForm(BoardForm,frmReloadUpdateCode);
		return;
	}  
		
	//handle AutoMatch confirmed
	ret = IGS_AutoMatchConfirmed(s, player);
	if (ret == 1)
	{
		UInt8 action = PLAY;
		UInt8 status = PREPARING;
		/*
  		workingBoard = gBoardPool.GetNewBoard();
  		if (!workingBoard) 
  		{
  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
  			workingBoard = gBoardPool.GetInactiveBoard();
  			if (workingBoard)
  			{
  				gBoardPool.CloseBoard(workingBoard); //close the inactive board
  			}
  			else
  			{
  				//No inactive board available. All boards are in process, 
  				//then we have to force current board to be discarded. 
  				workingBoard = gBoardPool.GetCurrentBoard();
				workingBoard->Unobserve();	//unobserve current game in case we are observing a game
				//discard current board,	need to remember the game id currently being observed
				//because it is going to be overlayed by a matching game.
  				gBoardPool.DiscardBoard(workingBoard);	
  			}
  			workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
  			if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  				return; 
  		}
  		*/
  		workingBoard = OpenNewBoard();
  		if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  			return;
  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
  		workingBoard->UpdateGameInfo(0,&action,&status,NULL,NULL); //Init working board, don't know game id yet for this moment
  		CChallengeItem *autoMatch=NULL;
  		//Char string[60];
  		autoMatch = gChallengeList.FindItemByName(player);
  		if (autoMatch)
  		{
  			//This automatch is offered by opponent
  			//Use opponent's defs stones
  			//WriteTerminal(player);
  			//WriteTerminal("\n");
  			//sprintf(string,"rank=%s,stones=%d\n",autoMatch->playerRank,autoMatch->stones);
  			//WriteTerminal(string);
  			workingBoard->UpdateByoyomiStones(0,autoMatch->stones);
  			//workingBoard->UpdateByoyomiStones(0,gDefaultStones);
  		}
  		else
  		{	//This automatch is issued by me
  			//Use my defs stones
  			workingBoard->UpdateByoyomiStones(0,gMyStatsDefs.maxStones);
  		}
		gBoardPool.matchingGameID = 0;
		gBoardPool.waitNewGameID = true;
		//Remove from challenger list
		gChallengeList.RemoveItem(player);	
		if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
			FrmGotoForm(BoardForm);
		else
			FrmUpdateForm(BoardForm,frmReloadUpdateCode);
		return;
	}  

	//handle CREATE new match
	ret = NNGS_MatchCreate(s, gameId, opponent);
	if (ret == 2)
	{
		UInt8 action = IDLE;	//It could be PLAY or TEACH on NNGS
		UInt8 status = PREPARING;
  		TGameInfo gameInfo;
  		//We are in the middle of initializing a game, like TEACH
  		//For NNGS, ignore this response. 
  		if (gBoardPool.matchingGameID ==0 && gBoardPool.waitNewGameID)
  			return;
  		
  		gBoardPool.GetCurrentBoard()->GetGameInfo(gameInfo);
  		if (gameInfo.id == gameId && (gameInfo.action == PLAY || gameInfo.action == TEACH) && gameInfo.status == PREPARING)	//Bug fix
  		{
  			//This is to deal with NNGS style Create Match.
  			//NNGS issue two NNGS_MatchCreate confirmation.
  			//One is 9 Creating matach [... , the other one is 9 Match [...
  			return;
  		}
  		/*
  		workingBoard = gBoardPool.GetNewBoard();
  		if (!workingBoard) 
  		{
  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
  			workingBoard = gBoardPool.GetInactiveBoard();
  			if (workingBoard)
  			{
  				gBoardPool.CloseBoard(workingBoard); //close the inactive board
  			}
  			else
  			{
  				//No inactive board available. All boards are in process, 
  				//then we have to force current board to be discarded. 
  				workingBoard = gBoardPool.GetCurrentBoard();
				workingBoard->Unobserve();	//unobserve current game in case we are observing a game
				//discard current board,	need to remember the game id currently being observed
				//because it is going to be ovwelayed by a matching game.
  				gBoardPool.DiscardBoard(workingBoard);	
  			}
  			workingBoard = gBoardPool.GetNewBoard(); //try to get a new board again	
  			if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  				return; 
  		}
  		*/
  		workingBoard = OpenNewBoard();
  		if (!workingBoard)	//For unknown reason, we are not able to get an new board, maybe my program goes wrong
  			return;
  		gBoardPool.SetCurrentBoard(workingBoard);	//Change current board to this new board
  		workingBoard->UpdateGameInfo(gameId,&action,&status,NULL,NULL); //Init working board
  		gBoardPool.matchingGameID = gameId;
  		gBoardPool.waitNewGameID = false;
		//Reset Seek flag
		gSeek = false;
		//Remove from challenger list
		gChallengeList.RemoveItem(opponent);	
		//Get game infomation
		Char	cmd[20],idString[10];
		StrIToA(idString,gameId);
		StrCopy(cmd,"game ");
		StrCat(cmd,idString);
		WriteServer(cmd);	//Request for current game's infomation
		if (FrmGetActiveFormID()== MatchForm)	//close match dialog window
			FrmReturnToForm(0);
		if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
			FrmGotoForm(BoardForm);
		else
			FrmUpdateForm(BoardForm,frmReloadUpdateCode);
		return;
	}
	//handle SEEK response
	ret = IGS_Seek(s);
	switch (ret){
		case 1:
			gSeek = true;
			return;
			break;
		case 2:
			gSeek = false;
			return;
			break;
		default:
			break;
	}
	//Handle STORED games
	ret = NNGS_StoredGame(s);
	if (ret > 0)
	{
		FrmGotoForm(StoredForm);
		return;
	}
	//handle check sore
	ret = NNGS_CheckScore(s);
	if (ret == 1)
	{
		//gBoard->CheckScore();
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
  			workingBoard->CheckScore();
		return;
	}		
	//Handle dead group
	Int16 x=-1,y=-1;
	ret=NNGS_DeadGroup(s,x,y);
	if (ret==1)
	{
		//gBoard->MarkDead(x,y);
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
  			workingBoard->MarkDead(x,y);
		return;
	}
	//Handle restore group
	ret=NNGS_RestoreGroup(s);
	if (ret==1)
	{
		//gBoard->UnmarkDead();
  		workingBoard = gBoardPool.GetBoardByID(gBoardPool.matchingGameID);
  		if (workingBoard)
  			workingBoard->UnmarkDead();
		return;
	}
	#endif	//of #ifndef _BASIC_
	//handle server response
	Char	*responseString = NULL;
	responseString = NNGS_ServerResponse(s);
	if (responseString)
	{
		FrmCustomAlert(ServerResponseAlert,responseString,"","");
		MemPtrFree(responseString);
		return;
	}
	//handle Chatting message
	responseString = NNGS_GetMessage(s,opponent);
	if (responseString)
	{
		if (FrmGetActiveFormID()!= MainForm)
		{
			if (gSupportSound && gPlaySound && newMessageAudioP)
				SndPlayResource(newMessageAudioP,sndGameVolume,sndFlagAsync);
			gNewMessage = true;
		}
		UpdateChatBuddyList(opponent);	//Add the opponent to my chat buddy list
		WriteTerminal(opponent);
		WriteTerminal(": ");
		WriteTerminal(responseString);
		WriteTerminal("\n");
		MemPtrFree(responseString);
		return;
	}
	//Handle AYT response
	ret = NNGS_GetAYT(s,gAytGetSeconds,gAytWaitCount);
	if (ret == 1)
		return;
}
	

void ProcessNetworkData()
{
	Err error = errNone;
	UInt16	buffLen;
	if (gInBuffer==NULL)
	{
		return;
	}
	else if (*gInBuffer==NULL)
	{
		error = ReadServer();
	}
	if (error==netErrSocketClosedByRemote)
	{
		DisconnectServer();
		return;
	}	
	else if (error != errNone)
	{
		return;
	}
	buffLen = StrLen(gInBuffer);
	while (buffLen>0 && error==errNone)
	{	
		//gLastSendSeconds = TimGetSeconds();
		//Update terminal buffer
#ifdef _DEBUG_
		WriteTerminal(gInBuffer);	//Echo every thing to our terminal for testing and debugging
#else
		if (!gChatMode || !IsLoggedIn())
			WriteTerminal(gInBuffer);	//Echo every thing to our terminal for testing and debugging
#endif

		//Parse line
		ProcessLine(gInBuffer);
		*gInBuffer = NULL;	//Cleanup gInBuffer after parsed the line
		error = ReadServer();		//Read next line
		buffLen = StrLen(gInBuffer);
	}
	if (error==netErrSocketClosedByRemote)
	{
		DisconnectServer();
	}	
}
void Login()
{
	if (!IsConnected())
	{
 		/*
 		StrCopy(gConnectedAccountDescription,gAccountList[gAccountIdToBeUsed].description);
		gConnectedAccountServerType = gAccountList[gAccountIdToBeUsed].serverType;
		gConnectedAccountSupportNmatch = gAccountList[gAccountIdToBeUsed].supportNmatch;
		*/
 		StrCopy(gMyAccountName,gAccountList[gAccountIdToBeUsed].userName);
 		
		ConnectServer();
		gGameList.Flush();
		gPlayerList.Flush();
		gChallengeList.Flush();
	}
}
void Logout()
{
	Char msg[100];
	//TGameInfo	info;
	//CBoard	*workingBoard;
	if (!IsConnected())
		return;
	else
	{
		WriteServer("quit"); 
		DisconnectServer();
		/*
		if (gBoard)
		{
			gBoard->GetGameInfo(info);
			if(info.status != GAMEOVER && info.status != PREPARING) 
			{
				UInt8 status = INTERRUPTED;
				gBoard->UpdateGameInfo(0,NULL,&status,NULL,NULL);
			}
		}
		*/
		/*
  		workingBoard = gBoardPool.GetCurrentBoard();
  		if (workingBoard)
		{
			workingBoard->GetGameInfo(info);
			if(info.status != GAMEOVER && info.status != PREPARING) 
			{
				UInt8 status = INTERRUPTED;
				workingBoard->UpdateGameInfo(0,NULL,&status,NULL,NULL);
			}
		}
		*/
		//Clean up lists
		//gGameList.Flush();
		//gPlayerList.Flush();
		//gChallengeList.Flush();
		//StrCopy(gTerminalBuffer,"Logout from ");
		//StrCat(gTerminalBuffer,gConnectedAccountDescription);
		StrCopy(msg,"Logout from ");
		StrCat(msg,gAccountList[gAccountIdInUsed].description);
		StrCat(msg,"\n"); 
		WriteTerminal(msg);
		//Change form title to the description of default account 
		//StrCopy(gConnectedAccountDescription,gAccountList[gAccountIdToBeUsed].description);
	}
}
void SetOptions()
{
	WriteServer("toggle client on");	//Toggle settings
	#ifndef	_BE_
	if (gLooking)	
		WriteServer("toggle looking on");
	else if (gOpen)
	{
		WriteServer("toggle looking off");
		WriteServer("toggle open on");
	}
	else
		WriteServer("toggle open off");
	#else
		WriteServer("toggle open off");
	#endif
	if (gAccountList[gAccountIdToBeUsed].serverType == IGS)
	{ 
		//IGS specific options
		WriteServer("toggle newrating on");
		WriteServer("toggle singlegame on");
		#ifndef	_BE_
		if (gAccountList[gAccountIdToBeUsed].supportNmatch)
		{	
			WriteServer("toggle nmatch on");
			WriteServer("toggle seek on");
		} 
		#endif  
	}	
	if (!gGuestAccount)
		WriteServer("stats");	//Get my defs		

}