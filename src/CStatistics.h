#ifndef _CSTATISTICS_H
#define _CSTATISTICS_H
#include <PalmOS.h>
#include "sections.h"
typedef struct  TStatistics
{
	UInt32	openingTimeStamp;	//Starting point of this statistics
	UInt32	endingTimeStamp;	//Last point of this statistics
	Int16	dayCount;	//Days since this statistics began
	UInt32	duration;	//Duration (in second) since this statistics began
	UInt32	readBytes;	
	UInt32	sentBytes;
};
const Int8	DIRECTION_READ = 0;
const Int8 DIRECTION_SENT = 1;

class CStatistics{
public:

	CStatistics() SEC_NET;
	virtual ~CStatistics() SEC_NET;
	void	UpdateDuration(UInt32) SEC_NET;
	void	UpdateBytes(Int8,UInt32) SEC_NET;
	void	ClearToday() SEC_NET;
	void	ResetAll() SEC_NET;
	void	ResetLastSession(UInt32) SEC_NET;
	void	ResetTotal(UInt32) SEC_NET;
	void 	TouchToday(UInt32) SEC_NET;
	Boolean SameAsToday(UInt32) SEC_NET;
	void	UpdateForm() SEC_NET;
	void	LoadPrefs() SEC_NET;
	void	SavePrefs() SEC_NET;
private:
	TStatistics lastSession;
	TStatistics	today;
	TStatistics	total;
//	TStatistics	average;	//Average Daily Online
	
};

#endif // _CSTATISTICS_H
