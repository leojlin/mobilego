//#include <TblGlue.h>
#include <FrmGlue.h>
#include <PalmOne_68K.h>
#include "main.h"
#include "forms.h"
#include "CGameArray.h"
#include "nngs.h"
#include "CGameListView.h"
#include "network.h"
#include "rsc.h"
#include "go.h"
#include "ui.h"
//extern CBoard	*gBoard;
extern CBoardPool	gBoardPool;
extern GAMEFIELD	gGameSortedField;
extern Boolean gGameAscending;
extern Boolean gNavSupport;

void CGameListView::SetTableDrawProcedure(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	customDrawFunction = f;
}
CGameListView::CGameListView()
{
	skippedItems=0; 
	//filterOn = false;
	updatingList = false;
	list = new CGameArray;
	//Check memory allocation here
 	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
}
CGameListView::CGameListView(void (*f)(void *tableP, Int16 row, Int16 column, RectanglePtr bounds))
{
	skippedItems=0; 
	//filterOn = false;
	updatingList = false;
	list = new CGameArray;
	//Check memory allocation here
 	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
   	highLighted = false;
   	highLightedRow = 0;
	customDrawFunction = f;
}
CGameListView::~CGameListView()
{
	Clear(); 
}
void CGameListView::Clear()
{
	delete list; 
}
Boolean CGameListView::HasItem()
{
	if (list->Size())
		return true;
	else
		return false;
}

void CGameListView::UpdateList(CGameItem *item)
{
	UInt32 	size;
	Int32	index;
	
	if (updatingList)
	{
//		list->Add(item);
		index = list->FindById(item->gameID);	
		if ( index != -1)			//there was a non-existing game with same id in the list
		{							//replace it with new game info	
			if (!list->Remove(index)) 	//remove old game first
				return;				//Fail to remove
		}
		if (!list->Insert(item,index))	//then to insert
			return;					//Fail to insert
		size = list->Size();
		if ((FrmGetActiveFormID() == GameListForm))
		{
			switch (size % 2)
			{
				case 0:
					WinDrawChars("Loading\\",StrLen("Loading\\"),70,1);
					break;
				case 1:
					WinDrawChars("Loading/",StrLen("Loading/"),70,1);
					break;
			}
		}
	}
	return ;
}
void CGameListView::SortList(GAMEFIELD fld, Boolean  ascending)
{
	/*
	//Sort list by using insertion sort algorithm
	CGameItem *temp;
	Int32	i,j,sz;
	sz = list->Size();
	if (!sz)	
		return;		//list is empty
	for (i = 1; i < sz; i++)
	{
		if ((FrmGetActiveFormID() == GameListForm))
		{
			switch (i % 2)
			{
				case 0:
					WinDrawChars("\\",1,80,1);
					break;
				case 1:
					WinDrawChars("/",1,80,1);
					break;
			}
		}
		j = i;
		temp = (*list)[i];
		while (j>0 && CompareItem((*list)[j-1],temp,fld,ascending)==-1)
		{
			list->Swap(j-1,j);
			j--;
		}
	}
	*/
	if ((FrmGetActiveFormID() == GameListForm))
		WinDrawChars("Sorting...",StrLen("Sorting..."),70,1);
	list->HeapSort(list->Size(),fld,ascending);
	
}		
	
void CGameListView::EndOfList()
{
	if (updatingList)
	{
		updatingList = false;
		SortList(gGameSortedField,gGameAscending);
		FrmUpdateForm(GameListForm,frmReloadUpdateCode);
	}
}
void CGameListView::BeginOfList()
{
	skippedItems=0; 
	updatingList = true;
   	highLighted = false;
   	highLightedRow = 0;
}
Boolean CGameListView::FindGame(UInt16 id,CGameItem **item)
{
	Int16	index;
	Boolean found = false;
	index = list->FindById(id);
	if (index!=-1)
	{
		*item = (*list)[index];
		found = true;
	}
	return found;
		
}
void CGameListView::Flush()
{
	if (list) 
		delete list;
	list = new CGameArray;
	ErrFatalDisplayIf(list==NULL,"Fail to allocate memory");
	skippedItems=0; 
   	highLighted = false;
   	highLightedRow = 0;
	//updatingList = true;
}
void CGameListView::DisplayItemTotal()
{
	Int16	totalItems;
	totalItems = list->Size();
	//Display item total
	if ((FrmGetActiveFormID() == GameListForm))
	{
	   	RectangleType	r;
	   	r.topLeft.x = 54;
	   	r.topLeft.y = 145;
	   	r.extent.x = 65;
	   	r.extent.y = 11;
		Char	totalString[10];
		Char	displayTitle[20];
		StrIToA(totalString,totalItems);
		StrCopy(displayTitle,"Total: ");
		StrCat(displayTitle,totalString);
		WinEraseRectangle(&r,0);
		WinDrawChars(displayTitle,StrLen(displayTitle),55,145);
	}
}
void CGameListView::UpdateTable(TablePtr table, ScrollBarPtr scrollBar, Boolean redraw)
{
	Int16	row;
	Int16	totalItems;
	Int16	tableRows = TABLEROWS;
	CGameItem *itemP;
	totalItems = list->Size();
	if( skippedItems < 0 )     // if we page down, this could end up negative
    	skippedItems = 0;

   	if( skippedItems + tableRows > totalItems && totalItems >= tableRows )
      	skippedItems = totalItems - tableRows;

   	if( skippedItems > 0 && totalItems <= tableRows )
      	skippedItems = 0;
   	InitTable(table);
	for( row=0; row<tableRows; row++ )
    {
    	if( row >= totalItems )
        	TblSetRowUsable( table, row, false );			// so it won't get drawn
		else
        {
        	itemP = (*list)[row + skippedItems ];
			TblSetRowUsable( table, row, true );			// so it will get drawn
			TblMarkRowInvalid( table, row );					// so it will get updated
			TblSetItemPtr( table, row, 0, (void *)itemP);
			itemP = (CGameItem*)TblGetItemPtr( table, row, 0 );
		}
	}
	if( redraw )
    	TblDrawTable( table );
	UpdateScrollers( table, scrollBar );   
}

void CGameListView::Scroll(TablePtr table, ScrollBarPtr scrollBar, Int16 dir, Boolean fullPage)
{
	Int16	tableRows = TABLEROWS;

	if( fullPage )
	{
    	dir *= tableRows;
		skippedItems += dir;
	}
	else
	{
		highLightedRow +=dir;
		if (skippedItems+highLightedRow >= list->Size()) //the last item is highlighted
			highLightedRow = highLightedRow -1;
		if (highLightedRow >= tableRows)
		{
			highLightedRow = highLightedRow -1;
			skippedItems += dir;
		}
		else if (highLightedRow < 0)
		{
			highLightedRow = 0;
			skippedItems += dir;
		}
	}
	UpdateTable( table, scrollBar, true );
}

void CGameListView::InitTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
	{		
		TblSetItemStyle( table, row, 0, customTableItem );
		TblSetItemFont( table, row, 0, stdFont );
		TblSetRowUsable( table, row, false );
	}
	TblSetColumnUsable( table, 0, true );
   	TblSetCustomDrawProcedure( table, 0, customDrawFunction );
}

void CGameListView::DisableTable(TablePtr table)
{
	Int16	row;
	Int16	tableRows = TABLEROWS;
	for( row = 0; row < tableRows; row++ )
		TblSetRowUsable( table, row, false );
}
void CGameListView::UpdateScrollers(TablePtr table, ScrollBarPtr scrollBar)
{
	Int16	totalItems;
   	Int16 	tableRows = TABLEROWS;
	
	totalItems = list->Size();
   	if( totalItems <= tableRows )
		SclSetScrollBar( scrollBar, 0, 0, 0, tableRows-1 );
	else	
		SclSetScrollBar( scrollBar, skippedItems, 0, totalItems - tableRows, tableRows-1 );
}

Boolean CGameListView::HandleEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	FormPtr	pForm = FrmGetActiveForm();
	RectangleType	recTable;
	//RctSetRectangle(&recTable,1,30,157,110);

	switch( event->eType )
	{
		case sclRepeatEvent:
			handled = HandleRepeatEvent( event, table, scrollBar );
			break;
		case tblEnterEvent:
			handled = HandleTableEnterEvent( event, table, scrollBar );
			break;
		case keyDownEvent:
			if (!gNavSupport || (gNavSupport && ObjectHasFocus(pForm, GameListFormTable)))
		  	{
		  		//in Object focus mode, only handle key down envent when table gets focus
				handled = HandleKeyDownEvent( event, table, scrollBar );
		  	}
			break;
		case frmObjectFocusTakeEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case GameListFormTable:
					FrmSetFocus(pForm,FrmGetObjectIndex(pForm,GameListFormTable)); 
					FrmGetObjectBounds(pForm,FrmGetObjectIndex(pForm,GameListFormTable),&recTable);
					FrmGlueNavDrawFocusRing(pForm,GameListFormTable,frmNavFocusRingNoExtraInfo,&recTable,frmNavFocusRingStyleObjectTypeDefault,true);
					handled = true;
					break;
				default:
					break;
			}
			break;
		case frmObjectFocusLostEvent:
			switch(event->data.frmObjectFocusTake.objectID) {
				case GameListFormTable:
					highLighted = false;
					highLightedRow = 0;
					UpdateTable(table, scrollBar, true );
					handled = true;
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
	return handled;
}

Boolean CGameListView::HandleKeyDownEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Boolean handled = false;
	if (NavSelectPressed(event))
	{
		if (!highLighted)
		{
			highLighted = true;
			highLightedRow = 0; //top item on table
			UpdateTable( table, scrollBar, true );
			handled = true;
		}
		else	//highlighted, same as tap on the highlighted item
		{
			CGameItem *item = (CGameItem*)TblGetItemPtr( table, highLightedRow, 0 );
			handled = HandleSelectItem(item);
		}
		//return handled;
	}
	if (NavDirectionPressed(event, Up))
	{
		Scroll( table, scrollBar, -1, highLighted?false:true );
		handled = true;
	}
	if (NavDirectionPressed(event, Down))
	{
		Scroll( table, scrollBar, 1, highLighted?false:true );
		handled = true;
	}
/*	
	switch( event->data.keyDown.chr )
    {
    	case vchrRockerCenter:
    		if (!highLighted)
    		{
    			highLighted = true;
    			highLightedRow = 0; //top item on table
    			UpdateTable( table, scrollBar, true );
    			handled = true;
    		}
    		else	//highlighted, same as tap on the highlighted item
    		{
				CGameItem *item = (CGameItem*)TblGetItemPtr( table, highLightedRow, 0 );
				handled = HandleSelectItem(item);
			}
			break;
    	case vchrRockerUp:
		case vchrPageUp:
			Scroll( table, scrollBar, -1, highLighted?false:true );
			handled = true;
         	break;
        case vchrRockerDown: 
		case vchrPageDown:
			Scroll( table, scrollBar, 1, highLighted?false:true );
			handled = true;
			break;
		default:	
			break;
	}
*/
    return handled;
}


/**
 * Handles repeat events (i.e. the scrollers).
 */
Boolean CGameListView::HandleRepeatEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	Scroll( table, scrollBar, event->data.sclRepeat.newValue - event->data.sclRepeat.value, false );
	return false;
}

/**
 * Handles what happens when the user taps on an item in the tree/table.
 */
Boolean CGameListView::HandleTableEnterEvent( EventPtr event, TablePtr table, ScrollBarPtr scrollBar )
{
	UInt16   row = event->data.tblSelect.row;
	//UInt16   column = event->data.tblSelect.column;
	CGameItem *item = (CGameItem*)TblGetItemPtr( table, row, 0 );
	return HandleSelectItem(item);
}
CGameItem::CGameItem()
{
	whitePlayer = NULL;
	blackPlayer = NULL;
	whiteRank = NULL;
	blackRank = NULL;
	komi = 65;
	flag = NULL;
	gameID = 0;
	moveCount = 0;
	size = 19;
	handicap = 0;
	byoyomi = 0;
	observer = 0;
}
CGameItem::~CGameItem()
{
	Release();
}
Boolean CGameItem::operator==(const CGameItem& gi)
{	
	return (gameID == gi.gameID);
}
void CGameItem::Release()
{
	if (whitePlayer)
	{
		MemPtrFree(whitePlayer);
		whitePlayer = NULL;
	}
	if (blackPlayer)
	{
		MemPtrFree(blackPlayer);
		blackPlayer = NULL;
	}
	if (whiteRank)
	{
		MemPtrFree(whiteRank);
		whiteRank = NULL;
	}
	if (blackRank)
	{
		MemPtrFree(blackRank);
		blackRank = NULL;
	}
	if (flag)
	{
		MemPtrFree(flag);
		flag = NULL;
	}
	return;
}
Boolean CGameListView::HandleSelectItem( CGameItem* item )
{
   	Char	cmdString[25],gameidStr[4],boardStr[7],boardIndexStr[4];
	TGameInfo	info;
	CBoard	*workingBoard;
	if( !item )
	{
		return false;
	}
	else 
	{
  		//Check if the selected game is already in our observation list	
  		workingBoard = gBoardPool.GetBoardByID(item->gameID);
  		if (workingBoard)
  		{
  			workingBoard->GetGameInfo(info);
  			if (info.action == OBSERVE && info.status == INPROGRESS)	//I'm observing this game
  			{
	  			gBoardPool.SetCurrentBoard(workingBoard);
				if (!FrmGetFormPtr(BoardForm))	//if we were not in borad window previously
					FrmGotoForm(BoardForm);
				else
					FrmUpdateForm(BoardForm,frmReloadUpdateCode);
	  			return true;
  			}
  		}
  		//The selected game is not currently in our observation list.
  		//We have to get a board and observe the selected game there.
  		workingBoard = gBoardPool.GetNewBoard();
  		if (!workingBoard) 
  		{
  			//No new board available, then we have to find a inactive board (interrupted, gameover or loaded).
  			workingBoard = gBoardPool.GetInactiveBoard(); 
  			if (!workingBoard)
  			{
  				//No inactive board available. All boards are in process, 
  				//then we have to use current board. 
  				workingBoard = gBoardPool.GetCurrentBoard();
  			}
  			//Base on the board status to ask for user's decision. 
			workingBoard->GetGameInfo(info);
			StrIToA(gameidStr,info.id);
			#ifdef _PE_
			StrCopy(boardStr," #");
			StrCat(boardStr,StrIToA(boardIndexStr,gBoardPool.GetBoardIndex(workingBoard)));
			StrCat(boardStr,".");
			#else
			StrCopy(boardStr,".");
			#endif
			if (info.action == OBSERVE && info.status == INPROGRESS)	//I'm observing another game
			{
				if (FrmCustomAlert(GameUnobserveAlert,gameidStr,boardStr," ") == 0)	//Yes
				{	//unobserve previouse game first
					workingBoard->Unobserve();
					/*
					StrCopy(cmdString,"unobserve ");
					StrCat(cmdString,gameidStr);
					WriteServer(cmdString);
					*/
					//Close (delete) current board
					gBoardPool.CloseBoard(workingBoard);
				}
				else
					return true; 
			}
			else if (info.status != PREPARING)	//The board is opened (reviewing) 
			{
				if (FrmCustomAlert(GameCloseAlert,gameidStr,boardStr," ") == 0)	//Yes
				{	//Close (delete) current board
					gBoardPool.CloseBoard(workingBoard);
				}
				else
					return true; 
			}
			if (info.action == PLAY && (info.status == INPROGRESS || info.status == SCORING)) //I'm playing a game
			{
				FrmAlert(GamePlayingAlert);
				return true;
			}
  		}
		//Observe the selected game
		//gameid = item->gameID;
		StrCopy(cmdString,"observe ");
		StrCat(cmdString,StrIToA(gameidStr,item->gameID));
		WriteServer(cmdString);
		return true;
	}
}
