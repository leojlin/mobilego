#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include	<PalmOS.h>
#include "sections.h"
void InitVFS(void) SEC_STRG;
Err SgfReadFile(char *name, char **sgf) SEC_STRG;
#endif // FUNCTIONS_H