#ifndef CSTRINGARRAY_H
#define CSTRINGARRAY_H
#include <PalmOS.h>
#include "sections.h"

class CStringArray{
public:

	CStringArray() SEC_ARR;
	virtual ~CStringArray() SEC_ARR;
	Boolean Add(Char*) SEC_ARR;
	void Clear() SEC_ARR;
	Boolean Insert(Char*,Int32) SEC_ARR;
	Int32 FindByName(Char*) SEC_ARR;
	Boolean Remove(Int32) SEC_ARR;
	Int32 Size() SEC_ARR;
	Char* operator[](Int32) SEC_ARR;

protected:
	Char*	*pArray;
	Int32	size;	
};

#endif // CSTRINGARRAY_H
